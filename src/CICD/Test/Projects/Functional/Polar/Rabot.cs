﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using TSU.Common.Guided.Group;
using TSU.Views;
using UnitTestGenerator;

namespace Functional_Tests.Polar
{
    [TestClass]
    public class RabotCSV
    {

        public static void CreateRadialFile()
        {
            string content = @"Radial,,,,,,,,,,,,
Source = Database Accl(s): L4LT Category : S Cumul : -.085492 - 11.43788,PyRabot Version 1.4.4_beta2,20-Sep-2024~09:21:49
Name, Rabot DCum,DCum,Smooth,Rough,Rough - Smooth,Displacement,Smooth - tol,Smooth + tol,Element,Point to move,Offset date, Offset comment
,,,,,,,,,,,,
TT41.QTGD.411500.E,0,25739.393,-2.467,-2.000,0.467,0,-2.647,-2.287,LHC.MQ.20L1,,03.05.2021,LS2 Depla_1 Radial Arc 81 - CUM = 25739.393
,1.422273,25740.815,-2.46,-2.47,-0.009,0,-2.64,-2.28,,,,
TT41.QTGD.411500.S,0,25739.393,-2.467,-2.000,0.467,0,-2.647,-2.287,LHC.MQ.20L1,,03.05.2021,LS2 Depla_1 Radial Arc 81 - CUM = 25739.393
,1.422273,25740.815,-2.46,-2.47,-0.009,0,-2.64,-2.28,,,,
TT41.MDGV.411506.E,0,25739.393,-2.467,-2.000,0.467,0,-2.647,-2.287,LHC.MQ.20L1,,03.05.2021,LS2 Depla_1 Radial Arc 81 - CUM = 25739.393
,1.422273,25740.815,-2.46,-2.47,-0.009,0,-2.64,-2.28,,,,
TT41.MDGV.411506.S,0,25739.393,-2.467,-2.000,0.467,0,-2.647,-2.287,LHC.MQ.20L1,,03.05.2021,LS2 Depla_1 Radial Arc 81 - CUM = 25739.393
,1.422273,25740.815,-2.46,-2.47,-0.009,0,-2.64,-2.28,,,,
LHC.MQ.20L1.S,2.844547,25742.237,-2.453,-2.472,-0.019,0,-2.633,-2.273,,,03.05.2021,LS2 Depla_1 Radial Arc 81 - CUM = 25742.237
";
            File.WriteAllText($@"c:\Data\tsunami\theoretical files\rad.csv", content);
        }

        public static void CreateVerticalFile()
        {
            string content = @"Vertical,,,,,,,,,,,,
Source = Database Accl(s): L4LT Category : S Cumul : -.085492 - 11.43788,PyRabot Version 1.4.4_beta2,20-Sep-2024~09:21:49
Name,Rabot DCum,DCum,Smooth,Rough,Rough - Smooth,Displacement,Smooth-tol,Smooth+tol,Element,Point to move,Offset date,Offset comment
,,,,,,,,,,,,
LHC.MQ.20L1.E,0,25739.393,-2.98,-2.98,0,0,-3.16,-2.8,LHC.MQ.20L1,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25739.393
,1.422273,25740.815,-2.924,-2.885,0.039,0,-3.104,-2.744,,,,
LHC.MQ.20L1.S,2.844547,25742.237,-2.869,-2.79,0.079,0,-3.049,-2.689,,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25742.237
,4.96106,25744.354,-2.862,,,,-3.042,-2.682,,,,
TT41.QTGD.411500.E,7.077573,25746.471,-2.855,-2.94,-0.085,0,-3.035,-2.675,LHC.MB.C20L1,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25746.471
LHC.MB.C20L1.M,12.476603,25751.87,-2.845,-3.02,-0.175,0,-3.025,-2.665,,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25751.870
";
            File.WriteAllText($@"c:\Data\tsunami\theoretical files\vertical.csv", content);
        }

        [TestMethod()]
        public void L2_R_GEM_Step1_open_theo_and_csv_files_and_setup()
        {
            // theo fiel already createdin L1_P_FS_AWAKE_Step01_Load_theofile
            CreateRadialFile();
            CreateVerticalFile();

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""375"" trace=""Guided modules+"" Button=""Left"" Direction=""Down"" Xpos=""219"" Ypos=""277"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""17"" trace=""Guided modules+"" Button=""Left"" Direction=""Up"" Xpos=""219"" Ypos=""277"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Guided modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Guided modules end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""364"" trace=""Element alignment+"" Button=""Left"" Direction=""Down"" Xpos=""422"" Ypos=""394"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""59"" trace=""Element alignment+"" Button=""Left"" Direction=""Up"" Xpos=""422"" Ypos=""394"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Element alignment (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Element alignment end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""457"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1321"" Ypos=""940"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""35"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1321"" Ypos=""940"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""329"" trace=""Select Element Theoretical File(s)+"" Button=""Left"" Direction=""Down"" Xpos=""658"" Ypos=""276"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""39"" trace=""Select Element Theoretical File(s)+"" Button=""Left"" Direction=""Up"" Xpos=""658"" Ypos=""276"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Select Element Theoretical File(s) (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: Select Element Theoretical File(s) end"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{W}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{K}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{E}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{D}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""275"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""337"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""602"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""64"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""602"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{r}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{a}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{d}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{c}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{s}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{v}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""275"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button"" />

<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""2748"" trace=""button2+Relative Displacement"" Button=""Left"" Direction=""Down"" Xpos=""1053"" Ypos=""979"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""70"" trace=""button2+Relative Displacement"" Button=""Left"" Direction=""Up"" Xpos=""1053"" Ypos=""979"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Please choose type of Rabot offset displacement: Relative Displacement"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""337"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""602"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""64"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""602"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{v}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{e}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{r}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{t}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{i}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{c}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{a}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{l}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{c}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{s}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{v}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""75"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button"" />

    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Import offsets/displacements end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""356"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{M}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""31"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{O}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />



    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""307"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1314"" Ypos=""952"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""8"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1314"" Ypos=""952"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""369"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""489"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""54"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""489"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""367"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1318"" Ypos=""924"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""55"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1318"" Ypos=""924"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""315"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Down"" Xpos=""1126"" Ypos=""850"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""69"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Up"" Xpos=""1126"" Ypos=""850"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Polar Alignment (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Polar Alignment end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""353"" trace=""orientationStation+"" Button=""Left"" Direction=""Down"" Xpos=""1608"" Ypos=""291"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""34"" trace=""orientationStation+"" Button=""Left"" Direction=""Up"" Xpos=""1608"" Ypos=""291"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Station Orientation only (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""369"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""220"" Ypos=""387"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""34"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""220"" Ypos=""387"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""383"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""817"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""67"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""817"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Point stationned: Select"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""333"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""887"" Ypos=""583"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""33"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""887"" Ypos=""583"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Instrument height: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Station Orientation only end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""338"" trace=""Select Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""1563"" Ypos=""447"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""33"" trace=""Select Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""1563"" Ypos=""447"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Select Instrument (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""335"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1508"" Ypos=""213"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""40"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1508"" Ypos=""213"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""BB: Options end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""365"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1633"" Ypos=""325"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""27"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1633"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""49"" Delay=""0"" Action=""BB: List View end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""941"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""308"" Ypos=""274"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""38"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""308"" Ypos=""274"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""52"" Delay=""619"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""53"" Delay=""265"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""1640"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""165"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""34"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""165"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""751"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""916"" Ypos=""959"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""29"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""916"" Ypos=""959"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""58"" Delay=""0"" Action=""MB: Select: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""59"" Delay=""0"" Action=""BB: Select Instrument end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""1164"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""1531"" Ypos=""547"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""22"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""1531"" Ypos=""547"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""62"" Delay=""0"" Action=""BB: Change Reflector (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""1375"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""189"" Ypos=""307"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""21"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""189"" Ypos=""307"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""882"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""851"" Ypos=""934"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""41"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""851"" Ypos=""934"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""67"" Delay=""0"" Action=""MB: Choose a Reflector: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""68"" Delay=""0"" Action=""BB: Change Reflector end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""1013"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Down"" Xpos=""1510"" Ypos=""636"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""30"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Up"" Xpos=""1510"" Ypos=""636"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""71"" Delay=""0"" Action=""BB: Switch between 1 face and double face (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""72"" Delay=""0"" Action=""BB: Switch between 1 face and double face end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""570"" trace=""Number of measurement+"" Button=""Left"" Direction=""Down"" Xpos=""1492"" Ypos=""747"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""25"" trace=""Number of measurement+"" Button=""Left"" Direction=""Up"" Xpos=""1492"" Ypos=""747"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""75"" Delay=""0"" Action=""BB: Number of measurement (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""649"" trace=""button4+1"" Button=""Left"" Direction=""Down"" Xpos=""1041"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""77"" Delay=""50"" trace=""button4+1"" Button=""Left"" Direction=""Up"" Xpos=""1041"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""78"" Delay=""0"" Action=""MB: Number of measurement: 1"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""79"" Delay=""0"" Action=""BB: Number of measurement end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""676"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1347"" Ypos=""955"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""12"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1347"" Ypos=""955"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""82"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""83"" Delay=""0"" Action=""BB: Next step end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""1903"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""591"" Ypos=""505"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""591"" Ypos=""505"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""857"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1286"" Ypos=""938"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""87"" Delay=""11"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1286"" Ypos=""938"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""88"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""89"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""90"" Delay=""1579"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1340"" Ypos=""930"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""91"" Delay=""14"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1340"" Ypos=""930"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""92"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""93"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""94"" Delay=""1021"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""544"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""29"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""544"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""96"" Delay=""1390"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""753"" Ypos=""726"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""97"" Delay=""28"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""756"" Ypos=""726"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""98"" Delay=""422"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""991"" Ypos=""730"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""45"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""991"" Ypos=""730"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""100"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""101"" Delay=""1342"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""312"" Ypos=""196"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""102"" Delay=""28"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""312"" Ypos=""196"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""643"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""875"" Ypos=""930"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""32"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""875"" Ypos=""930"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""105"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""1054"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""764"" Ypos=""593"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""107"" Delay=""30"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""764"" Ypos=""593"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""108"" Delay=""538"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""986"" Ypos=""599"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""33"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""986"" Ypos=""599"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""110"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""111"" Delay=""608"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""781"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""112"" Delay=""51"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""781"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""113"" Delay=""479"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1086"" Ypos=""650"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""114"" Delay=""36"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1087"" Ypos=""650"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""115"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""116"" Delay=""1055"" trace=""Next step blocked+"" Button=""Left"" Direction=""Down"" Xpos=""1333"" Ypos=""949"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""117"" Delay=""28"" trace=""Next step blocked+"" Button=""Left"" Direction=""Up"" Xpos=""1333"" Ypos=""949"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""118"" Delay=""0"" Action=""BB: Next step blocked (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""119"" Delay=""0"" Action=""BB: Next step blocked end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""120"" Delay=""1007"" trace=""Ctrl+"" Button=""Left"" Direction=""Down"" Xpos=""719"" Ypos=""295"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""29"" trace=""Ctrl+"" Button=""Left"" Direction=""Up"" Xpos=""719"" Ypos=""295"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""122"" Delay=""0"" Action=""BB: Control 'opening' (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""123"" Delay=""0"" Action=""BB: Control 'opening' end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""1079"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""525"" Ypos=""194"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""125"" Delay=""27"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""525"" Ypos=""194"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""126"" Delay=""638"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""878"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""127"" Delay=""38"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""878"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""128"" Delay=""0"" Action=""MB: Enter values: Valid"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""129"" Delay=""817"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""987"" Ypos=""580"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""130"" Delay=""57"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""987"" Ypos=""580"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""131"" Delay=""0"" Action=""MB: Closure OK: OK!"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""132"" Delay=""1011"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1366"" Ypos=""941"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""133"" Delay=""56"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1366"" Ypos=""941"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""134"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""135"" Delay=""0"" Action=""BB: Next step end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""136"" Delay=""996"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""673"" Ypos=""307"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""137"" Delay=""39"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""673"" Ypos=""307"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""138"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""139"" Delay=""1584"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""939"" Ypos=""696"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""140"" Delay=""34"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""939"" Ypos=""696"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""141"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""142"" Delay=""1736"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""878"" Ypos=""937"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""143"" Delay=""43"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""878"" Ypos=""937"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""144"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""145"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""146"" Delay=""1010"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1329"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""147"" Delay=""25"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1329"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""148"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""149"" Delay=""0"" Action=""BB: Next step end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""150"" Delay=""966"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1329"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""151"" Delay=""37"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1329"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""152"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""153"" Delay=""0"" Action=""BB: Next step end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""154"" Delay=""1006"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1326"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""155"" Delay=""70"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1326"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""156"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""157"" Delay=""0"" Action=""BB: Next step end"" />
  </KeyStrokesAndMouseClicks>
</Macro>",

                //JustRunAssertOnExistingTsuT = true,
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var gem = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;
                Assert.IsNotNull(gem, "no gem module");

                var pm = gem.SubGuidedModules.FirstOrDefault(x => x.guideModuleType == TSU.ENUM.GuidedModuleType.AlignmentImplantation);
                Assert.IsNotNull(pm, "no gem module");
                var stm = pm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module));
                var station = pm.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "no station ref 1");
                Assert.IsTrue(station.Parameters2._StationPoint._Name == "TT41.MDGV.411506.E", "station supposed to be on TT41.MDGV.411506.E");
                Assert.IsTrue(station.GoodMeasuresTaken[0]._Point._Name == "TT41.MDGV.411506.S", "first measure supposed to be on TT41.MDGV.411506.S");
                Assert.IsTrue(station.Parameters2.Setups.InitialValues.IsPositionKnown = true, "initial value steup is not set as position known");
                var am = gem.SubGuidedModules.FirstOrDefault(x => x.guideModuleType == TSU.ENUM.GuidedModuleType.AlignmentManager);
                Assert.IsNotNull(am, "no admin module");
                Assert.IsTrue(am.MagnetsToBeAligned[0]._Name == "TT41.QTGD.411500", "should align TT41.QTGD.411500");
                Assert.IsTrue(am.RabotStrategy == TSU.Common.Compute.Rabot.Strategy.Relative_Displacement, "RabotCSV method must Relative Displacement");
                Assert.IsTrue(am.RadialRabot.Count != 0, "no radial rabot found");
                Assert.IsTrue(am.VerticalRabot.Count != 0, "no vertical rabot found");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L2_R_GEM_Step2_Acquire()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_R_GEM_Step1_open_theo_and_csv_files_and_setup) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
     <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""318"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1385"" Ypos=""940"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""36"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1385"" Ypos=""940"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""357"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""690"" Ypos=""595"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""27"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""690"" Ypos=""595"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""371"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""609"" Ypos=""340"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""67"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""609"" Ypos=""340"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Goto and measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Goto and measure end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""318"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""510"" Ypos=""187"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""29"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""510"" Ypos=""187"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""340"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""135"" Ypos=""300"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""123"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""135"" Ypos=""300"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""66"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""05"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""30"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""386"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""128"" Ypos=""337"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""64"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""128"" Ypos=""337"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""334"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""850"" Ypos=""942"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""68"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""850"" Ypos=""942"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Enter values: Valid"" />
  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
                //JustRunAssertOnExistingTsuT = true,
            };


            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                // previous
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var gem = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;
                Assert.IsNotNull(gem, "no gem module");
                var pm = gem.SubGuidedModules.FirstOrDefault(x => x.guideModuleType == TSU.ENUM.GuidedModuleType.AlignmentImplantation);
                Assert.IsNotNull(pm, "no gem module");
                var stm = pm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module));
                var station = pm.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "no station ref 1");
                Assert.IsTrue(station.Parameters2._StationPoint._Name == "TT41.MDGV.411506.E", "station supposed to be on TT41.MDGV.411506.E");
                Assert.IsTrue(station.GoodMeasuresTaken[0]._Point._Name == "TT41.MDGV.411506.S", "first measure supposed to be on TT41.MDGV.411506.S");
                Assert.IsTrue(station.Parameters2.Setups.InitialValues.IsPositionKnown = true, "initial value steup is not set as position known");
                var am = gem.SubGuidedModules.FirstOrDefault(x => x.guideModuleType == TSU.ENUM.GuidedModuleType.AlignmentManager);
                Assert.IsNotNull(am, "no admin module");
                Assert.IsTrue(am.MagnetsToBeAligned[0]._Name == "TT41.QTGD.411500", "should align TT41.QTGD.411500");
                Assert.IsTrue(am.RabotStrategy == TSU.Common.Compute.Rabot.Strategy.Relative_Displacement, "RabotCSV method must Relative Displacement");
                Assert.IsTrue(am.RadialRabot.Count != 0, "no radial rabot found");
                Assert.IsTrue(am.VerticalRabot.Count != 0, "no vertical rabot found");

                // new

                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");


                Assert.IsTrue(dgv.Columns.Count == 15, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 5, "wrong rows count in dgv");

                int column = 2;
                Assert.AreEqual("0.00", dgv[column, 2].Value.ToString(), "wrong value in dgv for o(Rbv)"); column++;
                Assert.AreEqual("-2.00", dgv[column, 2].Value.ToString(), "wrong value in dgv for o(Lbv)"); column++;
                Assert.AreEqual("0.05", dgv[column, 2].Value.ToString() ,  "wrong value in dgv for o(Hbv)"); column++;

                Assert.AreEqual("-0.47", dgv[column, 2].Value.ToString(), "wrong value in dgv for expected"); column++;
                //Assert.IsTrue(dgv[column, 2].Value.ToString() == "-2.00", "wrong value in dgv for expected"); column++;
                Assert.AreEqual("0.14", dgv[column, 2].Value.ToString() , "wrong value in dgv for expected"); column++;

                Assert.AreEqual("-0.47", dgv[column, 2].Value.ToString() , "wrong value in dgv for move"); column++;
                //Assert.IsTrue(dgv[column, 2].Value.ToString() == "0.00", "wrong value in dgv for move"); column++;
                Assert.AreEqual("0.09", dgv[column, 2].Value.ToString(),  "wrong value in dgv for move"); column++;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

      

        public static void L2_R_APM_Step0_CreateRadialFile()
        {
            //rad
            // -0.15; 0.15; 0.3
            //  0.70; 0.60;-0.10
            //vert
            // -0.16; -0.20; -0.04
            //  0.26;  0.46;  0.20
            string content = @"radial,,,,,,,,,,,,
Source = Database Accl(s): PR Category : S Cumul : 3.24725 - 628.070515,PyRabot Version 1.4.4_beta2,07-Nov-2024~15:03:24,,,,,,,,,,
Name,Rabot DCum,Rel-DCum,Smooth,Rough,Rough - Smooth,Displacement,Smooth-tolerance,Smooth+tolerance,Element,Point to move,Offset date,Offset comment
,,,,,,,,,,,,
TT41.MDGV.411506.E,0,0,-0.15,0.15,0.3,0,-0.368,0.032,PR.BHT.01,,,
,1.95,1.95,0.019,0.019,0,0,-0.181,0.219,,,,
TT41.MDGV.411506.S,3.9,3.9,0.70,0.60,-.10,0,0.005,0.405,,,,";
            File.WriteAllText($@"c:\Data\tsunami\theoretical files\L2_R_APM_rad.csv", content);
        }
        public static void L2_R_APM_Step0_CreateVerticalFile()
        {
            // -0.16; -0.20; -0.04
            //  0.26;  0.46;  0.20
            string content = @"vertical,,,,,,,,,,,,
Source = Database Accl(s): PR Category : S Cumul : 3.24725 - 628.070515,PyRabot Version 1.4.4_beta2,07-Nov-2024~15:03:24,,,,,,,,,,
Name,Rabot DCum,Rel-DCum,Smooth,Rough,Rough - Smooth,Displacement,Smooth-tolerance,Smooth+tolerance,Element,Point to move,Offset date,Offset comment
,,,,,,,,,,,,
TT41.MDGV.411506.E,0,0,-0.16,-0.20,-0.04,0,-0.368,0.032,TT41.MDGV.411506.E,,,
,1.95,1.95,0.019,0.019,0,0,-0.181,0.219,,,,
TT41.MDGV.411506.S,3.9,3.9,0.26,0.46,0.20,0,0.005,0.405,,,,";
            File.WriteAllText($@"c:\Data\tsunami\theoretical files\L2_R_APM_vert.csv", content);
        }

        [TestMethod()]
        public void L2_R_APM_Step1_CreateFiles_and_select_points_and_first_measure()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            L2_R_APM_Step0_CreateVerticalFile();
            L2_R_APM_Step0_CreateRadialFile();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(RS2K.FreeStationInAwake.L1_P_FS_AWAKE_Step07_compute_as_free_station) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""374"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""424"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""424"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""321"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""431"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""431"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""332"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""523"" Ypos=""324"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""523"" Ypos=""324"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""322"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""668"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""97"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""668"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""390"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""246"" Ypos=""391"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""246"" Ypos=""391"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""318"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""248"" Ypos=""419"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""66"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""248"" Ypos=""419"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""341"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""857"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""89"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""857"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""BB: Add known Point(s) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""301"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""655"" Ypos=""333"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""83"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""655"" Ypos=""333"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""372"" trace=""Pin all 'Next Points'+"" Button=""Left"" Direction=""Down"" Xpos=""814"" Ypos=""905"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""74"" trace=""Pin all 'Next Points'+"" Button=""Left"" Direction=""Up"" Xpos=""814"" Ypos=""905"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""BB: Pin all 'Next Points' (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""46"" Delay=""0"" Action=""BB: Pin all 'Next Points' end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""397"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""640"" Ypos=""674"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""640"" Ypos=""674"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""377"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""512"" Ypos=""747"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""512"" Ypos=""747"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""362"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""473"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""342"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""486"" Ypos=""819"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""486"" Ypos=""819"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""370"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""385"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""378"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1289"" Ypos=""887"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""90"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1289"" Ypos=""887"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB:  (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB:  end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""397"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""640"" Ypos=""674"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""640"" Ypos=""674"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""370"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1283"" Ypos=""890"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""89"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1283"" Ypos=""890"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB:  (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""BB:  end"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""333"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""^"" Target=""SplitContainer"" />
  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
            };


            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var apm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(apm, "no gem module");
                var stm = apm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module)) as TSU.Polar.Station.Module;
                var station = stm.StationTheodolite;
                Assert.IsNotNull(station, "no station ref 1");

                Assert.IsNotNull(station, "no station ref 1");
                var firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.E");


                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.E");
                var firstAsPolar = firstMeas as TSU.Polar.Measure;

                Assert.AreEqual(299.9003163, firstAsPolar.Angles.Corrected.Horizontal.Value, 0.001, "wrong horiaontal angle for .E");
                Assert.AreEqual(1.0944291, firstAsPolar.Distance.Corrected.Value, 0.0001, "wrong horiaontal angle for .E");

                firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.S");

                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.S");
                firstAsPolar = firstMeas as TSU.Polar.Measure;

                Assert.AreEqual(299.9236393, firstAsPolar.Angles.Corrected.Horizontal.Value, 0.001, "wrong horiaontal angle for .E");
                Assert.AreEqual(1.3325901, firstAsPolar.Distance.Corrected.Value, 0.0001, "wrong horiaontal angle for .E");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 19, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");

                int column = 2;
                Assert.AreEqual("-0.11", dgv[column, 0].Value, "wrong value in dgv for D(Rbv) for .E"); column++;
                Assert.AreEqual("-0.93", dgv[column, 0].Value, "wrong value in dgv for D(Lbv) for .E"); column++;
                Assert.AreEqual("0.37", dgv[column, 0].Value, "wrong value in dgv for D(Hbv) for .E"); column++;
                Assert.IsTrue(dgv.Columns[column].Visible == false, $"Column {column} should not be visible");
                column++;
                Assert.IsTrue(dgv.Columns[column].Visible == false, $"Column {column} should not be visible");
                column++;
                Assert.IsTrue(dgv.Columns[column].Visible == false, $"Column {column} should not be visible");
                column++;
                Assert.IsTrue(dgv.Columns[column].Visible == false, $"Column {column} should not be visible");
                column = 2;
                Assert.AreEqual("0.00", dgv[column, 1].Value, "wrong value in dgv for D(Rbv) for .S"); column++;
                Assert.AreEqual("0.00", dgv[column, 1].Value, "wrong value in dgv for D(Lbv) for .S"); column++;
                Assert.AreEqual("0.00", dgv[column, 1].Value, "wrong value in dgv for D(Hbv) for .S"); column++;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }


        [TestMethod()]
        public void L2_R_APM_Step2_Second_Measure()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_R_APM_Step1_CreateFiles_and_select_points_and_first_measure) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
   <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""397"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""377"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""613"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""613"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""354"" trace=""Re-measure+"" Button=""Left"" Direction=""Down"" Xpos=""730"" Ypos=""471"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""106"" trace=""Re-measure+"" Button=""Left"" Direction=""Up"" Xpos=""730"" Ypos=""471"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Re-measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Re-measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""336"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""705"" Ypos=""678"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""705"" Ypos=""678"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""390"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""505"" Ypos=""728"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""505"" Ypos=""728"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""203"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""285"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""385"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1386"" Ypos=""859"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""75"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1386"" Ypos=""859"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""320"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""802"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""802"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""289"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1017"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""90"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1017"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""MB: Measure comparaison: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""813"" Ypos=""545"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""813"" Ypos=""545"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""361"" trace=""button2+Never"" Button=""Left"" Direction=""Down"" Xpos=""1119"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""74"" trace=""button2+Never"" Button=""Left"" Direction=""Up"" Xpos=""1119"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Already Measured: Never"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""322"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""710"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""710"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""305"" trace=""Re-measure+"" Button=""Left"" Direction=""Down"" Xpos=""832"" Ypos=""465"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""114"" trace=""Re-measure+"" Button=""Left"" Direction=""Up"" Xpos=""832"" Ypos=""465"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Re-measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Re-measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""358"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""626"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""626"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""338"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1312"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1312"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""BB: Valid end"" />
  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var apm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(apm, "no gem module");
                var stm = apm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module)) as TSU.Polar.Station.Module;
                var station = stm.StationTheodolite;
                Assert.IsNotNull(station, "no station ref 1");
                var firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.E");

                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.E");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 19, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");


                // header
                int column = 2;
                Assert.AreEqual("DRbv [mm]", dgv.Columns[column].HeaderText, "wrong header ORbv [mm]"); column++;
                Assert.AreEqual("DLbv [mm]", dgv.Columns[column].HeaderText, "wrong header OLbv [mm]"); column++;
                Assert.AreEqual("DHbv [mm]", dgv.Columns[column].HeaderText, "wrong header OHbv [mm]"); column++;

                Assert.IsTrue(dgv.Columns[column].Visible == false, $"Column {column} should not be visible"); column++;

                // .E
                column = 2;
                var row = dgv.Rows[0];
                Assert.AreEqual("-0.08", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .E"); column++;
                Assert.AreEqual("-2.79", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .E"); column++;
                Assert.AreEqual("1.09", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .E"); column++;

                
                // .S
                column = 2;
                row = dgv.Rows[1];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Rbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Lbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Hbv) for .S"); column++;


                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L2_R_APM_Step3_Displ_to_offset()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_R_APM_Step2_Second_Measure) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
   <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""230"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""426"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""426"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""213"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""626"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""626"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""289"" trace=""Change visual details+"" Button=""Left"" Direction=""Down"" Xpos=""824"" Ypos=""336"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""12"" trace=""Change visual details+"" Button=""Left"" Direction=""Up"" Xpos=""824"" Ypos=""336"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Change visual details (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Change visual details end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""254"" trace=""Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1028"" Ypos=""942"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""12"" trace=""Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1028"" Ypos=""942"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Offsets end"" />
  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var apm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(apm, "no gem module");
                var stm = apm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module)) as TSU.Polar.Station.Module;
                var station = stm.StationTheodolite;
                Assert.IsNotNull(station, "no station ref 1");
                var firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.E");

                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.E");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 19, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");



                // header
                int column = 2;
                Assert.AreEqual("ORbv [mm]", dgv.Columns[column].HeaderText, "wrong header ORbv [mm]"); column++;
                Assert.AreEqual("OLbv [mm]", dgv.Columns[column].HeaderText, "wrong header OLbv [mm]"); column++;
                Assert.AreEqual("OHbv [mm]", dgv.Columns[column].HeaderText, "wrong header OHbv [mm]"); column++;

                Assert.IsTrue(dgv.Columns[column].Visible == false, $"Column {column} should not be visible"); column++;

                // .E
                column = 2;
                var row = dgv.Rows[0];
                Assert.AreEqual("0.08", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .E"); column++;
                Assert.AreEqual("2.79", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .E"); column++;
                Assert.AreEqual("-1.09", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .E"); column++;

                // .S
                column = 2;
                row = dgv.Rows[1];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Rbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Lbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Hbv) for .S"); column++;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }


        [TestMethod()]
        public void L2_R_EO_APM_Step1_Choose_ONE_File_and_Expected_Offsets()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_R_APM_Step1_CreateFiles_and_select_points_and_first_measure) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""286"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""595"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""595"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""254"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""729"" Ypos=""447"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""121"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""729"" Ypos=""447"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />
    {Tools.GetKeySequence("L2_R_APM_rad.csv", "open")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""1335"" trace=""button1+Expected Offset"" Button=""Left"" Direction=""Down"" Xpos=""866"" Ypos=""567"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""98"" trace=""button1+Expected Offset"" Button=""Left"" Direction=""Up"" Xpos=""866"" Ypos=""567"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""MB: Please choose type of Rabot offset displacement: Expected Offset"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Import offsets/displacements end"" />

  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
            };


            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var apm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(apm, "no gem module");
                var stm = apm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module)) as TSU.Polar.Station.Module;
                var station = stm.StationTheodolite;
                Assert.IsNotNull(station, "no station ref 1");
                var firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.E");

                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.E");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                // datagridView

                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 19, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");


                // header
                int column = 2;
                Assert.AreEqual("ORbv [mm]", dgv.Columns[column].HeaderText, "wrong header ORbv [mm]"); column++;
                Assert.AreEqual("OLbv [mm]", dgv.Columns[column].HeaderText, "wrong header OLbv [mm]"); column++;
                Assert.AreEqual("OHbv [mm]", dgv.Columns[column].HeaderText, "wrong header OHbv [mm]"); column++;
                Assert.AreEqual("ExpO Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header EORbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;
                Assert.AreEqual("ExpO Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header EOHbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == false, $"Column {column} should not be visible");
                column++;
                Assert.AreEqual("Move Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Rbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;
                Assert.AreEqual("Move Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Hbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == false, $"Column {column} should not be visible");
                column++;

                // .E
                column = 2;
                var row = dgv.Rows[0];
                Assert.AreEqual("0.11", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .E"); column++;
                Assert.AreEqual("0.93", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .E"); column++;
                Assert.AreEqual("-0.37", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .E"); column++;

                // smooth, rough, rought- smooth
                //rad
                // -0.15; 0.15; 0.3
                //vert
                // -0.16; -0.20; -0.04
                Assert.AreEqual("-0.15", row.Cells[column].Value, "wrong value in dgv for O(Rbv)expected for .E"); column++; // must show:   OffsetExpected = smooth = -0.15
                column++;
                Assert.AreEqual("-0.26", row.Cells[column].Value, "wrong value in dgv for Rbv)move for .E"); column++; // must be exp - meas = - 0.26  

                // .S
                column = 2;
                row = dgv.Rows[1];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Rbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Lbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Hbv) for .S"); column++;

                // smooth, rough, rought- smooth
                //rad
                //  0.70; 0.60;-0.10
                //vert
                //  0.26;  0.46;  0.20
                Assert.AreEqual("0.70", row.Cells[column].Value, "wrong value in dgv for O(Rbv)expected for .S"); column++; // must show:   OffsetExpected = smooth 
                column++;
                Assert.AreEqual("0.70", row.Cells[column].Value, "wrong value in dgv for O(Hbv)move for .S"); column++;  // must be -(rought-smooth) = 0.1
                column++;
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L2_R_EO_APM_Step2_Choose_Second_File()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_R_EO_APM_Step1_Choose_ONE_File_and_Expected_Offsets) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""361"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""608"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""608"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""389"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""758"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""10"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""758"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />
    {Tools.GetKeySequence("L2_R_APM_vert.csv", "open")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />

  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
            };


            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var apm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(apm, "no gem module");
                var stm = apm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module)) as TSU.Polar.Station.Module;
                var station = stm.StationTheodolite;
                Assert.IsNotNull(station, "no station ref 1");
                var firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.E");

                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.E");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                // datagridView

                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 19, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");

                
               
                // header
                int column = 2;
                Assert.AreEqual("ORbv [mm]", dgv.Columns[column].HeaderText, "wrong header ORbv [mm]"); column++;
                Assert.AreEqual("OLbv [mm]", dgv.Columns[column].HeaderText, "wrong header OLbv [mm]"); column++;
                Assert.AreEqual("OHbv [mm]", dgv.Columns[column].HeaderText, "wrong header OHbv [mm]"); column++;
                Assert.AreEqual("ExpO Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header EORbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");column++; 
                Assert.AreEqual("ExpO Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header EOHbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");column++;

                Assert.AreEqual("Move Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Rbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;
                Assert.AreEqual("Move Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Hbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++; 

                // .E
                column = 2;
                var row = dgv.Rows[0];
                Assert.AreEqual("0.11", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .E"); column++;
                Assert.AreEqual("0.93", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .E"); column++;
                Assert.AreEqual("-0.37", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .E"); column++;

                // smooth, rough, rought- smooth
                //rad
                // -0.15; 0.15; 0.3
                //vert
                // -0.16; -0.20; -0.04
                Assert.AreEqual("-0.15", row.Cells[column].Value, "wrong value in dgv for O(Rbv)expected for .E"); column++; // must show:   OffsetExpected = smooth = -0.15
                Assert.AreEqual("-0.16", row.Cells[column].Value, "wrong value in dgv for O(Hbv)expected for .E"); column++;  // must show:  OffsetExpected = smooth = -0.16

                Assert.AreEqual("-0.26", row.Cells[column].Value, "wrong value in dgv for Rbv)move for .E"); column++; // must be exp - meas = - 0.26
                Assert.AreEqual("0.21", row.Cells[column].Value, "wrong value in dgv for Hbv)move for .E"); column++; // must be exp - meas =  21

                // .S
                column = 2;
                row = dgv.Rows[1];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Rbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Lbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Hbv) for .S"); column++;

                // smooth, rough, rought- smooth
                //rad
                //  0.70; 0.60;-0.10
                //vert
                //  0.26;  0.46;  0.20
                Assert.AreEqual("0.70", row.Cells[column].Value, "wrong value in dgv for O(Rbv)expected for .S"); column++; // must show:   OffsetExpected = smooth 
                Assert.AreEqual("0.26", row.Cells[column].Value, "wrong value in dgv for O(Lbv)expected for .S"); column++; // must show:   OffsetExpected = smooth 

                Assert.AreEqual("0.70", row.Cells[column].Value, "wrong value in dgv for O(Hbv)move for .S"); column++;  // must be -(rought-smooth) = 0.1
                Assert.AreEqual("0.26", row.Cells[column].Value, "wrong value in dgv for O(Rbv)move for .S"); column++; // must be -(rought-smooth) = -0.2

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L2_R_EO_APM_Step3_Second_Measure()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_R_EO_APM_Step2_Choose_Second_File) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
   <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""397"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""377"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""613"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""613"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""354"" trace=""Re-measure+"" Button=""Left"" Direction=""Down"" Xpos=""730"" Ypos=""471"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""106"" trace=""Re-measure+"" Button=""Left"" Direction=""Up"" Xpos=""730"" Ypos=""471"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Re-measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Re-measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""336"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""705"" Ypos=""678"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""705"" Ypos=""678"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""390"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""505"" Ypos=""728"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""505"" Ypos=""728"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""203"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""285"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""385"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1386"" Ypos=""859"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""75"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1386"" Ypos=""859"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""320"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""802"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""802"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""289"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1017"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""90"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1017"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""MB: Measure comparaison: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""813"" Ypos=""545"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""813"" Ypos=""545"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""361"" trace=""button2+Never"" Button=""Left"" Direction=""Down"" Xpos=""1119"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""74"" trace=""button2+Never"" Button=""Left"" Direction=""Up"" Xpos=""1119"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Already Measured: Never"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""322"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""710"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""710"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""305"" trace=""Re-measure+"" Button=""Left"" Direction=""Down"" Xpos=""832"" Ypos=""465"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""114"" trace=""Re-measure+"" Button=""Left"" Direction=""Up"" Xpos=""832"" Ypos=""465"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Re-measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Re-measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""358"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""626"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""626"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""338"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1312"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1312"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""BB: Valid end"" />
  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
            };


            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var apm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(apm, "no gem module");
                var stm = apm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module)) as TSU.Polar.Station.Module;
                var station = stm.StationTheodolite;
                Assert.IsNotNull(station, "no station ref 1");
                var firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.E");

                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.E");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 19, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                // header
                int column = 2;
                Assert.AreEqual("ORbv [mm]", dgv.Columns[column].HeaderText, "wrong header ORbv [mm]"); column++;
                Assert.AreEqual("OLbv [mm]", dgv.Columns[column].HeaderText, "wrong header OLbv [mm]"); column++;
                Assert.AreEqual("OHbv [mm]", dgv.Columns[column].HeaderText, "wrong header OHbv [mm]"); column++;
                Assert.AreEqual("ExpO Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header EORbv [mm]"); column++;
                Assert.AreEqual("ExpO Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header EOHbv [mm]"); column++;
                Assert.AreEqual("Move Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Rbv [mm]"); column++;
                Assert.AreEqual("Move Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Hbv [mm]"); column++;

                // .E
                column = 2;
                var row = dgv.Rows[0];
                Assert.AreEqual("0.08", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .E"); column++;
                Assert.AreEqual("2.79", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .E"); column++;
                Assert.AreEqual("-1.09", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .E"); column++;

                // smooth, rough, rought- smooth
                //rad
                // -0.15; 0.15; 0.3
                //vert
                // -0.16; -0.20; -0.04
                Assert.AreEqual("-0.15", row.Cells[column].Value, "wrong value in dgv for O(Rbv)expected for .E"); column++; // must show:   OffsetExpected = smooth = -0.15
                Assert.AreEqual("-0.16", row.Cells[column].Value, "wrong value in dgv for O(Hbv)expected for .E"); column++;  // must show:  OffsetExpected = smooth = -0.16

                Assert.AreEqual("-0.23", row.Cells[column].Value, "wrong value in dgv for Rbv)move for .E"); column++; // must be exp - meas = - 0.23
                Assert.AreEqual("0.93", row.Cells[column].Value, "wrong value in dgv for Hbv)move for .E"); column++; // must be exp - meas =  21

                // .S
                column = 2;
                row = dgv.Rows[1];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Rbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Lbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for O(Hbv) for .S"); column++;

                // smooth, rough, rought- smooth
                //rad
                //  0.70; 0.60;-0.10
                //vert
                //  0.26;  0.46;  0.20
                Assert.AreEqual("0.70", row.Cells[column].Value, "wrong value in dgv for O(Rbv)expected for .S"); column++; // must show:   OffsetExpected = smooth 
                Assert.AreEqual("0.26", row.Cells[column].Value, "wrong value in dgv for O(Lbv)expected for .S"); column++; // must show:   OffsetExpected = smooth 

                Assert.AreEqual("0.70", row.Cells[column].Value, "wrong value in dgv for O(Hbv)move for .S"); column++;  // must be -(rought-smooth) = 0.1
                Assert.AreEqual("0.26", row.Cells[column].Value, "wrong value in dgv for O(Rbv)move for .S"); column++; // must be -(rought-smooth) = -0.2

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L2_R_RD_APM_Step1_Choose_Files_and_relative_displacement()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_R_APM_Step1_CreateFiles_and_select_points_and_first_measure) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""286"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""595"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""595"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""254"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""729"" Ypos=""447"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""121"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""729"" Ypos=""447"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />
    {Tools.GetKeySequence("L2_R_APM_rad.csv", "open")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""358"" trace=""button2+Relative Displacement"" Button=""Left"" Direction=""Down"" Xpos=""1019"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""313"" trace=""button2+Relative Displacement"" Button=""Left"" Direction=""Up"" Xpos=""1019"" Ypos=""564"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""MB: Please choose type of Rabot offset displacement: Relative Displacement"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Import offsets/displacements end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""361"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""608"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""608"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""389"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""758"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""113"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""758"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />
    {Tools.GetKeySequence("L2_R_APM_vert.csv", "open")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />

  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var apm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(apm, "no gem module");
                var stm = apm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module)) as TSU.Polar.Station.Module;
                var station = stm.StationTheodolite;
                Assert.IsNotNull(station, "no station ref 1");
                var firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.E");

                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.E");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                // datagridView

                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 19, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");

                //test.Wait();
                // header
                int column = 2;
                Assert.AreEqual("ORbv [mm]", dgv.Columns[column].HeaderText, "wrong header ORbv [mm]"); column++;
                Assert.AreEqual("OLbv [mm]", dgv.Columns[column].HeaderText, "wrong header OLbv [mm]"); column++;
                Assert.AreEqual("OHbv [mm]", dgv.Columns[column].HeaderText, "wrong header OHbv [mm]"); column++;
                Assert.AreEqual("ExpO Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header EORbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should not be visible"); column++;
                Assert.AreEqual("ExpO Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header EOHbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should not be visible"); column++;
                Assert.AreEqual("Move Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Rbv [mm]"); column++;
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should  be visible");
                Assert.AreEqual("Move Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Hbv [mm]"); column++;
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should not be visible");

                // .E
                column = 2;
                var row = dgv.Rows[0];
                Assert.AreEqual("0.11", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .E"); column++;
                Assert.AreEqual("0.93", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .E"); column++;
                Assert.AreEqual("-0.37", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .E"); column++;

                // smooth, rough, rought- smooth
                //rad
                // -0.15; 0.15; 0.3
                //vert
                // -0.16; -0.20; -0.04
                Assert.AreEqual("-0.19", row.Cells[column].Value, "wrong value in dgv for o(Rbv)expected for .E"); column++; // must show:  Oexpected = Omeas + move  = 0.11 + -0.3 = -0.19
                Assert.AreEqual("-0.33", row.Cells[column].Value, "wrong value in dgv for o(Hbv)expected for .E"); column++; // must show:  Oexpected = Omeas + move = -0.37 + 0.04 +  = -0.33

                Assert.AreEqual("-0.30", row.Cells[column].Value, "wrong value in dgv for D(Rbv)move for .E"); column++; // must be move = -(rought-smooth) = - 0.3
                Assert.AreEqual("0.04", row.Cells[column].Value, "wrong value in dgv for D(Hbv)move for .E"); column++; // must be move = -(rought-smooth) =  0.04

                // .S
                column = 2;
                row = dgv.Rows[1];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for D(Rbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for D(Lbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for D(Hbv) for .S"); column++;

                // smooth, rough, rought- smooth
                //rad
                //  0.70; 0.60;-0.10
                //vert
                //  0.26;  0.46;  0.20
                Assert.AreEqual("0.10", row.Cells[column].Value, "wrong value in dgv for D(Rbv)expected for .S"); column++;  // must show: Oexpected = Omeas + move  = 0.1 + (0) = 0.1
                Assert.AreEqual("-0.20", row.Cells[column].Value, "wrong value in dgv for D(Lbv)expected for .S"); column++; // must show:  Oexpected = Omeas + move   = -0.2 + (0) = -0.20

                Assert.AreEqual("0.10", row.Cells[column].Value, "wrong value in dgv for D(Hbv)move for .S"); column++;  // must be  move = -(rought-smooth) = 0.1
                Assert.AreEqual("-0.20", row.Cells[column].Value, "wrong value in dgv for D(Rbv)move for .S"); column++; // must be  move = -(rought-smooth) = -0.2

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L2_R_RD_APM_Step2_Second_Measure()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_R_RD_APM_Step1_Choose_Files_and_relative_displacement) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
   <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""397"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""377"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""613"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""613"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""354"" trace=""Re-measure+"" Button=""Left"" Direction=""Down"" Xpos=""730"" Ypos=""471"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""106"" trace=""Re-measure+"" Button=""Left"" Direction=""Up"" Xpos=""730"" Ypos=""471"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Re-measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Re-measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""336"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""705"" Ypos=""678"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""705"" Ypos=""678"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""390"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""505"" Ypos=""728"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""505"" Ypos=""728"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""203"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""285"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""385"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1386"" Ypos=""859"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""75"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1386"" Ypos=""859"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""320"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""802"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""802"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""289"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1017"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""90"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1017"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""MB: Measure comparaison: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""813"" Ypos=""545"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""813"" Ypos=""545"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""361"" trace=""button2+Never"" Button=""Left"" Direction=""Down"" Xpos=""1119"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""74"" trace=""button2+Never"" Button=""Left"" Direction=""Up"" Xpos=""1119"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Already Measured: Never"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""322"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""710"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""710"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""305"" trace=""Re-measure+"" Button=""Left"" Direction=""Down"" Xpos=""832"" Ypos=""465"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""114"" trace=""Re-measure+"" Button=""Left"" Direction=""Up"" Xpos=""832"" Ypos=""465"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Re-measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Re-measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""358"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""626"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""626"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""338"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1312"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1312"" Ypos=""876"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""BB: Valid end"" />
  </KeyStrokesAndMouseClicks>
</Macro>",
                NewPlacementForMessage = true,
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count == 1, "must have 1 measuremetn module");
                var apm = tsunami.MeasurementModules[0] as TSU.Polar.Module;
                Assert.IsNotNull(apm, "no gem module");
                var stm = apm.StationModules.FirstOrDefault(x => x.GetType() == typeof(TSU.Polar.Station.Module)) as TSU.Polar.Station.Module;
                var station = stm.StationTheodolite;
                Assert.IsNotNull(station, "no station ref 1");
                var firstMeas = station.MeasuresTaken.FirstOrDefault(x => x._Point._Name == "TT41.MDGV.411506.E");

                Assert.IsNotNull(firstMeas, "no first measure for TT41.MDGV.411506.E");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 19, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                int column = 2;
                var row = dgv.Rows[0];
                Assert.AreEqual("0.08", row.Cells[column].Value, "wrong value in dgv for D(Rbv) for .E"); column++;
                Assert.AreEqual("2.79", row.Cells[column].Value, "wrong value in dgv for D(Lbv) for .E"); column++;
                Assert.AreEqual("-1.09", row.Cells[column].Value, "wrong value in dgv for D(Hbv) for .E"); column++;

               // test.Wait();
                // smooth, rough, rought- smooth                    spec says RD = (exp-displ) - (rough-smooth)
                //rad
                // -0.15; 0.15; 0.3
                //vert
                // -0.16; -0.20; -0.04 
                Assert.AreEqual("-0.19", row.Cells[column].Value, "wrong value in dgv for o(Rbv)expected for .E"); column++; //expected = firstOm - roughMinusSmoothR  = 0.11 + (-0.03) =  -0.19
                Assert.AreEqual("-0.33", row.Cells[column].Value, "wrong value in dgv for o(Lbv)expected for .E"); column++; //expected = firstOm - roughMinusSmoothR  = -0.37 + 0.04 = -0.33
                Assert.AreEqual("-0.27", row.Cells[column].Value, "wrong value in dgv for move for .E"); column++; // move = dxecpet-dmeas = -0.19 - 0.08 = -0.27
                Assert.AreEqual("0.76", row.Cells[column].Value, "wrong value in dgv for move for .E"); column++; // move = dxecpet-dmeas = -0.33 - -1.09 = 0.76 

                column = 2;
                row = dgv.Rows[1];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .S"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .S"); column++;

                // smooth, rough, rought- smooth
                //rad
                //  0.70; 0.60;-0.10
                //vert
                //  0.26;  0.46;  0.20
                Assert.AreEqual("0.10", row.Cells[column].Value, "wrong value in dgv for o(Rbv)expected for .S"); column++;  //expected = first - roughMinusSmoothR  = 0.1
                Assert.AreEqual("-0.20", row.Cells[column].Value, "wrong value in dgv for o(Lbv)expected for .S"); column++; //expected = first - roughMinusSmoothR  =-0.2

                Assert.AreEqual("0.10", row.Cells[column].Value, "wrong value in dgv for move for .S"); column++; // move = dxecpet-dmeas = 0.1 - 0 = 0.1
                Assert.AreEqual("-0.20", row.Cells[column].Value, "wrong value in dgv for move for .S"); column++;// move = dxecpet-dmeas = -0.2 - 0 = -0.2

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }


    }
}
