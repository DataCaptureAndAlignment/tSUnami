﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Functional_Tests.Polar.OLOC
{
    [TestClass]
    public class Intersection
    {

        /// <summary>
        /// network: the zone choosen id LHCb
        /// 926R005.REF.001.          1.658964     1.658964     0.609067
        /// 926R005.REF.002.         -0.058969     1.980180     0.372939
        /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
        /// 926R005.REF.004.          0.497408    -1.569622     0.559243
        /// 926R005.REF.005.          0.678820    -1.597149     0.553405
        //


        [TestMethod()]
        public void L3_P_OO_Step1_New_Station_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(NewFrameInR005.L2_P_NF_Step3_Compute_With_LGC_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
      <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""256"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""708"" Ypos=""117"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""89"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""708"" Ypos=""117"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""273"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""818"" Ypos=""153"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""146"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""818"" Ypos=""153"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: New Station (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""293"" trace=""button1+Clone"" Button=""Left"" Direction=""Down"" Xpos=""905"" Ypos=""672"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""98"" trace=""button1+Clone"" Button=""Left"" Direction=""Up"" Xpos=""905"" Ypos=""672"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""MB: Clone previous station parameters?: Clone"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""298"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1069"" Ypos=""626"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""394"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1069"" Ypos=""626"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: New Station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""257"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""452"" Ypos=""378"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""452"" Ypos=""378"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""206"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""630"" Ypos=""505"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""630"" Ypos=""505"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""28"" trace=""Known position+"" Button=""Left"" Direction=""Down"" Xpos=""740"" Ypos=""539"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""81"" trace=""Known position+"" Button=""Left"" Direction=""Up"" Xpos=""740"" Ypos=""539"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""BB: Known position (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""214"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""305"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""66"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""305"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""61"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""882"" Ypos=""926"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""74"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""882"" Ypos=""926"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Point stationned: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Known position end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);
                Assert.IsTrue(station.Parameters2._StationPoint._Name.Contains("STL"), "station must contain STL", "initial setup position must be known");
                Assert.IsTrue(station.Parameters2.Setups.InitialValues.IsPositionKnown = true);

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L3_P_OO_Step2_Select_3_Refrences_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_OO_Step1_New_Station_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""264"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""415"" Ypos=""255"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""415"" Ypos=""255"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""276"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""417"" Ypos=""287"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""417"" Ypos=""287"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""297"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""529"" Ypos=""312"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""529"" Ypos=""312"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""288"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""628"" Ypos=""345"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""90"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""628"" Ypos=""345"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""253"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""252"" Ypos=""353"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""252"" Ypos=""353"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""238"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""247"" Ypos=""380"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""247"" Ypos=""380"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""230"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""255"" Ypos=""405"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""255"" Ypos=""405"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""269"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""876"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""90"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""876"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Add known Point(s) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);
                Assert.IsTrue(station.MeasuresToDo.Count == 3, "wrong number of measureToDo left");
                Assert.IsTrue(station.MeasuresToDo.Find(x=>x._Point._Name== "926R005.REF.001.")!=null, "no 926R005.REF.001. as ref selected");
                Assert.IsTrue(station.MeasuresToDo.Find(x => x._Point._Name == "926R005.REF.002.") != null, "no 926R005.REF.001. as ref selected");
                Assert.IsTrue(station.MeasuresToDo.Find(x => x._Point._Name == "926R005.REF.003.") != null, "no 926R005.REF.001. as ref selected");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L3_P_OO_Step3_Acquire_2_first_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_OO_Step2_Select_3_Refrences_Test) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" FunctionKey=""F1"" speedFactor=""1"">
IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />    
<KeyStrokesAndMouseClicks>
	  <!-- Close the admin node-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""175"" Button=""Left"" Direction=""Down"" Xpos=""422"" Ypos=""255"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" Button=""Left"" Direction=""Up"" Xpos=""422"" Ypos=""255"" Delta=""0"" />
	  <!-- Close the measured node-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""205"" Button=""Left"" Direction=""Down"" Xpos=""416"" Ypos=""286"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""96"" Button=""Left"" Direction=""Up"" Xpos=""416"" Ypos=""286"" Delta=""0"" />

	  <!-- enter AH t2 field-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""15"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Valid end"" />

	  <!-- enter AH t2 field-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""205"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
      <!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Valid end"" />
	  
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);
                Assert.IsTrue(station.MeasuresToDo.Count == 1, "wrong number of measureToDo left");

                Assert.IsTrue(station.MeasuresToDo.Find(x => x._Point._Name == "926R005.REF.001.") == null, "no 926R005.REF.001. not measured");
                Assert.IsTrue(station.MeasuresToDo.Find(x => x._Point._Name == "926R005.REF.002.") == null, "no 926R005.REF.001. not measured");
                Assert.IsTrue(station.MeasuresToDo.Find(x => x._Point._Name == "926R005.REF.003.") != null, "no 926R005.REF.001. as ref selected");
                Assert.IsTrue(station.MeasuresTaken.Count == 2, "wrong number of measureTaken");
                var meas1 = station.MeasuresTaken.Find(x => x._Point._Name == "926R005.REF.001.");
                var meas2 = station.MeasuresTaken.Find(x => x._Point._Name == "926R005.REF.002.");
                Assert.IsTrue(meas1 != null, "no 926R005.REF.001. not measured");
                Assert.IsTrue(meas2 != null, "no 926R005.REF.001. as ref selected");
                Assert.AreEqual(2.423899, (meas1 as TSU.Polar.Measure).Distance.Corrected.Value, 0.0001, "not right distance for meas1");
                Assert.AreEqual(2.015856, (meas2 as TSU.Polar.Measure).Distance.Corrected.Value, 0.0001, "not right distance for meas2");


                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L3_P_OO_Step4_Acquire_Rest_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_OO_Step3_Acquire_2_first_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"">
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""211"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""420"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""80"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""420"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""289"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""421"" Ypos=""283"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""99"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""421"" Ypos=""283"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""281"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""529"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""529"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""222"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1316"" Ypos=""880"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1316"" Ypos=""880"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""237"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""560"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""83"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""560"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""285"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""704"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""74"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""704"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""278"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""638"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""638"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""833"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1325"" Ypos=""892"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1325"" Ypos=""892"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""162"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""968"" Ypos=""610"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""97"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""968"" Ypos=""610"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""MB: Closure OK: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""BB: Valid end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);
                Assert.IsTrue(station.MeasuresToDo.Count == 0, "wrong number of measureToDo");
                Assert.IsTrue(station.MeasuresTaken.Count == 4, "wrong number of measureTaken");

                var meas3 = station.MeasuresTaken.Find(x => x._Point._Name == "926R005.REF.003.");
                Assert.IsTrue(meas3 != null, "no 926R005.REF.003. not measured");
                Assert.AreEqual(1.5711444, (meas3 as TSU.Polar.Measure).Distance.Corrected.Value, 0.0001, "not right distance for meas1");


                var meas4 = station.MeasuresTaken.LastOrDefault();
                Assert.IsTrue(meas4 != null, "no control measured");
                Assert.IsTrue(meas4._Status.Type == TSU.Common.Measures.States.Types.Control, "no control measured");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }



        [TestMethod()]
        public void L3_P_OO_Step5_Compute_With_LGC_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_OO_Step4_Acquire_Rest_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""207"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""425"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""80"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""425"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""277"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""429"" Ypos=""297"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""429"" Ypos=""297"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""266"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""425"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""425"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""241"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""401"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""401"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""218"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""766"" Ypos=""500"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""82"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""766"" Ypos=""500"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""253"" trace=""button1+Orientation Only"" Button=""Left"" Direction=""Down"" Xpos=""897"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""81"" trace=""button1+Orientation Only"" Button=""Left"" Direction=""Up"" Xpos=""898"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""MB: Choose a type of setup: Orientation Only"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""266"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""997"" Ypos=""738"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""89"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""997"" Ypos=""738"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""286"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""866"" Ypos=""755"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""89"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""866"" Ypos=""755"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);

                Assert.IsTrue(station.MeasuresToDo.Count == 0, "wrong number of measureToDo left");
                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.X.Value, 0.001);
                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.Y.Value, 0.001);
                Assert.AreEqual(0, station.Parameters2._StationPoint._Coordinates.Su.Z.Value, 0.001);

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }

        [TestMethod()]
        public void L3_P_OO_Step6_New_point()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_OO_Step5_Compute_With_LGC_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""416"" Ypos=""258"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""416"" Ypos=""258"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""242"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""416"" Ypos=""285"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""416"" Ypos=""285"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""229"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""508"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""508"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""237"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""659"" Ypos=""433"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""98"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""659"" Ypos=""433"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""537"" Ypos=""677"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""537"" Ypos=""677"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""274"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1296"" Ypos=""872"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1296"" Ypos=""872"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""10"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""200"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""221"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""426"" Ypos=""295"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""53"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""426"" Ypos=""295"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""204"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""474"" Ypos=""332"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""52"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""474"" Ypos=""332"" Delta=""0"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;

                var pointMeasured = station.PointsMeasured;
                Assert.AreEqual(4, pointMeasured.Count, $"expecting 4 measured points and not{pointMeasured.Count}");
                var intersectedPoint = pointMeasured[3];
                Assert.AreEqual("926R005.REF.006.", intersectedPoint._Name,  "wrong new poonts name");
                Assert.AreEqual(-9.996757112316, intersectedPoint._Coordinates.Su.Y.Value, 0.001, "wrong new ponit coordinates");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsuT");
            });
        }

        [TestMethod()]
        public void L3_P_OO_Step7_cancel_setup()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_OO_Step6_New_point) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""299"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""541"" Ypos=""379"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""96"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""541"" Ypos=""379"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""286"" trace=""Cancel Setup+"" Button=""Left"" Direction=""Down"" Xpos=""637"" Ypos=""416"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""97"" trace=""Cancel Setup+"" Button=""Left"" Direction=""Up"" Xpos=""637"" Ypos=""416"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Cancel Setup (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Cancel Setup end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""226"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""630"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""121"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""630"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""286"" trace=""Set as a 'Free-Station'+"" Button=""Left"" Direction=""Down"" Xpos=""741"" Ypos=""412"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""58"" trace=""Set as a 'Free-Station'+"" Button=""Left"" Direction=""Up"" Xpos=""741"" Ypos=""412"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Set as a 'Free-Station' (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""297"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1046"" Ypos=""608"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""90"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1046"" Ypos=""608"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Set as a 'Free-Station' end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""221"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""597"" Ypos=""375"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""597"" Ypos=""375"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""238"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""716"" Ypos=""500"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""81"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""716"" Ypos=""500"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""214"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1269"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1269"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""161"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1269"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1269"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1269"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1269"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""241"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1269"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""89"" trace=""button4+Force (not recommended)"" Button=""Left"" Direction=""Up"" Xpos=""1269"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""214"" trace=""button4+Force (not recommended)"" Button=""Left"" Direction=""Down"" Xpos=""1262"" Ypos=""574"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""114"" trace=""button4+Force (not recommended)"" Button=""Left"" Direction=""Up"" Xpos=""1262"" Ypos=""574"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""MB: 'Control of opening' needed: Force (not recommended)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""289"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""941"" Ypos=""730"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""114"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""941"" Ypos=""730"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""229"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""904"" Ypos=""742"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""122"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""904"" Ypos=""742"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;
                Assert.IsTrue(station.Parameters2._StationPoint._Name.Contains("STL"), "station must contain STL");

                Assert.IsTrue(station.Parameters2.Setups.InitialValues.IsPositionKnown == false, "initial setup position must be known");
                Assert.IsTrue(station.Parameters2.Setups.FinalValues.Ready == true, "final setup not ready");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}
