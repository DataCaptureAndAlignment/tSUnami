﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using TSU.Views.Message;

namespace Functional_Tests.Polar.OLOC
{
    [TestClass]
    public class FreeStationInR005
    {

        /// <summary>
        /// network: the zone choosen id LHCb
        /// 926R005.REF.001.          1.658964     1.658964     0.609067
        /// 926R005.REF.002.         -0.058969     1.980180     0.372939
        /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
        /// 926R005.REF.004.          0.497408    -1.569622     0.559243
        /// 926R005.REF.005.          0.678820    -1.597149     0.553405
        //


        [TestMethod()]
        public void L3_P_FS_Step1_New_Station_as_FS_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(Functional_Tests.Polar.OLOC.NewFrameInR005.L2_P_NF_Step3_Compute_With_LGC_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""253"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""904"" Ypos=""108"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""904"" Ypos=""108"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""205"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""1005"" Ypos=""130"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""82"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""1005"" Ypos=""130"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: New Station (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""281"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""165"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""189"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""98"" trace=""button4+Force (not recommended)"" Button=""Left"" Direction=""Up"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""181"" trace=""button4+Force (not recommended)"" Button=""Left"" Direction=""Down"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""122"" trace=""button4+Force (not recommended)"" Button=""Left"" Direction=""Up"" Xpos=""1281"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: 'Control of opening' needed: Force (not recommended)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""213"" trace=""button1+Clone"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""650"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""114"" trace=""button1+Clone"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""650"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""MB: Clone previous station parameters?: Clone"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""229"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1079"" Ypos=""627"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""113"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1079"" Ypos=""627"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: New Station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""586"" Ypos=""382"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""121"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""586"" Ypos=""382"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""201"" trace=""Set as a 'Free-Station'+"" Button=""Left"" Direction=""Down"" Xpos=""801"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""114"" trace=""Set as a 'Free-Station'+"" Button=""Left"" Direction=""Up"" Xpos=""801"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""BB: Set as a 'Free-Station' (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""234"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1077"" Ypos=""625"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""122"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1077"" Ypos=""625"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Set as a 'Free-Station' end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""237"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""627"" Ypos=""418"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""627"" Ypos=""418"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""272"" trace=""Quick measure+"" Button=""Left"" Direction=""Down"" Xpos=""763"" Ypos=""759"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""98"" trace=""Quick measure+"" Button=""Left"" Direction=""Up"" Xpos=""763"" Ypos=""759"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""36"" Delay=""0"" Action=""BB: Quick measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""BB: Quick measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""249"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""640"" Ypos=""407"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""640"" Ypos=""407"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""257"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""809"" Ypos=""435"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""97"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""809"" Ypos=""435"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Change Reflector (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""242"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""280"" Ypos=""294"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""280"" Ypos=""294"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""246"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""898"" Ypos=""907"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""81"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""898"" Ypos=""907"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""MB: Choose a Reflector: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Change Reflector end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);
                Assert.IsTrue(station.Parameters2._StationPoint._Name.Contains("STL"), "station name must contain sTL");
                var points = tsunami.Points;
                var hiddens = tsunami.HiddenPoints;
                var temps = tsunami.TempPoints;
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
               
            }));
        }

        [TestMethod()]
        public void L3_P_FS_Step2_Select_Refrences_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_FS_Step1_New_Station_as_FS_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""294"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""255"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""255"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""254"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""417"" Ypos=""285"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""250"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""417"" Ypos=""285"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""229"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""541"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""541"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""237"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""695"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""106"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""696"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""294"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""254"" Ypos=""350"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""254"" Ypos=""350"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""270"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""257"" Ypos=""379"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""257"" Ypos=""379"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""293"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""246"" Ypos=""420"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""246"" Ypos=""420"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""226"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""247"" Ypos=""440"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""2"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""247"" Ypos=""440"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""234"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""239"" Ypos=""468"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""239"" Ypos=""468"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""282"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""243"" Ypos=""531"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""66"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""243"" Ypos=""531"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""277"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""895"" Ypos=""921"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""81"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""895"" Ypos=""921"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Add known Point(s) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);
                Assert.IsTrue(station.MeasuresToDo.Count == 5, $"wrong number of measureToDo left:  {station.MeasuresToDo.Count} instead of 6");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            }));
        }

        [TestMethod()]
        public void L3_P_FS_Step3_Acquire_2_first_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_FS_Step2_Select_Refrences_Test) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" FunctionKey=""F1"" speedFactor=""1"">
IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />    
<KeyStrokesAndMouseClicks>
	  <!-- Close the admin node-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""175"" Button=""Left"" Direction=""Down"" Xpos=""422"" Ypos=""255"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" Button=""Left"" Direction=""Up"" Xpos=""422"" Ypos=""255"" Delta=""0"" />
	  <!-- Close the measured node-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""205"" Button=""Left"" Direction=""Down"" Xpos=""416"" Ypos=""286"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""96"" Button=""Left"" Direction=""Up"" Xpos=""416"" Ypos=""286"" Delta=""0"" />

	  <!-- enter AH t2 field-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""15"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" /> 
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Valid end"" />

	  <!-- enter AH t2 field-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""205"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
      <!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Valid end"" />
	  
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);
                Assert.IsTrue(station.MeasuresToDo.Count == 3, $"wrong number of measureToDo left {station.MeasuresToDo.Count} instead of 3");
                var points = tsunami.Points;
                var hiddens = tsunami.HiddenPoints;
                var temps = tsunami.TempPoints;

                Assert.IsTrue(station.MeasuresTaken.Count == 2, $"wrong number of measureTaken{station.MeasuresTaken.Count} instead of 2");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L3_P_FS_Step4_Acquire_Rest_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_FS_Step3_Acquire_2_first_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"">
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""0.8"" />
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""211"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""420"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""80"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""420"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""289"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""421"" Ypos=""283"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""99"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""421"" Ypos=""283"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""281"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""529"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""529"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""222"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1316"" Ypos=""880"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1316"" Ypos=""880"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""214"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""598"" Ypos=""673"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""598"" Ypos=""673"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""249"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1263"" Ypos=""883"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""90"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1263"" Ypos=""883"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""269"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""764"" Ypos=""673"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""764"" Ypos=""673"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""290"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1266"" Ypos=""867"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""66"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1266"" Ypos=""867"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""254"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""738"" Ypos=""673"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""738"" Ypos=""673"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""281"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1316"" Ypos=""888"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""74"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1316"" Ypos=""888"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""237"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""560"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""83"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""560"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""285"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""704"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""74"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""704"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""278"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""638"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""638"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""833"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1325"" Ypos=""892"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1325"" Ypos=""892"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""162"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""968"" Ypos=""610"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""97"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""968"" Ypos=""610"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""MB: Closure OK: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""BB: Valid end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station, "station object is NULL");
                Assert.IsTrue(station.Parameters2._StationPoint.SocketCode.Id=="29", "socket code must be 29");

                var measureToDoCount = station.MeasuresToDo.Count;
                Assert.IsTrue(measureToDoCount == 0, $"expecting {0} measures to do and got {measureToDoCount}");

                var MeasuresTakenCount = station.MeasuresTaken.Count;
                Assert.IsTrue(MeasuresTakenCount == 7, $"expecting {7} measures to do and got {MeasuresTakenCount}");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L3_P_FS_Step5_Compute_With_LGC_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_FS_Step4_Acquire_Rest_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
	  <!-- Close the admin node-->
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""252"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""252"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""249"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""419"" Ypos=""284"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""419"" Ypos=""284"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""417"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""205"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""417"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""282"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""594"" Ypos=""381"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""297"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""594"" Ypos=""381"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""221"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""796"" Ypos=""496"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""122"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""796"" Ypos=""496"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""393"" trace=""button2+Compute in a new frame"" Button=""Left"" Direction=""Down"" Xpos=""1137"" Ypos=""488"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""298"" trace=""button2+Compute in a new frame"" Button=""Left"" Direction=""Up"" Xpos=""1137"" Ypos=""488"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Choose a type of setup: Compute in a new frame"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""258"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""976"" Ypos=""682"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""291"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""976"" Ypos=""682"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""264"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""898"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""213"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""898"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""2000"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);

                var measureToDoCount = station.MeasuresToDo.Count;
                Assert.IsTrue(measureToDoCount == 0, $"expecting {0} measures to do and got {measureToDoCount}");

                var MeasuresTakenCount = station.MeasuresTaken.Count;
                Assert.IsTrue(MeasuresTakenCount == 7, $"expecting {7} measures to do and got {MeasuresTakenCount}");

                var stationP = station.Parameters2._StationPoint;
                var su = stationP._Coordinates.Su;
                Assert.AreEqual(0, su.X.Value, 0.001, $"{su.X.Value} for X station instead of 0");
                Assert.AreEqual(0, su.Y.Value, 0.001, $"{su.Y.Value} for X station instead of 0");
                Assert.AreEqual(0, su.Z.Value, 0.001, $"{su.Z.Value} for X station instead of 0");

                var points = tsunami.Points;
                var stationStored = points.LastOrDefault(x => x._Name == stationP._Name);

                Assert.IsNotNull(stationStored, "last station nots store in element manager");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L3_P_FS_Step6_New_point()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_FS_Step5_Compute_With_LGC_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""0.5"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""416"" Ypos=""258"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""6"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""416"" Ypos=""258"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""242"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""416"" Ypos=""285"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""416"" Ypos=""285"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""229"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""508"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""508"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""237"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""659"" Ypos=""433"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""9"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""659"" Ypos=""433"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""537"" Ypos=""677"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""537"" Ypos=""677"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""274"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1296"" Ypos=""872"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1296"" Ypos=""872"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""200"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""237"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""560"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""83"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""560"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""285"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""704"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""74"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""704"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""278"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""638"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""638"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""333"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1325"" Ypos=""892"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1325"" Ypos=""892"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Valid (beginning)"" />



    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""307"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""845"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""845"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""302"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1065"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""10"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1065"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""MB: Measure comparaison: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""162"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""968"" Ypos=""610"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""97"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""968"" Ypos=""610"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""MB: Closure OK: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""BB: Valid end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""310"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""1226"" Ypos=""130"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""19"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""1226"" Ypos=""130"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Polar Module end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""307"" trace=""ST 1 (Created on: )+"" Button=""Left"" Direction=""Down"" Xpos=""1208"" Ypos=""484"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""13"" trace=""ST 1 (Created on: )+"" Button=""Left"" Direction=""Up"" Xpos=""1208"" Ypos=""484"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: ST 1 (Created on: 11/06/2024 16:08:38) (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: ST 1 (Created on: 11/06/2024 16:08:38) end"" />

   

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""300"" trace=""STtheo_240611_160838_ZONE_STL_20240829M01_+"" Button=""Left"" Direction=""Down"" Xpos=""924"" Ypos=""212"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""9"" trace=""STtheo_240611_160838_ZONE_STL_20240829M01_+"" Button=""Left"" Direction=""Up"" Xpos=""924"" Ypos=""212"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: STtheo_240611_160838_ZONE_STL_20240829M01_ (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: STtheo_240611_160838_ZONE_STL_20240829M01_ end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""321"" trace=""Close Station+"" Button=""Left"" Direction=""Down"" Xpos=""1014"" Ypos=""492"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""16"" trace=""Close Station+"" Button=""Left"" Direction=""Up"" Xpos=""1014"" Ypos=""492"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Close Station (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""304"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""888"" Ypos=""584"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""12"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""888"" Ypos=""584"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: Close the station?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Close Station end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""305"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""1229"" Ypos=""120"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""9"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""1229"" Ypos=""120"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: Polar Module end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""304"" trace=""ST 2 (Created on: )+"" Button=""Left"" Direction=""Down"" Xpos=""1226"" Ypos=""370"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""8"" trace=""ST 2 (Created on: )+"" Button=""Left"" Direction=""Up"" Xpos=""1226"" Ypos=""370"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: ST 2 (Created on: 30/08/2024 15:41:05) (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""BB: ST 2 (Created on: 30/08/2024 15:41:05) end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""302"" trace=""STtheo_240830_154105_926R005_STL_20240830M01_+"" Button=""Left"" Direction=""Down"" Xpos=""937"" Ypos=""220"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""8"" trace=""STtheo_240830_154105_926R005_STL_20240830M01_+"" Button=""Left"" Direction=""Up"" Xpos=""937"" Ypos=""220"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: STtheo_240830_154105_926R005_STL_20240830M01_ (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: STtheo_240830_154105_926R005_STL_20240830M01_ end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""301"" trace=""Close Station+"" Button=""Left"" Direction=""Down"" Xpos=""1034"" Ypos=""481"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""7"" trace=""Close Station+"" Button=""Left"" Direction=""Up"" Xpos=""1034"" Ypos=""481"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Close Station (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""318"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""911"" Ypos=""545"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""81"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""911"" Ypos=""545"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Close the station?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Close Station end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""305"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""904"" Ypos=""129"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""8"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""904"" Ypos=""129"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""BB: Polar Module end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""309"" trace=""Exportation+"" Button=""Left"" Direction=""Down"" Xpos=""979"" Ypos=""300"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""8"" trace=""Exportation+"" Button=""Left"" Direction=""Up"" Xpos=""979"" Ypos=""300"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""BB: Exportation (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""46"" Delay=""0"" Action=""BB: Exportation end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""307"" trace=""Geode+"" Button=""Left"" Direction=""Down"" Xpos=""1134"" Ypos=""421"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""10"" trace=""Geode+"" Button=""Left"" Direction=""Up"" Xpos=""1134"" Ypos=""421"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""49"" Delay=""0"" Action=""BB: Geode (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""50"" Delay=""0"" Action=""BB: Geode end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""305"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1404"" Ypos=""1047"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1404"" Ypos=""1047"" Delta=""0"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var station = tsunami.MeasurementModules[0]?.StationModules[1]?._Station as TSU.Polar.Station;

                var lastMeaure = station.PointsMeasured.FirstOrDefault(x => x._Name == "926R005.REF.007.");

                Assert.IsTrue(lastMeaure!=null, "lastMeaure measure must be on REF.007.");
                Assert.AreEqual(-0.2546, lastMeaure._Coordinates.Su.X.Value, 0.001);

                //.dat
                var datFileContent = test.GetLastDatFileFromCurrentDirectory(out _);
                Assert.IsTrue(datFileContent != "", "No data in .dat file");
                var pattern = "NP;926R005   ;REF.006.                         ;p;1.60936   ;-0.64935  ;0.55341   ; 0;  -999.900";
                bool exist = datFileContent.Contains(pattern);
                Assert.IsTrue(exist, $"cannot find NP 006 in dat file");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L3_P_FS_Step7_New_Station_WithNewPoint_IN_Setup()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_P_FS_Step6_New_point) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
       <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""360"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""260"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""260"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""346"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""422"" Ypos=""295"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""422"" Ypos=""295"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""329"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""562"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""562"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""398"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""732"" Ypos=""660"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""89"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""732"" Ypos=""660"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""366"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""721"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""721"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""386"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1341"" Ypos=""892"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""11"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1341"" Ypos=""892"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""357"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""845"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""845"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""302"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1065"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""10"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1065"" Ypos=""745"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""MB: Measure comparaison: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""381"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""952"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""9"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""952"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""MB: Closure OK: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""385"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""820"" Ypos=""113"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""10"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""820"" Ypos=""113"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Polar Module end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""366"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""921"" Ypos=""139"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""11"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""921"" Ypos=""139"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: New Station (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""374"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1330"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""21"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1330"" Ypos=""588"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""393"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1025"" Ypos=""794"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1025"" Ypos=""794"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""306"" trace=""button1+Clone"" Button=""Left"" Direction=""Down"" Xpos=""897"" Ypos=""638"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""113"" trace=""button1+Clone"" Button=""Left"" Direction=""Up"" Xpos=""897"" Ypos=""638"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""MB: Clone station parameters?: Clone"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""326"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1076"" Ypos=""611"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""9"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1076"" Ypos=""611"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""BB: New Station end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""341"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""425"" Ypos=""258"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""425"" Ypos=""258"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""374"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""426"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""426"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""326"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""723"" Ypos=""683"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""723"" Ypos=""683"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""334"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1319"" Ypos=""878"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""8"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1319"" Ypos=""878"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""52"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""342"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""601"" Ypos=""367"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""601"" Ypos=""367"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""366"" trace=""Remove+"" Button=""Left"" Direction=""Down"" Xpos=""722"" Ypos=""489"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""8"" trace=""Remove+"" Button=""Left"" Direction=""Up"" Xpos=""722"" Ypos=""489"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""59"" Delay=""0"" Action=""BB: Remove (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""60"" Delay=""0"" Action=""BB: Remove end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""394"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""595"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""595"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""304"" trace=""Remove+"" Button=""Left"" Direction=""Down"" Xpos=""700"" Ypos=""475"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""7"" trace=""Remove+"" Button=""Left"" Direction=""Up"" Xpos=""700"" Ypos=""475"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""65"" Delay=""0"" Action=""BB: Remove (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""66"" Delay=""0"" Action=""BB: Remove end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""342"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""602"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""602"" Ypos=""361"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""352"" trace=""Remove+"" Button=""Left"" Direction=""Down"" Xpos=""718"" Ypos=""458"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""10"" trace=""Remove+"" Button=""Left"" Direction=""Up"" Xpos=""718"" Ypos=""458"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""71"" Delay=""0"" Action=""BB: Remove (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""72"" Delay=""0"" Action=""BB: Remove end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""376"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""610"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""610"" Ypos=""362"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""366"" trace=""Remove+"" Button=""Left"" Direction=""Down"" Xpos=""718"" Ypos=""469"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""9"" trace=""Remove+"" Button=""Left"" Direction=""Up"" Xpos=""718"" Ypos=""469"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""77"" Delay=""0"" Action=""BB: Remove (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""78"" Delay=""0"" Action=""BB: Remove end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""321"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""590"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""590"" Ypos=""392"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""397"" trace=""Remove+"" Button=""Left"" Direction=""Down"" Xpos=""687"" Ypos=""487"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""82"" Delay=""9"" trace=""Remove+"" Button=""Left"" Direction=""Up"" Xpos=""688"" Ypos=""487"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""83"" Delay=""0"" Action=""BB: Remove (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""84"" Delay=""0"" Action=""BB: Remove end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""353"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""592"" Ypos=""385"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""592"" Ypos=""385"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""87"" Delay=""369"" trace=""Remove+"" Button=""Left"" Direction=""Down"" Xpos=""695"" Ypos=""487"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""88"" Delay=""8"" trace=""Remove+"" Button=""Left"" Direction=""Up"" Xpos=""695"" Ypos=""487"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""89"" Delay=""0"" Action=""BB: Remove (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""90"" Delay=""0"" Action=""BB: Remove end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""91"" Delay=""394"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""610"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""610"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""93"" Delay=""310"" trace=""Remove+"" Button=""Left"" Direction=""Down"" Xpos=""764"" Ypos=""501"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""94"" Delay=""10"" trace=""Remove+"" Button=""Left"" Direction=""Up"" Xpos=""764"" Ypos=""501"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""95"" Delay=""0"" Action=""BB: Remove (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""96"" Delay=""0"" Action=""BB: Remove end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""97"" Delay=""342"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""665"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""98"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""665"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""326"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1264"" Ypos=""885"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""8"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1264"" Ypos=""885"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""101"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""102"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""370"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""575"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""575"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""326"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""711"" Ypos=""596"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""8"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""711"" Ypos=""596"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""107"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""108"" Delay=""0"" Action=""BB: Control Opening end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""332"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""817"" Ypos=""663"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""110"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""817"" Ypos=""663"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""111"" Delay=""390"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""860"" Ypos=""673"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""112"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""860"" Ypos=""673"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""113"" Delay=""361"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1313"" Ypos=""885"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""114"" Delay=""7"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1313"" Ypos=""885"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""115"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""116"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""1070"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""575"" Ypos=""323"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""575"" Ypos=""323"" Delta=""0"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var stationMs = tsunami.MeasurementModules[0]?.StationModules;
                Assert.IsTrue(stationMs != null, "stationMs object is NULL");
                Assert.IsTrue(stationMs.Count ==3, "need 3 stationM");

                var station3 = tsunami.MeasurementModules[0]?.StationModules[2]?._Station as TSU.Polar.Station;
                Assert.IsTrue(station3 != null, "station3 object is NULL");

                var measuredPoints = station3.PointsMeasured;
                Assert.IsTrue(measuredPoints != null, "measuredPoints object is NULL");

                var measure1 = measuredPoints.FirstOrDefault(x => x._Name == "926R005.REF.001.");
                Assert.IsTrue(measure1 != null, "measure1 must be on REF.001.");

                var expected = "STtheo_[0-9]{1,8}_[0-9]{1,6}_926R005_STL_[0-9]+.*02_";
                var m = Regex.Matches(measure1._Origin, expected);
                Assert.IsTrue(m.Count == 1 && m[0].Index == 0 && m[0].Length == measure1._Origin.Length, $"\"{measure1._Origin}\" does not match the expected pattern \"{expected}\"");

                var measure2 = measuredPoints.FirstOrDefault(x => x._Name == "926R005.REF.006.");
                Assert.IsTrue(measure2 != null, "measure2 must be on REF.006.");

                Assert.AreEqual(measure2._Origin, measure1._Origin, "wrong origin");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

    }
}
