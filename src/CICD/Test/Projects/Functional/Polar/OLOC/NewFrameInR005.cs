﻿using Functional_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Functional_Tests.Polar.OLOC
{
    [TestClass()]
    public class NewFrameInR005
    {

        /// <summary>
        /// network: the zone choosen id LHCb
        /// 926R005.REF.001.          1.658964     1.658964     0.609067
        /// 926R005.REF.002.         -0.058969     1.980180     0.372939
        /// 926R005.REF.003.         -0.564738    -1.399225     0.437875
        /// 926R005.REF.004.          0.497408    -1.569622     0.559243
        /// 926R005.REF.005.          0.678820    -1.597149     0.553405
        /// 
        private List<TSU.Common.Elements.Point> GetNetworkPointsInLHCb()
        {
            string xmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<ArrayOfPoint xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"">
    <Point Name=""926R005.REF.001."" GUID=""b52f6a53-604a-40d5-bec9-c473754a79f7"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.001"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:33.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""072175d6-03bf-4321-8e03-3a707c96db3c"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""8bb5a16c-872c-4bbe-b9be-29cb46719dc6"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4471.6192930063435"" Sigma=""-999.9"" />
            <Y Value=""5008.1369592289921"" Sigma=""-999.9"" />
            <Z Value=""331.79080008462984"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""7372043d-72c2-4b1e-a9e8-d6aaa861b398"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""1.658964"" Sigma=""-999.9"" />
            <Y Value=""1.658964"" Sigma=""-999.9"" />
            <Z Value=""0.609067"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""1937ad03-6083-425d-8db5-a253096f494f"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""-1.6589639999999997"" Sigma=""-999.9"" />
            <Y Value=""0.61503796215914019"" Sigma=""-999.9"" />
            <Z Value=""-1.6567596291822584"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.001."" GUID=""cb12f561-bd91-43a4-aa74-419b6191c426"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""1.6589640000000006"" Sigma=""-999.9"" />
            <Y Value=""-1.6589639999999997"" Sigma=""-999.9"" />
            <Z Value=""0.609067"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""50490233-e513-4136-bf9c-569416c4d976"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4471.6192930063435"" Sigma=""-999.9"" />
            <Y Value=""5008.1369592289921"" Sigma=""-999.9"" />
            <Z Value=""2330.6574618557129"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.002."" GUID=""4ed5e955-fdb7-4f66-b2b8-aaf4c2ab615b"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.002"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:34.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""eac8907e-3fcd-4c89-9c9e-31d12de7669c"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""19b31f17-c511-4fcf-85fa-c1ac062218aa"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4470.6711273726451"" Sigma=""-999.9"" />
            <Y Value=""5006.6686466851152"" Sigma=""-999.9"" />
            <Z Value=""331.55467193677777"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""d343abca-31f3-4ed2-a741-a59f4bd72367"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""-0.058969"" Sigma=""-999.9"" />
            <Y Value=""1.98018"" Sigma=""-999.9"" />
            <Z Value=""0.372939"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""c9226fa5-a005-4c6c-b472-b22a7b05c387"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""-1.98018"" Sigma=""-999.9"" />
            <Y Value=""0.37272419889645797"" Sigma=""-999.9"" />
            <Z Value=""0.060311791873510504"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.002."" GUID=""1086eda2-b1ba-49c5-8066-6296000362bb"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""1.98018"" Sigma=""-999.9"" />
            <Y Value=""0.058969000000000438"" Sigma=""-999.9"" />
            <Z Value=""0.372939"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""2f5a5908-f605-4cb2-b763-43733a26f4e1"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4470.6711273726451"" Sigma=""-999.9"" />
            <Y Value=""5006.6686466851152"" Sigma=""-999.9"" />
            <Z Value=""2330.4223658147771"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.003."" GUID=""64a2b623-95ad-496a-b10d-266fb25bb010"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.003"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:35.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""2f402851-5db8-4a6d-bda4-06384774acb0"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""7db5b5fd-38f6-4350-82ae-e5aafa0b2780"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4473.6070787319568"" Sigma=""-999.9"" />
            <Y Value=""5004.9203880412861"" Sigma=""-999.9"" />
            <Z Value=""331.61960781092631"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""d728199b-be88-439a-8ef6-e6cd9d9ac776"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""-0.564738"" Sigma=""-999.9"" />
            <Y Value=""-1.399225"" Sigma=""-999.9"" />
            <Z Value=""0.437875"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""a3717ca3-a2e8-4d78-a2c7-56c5d9918578"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""1.3992249999999997"" Sigma=""-999.9"" />
            <Y Value=""0.43583820386412941"" Sigma=""-999.9"" />
            <Z Value=""0.56631138459463271"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.003."" GUID=""274fa341-c627-481f-acef-27eb56b7cb39"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""-1.3992250000000002"" Sigma=""-999.9"" />
            <Y Value=""0.56473799999999963"" Sigma=""-999.9"" />
            <Z Value=""0.437875"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""1d1d0fbb-fef0-415d-9007-8f590fa57f1f"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4473.6070787319568"" Sigma=""-999.9"" />
            <Y Value=""5004.9203880412861"" Sigma=""-999.9"" />
            <Z Value=""2330.4870067333427"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.004."" GUID=""40a1d1b6-8554-448e-b8fa-e39080f1c157"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.004"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:36.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""2ad00226-0973-45f9-a6cb-38ab3b698f26"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""47d40ed3-3af3-47dc-af03-ca4881e64cc4"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4474.1671924158709"" Sigma=""-999.9"" />
            <Y Value=""5005.8388733237407"" Sigma=""-999.9"" />
            <Z Value=""331.74097584872823"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""fd5f2389-64c5-47fa-a815-cfe089bbdb6e"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""0.497408"" Sigma=""-999.9"" />
            <Y Value=""-1.569622"" Sigma=""-999.9"" />
            <Z Value=""0.559243"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""54377433-3347-4edf-93c3-8d9c07b7c55e"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""1.569622"" Sigma=""-999.9"" />
            <Y Value=""0.56103083384441965"" Sigma=""-999.9"" />
            <Z Value=""-0.495390608498824"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.004."" GUID=""48c1718e-3874-459a-a6a4-3e16dd1b8cce"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""-1.569622"" Sigma=""-999.9"" />
            <Y Value=""-0.49740800000000035"" Sigma=""-999.9"" />
            <Z Value=""0.559243"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""ba7c00f1-18a0-4493-ba95-6b8fb26ed546"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4474.1671924158709"" Sigma=""-999.9"" />
            <Y Value=""5005.8388733237407"" Sigma=""-999.9"" />
            <Z Value=""2330.6077415693453"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.005."" GUID=""1bd12d56-4716-42e3-8937-728f2b881681"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.005"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:37.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""a51f6630-4c6a-48ea-b0a8-74e2ec86d0bf"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""6d81c2ff-309b-4be2-9ecb-94e6f9c8a960"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4474.2613896730245"" Sigma=""-999.9"" />
            <Y Value=""5005.9963387474236"" Sigma=""-999.9"" />
            <Z Value=""331.73513987360724"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""60b593f6-35af-4241-8ed5-67bcef5b3f0e"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""0.678825"" Sigma=""-999.9"" />
            <Y Value=""-1.597147"" Sigma=""-999.9"" />
            <Z Value=""0.553407"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""c6f1adae-c9a6-4c07-82b4-64f4675a7dce"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""1.5971470000000003"" Sigma=""-999.9"" />
            <Y Value=""0.5558482618148366"" Sigma=""-999.9"" />
            <Z Value=""-0.67682745076675532"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.005."" GUID=""f2bf2187-fd78-4809-8564-382f6934eb26"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""-1.5971469999999999"" Sigma=""-999.9"" />
            <Y Value=""-0.67882500000000034"" Sigma=""-999.9"" />
            <Z Value=""0.553407"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""6a5ebd94-3b49-4d8a-b930-efd4f89134c1"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4474.2613896730245"" Sigma=""-999.9"" />
            <Y Value=""5005.9963387474236"" Sigma=""-999.9"" />
            <Z Value=""2330.6017977010215"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
    <Point Name=""926R005.REF.006."" GUID=""a1a4d24a-7522-47d6-9601-6b0a9f28b369"" State=""Good"" _Origin=""C:\data\tsunami\Theoretical Files\926\926R005.SU.txt.SU.txt"" _Accelerator=""926R005"" _Zone=""926R005"" _ClassAndNumero=""REF.006"" _OriginType=""Unknown"" fileElementType=""point"" HasChild=""false"" Date=""2024-03-20T11:27:38.4278757+01:00"" RefRoll=""false"" _Point="""" SocketType=""32"" SerialNumber="""" LGCFixOption=""POIN"" Type=""Reference"">
      <_Coordinates Approx=""false"">
        <ListOfAllCoordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""e0ff2f95-ba56-4756-beaf-3f00b0eebe21"" Frame=""USERDEFINED"" ReferenceFrame=""Unknown"" CoordinateSystem=""_3DCartesian"" labels=""Xusr Yusr Zusr"">
            <X Value=""-999.9"" Sigma=""-999.9"" />
            <Y Value=""-999.9"" Sigma=""-999.9"" />
            <Z Value=""-999.9"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""f2a0cd4c-473a-4c14-aec6-9f058a1929c7"" Frame=""CCS-H"" ReferenceFrame=""Unknown"" CoordinateSystem=""_2DPlusH"" labels=""Xccs Yccs H"">
            <X Value=""4474.261390889309"" Sigma=""-999.9"" />
            <Y Value=""5005.996328522464"" Sigma=""-999.9"" />
            <Z Value=""331.73513587309492"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""4be01b16-02c4-457f-a1ed-80c7bea22fe9"" Frame=""SU"" ReferenceFrame=""Su1985Machine"" CoordinateSystem=""_3DCartesian"" labels=""Xsu Ysu Zsu"">
            <X Value=""0.678816"" Sigma=""-999.9"" />
            <Y Value=""-1.597152"" Sigma=""-999.9"" />
            <Z Value=""0.553403"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""6f36e920-9776-4101-a01c-2c049411ee8d"" Frame=""PHYSICIST"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xphys Yphys Zphys"">
            <X Value=""1.5971520000000001"" Sigma=""-999.9"" />
            <Y Value=""0.55584422942644651"" Sigma=""-999.9"" />
            <Z Value=""-0.67681846523149736"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""926R005.REF.006."" GUID=""46ef09c9-121a-484b-8e66-896bb08e7b05"" Frame=""MLA"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xmla Ymla Zmla"">
            <X Value=""-1.5971519999999997"" Sigma=""-999.9"" />
            <Y Value=""-0.67881600000000031"" Sigma=""-999.9"" />
            <Z Value=""0.553403"" Sigma=""-999.9"" />
          </Coordinates>
          <Coordinates Name=""Unknown"" GUID=""5c08770f-f703-451e-925e-d8c5ae54a4c0"" Frame=""CCS-Z"" ReferenceFrame=""Unknown"" CoordinateSystem=""Unknown"" labels=""Xccs Yccs ZRS2K"">
            <X Value=""4474.261390889309"" Sigma=""-999.9"" />
            <Y Value=""5005.996328522464"" Sigma=""-999.9"" />
            <Z Value=""2330.6017937047636"" Sigma=""-999.9"" />
          </Coordinates>
        </ListOfAllCoordinates>
      </_Coordinates>
      <_Parameters Tilt=""-999.9"" Slope=""-999.9"" GisementFaisceau=""-999.9"" R=""-999.9"" S=""-999.9"" Cumul=""-999.9"" T=""-999.9"" L=""-999.9"" />
    </Point>
  </ArrayOfPoint>
  ";
            var points = new List<TSU.Common.Elements.Point>();
            var type = points.GetType();

            // Specify the file path
            string filePath = "Zone926R005.xml";

            try
            {
                // Open or create the file for writing
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    // Write the content of the string variable to the file
                    writer.WriteLine(xmlContent);
                }

                Console.WriteLine("Content has been written to the file successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }

            points = TSU.IO.Xml.DeserializeFile(type, filePath) as List<TSU.Common.Elements.Point>;

            return points;
        }

        [TestMethod()]
        public void ExportPointsListToXml()
        {
            var points = new List<TSU.Common.Elements.Point>();
            points.Add(new TSU.Common.Elements.Point());
            var path = "test.xml";
            TSU.IO.Xml.CreateFile(points, path);
            //Process.Start(path);
        }

        [TestMethod()]
        public void L2_P_NF_Step0_Module_Creation_Test()
        {
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"); // used to fail if the splashscreen test had an error
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1531"" Button=""Left"" Direction=""Down"" Xpos=""196"" Ypos=""351"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""113"" Button=""Left"" Direction=""Up"" Xpos=""196"" Ypos=""351"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />
	  
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""473"" Button=""Left"" Direction=""Down"" Xpos=""380"" Ypos=""390"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""81"" Button=""Left"" Direction=""Up"" Xpos=""380"" Ypos=""390"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Polar Module end"" />
	  
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""462"" Button=""Left"" Direction=""Down"" Xpos=""467"" Ypos=""237"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""105"" Button=""Left"" Direction=""Up"" Xpos=""467"" Ypos=""237"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""322"" Button=""Left"" Direction=""Down"" Xpos=""532"" Ypos=""262"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""73"" Button=""Left"" Direction=""Up"" Xpos=""532"" Ypos=""262"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: New Station (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: New Station end"" />
	  
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""469"" Button=""Left"" Direction=""Down"" Xpos=""339"" Ypos=""123"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""98"" Button=""Left"" Direction=""Up"" Xpos=""339"" Ypos=""123"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: TSUNAMI (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: TSUNAMI end"" />
	  
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""453"" Button=""Left"" Direction=""Down"" Xpos=""485"" Ypos=""485"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""98"" Button=""Left"" Direction=""Up"" Xpos=""486"" Ypos=""485"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Save... (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Save... end"" />
	  
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""458"" Button=""Left"" Direction=""Down"" Xpos=""645"" Ypos=""531"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""98"" Button=""Left"" Direction=""Up"" Xpos=""645"" Ypos=""531"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Save (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""2000"" Action=""BB: Save end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_P_NF_Step1_Administrative_setup_with_T2_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_NF_Step0_Module_Creation_Test) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""316"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""457"" Ypos=""290"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""21"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""457"" Ypos=""290"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""381"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""607"" Ypos=""352"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""607"" Ypos=""352"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""317"" trace=""Team+"" Button=""Left"" Direction=""Down"" Xpos=""700"" Ypos=""379"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""9"" trace=""Team+"" Button=""Left"" Direction=""Up"" Xpos=""700"" Ypos=""379"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Team (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""530"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""530"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""67"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""68"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""60"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+{M}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+{O}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+{I}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""161"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""305"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""947"" Ypos=""579"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""9"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""947"" Ypos=""579"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""MB: Team: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Team end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""316"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""457"" Ypos=""290"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""21"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""457"" Ypos=""290"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""314"" Button=""Left"" Direction=""Down"" Xpos=""551"" Ypos=""353"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""74"" Button=""Left"" Direction=""Up"" Xpos=""551"" Ypos=""353"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""241"" Button=""Left"" Direction=""Down"" Xpos=""653"" Ypos=""387"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""113"" Button=""Left"" Direction=""Up"" Xpos=""653"" Ypos=""387"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Instrument (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""258"" Button=""Left"" Direction=""Down"" Xpos=""183"" Ypos=""357"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""113"" Button=""Left"" Direction=""Up"" Xpos=""183"" Ypos=""357"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""202"" Button=""Left"" Direction=""Down"" Xpos=""214"" Ypos=""424"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""89"" Button=""Left"" Direction=""Up"" Xpos=""214"" Ypos=""424"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""290"" Button=""Left"" Direction=""Down"" Xpos=""319"" Ypos=""455"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""74"" Button=""Left"" Direction=""Up"" Xpos=""319"" Ypos=""455"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""281"" Button=""Left"" Direction=""Down"" Xpos=""885"" Ypos=""926"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""114"" Button=""Left"" Direction=""Up"" Xpos=""885"" Ypos=""926"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""MB: Select instrument: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Instrument end"" />
	     
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""390"" Button=""Left"" Direction=""Down"" Xpos=""585"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""121"" Button=""Left"" Direction=""Up"" Xpos=""585"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""306"" Button=""Left"" Direction=""Down"" Xpos=""736"" Ypos=""422"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""113"" Button=""Left"" Direction=""Up"" Xpos=""736"" Ypos=""422"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Set as a 'Free-Station' (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""1729"" Button=""Left"" Direction=""Down"" Xpos=""1076"" Ypos=""617"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""138"" Button=""Left"" Direction=""Up"" Xpos=""1076"" Ypos=""617"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Set as a 'Free-Station' end"" />
	  
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""249"" Button=""Left"" Direction=""Down"" Xpos=""577"" Ypos=""412"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""298"" Button=""Left"" Direction=""Up"" Xpos=""577"" Ypos=""412"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""287"" Button=""Left"" Direction=""Down"" Xpos=""710"" Ypos=""748"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""281"" Button=""Left"" Direction=""Up"" Xpos=""710"" Ypos=""748"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""BB: Quick measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""46"" Delay=""0"" Action=""BB: Quick measure end"" />
	  
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""223"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""453"" Ypos=""409"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""453"" Ypos=""409"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""289"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""608"" Ypos=""435"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""290"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""608"" Ypos=""435"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""205"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""767"" Ypos=""479"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""290"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""767"" Ypos=""479"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Change Reflector (beginning)"" />
	
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""589"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""268"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""268"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""914"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""896"" Ypos=""914"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""82"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""896"" Ypos=""914"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""MB: Choose a Reflector: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Change Reflector end"" />
	  
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""236"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""785"" Ypos=""372"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""236"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""785"" Ypos=""372"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""246"" trace=""Change Point name+"" Button=""Left"" Direction=""Down"" Xpos=""902"" Ypos=""410"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""253"" trace=""Change Point name+"" Button=""Left"" Direction=""Up"" Xpos=""902"" Ypos=""410"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Change Point name (beginning)"" />
	  
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""266"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""839"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""229"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""839"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""286"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""839"" Ypos=""598"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""213"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""839"" Ypos=""598"" Delta=""0"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""75"" Delay=""85"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""76"" Delay=""63"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
	<IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""52"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""78"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""79"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""80"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
	<IInputActivity xsi:type=""KeyStroke____"" Pos.=""81"" Delay=""54"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
	<IInputActivity xsi:type=""KeyStroke____"" Pos.=""82"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""83"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""84"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""85"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""86"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
	<IInputActivity xsi:type=""KeyStroke____"" Pos.=""87"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
	<IInputActivity xsi:type=""KeyStroke____"" Pos.=""88"" Delay=""70"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""89"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""92"" Delay=""61"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{R}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""93"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""ComboBox"" />
	<IInputActivity xsi:type=""KeyStroke____"" Pos.=""94"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""95"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""96"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{.}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""97"" Delay=""81"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{R}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""98"" Delay=""06"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{E}"" Target=""ComboBox"" />
	<IInputActivity xsi:type=""KeyStroke____"" Pos.=""99"" Delay=""61"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{F}"" Target=""ComboBox"" />
	
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""290"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1089"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""274"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1089"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""281"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1089"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""206"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1089"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""249"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""ComboBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""278"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""912"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""205"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""912"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""MB: Point name: OK"" />
	
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""238"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""889"" Ypos=""849"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""297"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""889"" Ypos=""849"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""500"" Action=""MB: Select Socket Code: OK"" />
	  
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (Action<TSU.Common.Tsunami>)((tsunami) =>
            {
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var mms = tsunami.MeasurementModules;
                Assert.IsTrue(mms.Count > 0, "No modules in measurementModules");

                var sms = mms[0].StationModules;
                Assert.IsTrue(sms.Count > 0, $"No station modules in {mms}");

                var stm = sms[0] as TSU.Polar.Station.Module;
                Assert.IsTrue(stm != null, "No station module levelling");

                var station = stm?._Station as TSU.Polar.Station;
                Assert.IsNotNull(station);

                var points = tsunami.Points;
                var hiddens = tsunami.HiddenPoints;
                var temps = tsunami.TempPoints;

                Assert.AreEqual("926R005.REF.###.", station.ParametersBasic?.DefaultMeasure?._Point._Name);

                
            }));
        }

        [TestMethod()]
        public void L2_P_NF_Step2_Acquire_with_T2_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_NF_Step1_Administrative_setup_with_T2_Test) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" FunctionKey=""F1"" speedFactor=""1"">
IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />    
<KeyStrokesAndMouseClicks>
	  <!-- Close the admin node-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""275"" Button=""Left"" Direction=""Down"" Xpos=""422"" Ypos=""255"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""298"" Button=""Left"" Direction=""Up"" Xpos=""422"" Ypos=""255"" Delta=""0"" />
	  <!-- Close the measured node-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""205"" Button=""Left"" Direction=""Down"" Xpos=""416"" Ypos=""286"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""296"" Button=""Left"" Direction=""Up"" Xpos=""416"" Ypos=""286"" Delta=""0"" />
	  <!-- Add new point-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""213"" Button=""Left"" Direction=""Down"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""214"" Button=""Left"" Direction=""Up"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""218"" Button=""Left"" Direction=""Down"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""214"" Button=""Left"" Direction=""Up"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Add New point end"" />
	  <!-- enter AH t2 field-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""205"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Valid end"" />
	  <!-- check dsa and valide message-->
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1531"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""825"" Ypos=""651"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""825"" Ypos=""651"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""729"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1054"" Ypos=""656"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""129"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1054"" Ypos=""656"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Valid end"" />
	 <!-- Add new point-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""213"" Button=""Left"" Direction=""Down"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""214"" Button=""Left"" Direction=""Up"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""218"" Button=""Left"" Direction=""Down"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""214"" Button=""Left"" Direction=""Up"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Add New point end"" />
	  <!-- enter AH t2 field-->
	  	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""205"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
<!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{6}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Valid end"" />
	  <!-- Add new point-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""213"" Button=""Left"" Direction=""Down"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""114"" Button=""Left"" Direction=""Up"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""218"" Button=""Left"" Direction=""Down"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""114"" Button=""Left"" Direction=""Up"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Add New point end"" />
	  <!-- enter AH t2 field-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""205"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""51"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""52"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""53"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""55"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""56"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""57"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""58"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""59"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""60"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""61"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""62"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""63"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""64"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""65"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""66"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""67"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""68"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""69"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""70"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""71"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""72"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""73"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""74"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""75"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""76"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Valid end"" />
	  <!-- Add new point-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""213"" Button=""Left"" Direction=""Down"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""114"" Button=""Left"" Direction=""Up"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""218"" Button=""Left"" Direction=""Down"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""114"" Button=""Left"" Direction=""Up"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Add New point end"" />
	  <!-- enter AH t2 field-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""205"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""92"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""93"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""94"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""95"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""96"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""97"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""98"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""99"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""100"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""101"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""102"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""103"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""104"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""105"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""106"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""107"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""109"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""110"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""111"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""112"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""113"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""114"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""116"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Valid end"" />
	  <!-- Add new point-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""213"" Button=""Left"" Direction=""Down"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""114"" Button=""Left"" Direction=""Up"" Xpos=""525"" Ypos=""316"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""218"" Button=""Left"" Direction=""Down"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""114"" Button=""Left"" Direction=""Up"" Xpos=""623"" Ypos=""434"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Add New point end"" />
	  <!-- enter AH t2 field-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""242"" Button=""Left"" Direction=""Down"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""205"" Button=""Left"" Direction=""Up"" Xpos=""587"" Ypos=""743"" Delta=""0"" />
	  <!-- enter values-->
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""129"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""130"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""131"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""132"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""133"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{9}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""134"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""135"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""136"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""137"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""138"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""139"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""140"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""141"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{3}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""142"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{4}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""143"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{7}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""144"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""145"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""146"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""147"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""148"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""149"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""150"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""151"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""152"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""153"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""154"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
	  <IInputActivity xsi:type=""KeyStroke____"" Pos.=""155"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitterPanel"" />
	  <!-- validate values-->
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" Button=""Left"" Direction=""Down"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""206"" Button=""Left"" Direction=""Up"" Xpos=""1343"" Ypos=""890"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Valid (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""2000"" Action=""BB: Valid end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var measureModule = tsunami.MeasurementModules;
                Assert.IsTrue(measureModule.Count > 0, "No measurement modules found!");

                var stationModules = measureModule[0]?.StationModules;
                Assert.IsTrue(stationModules.Count > 0, $"No station module in measurement modules {measureModule} found!");

                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station;
                Assert.IsTrue(station != null, "station object is NULL");

                var measures = station.MeasuresTaken;
                Assert.IsTrue(measures.Count > 0, "no measure founds");
                Assert.IsTrue(measures.Count == 5, $"expecting 5 measurement!");

                var lastMeasure = measures[4] as TSU.Polar.Measure;
                Assert.AreEqual(1.821, lastMeasure.Distance.Corrected.Value, 0.001);

                var points = tsunami.Points;
                var hiddens = tsunami.HiddenPoints;
                var temps = tsunami.TempPoints;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_P_NF_Step3_Compute_With_LGC_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_NF_Step2_Acquire_with_T2_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
	  <!-- Close the admin node-->
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""280"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""252"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""252"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""249"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""419"" Ypos=""284"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""419"" Ypos=""284"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""263"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""500"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""500"" Ypos=""318"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""292"" trace=""Control Opening+"" Button=""Left"" Direction=""Down"" Xpos=""661"" Ypos=""589"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""10"" trace=""Control Opening+"" Button=""Left"" Direction=""Up"" Xpos=""661"" Ypos=""589"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Control Opening (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Control Opening end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""561"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""633"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""633"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""262"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1319"" Ypos=""887"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""13"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1319"" Ypos=""887"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""375"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""850"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""nàone"" Button=""Left"" Direction=""Up"" Xpos=""850"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""375"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1058"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1058"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Measure comparaison: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""297"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""984"" Ypos=""613"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""29"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""984"" Ypos=""613"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""MB: Closure OK: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""249"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""776"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""27"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""776"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""225"" trace=""button2+Never"" Button=""Left"" Direction=""Down"" Xpos=""1155"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""22"" trace=""button2+Never"" Button=""Left"" Direction=""Up"" Xpos=""1155"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""MB: Do you want to start the station setup compensation?: Never"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""417"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""20"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""417"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""282"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""594"" Ypos=""381"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""29"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""594"" Ypos=""381"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""221"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""796"" Ypos=""496"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""12"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""796"" Ypos=""496"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""393"" trace=""button2+Compute in a new frame"" Button=""Left"" Direction=""Down"" Xpos=""1137"" Ypos=""488"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""29"" trace=""button2+Compute in a new frame"" Button=""Left"" Direction=""Up"" Xpos=""1137"" Ypos=""488"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""MB: Choose a type of setup: Compute in a new frame"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""258"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""976"" Ypos=""682"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""29"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""976"" Ypos=""682"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""264"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""898"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""21"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""898"" Ypos=""750"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""2000"" Action=""BB: Computing (LGC2) end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules.Count > 0, "No measurement modules found!");
                Assert.IsTrue(tsunami.MeasurementModules[0]?.StationModules.Count > 0, $"No station module in measurement modules {tsunami.MeasurementModules} found!");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;

                Assert.AreEqual(1.609, station.PointsMeasured[4]._Coordinates.Su.X.Value, 0.001);
                Assert.AreEqual(0, station.Parameters2.Setups.BestValues.SigmaZero, 0.001);
                Assert.AreEqual(true, station.Parameters2.Setups.BestValues.Verified);

                var points = tsunami.Points;
                var hiddens = tsunami.HiddenPoints;
                var temps = tsunami.TempPoints;

                var count = points.Count;
                Assert.AreEqual(6,count, $"5 + station shoudl now exist, got {count}");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        /// <summary>
        /// new point, nf , new frame, code
        /// </summary>
        [TestMethod()]
        public void L2_P_NF_Step4_Cancel_compensation()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_NF_Step3_Compute_With_LGC_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
       <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""282"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""718"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""58"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""718"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""243"" trace=""Cancel Setup+"" Button=""Left"" Direction=""Down"" Xpos=""826"" Ypos=""420"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""44"" trace=""Cancel Setup+"" Button=""Left"" Direction=""Up"" Xpos=""826"" Ypos=""420"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Cancel Setup (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Cancel Setup end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""247"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""776"" Ypos=""379"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""44"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""776"" Ypos=""379"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""209"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""927"" Ypos=""488"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""26"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""927"" Ypos=""488"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""276"" trace=""button2+Compute in a new frame"" Button=""Left"" Direction=""Down"" Xpos=""1056"" Ypos=""575"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""35"" trace=""button2+Compute in a new frame"" Button=""Left"" Direction=""Up"" Xpos=""1056"" Ypos=""575"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""MB: Choose a type of setup: Compute in a new frame"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""1854"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""986"" Ypos=""700"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""13"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""986"" Ypos=""700"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""273"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""888"" Ypos=""737"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""30"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""888"" Ypos=""737"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules.Count > 0, "No measurement modules found!");
                Assert.IsTrue(tsunami.MeasurementModules[0]?.StationModules.Count > 0, $"No station module in measurement modules {tsunami.MeasurementModules} found!");
                var station = tsunami.MeasurementModules[0]?.StationModules[0]?._Station as TSU.Polar.Station;

                var measureTaken = station.MeasuresTaken;

                var points = tsunami.Points;
                Assert.AreEqual(6, points.Count, "wrong count of points kept");

                var hiddens = tsunami.HiddenPoints;
                var temps = tsunami.TempPoints;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        /// <summary>
        /// new point, nf , new frame, code
        /// </summary>
        [TestMethod()]
        public void L2_P_NF_Step5_New_points()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_P_NF_Step4_Cancel_compensation) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""0.5"" />
<KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""261"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""34"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""264"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""202"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""425"" Ypos=""294"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""425"" Ypos=""294"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""241"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""542"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""25"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""542"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""212"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""683"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""54"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""683"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""299"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""465"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""49"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""465"" Ypos=""359"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""263"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""700"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""15"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""700"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""200"" trace=""Change Socket Code+"" Button=""Left"" Direction=""Down"" Xpos=""797"" Ypos=""431"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""43"" trace=""Change Socket Code+"" Button=""Left"" Direction=""Up"" Xpos=""797"" Ypos=""431"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: Change Socket Code (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""1222"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1614"" Ypos=""251"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""61"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1614"" Ypos=""251"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""222"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""594"" Ypos=""503"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""591"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""611"" Ypos=""481"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""248"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""908"" Ypos=""837"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""52"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""908"" Ypos=""837"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""MB: Select Socket Code: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Change Socket Code end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""245"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""657"" Ypos=""685"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""39"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""657"" Ypos=""685"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""218"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1210"" Ypos=""873"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""57"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1210"" Ypos=""873"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: Valid end"" />




    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""229"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""573"" Ypos=""335"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""573"" Ypos=""335"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""269"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""710"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""79"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""710"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""36"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""233"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""459"" Ypos=""357"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""111"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""459"" Ypos=""357"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""231"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""581"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""93"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""581"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""273"" trace=""Change Extension+"" Button=""Left"" Direction=""Down"" Xpos=""694"" Ypos=""389"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""108"" trace=""Change Extension+"" Button=""Left"" Direction=""Up"" Xpos=""694"" Ypos=""389"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Change Extension (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""221"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""542"" Ypos=""538"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""544"" Ypos=""538"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""238"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1570"" Ypos=""528"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""63"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1570"" Ypos=""528"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""273"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""598"" Ypos=""584"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""54"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""598"" Ypos=""584"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""286"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""911"" Ypos=""592"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""37"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""911"" Ypos=""592"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""53"" Delay=""0"" Action=""MB: Extension: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""54"" Delay=""0"" Action=""BB: Change Extension end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""238"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""700"" Ypos=""395"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""45"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""700"" Ypos=""395"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""2229"" trace=""Change Socket Code+"" Button=""Left"" Direction=""Down"" Xpos=""796"" Ypos=""417"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""110"" trace=""Change Socket Code+"" Button=""Left"" Direction=""Up"" Xpos=""796"" Ypos=""417"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Change Socket Code (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""215"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1263"" Ypos=""255"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""69"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1263"" Ypos=""255"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""62"" Delay=""235"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""63"" Delay=""215"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""64"" Delay=""247"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""ComboBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""254"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""905"" Ypos=""843"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""78"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""905"" Ypos=""843"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""67"" Delay=""0"" Action=""MB: Select Socket Code: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""233"" trace=""button2+No"" Button=""Left"" Direction=""Down"" Xpos=""1001"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""68"" trace=""button2+No"" Button=""Left"" Direction=""Up"" Xpos=""1001"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""70"" Delay=""0"" Action=""MB: Update Extension?: No"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""71"" Delay=""0"" Action=""BB: Change Socket Code end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""250"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""533"" Ypos=""677"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""36"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""533"" Ypos=""677"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""235"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1251"" Ypos=""858"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""182"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1251"" Ypos=""858"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""76"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""77"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""279"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""545"" Ypos=""332"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""545"" Ypos=""332"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""224"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""673"" Ypos=""440"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""11"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""673"" Ypos=""440"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""82"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""83"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""227"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""456"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""456"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""282"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""602"" Ypos=""366"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""87"" Delay=""72"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""603"" Ypos=""366"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""88"" Delay=""290"" trace=""Change Extension+"" Button=""Left"" Direction=""Down"" Xpos=""695"" Ypos=""391"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""89"" Delay=""107"" trace=""Change Extension+"" Button=""Left"" Direction=""Up"" Xpos=""695"" Ypos=""391"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""90"" Delay=""0"" Action=""BB: Change Extension (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""91"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1569"" Ypos=""522"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""88"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1569"" Ypos=""522"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""93"" Delay=""243"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""674"" Ypos=""579"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""94"" Delay=""45"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""674"" Ypos=""579"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""240"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""847"" Ypos=""575"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""96"" Delay=""11"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""847"" Ypos=""575"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""97"" Delay=""0"" Action=""MB: Extension: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""98"" Delay=""0"" Action=""BB: Change Extension end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""227"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""835"" Ypos=""398"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""835"" Ypos=""398"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""101"" Delay=""221"" trace=""Change Socket Code+"" Button=""Left"" Direction=""Down"" Xpos=""967"" Ypos=""414"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""102"" Delay=""14"" trace=""Change Socket Code+"" Button=""Left"" Direction=""Up"" Xpos=""967"" Ypos=""414"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""103"" Delay=""0"" Action=""BB: Change Socket Code (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""268"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1609"" Ypos=""248"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""58"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1609"" Ypos=""248"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""281"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""988"" Ypos=""377"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""107"" Delay=""163"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""988"" Ypos=""377"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""108"" Delay=""285"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""900"" Ypos=""830"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""116"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""899"" Ypos=""830"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""110"" Delay=""0"" Action=""MB: Select Socket Code: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""111"" Delay=""287"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""870"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""112"" Delay=""65"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""870"" Ypos=""570"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""113"" Delay=""0"" Action=""MB: Update Extension?: Yes"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""114"" Delay=""0"" Action=""BB: Change Socket Code end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""115"" Delay=""273"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""669"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""116"" Delay=""36"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""669"" Ypos=""679"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""117"" Delay=""258"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1237"" Ypos=""870"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""118"" Delay=""34"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1237"" Ypos=""870"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""119"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""120"" Delay=""0"" Action=""BB: Valid end"" />




    <IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""236"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""537"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""122"" Delay=""42"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""537"" Ypos=""326"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""123"" Delay=""288"" trace=""Add New point+"" Button=""Left"" Direction=""Down"" Xpos=""670"" Ypos=""430"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""57"" trace=""Add New point+"" Button=""Left"" Direction=""Up"" Xpos=""670"" Ypos=""430"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""125"" Delay=""0"" Action=""BB: Add New point (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""126"" Delay=""0"" Action=""BB: Add New point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""127"" Delay=""268"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""624"" Ypos=""363"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""128"" Delay=""38"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""624"" Ypos=""363"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""129"" Delay=""276"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""457"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""130"" Delay=""84"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""458"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""131"" Delay=""276"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""609"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""132"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""609"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""133"" Delay=""260"" trace=""Change Point name+"" Button=""Left"" Direction=""Down"" Xpos=""726"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""134"" Delay=""59"" trace=""Change Point name+"" Button=""Left"" Direction=""Up"" Xpos=""726"" Ypos=""329"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""135"" Delay=""0"" Action=""BB: Change Point name (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""136"" Delay=""279"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""863"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""137"" Delay=""51"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""756"" Ypos=""553"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""138"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""138"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""138"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""138"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{S}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""139"" Delay=""95"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""140"" Delay=""81"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{L}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""141"" Delay=""277"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""907"" Ypos=""654"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""142"" Delay=""11"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""907"" Ypos=""654"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""143"" Delay=""0"" Action=""MB: Point name: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""144"" Delay=""278"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""870"" Ypos=""846"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""145"" Delay=""68"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""870"" Ypos=""846"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""146"" Delay=""0"" Action=""MB: Special Code proposed: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""147"" Delay=""0"" Action=""BB: Change Point name end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""148"" Delay=""251"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""685"" Ypos=""666"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""149"" Delay=""69"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""685"" Ypos=""666"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""150"" Delay=""207"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1233"" Ypos=""872"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""151"" Delay=""12"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1233"" Ypos=""872"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""152"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""153"" Delay=""0"" Action=""BB: Valid end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules.Count > 0, "No measurement modules found!");

                var polarModule = tsunami.MeasurementModules[0];
                Assert.IsNotNull(polarModule, "no polar module found");

                Assert.IsTrue(polarModule.StationModules.Count > 0, $"No station module in measurement modules {tsunami.MeasurementModules} found!");
                var station = polarModule.StationModules[0]?._Station as TSU.Polar.Station;

                var measureTaken = station.MeasuresTaken;
                Assert.IsTrue(measureTaken.Count > 9, $"{measureTaken.Count} and not more than 9 measuretaken!");

                var PointsMeasured = station.PointsMeasured;
                Assert.IsTrue(PointsMeasured.Count > 8, $"{PointsMeasured.Count} and not more than 8 new PointsMeasured!");

                Assert.AreEqual(-0.2546, station.PointsMeasured[6]._Coordinates.Su.X.Value, 0.001, "Wrng x for point 6");

                Assert.AreEqual("9", station.PointsMeasured[5].SocketCode.Id, "wrong code for p6" );
                Assert.AreEqual(0.0, measureTaken[6].Extension.Value, 0.001, "wrong ext for p6");

                Assert.AreEqual("26", station.PointsMeasured[6].SocketCode.Id, "wrong code for p7");
                Assert.AreEqual(0.07, measureTaken[7].Extension.Value, 0.001, "wrong ext for p7");

                Assert.AreEqual("4", station.PointsMeasured[7].SocketCode.Id, "wrong code for p8");
                Assert.AreEqual(0.06455, measureTaken[8].Extension.Value, 0.001, "wrong ext for p8");

                Assert.IsTrue( station.PointsMeasured[8]._Name.Contains("STL"), "wrong name for p9");
                Assert.AreEqual("29", station.PointsMeasured[8].SocketCode.Id, "wrong code for p9");
                Assert.AreEqual(0.0, measureTaken[9].Extension.Value, 0.001, "wrong ext for p9");


                var points = tsunami.Points;
                Assert.IsTrue(points.Count== 10, $"we would like 10 points to be saved");
                var hiddens = tsunami.HiddenPoints;
                var temps = tsunami.TempPoints;

                var inp = $"L2_P_NF_Step5_New_points.inp";
                TSU.IO.SUSoft.Lgc2.ToLgc2(polarModule, inp);
                Assert.IsTrue(File.Exists(inp), $"lgc2 export '{inp}' not found");
                var lgc2 = TSU.Common.Compute.Compensations.Lgc2.RunInput(inp);
                Assert.IsTrue(File.Exists(lgc2._Output.OutputFilePath), $"lgc2 export '{inp}' not found");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}
