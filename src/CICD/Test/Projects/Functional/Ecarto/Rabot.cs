﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Functional_Tests;
using TSU.Common.Measures;
using System.Text.RegularExpressions;
using System.Linq;
using TSU.Views;
using System.Windows.Shapes;

namespace Functional_Tests.Ecarto
{
    [TestClass]
    public class RS2K
    {
        //L3 because we need  afile created by L2_R_GEM_Step1_open_theo_and_csv_files_and_setup
        [TestMethod()]
        public void L3_ECARTO_RS2K_Step1_Measure()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""348"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""237"" Ypos=""354"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""237"" Ypos=""354"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""354"" trace=""Offset Module+"" Button=""Left"" Direction=""Down"" Xpos=""377"" Ypos=""611"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""7"" trace=""Offset Module+"" Button=""Left"" Direction=""Up"" Xpos=""377"" Ypos=""611"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Offset Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Offset Module end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""330"" trace=""Offset Module (Created on: )+"" Button=""Left"" Direction=""Down"" Xpos=""657"" Ypos=""129"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""8"" trace=""Offset Module (Created on: )+"" Button=""Left"" Direction=""Up"" Xpos=""657"" Ypos=""129"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Offset Module (Created on: 17/11/2024 14:13:47) (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Offset Module (Created on: 17/11/2024 14:13:47) end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""341"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""762"" Ypos=""158"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""2"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""762"" Ypos=""158"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: New Station (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: New Station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""321"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""615"" Ypos=""427"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""6"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""615"" Ypos=""427"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""337"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""111"" Ypos=""340"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""111"" Ypos=""340"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""330"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""210"" Ypos=""364"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""210"" Ypos=""364"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""834"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""882"" Ypos=""922"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""13"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""882"" Ypos=""922"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""MB: Select an ecartometry device: Select"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""381"" trace=""A--B+"" Button=""Left"" Direction=""Down"" Xpos=""812"" Ypos=""205"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""9"" trace=""A--B+"" Button=""Left"" Direction=""Up"" Xpos=""812"" Ypos=""204"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: A--B (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: A--B end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""341"" trace=""Select elements+"" Button=""Left"" Direction=""Down"" Xpos=""973"" Ypos=""226"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""9"" trace=""Select elements+"" Button=""Left"" Direction=""Up"" Xpos=""973"" Ypos=""226"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""BB: Select elements (beginning)"" />


    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{A}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{W}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{A}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{K}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{E}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{.}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{D}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{A}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{T}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""275"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""Button"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""353"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""242"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""242"" Ypos=""355"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""353"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""242"" Ypos=""415"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""242"" Ypos=""415"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""353"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""242"" Ypos=""475"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""242"" Ypos=""475"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""310"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""832"" Ypos=""920"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""8"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""832"" Ypos=""920"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""46"" Delay=""0"" Action=""BB: Select elements end"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""47"" Delay=""359"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""333"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""894"" Ypos=""621"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""894"" Ypos=""621"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""385"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""887"" Ypos=""636"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""887"" Ypos=""636"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""52"" Delay=""354"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""393"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""898"" Ypos=""612"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""15"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""898"" Ypos=""612"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""379"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""897"" Ypos=""607"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""897"" Ypos=""607"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""57"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""58"" Delay=""89"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""59"" Delay=""395"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                //test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Line.Station.Module;
                var avgstations = stm?.AverageStations;
                Assert.IsTrue(avgstations.Count > 0, $"no averagestation");
                var station = stm?.AverageStations[0];
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken.FindAll(x=>x.IsObsolete == false);
                Assert.IsTrue(taken.Count == 3, $"should be 3 measures taken {taken.Count}");
                var mes500S = (taken.Find(x => x._PointName.Contains("500.S")) as MeasureOfOffset);
                Assert.IsTrue(mes500S != null, $"measure506.E is null");
                var mes506s = (taken.Find(x => x._PointName.Contains("506.S")) as MeasureOfOffset);
                Assert.IsTrue(mes506s != null, $"measure506.S is null");
                var mes508S = (taken.Find(x => x._PointName.Contains("508.S")) as MeasureOfOffset);
                Assert.IsTrue(mes508S != null, $"measure508.E is null");

                Assert.IsTrue(mes500S._CorrectedReading == 0, $"wrong level reading value {mes500S._CorrectedReading}");
                Assert.IsTrue(mes506s._CorrectedReading == 0.01, $"wrong level reading value");
                Assert.IsTrue(mes508S._CorrectedReading == 0, $"wrong level reading value {mes508S._CorrectedReading}");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 15, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 4, "wrong rows count in dgv");


                int column = 3;
                var row = dgv.Rows[2];
                Assert.AreEqual("598.283", row.Cells[column].Value, "wrong value in dgv for dcum for .S"); column++;
                Assert.AreEqual("10.00", row.Cells[column].Value, "wrong value in dgv for raw for .S"); column++;
                Assert.AreEqual("10.00", row.Cells[column].Value, "wrong value in dgv for raw for .S"); column++;
                Assert.AreEqual("-3.10", row.Cells[column].Value, "wrong value in dgv for theo for .S"); column++;
                Assert.AreEqual("13.10", row.Cells[column].Value, "wrong value in dgv for offset for .S"); column++;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                var geodeExport = station.ParametersBasic._GeodeDatFilePath;
                if (File.Exists(geodeExport))
                    File.Delete(geodeExport);

            });
        }

        [TestMethod()]
        public void L3_ECARTO_RS2K_Step2_ExpectedOffset()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_ECARTO_RS2K_Step1_Measure) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""389"" trace=""TT41.QTGD.411500.E=&gt;TT41.MDGV.411506.E+"" Button=""Left"" Direction=""Down"" Xpos=""785"" Ypos=""200"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""9"" trace=""TT41.QTGD.411500.E=&gt;TT41.MDGV.411506.E+"" Button=""Left"" Direction=""Up"" Xpos=""785"" Ypos=""200"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""62"" Delay=""0"" Action=""BB: TT41.QTGD.411500.E=&gt;TT41.MDGV.411506.E (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""63"" Delay=""0"" Action=""BB: TT41.QTGD.411500.E=&gt;TT41.MDGV.411506.E end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""322"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""975"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""13"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""975"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""66"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{r}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{a}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{d}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{.}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{c}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{s}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{v}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""Button"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""341"" trace=""button1+Expected Offset"" Button=""Left"" Direction=""Down"" Xpos=""878"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""12"" trace=""button1+Expected Offset"" Button=""Left"" Direction=""Up"" Xpos=""878"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""MB: Please choose type of Rabot offset displacement: Expected Offset"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Import offsets/displacements end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""312"" trace=""AM_Levelling Module+"" Button=""Left"" Direction=""Down"" Xpos=""281"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""AM_Levelling Module+"" Button=""Left"" Direction=""Up"" Xpos=""281"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: AM_Levelling Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: AM_Levelling Module end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Line.Station.Module;
                var avgstations = stm?.AverageStations;
                Assert.IsTrue(avgstations.Count > 0, $"no averagestation");
                var station = stm?.AverageStations[0];
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken.FindAll(x => x.IsObsolete == false);
                Assert.IsTrue(taken.Count == 3, $"should be 3 measures taken {taken.Count}");
                var mes500S = (taken.Find(x => x._PointName.Contains("500.S")) as MeasureOfOffset);
                Assert.IsTrue(mes500S != null, $"measure506.E is null");
                var mes506s = (taken.Find(x => x._PointName.Contains("506.S")) as MeasureOfOffset);
                Assert.IsTrue(mes506s != null, $"measure506.S is null");
                var mes508S = (taken.Find(x => x._PointName.Contains("508.S")) as MeasureOfOffset);
                Assert.IsTrue(mes508S != null, $"measure508.E is null");

                Assert.IsTrue(mes500S._CorrectedReading == 0, $"wrong level reading value {mes500S._CorrectedReading}");
                Assert.IsTrue(mes506s._CorrectedReading == 0.01, $"wrong level reading value");
                Assert.IsTrue(mes508S._CorrectedReading == 0, $"wrong level reading value {mes508S._CorrectedReading}");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 15, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 4, "wrong rows count in dgv");

                //test.Wait();
                int column = 3;
                var row = dgv.Rows[2];
                Assert.AreEqual("598.283", row.Cells[column].Value, "wrong value in dgv for dcum for .S"); column++;
                Assert.AreEqual("10.00", row.Cells[column].Value, "wrong value in dgv for raw for .S"); column++;
                Assert.AreEqual("10.00", row.Cells[column].Value, "wrong value in dgv for raw for .S"); column++;
                Assert.AreEqual("-3.10", row.Cells[column].Value, "wrong value in dgv for theo for .S"); column++;
                Assert.AreEqual("13.10", row.Cells[column].Value, "wrong value in dgv for offset for .S"); column++;
                Assert.AreEqual("-2.47", row.Cells[column].Value, "wrong value in dgv for expexted for .S"); column++;
                Assert.AreEqual("-15.56", row.Cells[column].Value, "wrong value in dgv for move for .S"); column++;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

            });
        }

        [TestMethod()]
        public void L3_ECARTO_RS2K_Step2_Relative_displacement()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_ECARTO_RS2K_Step1_Measure) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""365"" trace=""TT41.QTGD.411500.E=&gt;TT41.MDGV.411506.E+"" Button=""Left"" Direction=""Down"" Xpos=""785"" Ypos=""200"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""9"" trace=""TT41.QTGD.411500.E=&gt;TT41.MDGV.411506.E+"" Button=""Left"" Direction=""Up"" Xpos=""785"" Ypos=""200"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""62"" Delay=""0"" Action=""BB: TT41.QTGD.411500.E=&gt;TT41.MDGV.411506.E (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""63"" Delay=""0"" Action=""BB: TT41.QTGD.411500.E=&gt;TT41.MDGV.411506.E end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""322"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""975"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""13"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""975"" Ypos=""315"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""66"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{r}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{a}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{d}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{.}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{c}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{s}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{v}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""Button"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""392"" trace=""button2+Relative Displacement"" Button=""Left"" Direction=""Down"" Xpos=""1047"" Ypos=""550"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""9"" trace=""button2+Relative Displacement"" Button=""Left"" Direction=""Up"" Xpos=""1047"" Ypos=""550"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""MB: Please choose type of Rabot offset displacement: Relative Displacement"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Import offsets/displacements end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""312"" trace=""AM_Levelling Module+"" Button=""Left"" Direction=""Down"" Xpos=""281"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""AM_Levelling Module+"" Button=""Left"" Direction=""Up"" Xpos=""281"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: AM_Levelling Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: AM_Levelling Module end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Line.Station.Module;
                var avgstations = stm?.AverageStations;
                Assert.IsTrue(avgstations.Count > 0, $"no averagestation");
                var station = stm?.AverageStations[0];
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken.FindAll(x => x.IsObsolete == false);
                Assert.IsTrue(taken.Count == 3, $"should be 3 measures taken {taken.Count}");
                var mes500S = (taken.Find(x => x._PointName.Contains("500.S")) as MeasureOfOffset);
                Assert.IsTrue(mes500S != null, $"measure506.E is null");
                var mes506s = (taken.Find(x => x._PointName.Contains("506.S")) as MeasureOfOffset);
                Assert.IsTrue(mes506s != null, $"measure506.S is null");
                var mes508S = (taken.Find(x => x._PointName.Contains("508.S")) as MeasureOfOffset);
                Assert.IsTrue(mes508S != null, $"measure508.E is null");

                Assert.IsTrue(mes500S._CorrectedReading == 0, $"wrong level reading value {mes500S._CorrectedReading}");
                Assert.IsTrue(mes506s._CorrectedReading == 0.01, $"wrong level reading value");
                Assert.IsTrue(mes508S._CorrectedReading == 0, $"wrong level reading value {mes508S._CorrectedReading}");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 15, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 4, "wrong rows count in dgv");

                //test.Wait();
                int column = 3;
                var row = dgv.Rows[2];
                Assert.AreEqual("598.283", row.Cells[column].Value, "wrong value in dgv for dcum for .S"); column++;
                Assert.AreEqual("10.00", row.Cells[column].Value, "wrong value in dgv for raw for .S"); column++;
                Assert.AreEqual("10.00", row.Cells[column].Value, "wrong value in dgv for raw for .S"); column++;
                Assert.AreEqual("-3.10", row.Cells[column].Value, "wrong value in dgv for theo for .S"); column++;
                Assert.AreEqual("13.10", row.Cells[column].Value, "wrong value in dgv for offset for .S"); column++;
                Assert.AreEqual("12.63", row.Cells[column].Value, "wrong value in dgv for expexted for .S"); column++;
                Assert.AreEqual("-0.47", row.Cells[column].Value, "wrong value in dgv for move for .S"); column++;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

            });
        }

        [TestMethod()]
        public void L3_ECARTO_RS2K_Step3_Relative_displacement_second_meas()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_ECARTO_RS2K_Step2_Relative_displacement) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""0.5"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""77"" Delay=""350"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""890"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""16"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""890"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""318"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""868"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""16"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""868"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""81"" Delay=""85"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""82"" Delay=""98"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""83"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Line.Station.Module;
                var avgstations = stm?.AverageStations;
                Assert.IsTrue(avgstations.Count > 0, $"no averagestation");
                var station = stm?.AverageStations[0];
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken.FindAll(x => x.IsObsolete == false);
                Assert.IsTrue(taken.Count == 3, $"should be 3 measures taken {taken.Count}");
                var mes500S = (taken.Find(x => x._PointName.Contains("500.S")) as MeasureOfOffset);
                Assert.IsTrue(mes500S != null, $"measure506.E is null");
                var mes506s = (taken.Find(x => x._PointName.Contains("506.S")) as MeasureOfOffset);
                Assert.IsTrue(mes506s != null, $"measure506.S is null");
                var mes508S = (taken.Find(x => x._PointName.Contains("508.S")) as MeasureOfOffset);
                Assert.IsTrue(mes508S != null, $"measure508.E is null");

                Assert.IsTrue(mes500S._CorrectedReading == 0, $"wrong level reading value {mes500S._CorrectedReading}");
                Assert.IsTrue(mes506s._CorrectedReading == 0.01, $"wrong level reading value");
                Assert.IsTrue(mes508S._CorrectedReading == 0, $"wrong level reading value {mes508S._CorrectedReading}");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 15, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 4, "wrong rows count in dgv");

                //test.Wait();
                int column = 3;
                var row = dgv.Rows[2];
                Assert.AreEqual("598.283", row.Cells[column].Value, "wrong value in dgv for dcum for .S"); column++;
                Assert.AreEqual("20.00", row.Cells[column].Value, "wrong value in dgv for raw for .S"); column++;
                Assert.AreEqual("20.00", row.Cells[column].Value, "wrong value in dgv for raw for .S"); column++;
                Assert.AreEqual("-3.10", row.Cells[column].Value, "wrong value in dgv for theo for .S"); column++;
                Assert.AreEqual("23.10", row.Cells[column].Value, "wrong value in dgv for offset for .S"); column++;
                Assert.AreEqual("12.63", row.Cells[column].Value, "wrong value in dgv for expexted for .S"); column++;
                Assert.AreEqual("-10.47", row.Cells[column].Value, "wrong value in dgv for move for .S"); column++;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

            });
        }
    }
}