﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Functional_Tests;
using TSU.Common.Measures;
using System.Text.RegularExpressions;
using System.Linq;
using TSU.Views;

namespace Functional_Tests.Leveling
{
    [TestClass]
    public class RS2K
    {

        [TestMethod()]
        public void L3_L_RS2K_Step1_Measure()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(OLOC.L2_L_OLOC1_Select_instruments) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
      <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""376"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""771"" Ypos=""184"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""771"" Ypos=""184"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Outward run station  (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Outward run station  end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""300"" trace=""Select element+"" Button=""Left"" Direction=""Down"" Xpos=""948"" Ypos=""230"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""154"" trace=""Select element+"" Button=""Left"" Direction=""Up"" Xpos=""948"" Ypos=""230"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Select element (beginning)"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{A}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{W}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{A}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{K}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{E}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{.}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{D}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{A}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{T}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""275"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""Button"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""361"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""251"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""251"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""313"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""246"" Ypos=""360"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""246"" Ypos=""360"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""341"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""874"" Ypos=""933"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""82"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""874"" Ypos=""933"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Select element end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""374"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""649"" Ypos=""605"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""649"" Ypos=""605"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""305"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1144"" Ypos=""604"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1144"" Ypos=""604"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""382"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""301"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1165"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1165"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{-}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""306"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                //test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                //test.Wait();

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Level.Station.Module;
                var station = stm?._Station as TSU.Level.Station;
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken;
                Assert.IsTrue(taken.Count == 2, $"should be 2 measures taken {taken.Count}");
                var mesE = (taken.Find(x=>x._PointName.Contains(".E")) as MeasureOfLevel);
                Assert.IsTrue(mesE != null, $"measure 1 is null");
                var mesS = (taken.Find(x => x._PointName.Contains(".S")) as MeasureOfLevel);
                Assert.IsTrue(mesS != null, $"measure 2 is null");

                Assert.IsTrue(mesE._RawLevelReading == -0.08, $"wrong level reading value {mesE._RawLevelReading}");
                Assert.IsTrue(mesS._RawLevelReading == 0, $"wrong level reading value");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 22, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");


                int column = 10;
                var row = dgv.Rows[0];
                Assert.AreEqual("-8000", row.Cells[column].Value, "wrong value in dgv for rawmeas for .E"); column++;
                Assert.AreEqual("-8134", row.Cells[column].Value, "wrong value in dgv for theo for .E"); column++;
                Assert.AreEqual("-134", row.Cells[column].Value, "wrong value in dgv for offset for .E"); column++;
               
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                var geodeExport = station.ParametersBasic._GeodeDatFilePath;
                if (File.Exists(geodeExport))
                    File.Delete(geodeExport);

            });
        }

        [TestMethod()]
        public void L3_L_RS2K_Step2_ExpectedOffset()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_L_RS2K_Step1_Measure) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""365"" trace=""Levelling station+"" Button=""Left"" Direction=""Down"" Xpos=""868"" Ypos=""217"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""89"" trace=""Levelling station+"" Button=""Left"" Direction=""Up"" Xpos=""868"" Ypos=""217"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""BB: Outward run station  (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Outward run station  end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""318"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""965"" Ypos=""316"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""90"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""965"" Ypos=""316"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""36"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""318"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{v}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{e}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{r}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{t}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{i}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{c}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{a}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{l}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{.}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{c}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{s}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{v}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""Button"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""341"" trace=""button1+Expected Offset"" Button=""Left"" Direction=""Down"" Xpos=""878"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""122"" trace=""button1+Expected Offset"" Button=""Left"" Direction=""Up"" Xpos=""878"" Ypos=""554"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""MB: Please choose type of Rabot offset displacement: Expected Offset"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Import offsets/displacements end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""312"" trace=""AM_Levelling Module+"" Button=""Left"" Direction=""Down"" Xpos=""281"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""96"" trace=""AM_Levelling Module+"" Button=""Left"" Direction=""Up"" Xpos=""281"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: AM_Levelling Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: AM_Levelling Module end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                //test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                //test.Wait();

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Level.Station.Module;
                var station = stm?._Station as TSU.Level.Station;
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken;
                Assert.IsTrue(taken.Count == 2, $"should be 2 measures taken {taken.Count}");
                var mesE = (taken.Find(x => x._PointName.Contains(".E")) as MeasureOfLevel);
                Assert.IsTrue(mesE != null, $"measure 1 is null");
                var mesS = (taken.Find(x => x._PointName.Contains(".S")) as MeasureOfLevel);
                Assert.IsTrue(mesS != null, $"measure 2 is null");

                Assert.IsTrue(mesE._RawLevelReading == -0.08, $"wrong level reading value {mesE._RawLevelReading}");
                Assert.IsTrue(mesS._RawLevelReading == 0, $"wrong level reading value");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 22, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");


                int column = 10;
                var row = dgv.Rows[0];
                Assert.AreEqual("-8000", row.Cells[column].Value, "wrong value in dgv for rawmeas for .E"); column++;
                Assert.AreEqual("-8134", row.Cells[column].Value, "wrong value in dgv for theo for .E"); column++;
                Assert.AreEqual("-134", row.Cells[column].Value, "wrong value in dgv for offset for .E"); column++;
                // Smooth,  Rough,    Rough - Smoot
                // -2.855,  -2.94,    -0.085
                // Clean export for next test
                Assert.AreEqual("-286", row.Cells[column].Value, "wrong value in dgv for objective for .E"); column++;
                Assert.AreEqual("-151", row.Cells[column].Value, "wrong value in dgv for move for .E"); column++;
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                var geodeExport = station.ParametersBasic._GeodeDatFilePath;
                if (File.Exists(geodeExport))
                    File.Delete(geodeExport);

            });
        }

        [TestMethod()]
        public void L3_L_RS2K_Step2_Relative_displacement()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_L_RS2K_Step1_Measure) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""365"" trace=""Levelling station+"" Button=""Left"" Direction=""Down"" Xpos=""868"" Ypos=""217"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""89"" trace=""Levelling station+"" Button=""Left"" Direction=""Up"" Xpos=""868"" Ypos=""217"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""BB: Outward run station  (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Outward run station  end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""318"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""965"" Ypos=""316"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""90"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""965"" Ypos=""316"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""36"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""318"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""448"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{v}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{e}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{r}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{t}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{i}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{c}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{a}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{l}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{.}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{c}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{s}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{v}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""Button"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""392"" trace=""button2+Relative Displacement"" Button=""Left"" Direction=""Down"" Xpos=""1047"" Ypos=""550"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""90"" trace=""button2+Relative Displacement"" Button=""Left"" Direction=""Up"" Xpos=""1047"" Ypos=""550"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""MB: Please choose type of Rabot offset displacement: Relative Displacement"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Import offsets/displacements end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""312"" trace=""AM_Levelling Module+"" Button=""Left"" Direction=""Down"" Xpos=""281"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""96"" trace=""AM_Levelling Module+"" Button=""Left"" Direction=""Up"" Xpos=""281"" Ypos=""514"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: AM_Levelling Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: AM_Levelling Module end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                //test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                //test.Wait();

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Level.Station.Module;
                var station = stm?._Station as TSU.Level.Station;
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken;
                Assert.IsTrue(taken.Count == 2, $"should be 2 measures taken {taken.Count}");
                var mesE = (taken.Find(x => x._PointName.Contains(".E")) as MeasureOfLevel);
                Assert.IsTrue(mesE != null, $"measure 1 is null");
                var mesS = (taken.Find(x => x._PointName.Contains(".S")) as MeasureOfLevel);
                Assert.IsTrue(mesS != null, $"measure 2 is null");

                Assert.IsTrue(mesE._RawLevelReading == -0.08, $"wrong level reading value {mesE._RawLevelReading}");
                Assert.IsTrue(mesS._RawLevelReading == 0, $"wrong level reading value");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 22, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");


                int column = 10;
                var row = dgv.Rows[0];
                Assert.AreEqual("-8000", row.Cells[column].Value, "wrong value in dgv for rawmeas for .E"); column++;
                Assert.AreEqual("-8134", row.Cells[column].Value, "wrong value in dgv for theo for .E"); column++;
                Assert.AreEqual("-134", row.Cells[column].Value, "wrong value in dgv for offset for .E"); column++;
                // Smooth,  Rough,    Rough - Smoot
                // -2.855,  -2.94,    -0.085
                // Clean export for next test
                Assert.AreEqual("-126", row.Cells[column].Value, "wrong value in dgv for objective for .E"); column++;
                Assert.AreEqual("8", row.Cells[column].Value, "wrong value in dgv for move for .E"); column++;
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                var geodeExport = station.ParametersBasic._GeodeDatFilePath;
                if (File.Exists(geodeExport))
                    File.Delete(geodeExport);

            });
        }

        [TestMethod()]
        public void L3_L_RS2K_Step3_Relative_displacement_second_meas()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L3_L_RS2K_Step2_Relative_displacement) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""312"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1157"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1157"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""307"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{SUBTRACT}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""6"" Delay=""66"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""7"" Delay=""90"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{8}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                //test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
                //test.Wait();

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Level.Station.Module;
                var station = stm?._Station as TSU.Level.Station;
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken;
                Assert.IsTrue(taken.Count == 3, $"should be 2 measures taken {taken.Count}");
                var mesE = (taken.LastOrDefault(x => x._PointName.Contains(".E")) as MeasureOfLevel);
                Assert.IsTrue(mesE != null, $"measure 1 is null");
                var mesS = (taken.Find(x => x._PointName.Contains(".S")) as MeasureOfLevel);
                Assert.IsTrue(mesS != null, $"measure 2 is null");

                Assert.IsTrue(mesE._RawLevelReading == -0.08008, $"wrong level reading value {mesE._RawLevelReading}");
                Assert.IsTrue(mesS._RawLevelReading == 0, $"wrong level reading value");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 22, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 2, "wrong rows count in dgv");


                int column = 10;
                var row = dgv.Rows[0];
                Assert.AreEqual("-8008", row.Cells[column].Value, "wrong value in dgv for rawmeas for .E"); column++;
                Assert.AreEqual("-8134", row.Cells[column].Value, "wrong value in dgv for theo for .E"); column++;
                Assert.AreEqual("-126", row.Cells[column].Value, "wrong value in dgv for offset for .E"); column++;
                // Smooth,  Rough,    Rough - Smoot
                // -2.855,  -2.94,    -0.085
                // Clean export for next test
                Assert.AreEqual("-126", row.Cells[column].Value, "wrong value in dgv for objective for .E"); column++;
                Assert.AreEqual("0", row.Cells[column].Value, "wrong value in dgv for move for .E"); column++;
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                var geodeExport = station.ParametersBasic._GeodeDatFilePath;
                if (File.Exists(geodeExport))
                    File.Delete(geodeExport);

            });
        }
    }
}
