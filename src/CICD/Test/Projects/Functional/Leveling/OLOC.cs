﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Functional_Tests;
using TSU.Common.Measures;
using System.Text.RegularExpressions;
using System.Linq;
using TSU.Views;

namespace Functional_Tests.Leveling
{
    [TestClass]
    public class OLOC
    {
        [TestMethod()]
        public void L2_L_OLOC1_Select_instruments()
        {
            Tools.Artefact.GetArtefactFullPath("L0_START_STEP1_DetectWrongResolution_Test.tsut"); // used to fail if resolution is wrong
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""286"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""248"" Ypos=""342"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""248"" Ypos=""342"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""209"" trace=""Levelling Module+"" Button=""Left"" Direction=""Down"" Xpos=""393"" Ypos=""526"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""90"" trace=""Levelling Module+"" Button=""Left"" Direction=""Up"" Xpos=""393"" Ypos=""526"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Levelling Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Levelling Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""741"" Ypos=""338"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""741"" Ypos=""338"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""293"" trace=""Levelling Module (Created on: Replaced_Date_And_Time)+"" Button=""Left"" Direction=""Down"" Xpos=""651"" Ypos=""117"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""113"" trace=""Levelling Module (Created on: Replaced_Date_And_Time)+"" Button=""Left"" Direction=""Up"" Xpos=""651"" Ypos=""117"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Levelling Module (Created on: Replaced_Date_And_Time) (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Levelling Module (Created on: Replaced_Date_And_Time) end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""284"" trace=""New station+"" Button=""Left"" Direction=""Down"" Xpos=""788"" Ypos=""159"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""99"" trace=""New station+"" Button=""Left"" Direction=""Up"" Xpos=""788"" Ypos=""159"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: New station (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""351"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""607"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""607"" Ypos=""358"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""314"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1080"" Ypos=""522"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1080"" Ypos=""522"" Delta=""0"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""2"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""31"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{M}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""6"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{O}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""7"" Delay=""14"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""339"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""945"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""98"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""945"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""MB: Team: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""242"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""636"" Ypos=""414"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""636"" Ypos=""414"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""328"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""700"" Ypos=""422"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""6"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""700"" Ypos=""422"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""302"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1502"" Ypos=""215"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""10"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1502"" Ypos=""215"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Options end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""329"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1605"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""9"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1605"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: List View end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""344"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""225"" Ypos=""270"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""225"" Ypos=""270"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""14"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{N}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""1077"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""126"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""126"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""354"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""854"" Ypos=""935"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""9"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""854"" Ypos=""935"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""MB: Choose a level: Select"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""326"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""130"" Ypos=""269"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""130"" Ypos=""269"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""29"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{G}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""15"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""1049"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""100"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""100"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""326"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""965"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""9"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""965"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Choose a default main staff: Select"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""318"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""165"" Ypos=""269"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""165"" Ypos=""269"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""377"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""41"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""42"" Delay=""357"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""43"" Delay=""5"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""1025"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""81"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""81"" Ypos=""331"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""305"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""860"" Ypos=""923"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""8"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""860"" Ypos=""923"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""MB: Choose an optional secondary staff: Select"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""334"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""297"" Ypos=""300"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""297"" Ypos=""300"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""209"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""898"" Ypos=""918"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""9"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""898"" Ypos=""918"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""MB: Choose a level: Select"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var mms = tsunami.MeasurementModules;
                Assert.IsTrue(mms.Count > 0, "No modules in measurementModules");

                var sms = mms[0].StationModules;
                Assert.IsTrue(sms.Count > 0, $"No station modules in {mms}");

                var stm = sms[0] as TSU.Level.Station.Module;
                Assert.IsTrue(stm != null, "No station module levelling");

                var station = stm?._Station as TSU.Level.Station;
                Assert.IsNotNull(station, "Level station is null");

                Assert.IsTrue(station._Parameters._Team == "MOI", "Bad TEAM");
                Assert.IsTrue(station._Parameters._Instrument._Model == "NA2", "Bad instrument selected");
                Assert.IsTrue(stm._defaultStaff._Model == "G16", $"Bad default rod selected G16 <> {stm._defaultStaff._Model}");
                Assert.IsTrue(stm._secondaryStaff._Model == "G60", $"Bad secondary rod selected G56 <> {stm._secondaryStaff._Model}");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }



        [TestMethod()]
        public void L2_L_OLOC2_Create_2_new_points()
        {

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_L_OLOC1_Select_instruments) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""0.5"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""258"" trace=""Levelling station+"" Button=""Left"" Direction=""Down"" Xpos=""753"" Ypos=""210"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""80"" trace=""Levelling station+"" Button=""Left"" Direction=""Up"" Xpos=""753"" Ypos=""210"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Outward run station  (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Outward run station  end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""289"" trace=""Create new point+"" Button=""Left"" Direction=""Down"" Xpos=""867"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""82"" trace=""Create new point+"" Button=""Left"" Direction=""Up"" Xpos=""867"" Ypos=""396"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Create new point (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""783"" Ypos=""525"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""783"" Ypos=""525"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""274"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{R}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""290"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""225"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""97"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""TextBox"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""201"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1075"" Ypos=""525"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1075"" Ypos=""525"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{BACKSPACE}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""97"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{5}"" Target=""TextBox"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""241"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""857"" Ypos=""603"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""90"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""857"" Ypos=""603"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""MB: Point name?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""241"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""872"" Ypos=""591"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""82"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""872"" Ypos=""591"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""MB: Select Socket Code: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Create new point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""223"" trace=""Levelling station+"" Button=""Left"" Direction=""Down"" Xpos=""912"" Ypos=""192"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""98"" trace=""Levelling station+"" Button=""Left"" Direction=""Up"" Xpos=""912"" Ypos=""192"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Outward run station  (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Outward run station  end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""285"" trace=""Create new point+"" Button=""Left"" Direction=""Down"" Xpos=""1022"" Ypos=""372"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""81"" trace=""Create new point+"" Button=""Left"" Direction=""Up"" Xpos=""1022"" Ypos=""372"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Create new point (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""297"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""908"" Ypos=""616"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""82"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""908"" Ypos=""616"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""MB: Point name?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""433"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""886"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""439"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""886"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""MB: Select Socket Code: OK"" />

    <IInputActivity xsi:type=""Trace________"" Pos.=""36"" Delay=""0"" Action=""BB: Create new point end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""50"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1118"" Ypos=""572"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1118"" Ypos=""572"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{1}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""50"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1148"" Ypos=""600"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1148"" Ypos=""600"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""43"" Delay=""609"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""44"" Delay=""349"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""230"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""640"" Ypos=""568"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""640"" Ypos=""568"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""173"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""640"" Ypos=""568"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""640"" Ypos=""568"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1126"" Ypos=""103"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1126"" Ypos=""103"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""BB: Levelling Module (Created on: // ::): Level station 1 (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""52"" Delay=""0"" Action=""BB: Levelling Module (Created on: // ::): Level station 1 end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""221"" trace=""Export all levelling+"" Button=""Left"" Direction=""Down"" Xpos=""1254"" Ypos=""446"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""81"" trace=""Export all levelling+"" Button=""Left"" Direction=""Up"" Xpos=""1254"" Ypos=""446"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""55"" Delay=""0"" Action=""BB: Export all levelling (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""218"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""980"" Ypos=""556"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""89"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""980"" Ypos=""556"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""58"" Delay=""0"" Action=""MB: Outward station level exported: OK"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""233"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""877"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""81"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""877"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""61"" Delay=""0"" Action=""MB: Successfull export of all levelling: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""62"" Delay=""0"" Action=""BB: Export all levelling end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {

                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Level.Station.Module;
                var station = stm?._Station as TSU.Level.Station;
                Assert.IsNotNull(station, "station is null");

                var taken = station.MeasuresTaken;
                Assert.IsTrue(taken.Count == 2, $"should be 2 measures taken {taken.Count}");
                var mes1 = (taken[0] as MeasureOfLevel);
                Assert.IsTrue(mes1 != null, $"measure 1 is null");
                var mes2 = (taken[1] as MeasureOfLevel);
                Assert.IsTrue(mes2 != null, $"measure 2 is null");

                Assert.IsTrue(mes1._RawLevelReading == 0.00001, $"wrong level reading value {mes1._RawLevelReading}");
                Assert.IsTrue(mes2._RawLevelReading == 0.00002, $"wrong level reading value");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                // Clean export for next test
                var geodeExport = station.ParametersBasic._GeodeDatFilePath;
                if (File.Exists(geodeExport))
                    File.Delete(geodeExport);

            });
        }

        [TestMethod()]
        public void L2_L_OLOC3_CheckRawIsExporter()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_L_OLOC2_Create_2_new_points) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""214"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""554"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""554"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""75"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""73"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{DOWN}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""242"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""668"" Ypos=""388"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""668"" Ypos=""388"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""15"" Delay=""242"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""121"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""297"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""233"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""912"" Ypos=""575"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""122"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""912"" Ypos=""575"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""MB: Ambient Temperature: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""226"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1171"" Ypos=""610"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1171"" Ypos=""610"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{2}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{0}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""28"" Delay=""48"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{ENTER}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""233"" trace=""Levelling Module (Created on: ): Level station 1+"" Button=""Left"" Direction=""Down"" Xpos=""1059"" Ypos=""140"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""138"" trace=""Levelling Module (Created on: ): Level station 1+"" Button=""Left"" Direction=""Up"" Xpos=""1059"" Ypos=""140"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: Levelling Module (Created on: 29/07/2024 13:47:36): Level station 1 (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""32"" Delay=""0"" Action=""BB: Levelling Module (Created on: 29/07/2024 13:47:36): Level station 1 end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""293"" trace=""Export all levelling+"" Button=""Left"" Direction=""Down"" Xpos=""1191"" Ypos=""484"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""90"" trace=""Export all levelling+"" Button=""Left"" Direction=""Up"" Xpos=""1191"" Ypos=""484"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Export all levelling (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""262"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""910"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""97"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""910"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""MB: Successfull export of all levelling: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Export all levelling end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {

            Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
            var stm = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Level.Station.Module;
            var station = stm?._Station as TSU.Level.Station;
            Assert.IsNotNull(station, "station is null");

            var taken = station.MeasuresTaken;
            Assert.IsTrue(taken.Count == 3, $"should be 2 measures, taken: {taken.Count}");
            var mes1 = (taken[1] as MeasureOfLevel);
            Assert.IsTrue(mes1 != null, $"measure 1 is null");
            var mes2 = (taken[2] as MeasureOfLevel);
            Assert.IsTrue(mes2 != null, $"measure 2 is null");

            Assert.IsTrue(mes1._RawLevelReading == 0.00001, $"wrong level reading value {mes1._RawLevelReading}");
            Assert.IsTrue(mes2._RawLevelReading == 0.2, $"wrong level raw reading value");
            Assert.IsTrue(mes2._CorrLevelReadingForEtalonnage == 0.200054, $"wrong level corrected reading value");

            // Check export raw
            var geodeExport = station.ParametersBasic._GeodeDatFilePath;
            Assert.IsTrue(File.Exists(geodeExport), $"Geode export '{geodeExport}' do not exist");
            string patternOk = @";\s*CP\.6\.\s*;\s*20000\s*;"; // ;CP.24102402.                     ;   20000;
            string patternKo = @";\s*CP\.6\.\s*;\s*20005";     // ;CP.24102402.                     ;   20005
            string fileContent = File.ReadAllText(geodeExport);
            Assert.IsTrue(Regex.IsMatch(fileContent, patternOk, RegexOptions.IgnoreCase), $"Raw value {patternOk} not found in geode export: {fileContent}");
            Assert.IsFalse(Regex.IsMatch(fileContent, patternKo, RegexOptions.IgnoreCase), $"Corrected value {patternOk} found in geode export, (should not be): {fileContent}");

            test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
        });
        }

     
    }
}
