﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Input;
using TSU;
using TSU.Common;
using TSU.Level.Smart;
using CI = TSU.Common.Instruments;
using LS = TSU.Level.Smart;

namespace Functional_Tests.SmartNami
{
    [TestClass]
    public class LevellingViewModelTest
    {
        private void CreateGeodeInputDatFile(string name)
        {
            string xmlContent = @"LHC       ;MQXA.3R5.P                      ;13381.67364; -999.17770;10486.25593;2413.69048;419.89745;99.999;  .000041; -.012428; 74.7338; 1;O;19-SEP-2024; HCLQXC_001-FL000007;;;
LHC       ;MBXW.A4R5.E                     ;13389.67166; -992.02178;10489.87030;2413.54398;419.75254;  .382; -.007927; -.012428; 74.7278; 1;A;19-SEP-2024;                    ;;Paramètres recalculés 14-SEPT-2016- Socket Circulant - ;
LHC       ;MBXW.A4R5.S                     ;13391.55166; -990.28804;10490.59694;2413.52061;419.72936; 3.400; -.007927; -.012428; 74.7278; 1;O;19-SEP-2024;                    ;;Paramètres recalculés 14-SEPT-2016- Socket Circulant - ;
LHC       ;MBXW.B4R5.S                     ;13395.81766; -986.35394;10492.24581;2413.46760;419.67678; 3.400; -.007927; -.012426; 74.7158; 1;O;19-SEP-2024;                    ;;Paramètres recalculés 14-SEPT-2016- Socket Circulant - ;
LHC       ;GGPSO.4R505.                    ;13397.17500; -984.91860;10491.92861;         0;418.37323;      ;         ;         ;        ;16;O;07-DEC-2023;                    ;;EYETS_23-24 Mesure 2D - RADI-LONGI GISP.UPS4L5. et GISP.UPS4R5.;
LHC       ;GITL.4R515.                     ;13417.10000; -966.54778;10499.66838;         0;418.08036;      ;         ;         ;        ; 7;O;28-NOV-2021;                    ;;YETS_21-22 Mesure ini 3D;
LHC       ;GGPSO.4R517.                    ;13420.55000; -963.32698;10500.90378;         0;418.10572;      ;         ;         ;        ;16;O;07-DEC-2023;                    ;;EYETS_23-24 Mesure 2D - RADI-LONGI GISP.UPS4L5. et GISP.UPS4R5.;
";
            using (StreamWriter writer = new StreamWriter(GetPathOfTheoFile(name)))
            {
                // Write the content of the string variable to the file
                writer.WriteLine(xmlContent);
            }
        }

        private static string GetPathOfTheoFile(string name)
        {
            return $@"C:\data\tsunami\Theoretical Files\{name}.dat";
        }

        private void CreateGeodeLevSequenceDatFile(string name)
        {
            string xmlContent = @"   1;LHC       ;MQXA.3R5.B        ;
   1;LHC       ;MQXA.3R5.P        ;
   1;LHC       ;MBXW.A4R5.E       ;
   1;LHC       ;MBXW.A4R5.S       ;
   2;LHC       ;MBXW.A4R5.E       ;
   2;LHC       ;MBXW.A4R5.S       ;
   2;LHC       ;GGPSO.4R505.      ;
   2;LHC       ;MBXW.B4R5.S       ;
   3;LHC       ;GGPSO.4R517.      ;
   3;LHC       ;RV.4R517.         ;
   3;LHC       ;GITL.4R515.       ;
   3;LHC       ;GGPSO.4R505.      ;
   3;LHC       ;MBXW.B4R5.S       ;
";
            using (StreamWriter writer = new StreamWriter(GetPathOfTheoFile(name)))
            {
                // Write the content of the string variable to the file
                writer.WriteLine(xmlContent);
            }
        }


        [TestMethod()]
        public void L2_SN_L_VM_Step1_Admin_setup()
        {
            Tools.Artefact.GetArtefactFullPath("L0_START_STEP1_DetectWrongResolution_Test.tsut"); // used to fail if resolution is wrong
            CreateGeodeInputDatFile("TEST_THEO_without_B&RV");
            CreateGeodeLevSequenceDatFile("TEST_SEQ");

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
          <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""200"" trace=""Smart Module+"" Button=""Left"" Direction=""Down"" Xpos=""203"" Ypos=""202"" Delta=""0"" />
          <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""10"" trace=""Smart Module+"" Button=""Left"" Direction=""Up"" Xpos=""203"" Ypos=""202"" Delta=""0"" />
          <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Smart Module (beginning)"" />
          <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Smart Module end"" />
          <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""200"" trace=""Smart Levelling+"" Button=""Left"" Direction=""Down"" Xpos=""354"" Ypos=""318"" Delta=""0"" />
          <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""10"" trace=""Smart Levelling+"" Button=""Left"" Direction=""Up"" Xpos=""354"" Ypos=""318"" Delta=""0"" />
          <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Smart Levelling (beginning)"" />
          <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Smart Levelling end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsNotNull(tsunami, "TSUNAMI object is NULL");

                Assert.IsTrue(tsunami.MeasurementModules.Count > 0, "No measurement modules found!");

                FinalModule measurementModule = tsunami.MeasurementModules[0];

                Assert.IsNotNull(measurementModule, $"No measurement module found !");

                LS.Module smartLevellingModule = measurementModule as LS.Module;

                Assert.IsNotNull(measurementModule, $"1st measurement module is not a smart levelling module ! Name:{measurementModule._Name}");

                ViewModel vm = smartLevellingModule.ViewModel;

                Assert.IsNotNull(vm, $"ViewModel not found !");

                AdministrativeSetup(vm);

                // Select the first sequence
                Invoke(vm, () => vm.CurrentStationIndex = 0);

                // Go to measure tab
                UseNavigationButton(vm, vm.BtnMeasure, nameof(vm.BtnMeasure));

                //Enter the values, each time we check we are in the right expected situation
                EnterValues(vm, 0, 1, "LHC.MQXA.3R5.B", "TAYLOR_SPHR_1", "0", "578");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 0, 1, "LHC.MQXA.3R5.P", "G80_1", "70", "55610");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 0, 1, "LHC.MBXW.A4R5.E", "G80_1", "70", "70083");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 0, 1, "LHC.MBXW.A4R5.S", "G80_1", "70", "72402");

                //Thread.Sleep(10000);

                //Next round
                UseNavigationButton(vm, vm.BtnNextRound, nameof(vm.BtnNextRound));

                Invoke(vm, () => vm.CurrentMeasureIndex = 0);
                EnterValues(vm, 0, 2, "LHC.MQXA.3R5.B", "TAYLOR_SPHR_1", "0", "619");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 0, 2, "LHC.MQXA.3R5.P", "G80_1", "70", "55652");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 0, 2, "LHC.MBXW.A4R5.E", "G80_1", "70", "70126");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 0, 2, "LHC.MBXW.A4R5.S", "G80_1", "70", "72443");

                //Thread.Sleep(10000);

                //Next station
                UseNavigationButton(vm, vm.BtnNextStation, nameof(vm.BtnNextStation));

                //Enter the values, each time we check we are in the right expected situation
                EnterValues(vm, 1, 1, "LHC.MBXW.A4R5.E", "LCBL110_4", "70", "31885");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 1, 1, "LHC.MBXW.A4R5.S", "LCBL110_4", "70", "34199");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 1, 1, "LHC.GGPSO.4R505.", "LCB190_1", "-70", "183666");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 1, 1, "LHC.MBXW.B4R5.S", "LCBL110_4", "70", "39458");

                //Thread.Sleep(10000);

                //Next round
                UseNavigationButton(vm, vm.BtnNextRound, nameof(vm.BtnNextRound));

                Invoke(vm, () => vm.CurrentMeasureIndex = 0);
                EnterValues(vm, 1, 2, "LHC.MBXW.A4R5.E", "LCBL110_4", "70", "31881");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 1, 2, "LHC.MBXW.A4R5.S", "LCBL110_4", "70", "34195");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 1, 2, "LHC.GGPSO.4R505.", "LCB190_1", "-70", "183663");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 1, 2, "LHC.MBXW.B4R5.S", "LCBL110_4", "70", "39453");

                //Thread.Sleep(10000);

                //Next station
                UseNavigationButton(vm, vm.BtnNextStation, nameof(vm.BtnNextStation));

                //Enter the values, each time we check we are in the right expected situation
                EnterValues(vm, 2, 1, "LHC.GGPSO.4R517.", "LCB190_1", "-70", "187331");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 2, 1, "LHC.RV.4R517.", "LCB190_1", "42", "181029");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 2, 1, "LHC.GITL.4R515.", "LCB190_1", "0", "182872");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 2, 1, "LHC.GGPSO.4R505.", "LCB190_1", "-70", "160579");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 2, 1, "LHC.MBXW.B4R5.S", "LCBL110_4", "70", "16368");

                //Thread.Sleep(10000);

                //Next round
                UseNavigationButton(vm, vm.BtnNextRound, nameof(vm.BtnNextRound));

                Invoke(vm, () => vm.CurrentMeasureIndex = 0);
                EnterValues(vm, 2, 2, "LHC.GGPSO.4R517.", "LCB190_1", "-70", "187292");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 2, 2, "LHC.RV.4R517.", "LCB190_1", "42", "180992");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 2, 2, "LHC.GITL.4R515.", "LCB190_1", "0", "182843");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 2, 2, "LHC.GGPSO.4R505.", "LCB190_1", "-70", "160546");
                Invoke(vm, () => vm.CurrentMeasureIndex++);
                EnterValues(vm, 2, 2, "LHC.MBXW.B4R5.S", "LCBL110_4", "70", "16333");

                //Thread.Sleep(10000);

                // Switch to log tab
                UseNavigationButton(vm, vm.BtnLog, nameof(vm.BtnLog));

                //Thread.Sleep(60000);

                // Export all measures
                Assert.IsTrue(vm.BtnExportAllMeasurement.CanExecute(null), $"{nameof(vm.BtnExportAllMeasurement)} is not clickable");
                string filepath = null;
                Result saveDatOK = null;
                Invoke(vm, () => vm.Test_ExportAllMeasurementsToGeode(out filepath, out saveDatOK));
                Assert.IsTrue(saveDatOK.Success, $"Geode export failed, ErrorMessage={saveDatOK.ErrorMessage}, AccessToken={saveDatOK.AccessToken}");
                Assert.IsFalse(string.IsNullOrEmpty(filepath), $"Geode output file path is null or empty.");
                Assert.IsTrue(File.Exists(filepath), $"Geode output file path {filepath} does not exist.");

                Console.WriteLine("*** Start of Geode file ***");
                Console.WriteLine(File.ReadAllText(filepath));
                Console.WriteLine("*** End of Geode file ***");

                // The common points should not have big delta
                foreach (StationViewModel s in vm.AllStations)
                    foreach (CommonPointViewModel cp in s.S2SComparison.CommonPoints)
                    {
                        Assert.AreEqual(0, long.Parse(cp.MeanVsStation1Aller), 10, $"Station to station comparison for point {cp.PointName} has big MeanVsStation1Aller:{cp.MeanVsStation1Aller}");
                        Assert.AreEqual(0, long.Parse(cp.MeanVsStation1Retour), 10, $"Station to station comparison for point {cp.PointName} has big MeanVsStation1Retour:{cp.MeanVsStation1Retour}");
                        Assert.AreEqual(0, long.Parse(cp.MeanVsStation2Aller), 10, $"Station to station comparison for point {cp.PointName} has big MeanVsStation2Aller:{cp.MeanVsStation2Aller}");
                        Assert.AreEqual(0, long.Parse(cp.MeanVsStation2Retour), 10, $"Station to station comparison for point {cp.PointName} has big MeanVsStation2Retour:{cp.MeanVsStation2Retour}");
                    }

                Assert.IsTrue(saveDatOK.Success, $"Geode export failed, ErrorMessage={saveDatOK.ErrorMessage}, AccessToken={saveDatOK.AccessToken}");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        private static void AdministrativeSetup(ViewModel vm)
        {
            //Set the Team
            Invoke(vm, () => vm.Team = "PW");
            Assert.IsTrue(string.IsNullOrEmpty(vm.TeamError), $"Theo file loading failed {vm.TeamError}!");

            //Open theo file
            Invoke(vm, () => vm.Test_BtnTheoClickInnerAction(GetPathOfTheoFile("TEST_THEO_without_B&RV")));
            Assert.IsTrue(string.IsNullOrEmpty(vm.TheoError), $"Theo file loading failed {vm.TheoError}!");

            //Open sequence file
            Invoke(vm, () => vm.Test_BtnOpenSequenceInnerAction(GetPathOfTheoFile("TEST_SEQ"), 0));
            Assert.IsTrue(string.IsNullOrEmpty(vm.SequenceError), $"Sequence file loading failed {vm.SequenceError}!");

            //Simulate an operation selection
            Invoke(vm, () => vm.OperationNumber = "11024");
            Assert.IsTrue(string.IsNullOrEmpty(vm.OperationError), $"Operation selection failed {vm.OperationError}!");

            //Simulate a level selection
            CI.Manager.Module instrumentManagerModule = null;
            CI.Level levelInstrument = null;
            Invoke(vm, () =>
            {
                instrumentManagerModule = vm.DefaultsStationModule._InstrumentManager;
                instrumentManagerModule._SelectedObjects.Clear();
                levelInstrument = instrumentManagerModule.GetByClass(CI.InstrumentClasses.NIVEAU)
                                                         .OfType<CI.Level>()
                                                         .FirstOrDefault(x => x._Model == "NA2" && x._SerialNumber == "5743619");
            });
            Assert.IsNotNull(levelInstrument, $"Instrument NA2 / 5743619 not found !");
            Invoke(vm, () =>
            {
                instrumentManagerModule.SetInstrument(levelInstrument);
                vm.CurrentInstrumentName = levelInstrument._Name;
            });
            Assert.IsTrue(string.IsNullOrEmpty(vm.InstrumentError), $"Instrument selection failed {vm.InstrumentError}!");

            //Simulate a main staff selection
            Invoke(vm, () =>
            {
                CI.LevelingStaff mainStaff = GetStaffByName(vm, "G80_1");
                vm.DefaultsStationModule._defaultStaff = mainStaff;
                vm.CurrentStaffForSocketsName = mainStaff._Name;
            });
            Assert.IsTrue(string.IsNullOrEmpty(vm.StaffError), $"Staff selection failed {vm.StaffError}!");

            //Check if the validate Button is ok and click it
            UseNavigationButton(vm, vm.BtnValidate, nameof(vm.BtnValidate));
        }

        private static void Invoke(ViewModel vm, Action a)
        {
            vm.MainView.Invoke(a);
        }

        private static void UseNavigationButton(ViewModel vm, ICommand command, string buttonName)
        {
            Assert.IsTrue(command.CanExecute(null), $"{buttonName} is not clickable");
            //The click must be done in main thread, because it uses Windows.Forms
            Invoke(vm,() => command.Execute(null));
        }

        private static void EnterValues(ViewModel vm, int expectedStationIndex, int expectedRoundNumber, string expectedPointName, string staffName, string extension, string rawLevelreading)
        {
            // Check the current station, round and point
            int stationIndex = vm.CurrentStationIndex;
            Assert.AreEqual(expectedStationIndex, stationIndex, $"CurrentStationIndex is {stationIndex} instead of {expectedStationIndex}");
            int roundNumber = vm.CurrentStation?.CurrentRoundAsInt ?? -1;
            Assert.AreEqual(expectedRoundNumber, roundNumber, $"CurrentStation.CurrentRoundAsInt is {roundNumber} instead of {expectedRoundNumber}");
            string pointName = vm.CurrentMeasure?.PointName;
            Assert.AreEqual(expectedPointName, pointName, $"CurrentMeasure.PointName is {pointName} instead of {expectedPointName}");

            Invoke(vm, () =>
            {
                // Type the values and switch to next
                vm.CurrentMeasure.Extension = extension;
                vm.CurrentMeasure.RawLevelReading = rawLevelreading;

                // Select the staff at the end, to prevent a crash on the first measure beacuase there's no StationModule
                // (Note : this should be fixed someday)
                vm.Test_SetCurrentMeasureStaff(GetStaffByName(vm, staffName));
            });
        }

        private static CI.LevelingStaff GetStaffByName(ViewModel vm, string staffName)
        {
            CI.LevelingStaff newStaff = vm.Test_GetStaffByName(staffName);
            Assert.IsNotNull(newStaff, $"The staff {staffName} was not found");
            return newStaff;
        }
    }
}
