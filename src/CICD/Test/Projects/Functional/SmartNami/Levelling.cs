﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Reflection;
using TSU;

namespace Functional_Tests.SmartNami
{
    [TestClass]
    public class Levelling
    {
        private void CreateGeodeInputDatFile(string name)
        {
            string xmlContent = @"TT41      ;QTGD.411500.E                   ;  595.57395; 2854.99786; 4222.27549;2393.35058;393.75895;  .495; -.00915; -.056608;107.9754; 1;A;23-MAR-2016;                    ;;
TT41      ;QTGD.411500.S                   ;  597.01395; 2856.42428; 4222.09585;2393.26911;393.67761; 2.200; -.00915; -.056608;107.9754; 1;O;23-MAR-2016;                    ;;
TT41      ;MDGV.411506.E                   ;  598.08295; 2857.42468; 4221.97131;2393.04828;393.45686;  .490; -.00915; -.056608;107.9754; 1;A;23-MAR-2016;                    ;;
TT41      ;MDGV.411506.S                   ;  598.28295; 2857.67232; 4221.94013;2393.03412;393.44273;  .548; -.00915; -.056608;107.9754; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411508.E                    ;  600.49395; 2859.87064; 4221.67338;2393.03229;393.44110;  .657; -.00035; -.056608;107.7206; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411508.S                    ;  603.98395; 2863.32946; 4221.25184;2392.83483;393.24395; 6.300; -.00035; -.056608;107.7206; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411522.E                    ;  607.36396; 2866.68083; 4220.85712;2392.64360;393.05303;  .570; -.00035; -.056608;107.2111; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411522.S                    ;  610.85396; 2870.14291; 4220.46328;2392.44613;392.85589; 6.300; -.00035; -.056608;107.2111; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411536.E                    ;  614.23398; 2873.49733; 4220.09540;2392.25491;392.66499;  .570; -.00035; -.056608;106.7015; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411536.S                    ;  617.72398; 2876.96246; 4219.72928;2392.05745;392.46787; 6.300; -.00035; -.056608;106.7015; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411550.E                    ;  621.10400; 2880.31973; 4219.38827;2391.86621;392.27697;  .570; -.00034; -.056608;106.1919; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411550.S                    ;  624.59400; 2883.78767; 4219.04990;2391.66876;392.07987; 6.300; -.00034; -.056608;106.1919; 1;O;23-MAR-2016;                    ;;
TT41      ;QTGF.411600.E                   ;  626.87401; 2886.05524; 4218.82587;2391.57968;391.99102;  .495; -.01095; -.056608;105.9371; 1;A;23-MAR-2016;                    ;;
TT41      ;QTGF.411600.S                   ;  628.31401; 2887.48669; 4218.69199;2391.49821;391.90970; 2.200; -.01095; -.056608;105.9371; 1;O;23-MAR-2016;                    ;;
TT41      ;BPG.411605.E                    ;  628.89401; 2888.06712; 4218.63695;2391.53527;391.94682;  .160; -.01095; -.056608;105.9371; 7;A;23-MAR-2016;                    ;;
TT41      ;BPG.411605.S                    ;  629.06401; 2888.23611; 4218.62114;2391.52566;391.93723;  .250; -.01095; -.056608;105.9371; 7;O;23-MAR-2016;                    ;;
TT41      ;MBG.411608.E                    ;  631.79401; 2890.94486; 4218.38059;2391.26138;391.67324; 1.285; -.00034; -.056608;105.6824; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411608.S                    ;  635.28401; 2894.41540; 4218.06999;2391.06392;391.47615; 6.300; -.00034; -.056608;105.6824; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411622.E                    ;  638.66403; 2897.77769; 4217.78275;2390.87270;391.28529;  .570; -.00034; -.056608;105.1728; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411622.S                    ;  642.15403; 2901.25060; 4217.49994;2390.67524;391.08821; 6.300; -.00034; -.056608;105.1728; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411636.E                    ;  645.53405; 2904.61508; 4217.23963;2390.48400;390.89735;  .570; -.00034; -.056608;104.6633; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411636.S                    ;  649.02405; 2908.09014; 4216.98462;2390.28655;390.70029; 6.300; -.00034; -.056608;104.6633; 1;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.571.                      ;  570.93800; 2830.71683; 4226.50115;2393.49345;393.90011;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.591.                      ;  590.59800; 2850.13398; 4223.71076;2392.38002;392.78824;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.610.                      ;  610.05800; 2869.42602; 4221.34049;2391.27522;391.68518;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.634.                      ;  633.53800; 2892.71484; 4219.02910;2389.94861;390.36092;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.647.                      ;  647.27800; 2906.35278; 4217.93419;2389.15946;389.57328;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.669.                      ;  668.70800; 2927.62769; 4216.53995;2387.96197;388.37830;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
";
            using (StreamWriter writer = new StreamWriter($@"C:\data\tsunami\Theoretical Files\{name}.dat"))
            {
                // Write the content of the string variable to the file
                writer.WriteLine(xmlContent);
            }
        }

        private void CreateGeodeLevSequenceDatFile(string name)
        {
            string xmlContent = @"1;TT41      ;MBG.411508.E;
1;TT41      ;MBG.411508.S;
1;TT41      ;MBG.411522.E;
1;TT41      ;MBG.411522.S;
2;TT41      ;MBG.411522.E;
2;TT41      ;MBG.411522.S;
2;TT41      ;MBG.411536.E;
2;TT41      ;MBG.411536.S;
";
            using (StreamWriter writer = new StreamWriter($@"C:\data\tsunami\Theoretical Files\{name}.dat"))
            {
                // Write the content of the string variable to the file
                writer.WriteLine(xmlContent);
            }
        }


        [TestMethod()]
        public void L2_SN_L_Step1_Admin_setup()
        {
            CreateGeodeInputDatFile("snlt"); //F-test SnmartNami level GeodeInput
            CreateGeodeLevSequenceDatFile("snls"); //F-test SnmartNami level GeodeSequence
            
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
    <WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
    <KeyStrokesAndMouseClicks>
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""200"" trace=""Smart Module+"" Button=""Left"" Direction=""Down"" Xpos=""203"" Ypos=""202"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""10"" trace=""Smart Module+"" Button=""Left"" Direction=""Up"" Xpos=""203"" Ypos=""202"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Smart Module (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Smart Module end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""200"" trace=""Smart Levelling+"" Button=""Left"" Direction=""Down"" Xpos=""354"" Ypos=""318"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""10"" trace=""Smart Levelling+"" Button=""Left"" Direction=""Up"" Xpos=""354"" Ypos=""318"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Smart Levelling (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Smart Levelling end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""500"" Ypos=""210"" Delta=""0""/>
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""500"" Ypos=""210"" Delta=""0""/>
        {Tools.GetKeySequence("PW", "View of 'Smart Levelling'", 200)}
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""501"" Ypos=""325"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""501"" Ypos=""325"" Delta=""0"" />
        {Tools.GetKeySequence("snlt.dat", "View of 'Smart Levelling'", 200)}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""View of 'Smart Levelling'"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""639"" Ypos=""440"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""639"" Ypos=""440"" Delta=""0"" />
        {Tools.GetKeySequence("snls.dat", "View of 'Smart Levelling'", 200)}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""View of 'Smart Levelling'"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""443"" Ypos=""511"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""443"" Ypos=""511"" Delta=""0"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""View of 'Smart Levelling'"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""41"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""View of 'Smart Levelling'"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""42"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""View of 'Smart Levelling'"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""43"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""View of 'Smart Levelling'"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""44"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""View of 'Smart Levelling'"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""45"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""View of 'Smart Levelling'"" />
        {Tools.GetKeySequence("123", "View of 'Smart Levelling'")}
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""512"" Ypos=""600"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""512"" Ypos=""600"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1813"" Ypos=""28"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1813"" Ypos=""28"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""200"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""647"" Ypos=""222"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""10"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""647"" Ypos=""222"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""55"" Delay=""0"" Action=""BB: Options (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""56"" Delay=""0"" Action=""BB: Options end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""200"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""781"" Ypos=""347"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""10"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""781"" Ypos=""347"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""59"" Delay=""0"" Action=""BB: List View (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""60"" Delay=""0"" Action=""BB: List View end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""116"" Ypos=""264"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""116"" Ypos=""264"" Delta=""0"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""63"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+{{N}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""64"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+{{A}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""65"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""66"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""1000"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""117"" Ypos=""339"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""117"" Ypos=""339"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""200"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""272"" Ypos=""951"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""272"" Ypos=""951"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""71"" Delay=""0"" Action=""MB: Choose a level: Select"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""379"" Ypos=""807"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""379"" Ypos=""807"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""264"" Ypos=""319"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""264"" Ypos=""319"" Delta=""0"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""76"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""78"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""1000"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""160"" Ypos=""382"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""160"" Ypos=""382"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""200"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""299"" Ypos=""936"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""82"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""299"" Ypos=""936"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""83"" Delay=""0"" Action=""MB: Choose an optional secondary staff: Select"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""301"" Ypos=""901"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""301"" Ypos=""901"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""237"" Ypos=""381"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""87"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""237"" Ypos=""381"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""88"" Delay=""200"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""290"" Ypos=""935"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""89"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""290"" Ypos=""935"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""90"" Delay=""0"" Action=""MB: Choose a default main staff: Select"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""91"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""267"" Ypos=""973"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""267"" Ypos=""973"" Delta=""0"" />
    </KeyStrokesAndMouseClicks>                               
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules.Count > 0, "No measurement modules found!");

                var mod = tsunami.MeasurementModules[0] as TSU.Level.Smart.Module;
                Assert.IsTrue(mod != null, $"The 1st measurement module is not Smart Levelling !");

                Assert.IsTrue(mod.StationModules.Count > 0, $"No station module in measurement modules {tsunami.MeasurementModules} found!");

                var stationM = mod.StationModules[0] as TSU.Level.Station.Module;
                var station = stationM._Station as TSU.Level.Station;

                var parameters = station.ParametersBasic;
                var model = parameters._Instrument._Model;
                Assert.IsTrue(model == "NA2", $"wrong na2 selection {model}");

                var vm = mod.ViewModel;
                Assert.IsTrue(vm?.CurrentPage ==  TSU.Level.Smart.ViewModel.Page.Sequence, $"CurrentPage is {vm?.CurrentPage} instead of {TSU.Level.Smart.ViewModel.Page.Sequence}");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_SN_L_Step2_Acquire()
        {
            CreateGeodeInputDatFile("ftsnrgi"); //F-test SnmartNami Roll GeodeInput
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_SN_L_Step1_Admin_setup) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""720"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""393"" Ypos=""969"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""393"" Ypos=""969"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""364"" Ypos=""479"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""364"" Ypos=""479"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""467"" Ypos=""279"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""467"" Ypos=""279"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""53"" Ypos=""535"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""53"" Ypos=""535"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""16"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""17"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""50"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""50"" Ypos=""559"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""46"" Ypos=""595"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""46"" Ypos=""595"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""30"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""31"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""212"" Ypos=""444"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""212"" Ypos=""444"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""52"" Ypos=""535"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""52"" Ypos=""535"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""44"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""45"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""46"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""53"" Ypos=""563"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""52"" Ypos=""563"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""51"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""52"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""53"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""47"" Ypos=""607"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""47"" Ypos=""607"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""58"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""59"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""60"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""209"" Ypos=""998"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""209"" Ypos=""998"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""63"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""64"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""65"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""234"" Ypos=""366"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""234"" Ypos=""366"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""68"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""69"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""70"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""71"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""56"" Ypos=""542"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""56"" Ypos=""542"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""76"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""78"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""79"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""42"" Ypos=""567"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""42"" Ypos=""567"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""84"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""85"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""86"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""45"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""93"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""45"" Ypos=""606"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""96"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""97"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""98"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""99"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""231"" Ypos=""374"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""101"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""231"" Ypos=""374"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""102"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""206"" Ypos=""443"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""206"" Ypos=""443"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""46"" Ypos=""507"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""46"" Ypos=""507"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""505"" Ypos=""993"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""107"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""505"" Ypos=""993"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""50"" Ypos=""532"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""110"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""50"" Ypos=""532"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""113"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""114"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""117"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""49"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""118"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""49"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""121"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""122"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""123"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""41"" Ypos=""601"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""125"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""41"" Ypos=""601"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""128"" Delay=""200"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""129"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""View of 'Smart Levelling'"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""130"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""View of 'Smart Levelling'"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""131"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""572"" Ypos=""280"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""132"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""572"" Ypos=""280"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""133"" Delay=""200"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""325"" Ypos=""976"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""134"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""325"" Ypos=""976"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""135"" Delay=""200"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""976"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""136"" Delay=""10"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""976"" Ypos=""558"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""137"" Delay=""0"" Action=""MB: Successful export: OK"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules.Count > 0, "No measurement modules found!");
                Assert.IsTrue(tsunami.MeasurementModules[0]?.StationModules.Count > 0, $"No station module in measurement modules {tsunami.MeasurementModules} found!");

                var stationModules = tsunami.MeasurementModules[0]?.StationModules;

                var pointName = "TT41.MBG.411508.E";
                var stationModule = stationModules.Find(x => x._Name.Contains(pointName)) as TSU.Level.Station.Module;
                var stationA = stationModule._AllerStationLevel as TSU.Level.Station;
                var stationR = stationModule._RetourStationLevel as TSU.Level.Station;
                var stationReprises = stationModule._RepriseStationLevel as CloneableList<TSU.Level.Station>;
                var repriseCount = stationReprises.Count;
                Assert.IsTrue(repriseCount == 0, $"wrong count of reprise station {repriseCount}");

                var measures = stationA._MeasureOfLevel;

                var measure = measures.Find(x => x._Name.Contains(pointName));
                var reading = measure._RawLevelReading;
                var expected = 0.0100;
                Assert.AreEqual(expected, reading, 0.001, $"wrong raw reading {reading} instead of {expected}");

                pointName = "TT41.MBG.411522.S";
                measure = measures.Find(x => x._Name.Contains(pointName));
                reading = measure._RawLevelReading;
                expected = 0.01003;
                Assert.AreEqual(expected, reading, 0.001, $"wrong raw reading {reading} instead of {expected}");

                // next station
                pointName = "TT41.MBG.411522.E";
                stationModule = stationModules.Find(x => x._Name.Contains(pointName)) as TSU.Level.Station.Module;
                stationA = stationModule._AllerStationLevel as TSU.Level.Station;
                stationR = stationModule._RetourStationLevel as TSU.Level.Station;
                stationReprises = stationModule._RepriseStationLevel as CloneableList<TSU.Level.Station>;
                repriseCount = stationReprises.Count;
                Assert.IsTrue(repriseCount == 1, $"wrong count of reprise station {repriseCount}");

                measures = stationReprises[repriseCount-1]._MeasureOfLevel;

                measure = measures.Find(x => x._Name.Contains(pointName));
                reading = measure._RawLevelReading;
                expected = 0.00000;
                Assert.AreEqual(expected, reading, 0.001, $"wrong raw reading {reading} instead of {expected}");

                pointName = "TT41.MBG.411536.E";
                measure = measures.Find(x => x._Name.Contains(pointName));
                reading = measure._RawLevelReading;
                expected = 0.00102;
                Assert.AreEqual(expected, reading, 0.001, $"wrong raw reading {reading} instead of {expected}");


                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}
