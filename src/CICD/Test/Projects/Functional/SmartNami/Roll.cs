﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows;
using System.IO;
using System.Reflection;

namespace Functional_Tests.SmartNami
{
    [TestClass]
    public class Roll
    {

        private void CreateGeodeInputDatFile(string name)
        {
            string xmlContent = @"TT41      ;QTGD.411500.E                   ;  595.57395; 2854.99786; 4222.27549;2393.35058;393.75895;  .495; -.00915; -.056608;107.9754; 1;A;23-MAR-2016;                    ;;
TT41      ;QTGD.411500.S                   ;  597.01395; 2856.42428; 4222.09585;2393.26911;393.67761; 2.200; -.00915; -.056608;107.9754; 1;O;23-MAR-2016;                    ;;
TT41      ;MDGV.411506.E                   ;  598.08295; 2857.42468; 4221.97131;2393.04828;393.45686;  .490; -.00915; -.056608;107.9754; 1;A;23-MAR-2016;                    ;;
TT41      ;MDGV.411506.S                   ;  598.28295; 2857.67232; 4221.94013;2393.03412;393.44273;  .548; -.00915; -.056608;107.9754; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411508.E                    ;  600.49395; 2859.87064; 4221.67338;2393.03229;393.44110;  .657; -.00035; -.056608;107.7206; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411508.S                    ;  603.98395; 2863.32946; 4221.25184;2392.83483;393.24395; 6.300; -.00035; -.056608;107.7206; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411522.E                    ;  607.36396; 2866.68083; 4220.85712;2392.64360;393.05303;  .570; -.00035; -.056608;107.2111; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411522.S                    ;  610.85396; 2870.14291; 4220.46328;2392.44613;392.85589; 6.300; -.00035; -.056608;107.2111; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411536.E                    ;  614.23398; 2873.49733; 4220.09540;2392.25491;392.66499;  .570; -.00035; -.056608;106.7015; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411536.S                    ;  617.72398; 2876.96246; 4219.72928;2392.05745;392.46787; 6.300; -.00035; -.056608;106.7015; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411550.E                    ;  621.10400; 2880.31973; 4219.38827;2391.86621;392.27697;  .570; -.00034; -.056608;106.1919; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411550.S                    ;  624.59400; 2883.78767; 4219.04990;2391.66876;392.07987; 6.300; -.00034; -.056608;106.1919; 1;O;23-MAR-2016;                    ;;
TT41      ;QTGF.411600.E                   ;  626.87401; 2886.05524; 4218.82587;2391.57968;391.99102;  .495; -.01095; -.056608;105.9371; 1;A;23-MAR-2016;                    ;;
TT41      ;QTGF.411600.S                   ;  628.31401; 2887.48669; 4218.69199;2391.49821;391.90970; 2.200; -.01095; -.056608;105.9371; 1;O;23-MAR-2016;                    ;;
TT41      ;BPG.411605.E                    ;  628.89401; 2888.06712; 4218.63695;2391.53527;391.94682;  .160; -.01095; -.056608;105.9371; 7;A;23-MAR-2016;                    ;;
TT41      ;BPG.411605.S                    ;  629.06401; 2888.23611; 4218.62114;2391.52566;391.93723;  .250; -.01095; -.056608;105.9371; 7;O;23-MAR-2016;                    ;;
TT41      ;MBG.411608.E                    ;  631.79401; 2890.94486; 4218.38059;2391.26138;391.67324; 1.285; -.00034; -.056608;105.6824; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411608.S                    ;  635.28401; 2894.41540; 4218.06999;2391.06392;391.47615; 6.300; -.00034; -.056608;105.6824; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411622.E                    ;  638.66403; 2897.77769; 4217.78275;2390.87270;391.28529;  .570; -.00034; -.056608;105.1728; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411622.S                    ;  642.15403; 2901.25060; 4217.49994;2390.67524;391.08821; 6.300; -.00034; -.056608;105.1728; 1;O;23-MAR-2016;                    ;;
TT41      ;MBG.411636.E                    ;  645.53405; 2904.61508; 4217.23963;2390.48400;390.89735;  .570; -.00034; -.056608;104.6633; 1;A;23-MAR-2016;                    ;;
TT41      ;MBG.411636.S                    ;  649.02405; 2908.09014; 4216.98462;2390.28655;390.70029; 6.300; -.00034; -.056608;104.6633; 1;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.571.                      ;  570.93800; 2830.71683; 4226.50115;2393.49345;393.90011;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.591.                      ;  590.59800; 2850.13398; 4223.71076;2392.38002;392.78824;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.610.                      ;  610.05800; 2869.42602; 4221.34049;2391.27522;391.68518;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.634.                      ;  633.53800; 2892.71484; 4219.02910;2389.94861;390.36092;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.647.                      ;  647.27800; 2906.35278; 4217.93419;2389.15946;389.57328;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
TT41      ;GGPSO.669.                      ;  668.70800; 2927.62769; 4216.53995;2387.96197;388.37830;      ;        ;         ;        ;16;O;23-MAR-2016;                    ;;
";
            using (StreamWriter writer = new StreamWriter($@"C:\data\tsunami\Theoretical Files\{name}.dat"))
            {
                // Write the content of the string variable to the file
                writer.WriteLine(xmlContent);
            }
        }

        [TestMethod()]
        public void L2_SN_R_Step1_Admin_setup()
        {
            const string GeodeInput = "ftsnrgi";
            CreateGeodeInputDatFile(GeodeInput); //F-test SnmartNami Roll GeodeInput
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
    <WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
    <KeyStrokesAndMouseClicks>
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""100"" trace=""Smart Module+"" Button=""Left"" Direction=""Down"" Xpos=""297"" Ypos=""195"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""10"" trace=""Smart Module+"" Button=""Left"" Direction=""Up"" Xpos=""297"" Ypos=""195"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Smart Module (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Smart Module end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""100"" trace=""Smart Roll+"" Button=""Left"" Direction=""Down"" Xpos=""483"" Ypos=""239"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""10"" trace=""Smart Roll+"" Button=""Left"" Direction=""Up"" Xpos=""483"" Ypos=""239"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Smart Roll (beginning)"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Smart Roll end"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""523"" Ypos=""344"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""523"" Ypos=""344"" Delta=""0"" />

        {Tools.GetKeySequence("PW", "TextBox", 100)}
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""525"" Ypos=""456"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""525"" Ypos=""456"" Delta=""0"" />
        {Tools.GetKeySequence(GeodeInput, "TextBox", 100)}
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""TextBox"" />

        <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""604"" Ypos=""632"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""604"" Ypos=""632"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""185"" Ypos=""300"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""185"" Ypos=""300"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""310"" Ypos=""331"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""310"" Ypos=""331"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""100"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""847"" Ypos=""922"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""847"" Ypos=""922"" Delta=""0"" />
        <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""MB: Operation Selection: Select"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""777"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""777"" Delta=""0"" />

        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""100"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
        <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />

        <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""429"" Ypos=""955"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""429"" Ypos=""955"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""228"" Ypos=""300"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""229"" Ypos=""300"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""76"" Ypos=""372"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""76"" Ypos=""372"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""49"" Ypos=""429"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""49"" Ypos=""429"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""35"" Ypos=""461"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""35"" Ypos=""461"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""42"" Ypos=""758"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""42"" Ypos=""758"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""100"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""295"" Ypos=""220"" Delta=""0"" />
        <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""295"" Ypos=""220"" Delta=""0"" />
    </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                Assert.IsTrue(tsunami.MeasurementModules.Count > 0, "No measurement modules found!");
                Assert.IsTrue(tsunami.MeasurementModules[0]?.StationModules.Count > 0, $"No station module in measurement modules {tsunami.MeasurementModules} found!");
                var stationM = tsunami.MeasurementModules[0]?.StationModules[0] as TSU.Tilt.Station.Module;
                var station = stationM._Station as TSU.Tilt.Station;
                var initCount = stationM._InitialStationsTilt.Count;
                Assert.IsTrue(initCount == 3, $"wrong count of intial tilt station {initCount}");

                var adjCount = stationM._AdjustingStationsTilt.Count;
                Assert.IsTrue(adjCount == 3, $"wrong count of adjust tilt station {adjCount}");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}
