﻿using Functional_Tests;
using MathNet.Numerics.LinearRegression;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Markup;
using System.Windows.Media.Animation;
using TSU;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Measures;
using TSU.Line.GuidedModules;
using UnitTestGenerator;

namespace Functional_Tests.BOC
{
    [TestClass]
    public class APM
    {
        [TestMethod()]
        public void L2_BAP_Step0_dat_file_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"); // used to fail if the splashscreen test had an error
            string datContent = @"926R033   ;NID.3.                          ; -999.00000; 1091.62262; 5187.55579;         0;461.20225;      ;        ;         ;        ;18;O;16-JUL-2024;                    ;;;
926R033   ;NID.6.                          ; -999.00000; 1090.78977; 5194.70111;         0;461.79305;      ;        ;         ;        ;18;O;16-JUL-2024;                    ;;;
926R033   ;NID.1.                          ; -999.00000; 1092.64630; 5189.84886;         0;461.61813;      ;        ;         ;        ;18;O;16-JUL-2024;                    ;;;
926R033   ;NID.4.                          ; -999.00000; 1089.08745; 5189.12551;         0;461.06222;      ;        ;         ;        ;18;O;16-JUL-2024;                    ;;;
926R033   ;NID.8.                          ; -999.00000; 1092.91457; 5192.08595;         0;461.18275;      ;        ;         ;        ;18;O;16-JUL-2024;                    ;;;
926R033   ;NID.5.                          ; -999.00000; 1089.49168; 5192.48799;         0;461.26804;      ;        ;         ;        ;18;O;16-JUL-2024;                    ;;;
926R033   ;NID.7.                          ; -999.00000; 1092.52925; 5194.49888;         0;461.81085;      ;        ;         ;        ;18;O;16-JUL-2024;                    ;;;
926R033   ;FAISCEAU.E.                     ;    1.00000; 4466.69727; 5020.57422;         0;341.51001;      ;        ;         ;        ; 5;O;06-DEC-2017;                    ;;Reseau carnet Alexandre;
926R033   ;FAISCEAU.S.                     ;    2.48131; 4467.21000; 5021.92743;         0;341.82740;      ;        ;         ;        ; 5;O;06-DEC-2017;                    ;;Reseau carnet Alexandre;
926P_2024 ;BAM.1.A                         ;   10.01126; 1091.41586; 5194.48521;2459.88419;460.70072;99.999; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAM.1;;;
926P_2024 ;BAM.1.B                         ;   10.01832; 1091.39125; 5194.26302;2459.88443;460.70085;99.999; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAM.1;;;
926P_2024 ;BAM.1.E                         ;   10.06123; 1091.44721; 5194.37133;2460.07932;460.89579;10.000; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAM.1;;;
926P_2024 ;BAM.1.D                         ;   10.08216; 1091.45795; 5194.28689;2459.96629;460.78272;99.999; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAM.1;;;
926P_2024 ;BAM.1.G                         ;   10.36460; 1091.76668; 5194.44273;2459.88955;460.70601;99.999; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAM.1;;;
926P_2024 ;BAM.1.F                         ;   10.37700; 1091.74762; 5194.22161;2459.88953;460.70588;99.999; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAM.1;;;
926P_2024 ;MAIN.2.E                        ;   10.65000; 1092.03155; 5194.28978;2459.99904;460.81539;  .250;  .002180;  .014544;109.0321;18; ;29-JUL-2024; MAIN.2;;;
926P_2024 ;MAIN.2.A                        ;   10.65391; 1092.03170; 5194.28778;2460.23818;461.05453;99.999;  .002180;  .014544;109.0321;18; ;29-JUL-2024; MAIN.2;;;
926P_2024 ;MAIN.2.B                        ;   10.66826; 1092.05568; 5194.35594;2460.25536;461.07174;99.999;  .002180;  .014544;109.0321;18; ;29-JUL-2024; MAIN.2;;;
926P_2024 ;MAIN.2.C                        ;   10.69888; 1092.06326; 5194.19076;2460.23954;461.05584;99.999;  .002180;  .014544;109.0321;18; ;29-JUL-2024; MAIN.2;;;
926P_2024 ;MAIN.2.F                        ;   10.72484; 1092.09281; 5194.18909;2459.99677;460.81306;99.999;  .002180;  .014544;109.0321;18; ;29-JUL-2024; MAIN.2;;;
926P_2024 ;MAIN.2.G                        ;   10.77319; 1092.15735; 5194.32562;2460.25538;461.07173;99.999;  .002180;  .014544;109.0321;18; ;29-JUL-2024; MAIN.2;;;
926P_2024 ;MAIN.2.D                        ;   10.78090; 1092.14769; 5194.20342;2460.25592;461.07221;99.999;  .002180;  .014544;109.0321;18; ;29-JUL-2024; MAIN.2;;;
926P_2024 ;MAIN.2.S                        ;   10.79182; 1092.17193; 5194.26973;2460.00110;460.81742;  .142;  .002180;  .014544;109.0321;18; ;29-JUL-2024; MAIN.2;;;
926P_2024 ;BAV.3.G                         ;   11.06719; 1092.43159; 5194.12735;2459.87851;460.69472;99.999; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAV.3;;;
926P_2024 ;BAV.3.H                         ;   11.11162; 1092.50627; 5194.33605;2459.87910;460.69540;99.999; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAV.3;;;
926P_2024 ;BAV.3.E                         ;   11.13875; 1092.50393; 5194.13294;2459.92915;460.74535;  .240; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAV.3;;;
926P_2024 ;BAV.3.C                         ;   11.33769; 1092.70329; 5194.12498;2459.96257;460.77874;99.999; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAV.3;;;
926P_2024 ;BAV.3.S                         ;   11.37258; 1092.74688; 5194.18875;2460.01468;460.83088;  .400; -.000461;  .014544;109.0321;18; ;29-JUL-2024; BAV.3;;;
BEAM_926P_2024 ;BAM.1.E                         ;   10.00000; 1091.38815; 5194.38168;2459.98959;460.80607;;;  .014544;109.0321;;;29-JUL-2024; ;;coordonnées théoriques dans le système CCS au 29-JUL-2024 15:19:42;
BEAM_926P_2024 ;BAM.1.S                         ;   10.40000; 1091.78409; 5194.32512;2459.99541;460.81180;;;  .014544;109.0321;;;29-JUL-2024; ;;coordonnées théoriques dans le système CCS au 29-JUL-2024 15:19:42;
*FRAME RSTI_926P_2024.BAM.1   1091.388147   5194.381678   2459.989588 0 0  109.032069 1
*FRAME RST_926P_2024.BAM.1 0 0 0 -.92589979693 0 0 1
*FRAME RSTRI_926P_2024.BAM.1      0.014486      0.062839     -0.009713 .92742865705 0 1.3592715578 1 TX TY TZ RX RZ 
*FRAME RSTR_926P_2024.BAM.1 0 0 0 0 0 0 1 RY
*CALA
BEAM_926P_2024.BAM.1.E                                           0.000000      0.000000      0.000000   $10.000 842129 coordonnées théoriques dans le système RST au 29-JUL-2024 15:19:42
BEAM_926P_2024.BAM.1.S                                           0.000000      0.400000      0.000000   $10.400 842132 coordonnées théoriques dans le système RST au 29-JUL-2024 15:19:42
*OBSXYZ
926P_2024.BAM.1.E      0.001888      0.061233      0.088855 0.1 0.1 0.1   $10.061 842329 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAM.1.B      0.117033      0.018315     -0.105438 0.1 0.1 0.1   $10.018 842330 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAM.1.D      0.083969      0.082161     -0.024492 0.1 0.1 0.1   $10.082 842331 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAM.1.A     -0.106410      0.011258     -0.105569 0.1 0.1 0.1   $10.011 842332 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAM.1.F      0.107630      0.377002     -0.105552 0.1 0.1 0.1   $10.377 842346 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAM.1.G     -0.113958      0.364604     -0.105353 0.1 0.1 0.1   $10.365 842347 paramètres RST 29-JUL-2024 15:19:42
*INCLY Instr_Roll_Theo
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_926P_2024 ;MAIN.2.S                        ;   10.79182; 1092.17193; 5194.26973;2460.00110;460.81742;;;  .014544;109.0321;;;29-JUL-2024; ;;coordonnées théoriques dans le système CCS au 29-JUL-2024 15:19:42;
BEAM_926P_2024 ;MAIN.2.E                        ;   10.65000; 1092.03155; 5194.28978;2459.99904;460.81539;;;  .014544;109.0321;;;29-JUL-2024; ;;coordonnées théoriques dans le système CCS au 29-JUL-2024 15:19:42;
*FRAME RSTI_926P_2024.MAIN.2   1092.031548   5194.289777   2459.999041 0 0  109.032069 1
*FRAME RST_926P_2024.MAIN.2 0 0 0 -.92589979693 0 0 1
*FRAME RSTRI_926P_2024.MAIN.2     -0.000015     -0.000004      0.000026 .0008977856 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_926P_2024.MAIN.2 0 0 0 0 0 0 1 RY
*CALA
BEAM_926P_2024.MAIN.2.S                                          0.000000      0.141820      0.000000   $10.792 842128 coordonnées théoriques dans le système RST au 29-JUL-2024 15:19:42
BEAM_926P_2024.MAIN.2.E                                          0.000000      0.000000      0.000000   $10.650 842133 coordonnées théoriques dans le système RST au 29-JUL-2024 15:19:42
*OBSXYZ
926P_2024.MAIN.2.A      0.001961      0.003911      0.239109 0.1 0.1 0.1   $10.654 842335 paramètres RST 29-JUL-2024 15:19:42
926P_2024.MAIN.2.B     -0.068910      0.018258      0.256082 0.1 0.1 0.1   $10.668 842336 paramètres RST 29-JUL-2024 15:19:42
926P_2024.MAIN.2.G     -0.053273      0.123187      0.254573 0.1 0.1 0.1   $10.773 842337 paramètres RST 29-JUL-2024 15:19:42
926P_2024.MAIN.2.D      0.069068      0.130903      0.255002 0.1 0.1 0.1   $10.781 842338 paramètres RST 29-JUL-2024 15:19:42
926P_2024.MAIN.2.E      0.000000      0.000000      0.000000 0.1 0.1 0.1   $10.650 842339 paramètres RST 29-JUL-2024 15:19:42
926P_2024.MAIN.2.S      0.000000      0.141820      0.000000 0.1 0.1 0.1   $10.792 842340 paramètres RST 29-JUL-2024 15:19:42
926P_2024.MAIN.2.F      0.091014      0.074841     -0.003364 0.1 0.1 0.1   $10.725 842333 paramètres RST 29-JUL-2024 15:19:42
926P_2024.MAIN.2.C      0.093539      0.048884      0.239815 0.1 0.1 0.1   $10.699 842334 paramètres RST 29-JUL-2024 15:19:42
*INCLY Instr_Roll_Theo
926P_2024.MAIN.2.E  0.002180000 RF -0.000461009
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_926P_2024 ;BAV.3.E                         ;   11.03182; 1092.40949; 5194.23579;2460.00459;460.82086;;;  .014544;109.0321;;;29-JUL-2024; ;;coordonnées théoriques dans le système CCS au 29-JUL-2024 15:19:42;
BEAM_926P_2024 ;BAV.3.S                         ;   11.43182; 1092.80543; 5194.17924;2460.01041;460.82659;;;  .014544;109.0321;;;29-JUL-2024; ;;coordonnées théoriques dans le système CCS au 29-JUL-2024 15:19:42;
*FRAME RSTI_926P_2024.BAV.3   1092.409492   5194.235794   2460.004595 0 0  109.032069 1
*FRAME RST_926P_2024.BAV.3 0 0 0 -.92589979693 0 0 1
*FRAME RSTRI_926P_2024.BAV.3     -0.009958     -0.098902     -0.002083 .68199197536 0 -1.82124460677 1 TX TY TZ RX RZ 
*FRAME RSTR_926P_2024.BAV.3 0 0 0 0 0 0 1 RY
*CALA
BEAM_926P_2024.BAV.3.E                                           0.000000      0.000000      0.000000   $11.032 842130 coordonnées théoriques dans le système RST au 29-JUL-2024 15:19:42
BEAM_926P_2024.BAV.3.S                                           0.000000      0.400000      0.000000   $11.432 842131 coordonnées théoriques dans le système RST au 29-JUL-2024 15:19:42
*OBSXYZ
926P_2024.BAV.3.H     -0.112934      0.079800     -0.126667 0.1 0.1 0.1   $11.112 842341 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAV.3.C      0.068158      0.305872     -0.046479 0.1 0.1 0.1   $11.338 842342 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAV.3.E      0.088463      0.106925     -0.077012 0.1 0.1 0.1   $11.139 842343 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAV.3.G      0.104230      0.035367     -0.126611 0.1 0.1 0.1   $11.067 842344 paramètres RST 29-JUL-2024 15:19:42
926P_2024.BAV.3.S     -0.001132      0.340763      0.005130 0.1 0.1 0.1   $11.373 842345 paramètres RST 29-JUL-2024 15:19:42
*INCLY Instr_Roll_Theo
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
";
            string path = testMethod.Name + ".dat";
            Tools.Artefact.CreateArtefact(fileContent: datContent, artefactName: path);
        }

        /// </summary>
        [TestMethod()]
        public void L2_BAP_Step1_Start_with_dat_file_and_select_instrument_Test()
        {
            var datPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BAP_Step0_dat_file_Test) + ".dat"); // here first so that the test fails if the file is not existing
           
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""252"" trace=""Advanced modules+"" Button=""Left"" Direction=""Down"" Xpos=""289"" Ypos=""373"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""59"" trace=""Advanced modules+"" Button=""Left"" Direction=""Up"" Xpos=""289"" Ypos=""373"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Advanced modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Advanced modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""273"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""431"" Ypos=""402"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""70"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""431"" Ypos=""402"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""251"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""609"" Ypos=""301"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""64"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""609"" Ypos=""301"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""239"" trace=""New Station+"" Button=""Left"" Direction=""Down"" Xpos=""708"" Ypos=""337"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""84"" trace=""New Station+"" Button=""Left"" Direction=""Up"" Xpos=""708"" Ypos=""337"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: New Station (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: New Station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""290"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""642"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""53"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""642"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""213"" trace=""Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""754"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""57"" trace=""Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""754"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Instrument (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""351"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""178"" Ypos=""364"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""59"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""178"" Ypos=""364"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""207"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""220"" Ypos=""425"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""70"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""220"" Ypos=""425"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""283"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""327"" Ypos=""464"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""40"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""327"" Ypos=""464"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""261"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""908"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""53"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""908"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""MB: Select instrument: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Instrument end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
            Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

            var mms = tsunami.MeasurementModules;
            Assert.IsTrue(mms.Count > 0, $"No measurement modules");

            var apm = tsunami.MeasurementModules[0] as TSU.Common.FinalModule;
            Assert.IsTrue(apm != null, "Guided Group Module object is NULL");


            var instrument = apm._ActiveStationModule._InstrumentManager.SelectedInstrument._Model;
            Assert.IsTrue(instrument == "T2", "T2 not selected");

            apm._ElementManager.AddElementsFromFile(new FileInfo(datPath), TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes.CCS, true, TSU.Common.Elements.Point.Types.Reference, updateView: false);
            tsunami.BOC_Context.SelectedGeodeFilePaths.Add(datPath);
            test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

            Assert.IsTrue(tsunami != null, "TSUNAMI oject is NULL");
            var count = tsunami.Points.Count;
            Assert.IsTrue(count == 28, $"Expecting 14 (not {count} points in BGM_Step1_Start_with_dat_file_Test.tsu");
            });
        }

        [TestMethod()]
        public void L2_BAP_Step2_Setup_and_points_selection_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BAP_Step1_Start_with_dat_file_and_select_instrument_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""2133"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""468"" Ypos=""391"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""57"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""468"" Ypos=""391"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""891"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""623"" Ypos=""515"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""48"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""623"" Ypos=""515"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""524"" trace=""Known position+"" Button=""Left"" Direction=""Down"" Xpos=""677"" Ypos=""532"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""55"" trace=""Known position+"" Button=""Left"" Direction=""Up"" Xpos=""677"" Ypos=""532"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Known position (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""246"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""248"" Ypos=""703"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""41"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""248"" Ypos=""703"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""217"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""824"" Ypos=""916"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""55"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""824"" Ypos=""916"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""MB: Point stationned: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Known position end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""265"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""695"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""46"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""695"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""239"" trace=""Manually set the V0+"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""705"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""72"" trace=""Manually set the V0+"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""705"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: Manually set the V0 (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""201"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""902"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""54"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""902"" Ypos=""578"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""MB: Enter manually the v0 of the station: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Manually set the V0 end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""218"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""462"" Ypos=""389"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""462"" Ypos=""389"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""228"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""616"" Ypos=""429"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""63"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""616"" Ypos=""429"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""289"" trace=""Quick measure+"" Button=""Left"" Direction=""Down"" Xpos=""746"" Ypos=""759"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""74"" trace=""Quick measure+"" Button=""Left"" Direction=""Up"" Xpos=""746"" Ypos=""759"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Quick measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Quick measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""246"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""425"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""63"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""425"" Ypos=""263"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""207"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""250"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""294"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""88"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""294"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""250"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""574"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""574"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""293"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Down"" Xpos=""711"" Ypos=""369"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""89"" trace=""Add known Point(s)+"" Button=""Left"" Direction=""Up"" Xpos=""711"" Ypos=""369"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Add known Point(s) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""263"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""638"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""88"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""638"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""857"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{M}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""186"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""442"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""162"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{N}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""3078"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""85"" Ypos=""324"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""52"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""85"" Ypos=""324"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""256"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""82"" Ypos=""352"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""64"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""85"" Ypos=""353"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""250"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""77"" Ypos=""385"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""68"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""81"" Ypos=""384"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""255"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""80"" Ypos=""406"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""39"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""80"" Ypos=""406"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""232"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""865"" Ypos=""930"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""68"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""865"" Ypos=""930"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""MB: Elements Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: Add known Point(s) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""432"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""749"" Ypos=""670"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""68"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""749"" Ypos=""670"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""237"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1285"" Ypos=""898"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""86"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1285"" Ypos=""898"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""BB: Valid (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""454"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""748"" Ypos=""754"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""748"" Ypos=""754"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""291"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""968"" Ypos=""744"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""68"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""968"" Ypos=""744"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""499"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""579"" Ypos=""683"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""36"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""579"" Ypos=""683"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""233"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1275"" Ypos=""886"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""38"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1275"" Ypos=""886"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""483"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""662"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""36"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""662"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""266"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1299"" Ypos=""885"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1300"" Ypos=""885"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""47"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""258"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""634"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""57"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""634"" Ypos=""299"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""262"" trace=""Show 'Pinned Points Table (PPT)'+"" Button=""Left"" Direction=""Down"" Xpos=""793"" Ypos=""491"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""70"" trace=""Show 'Pinned Points Table (PPT)'+"" Button=""Left"" Direction=""Up"" Xpos=""793"" Ypos=""491"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""53"" Delay=""0"" Action=""BB: Show 'Pinned Points Table (PPT)' (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""54"" Delay=""0"" Action=""BB: Hide table  end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""219"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""633"" Ypos=""291"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""60"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""633"" Ypos=""291"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""373"" trace=""Pin all 'Measured' points+"" Button=""Left"" Direction=""Down"" Xpos=""778"" Ypos=""402"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""93"" trace=""Pin all 'Measured' points+"" Button=""Left"" Direction=""Up"" Xpos=""778"" Ypos=""402"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""59"" Delay=""0"" Action=""BB: Pin all 'Measured' points (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""60"" Delay=""0"" Action=""BB: Pin all 'Measured' points end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var mms = tsunami.MeasurementModules;
                Assert.IsTrue(mms.Count > 0, $"No measurement modules");

                var apm = tsunami.MeasurementModules[0] as TSU.Common.FinalModule;
                Assert.IsTrue(apm != null, " Module object is NULL");

                var station = apm._ActiveStationModule._Station;

                Assert.IsTrue(station.ParametersBasic._IsSetup, $"station not setup");

                var measures = station.MeasuresTaken;
                Assert.IsTrue(measures.Count == 3, $"{measures.Count} measure instead of 3");

                //Assert.IsTrue(count == 28, $"Expecting 14 (not {count} points in BGM_Step1_Start_with_dat_file_Test.tsu");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }


        /// <summary>
        /// compute boc with 3 points E A B, then mesaure back the 3 plus a 4th that you compute smooth
        /// </summary>
        [TestMethod()]
        public void L2_BAP_Step3_BOC_3POINTS_compute_4POINTS_compute_NewSetup_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BAP_Step2_Setup_and_points_selection_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>    
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""385"" trace=""Polar Module+"" Button=""Left"" Direction=""Down"" Xpos=""1250"" Ypos=""121"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""42"" trace=""Polar Module+"" Button=""Left"" Direction=""Up"" Xpos=""1250"" Ypos=""121"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Polar Module (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""BB: Polar Module end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""322"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1127"" Ypos=""188"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""46"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1126"" Ypos=""187"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: STtheo_240830_164431_926P_2024_BAM_1_G (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: STtheo_240830_164431_926P_2024_BAM_1_G end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""372"" trace=""Pinned Point Table Options+"" Button=""Left"" Direction=""Down"" Xpos=""1361"" Ypos=""617"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""66"" trace=""Pinned Point Table Options+"" Button=""Left"" Direction=""Up"" Xpos=""1361"" Ypos=""617"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Pinned Point Table Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Pinned Point Table Options end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""373"" trace=""Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1516"" Ypos=""733"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""66"" trace=""Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1516"" Ypos=""733"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Beam Offsets end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""351"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1569"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""19"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1569"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />

{Tools.CommentWithinTheMacro("force refresh by clicking the moduel menu buton")}
    
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""373"" trace="""" Button=""Left"" Direction=""Down"" Xpos=""217"" Ypos=""510"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""6"" trace="""" Button=""Left"" Direction=""Up"" Xpos=""217"" Ypos=""510"" Delta=""0"" />
    

{Tools.CommentWithinTheMacro("close BOC notif")}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""373"" trace="""" Button=""Left"" Direction=""Down"" Xpos=""38"" Ypos=""830"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""6"" trace="""" Button=""Left"" Direction=""Up"" Xpos=""38"" Ypos=""830"" Delta=""0"" />
    

    {Tools.CommentWithinTheMacro("measure.E")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""2417"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1114"" Ypos=""568"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1114"" Ypos=""568"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""322"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""754"" Ypos=""716"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""754"" Ypos=""716"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""226"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""918"" Ypos=""713"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""97"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""918"" Ypos=""713"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""309"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""789"" Ypos=""666"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""99"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""789"" Ypos=""666"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""365"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1277"" Ypos=""863"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""105"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1277"" Ypos=""863"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Valid (beginning)"" />


    {Tools.CommentWithinTheMacro("measure.E")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1075"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""858"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""858"" Ypos=""740"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1075"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1114"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""98"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1114"" Ypos=""740"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: Measure comparaison: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""374"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""804"" Ypos=""550"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""804"" Ypos=""550"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""329"" trace=""button1+Always"" Button=""Left"" Direction=""Down"" Xpos=""933"" Ypos=""555"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""90"" trace=""button1+Always"" Button=""Left"" Direction=""Up"" Xpos=""933"" Ypos=""555"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""MB: Already Measured: Always"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Valid end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""333"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1114"" Ypos=""608"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1114"" Ypos=""608"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""354"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""758"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""758"" Ypos=""680"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""317"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1255"" Ypos=""859"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""57"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1255"" Ypos=""859"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""52"" Delay=""0"" Action=""BB: Valid end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""378"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1893"" Ypos=""617"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""55"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1893"" Ypos=""617"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""358"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1114"" Ypos=""609"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1114"" Ypos=""609"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""390"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""963"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""963"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""318"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1332"" Ypos=""897"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""81"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1332"" Ypos=""897"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""61"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""62"" Delay=""0"" Action=""BB: Valid end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""3073"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1892"" Ypos=""618"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""30"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1892"" Ypos=""618"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1892"" Ypos=""618"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1892"" Ypos=""618"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""377"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1892"" Ypos=""618"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""05"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1892"" Ypos=""618"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""349"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1114"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1114"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""398"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""871"" Ypos=""685"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""871"" Ypos=""685"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""357"" trace=""Valid+"" Button=""Left"" Direction=""Down"" Xpos=""1362"" Ypos=""882"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""98"" trace=""Valid+"" Button=""Left"" Direction=""Up"" Xpos=""1362"" Ypos=""882"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""75"" Delay=""0"" Action=""BB: Valid (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""76"" Delay=""0"" Action=""BB: Valid end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""322"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1127"" Ypos=""188"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""46"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1126"" Ypos=""187"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: STtheo_240830_164431_926P_2024_BAM_1_G (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: STtheo_240830_164431_926P_2024_BAM_1_G end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""372"" trace=""Pinned Point Table Options+"" Button=""Left"" Direction=""Down"" Xpos=""1361"" Ypos=""617"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""66"" trace=""Pinned Point Table Options+"" Button=""Left"" Direction=""Up"" Xpos=""1361"" Ypos=""617"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Pinned Point Table Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Pinned Point Table Options end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""373"" trace=""Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1516"" Ypos=""733"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""66"" trace=""Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1516"" Ypos=""733"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""BB: Beam Offsets end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""351"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1569"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""19"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1569"" Ypos=""760"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""43"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""3346"" trace=""STtheo_240830_164431_926P_2024_BAM_1_G+"" Button=""Left"" Direction=""Down"" Xpos=""830"" Ypos=""202"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""73"" trace=""STtheo_240830_164431_926P_2024_BAM_1_G+"" Button=""Left"" Direction=""Up"" Xpos=""830"" Ypos=""202"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: STtheo_240830_164431_926P_2024_BAM_1_G (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: STtheo_240830_164431_926P_2024_BAM_1_G end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""247"" trace=""Pinned Point Table Options+"" Button=""Left"" Direction=""Down"" Xpos=""971"" Ypos=""623"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""52"" trace=""Pinned Point Table Options+"" Button=""Left"" Direction=""Up"" Xpos=""971"" Ypos=""623"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Pinned Point Table Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Pinned Point Table Options end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""286"" trace=""Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1118"" Ypos=""734"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""49"" trace=""Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1118"" Ypos=""734"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: Beam Offsets end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""288"" trace=""Setup Beam Offsets compute+"" Button=""Left"" Direction=""Down"" Xpos=""1243"" Ypos=""841"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""30"" trace=""Setup Beam Offsets compute+"" Button=""Left"" Direction=""Up"" Xpos=""1243"" Ypos=""841"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Setup Beam Offsets compute (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""1892"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""881"" Ypos=""752"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""96"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""881"" Ypos=""752"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""MB: Setup of Beam points offsets computation for 'Polar type' observations: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""20"" Action=""BB: Setup Beam Offsets compute end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""239"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""878"" Ypos=""566"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""239"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""878"" Ypos=""566"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""1892"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""881"" Ypos=""752"" Delta=""0"" />
    
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var bocContext = Tsunami2.Properties.BOC_Context;
                Assert.IsTrue(bocContext != null, $"No boc context");

                var results = bocContext.ExistingResults;
                string listOfBoc = "";
                foreach (var item in results)
                {
                    listOfBoc += item.AssemblyId + item.Parameters.ToString() + "\r\n";
                }
                Assert.IsTrue(results.Count == 3, $"{results.Count} boc results instead of 3: {listOfBoc}");

                var measures = Tsunami2.Properties.MeasurementModules[0].StationModules[0]._Station.MeasuresTaken;
                Assert.IsTrue(measures.Count == 7, $"{measures.Count} computed points instead of 7 in 1st compute");
                Assert.IsTrue(measures[0]._Point._Name == "926P_2024.MAIN.2.E", $"no 926P_2024.MAIN.2.E");
                Assert.IsTrue(measures[1]._Point._Name == "926P_2024.MAIN.2.A", $"no 926P_2024.MAIN.2.A");
                Assert.IsTrue(measures[2]._Point._Name == "926P_2024.MAIN.2.B", $"no 926P_2024.MAIN.2.B");
                Assert.IsTrue(measures[3]._Point._Name == "926P_2024.MAIN.2.E", $"no 926P_2024.MAIN.2.E");
                Assert.IsTrue(measures[4]._Point._Name == "926P_2024.MAIN.2.A", $"no 926P_2024.MAIN.2.A");
                Assert.IsTrue(measures[5]._Point._Name == "926P_2024.MAIN.2.B", $"no 926P_2024.MAIN.2.B");
                Assert.IsTrue(measures[6]._Point._Name == "926P_2024.MAIN.2.G", $"no 926P_2024.MAIN.2.G");

                var results0 = bocContext.ExistingResults[0];
                Assert.IsTrue(results0.ComputedPoints.Count == 7, $"{results0.ComputedPoints.Count} computed points instead of 7 in 1st compute");
                Assert.IsTrue(results0.Parameters.PointsToUseAsOBSXYZ.Exists(x=>x.Used && x.Point == "926P_2024.MAIN.2.A"), $"Expecting usage of \"926P_2024.MAIN.2.A\" in the boc");
                Assert.IsTrue(results0.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.B"), $"Expecting usage of \"926P_2024.MAIN.2.B\" in the boc");
                Assert.IsTrue(results0.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.E"), $"Expecting usage of \"926P_2024.MAIN.2.E\" in the boc");

                var results1 = bocContext.ExistingResults[1];
                Assert.IsTrue(results1.ComputedPoints.Count == 6, $"{results1.ComputedPoints.Count} computed points instead of 6 in 2th compute");
                
                Assert.IsTrue(results1.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.A"), $"Expecting usage of \"926P_2024.MAIN.2.A\" in the boc");
                Assert.IsTrue(results1.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.B"), $"Expecting usage of \"926P_2024.MAIN.2.B\" in the boc");
                Assert.IsTrue(results1.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.E"), $"Expecting usage of \"926P_2024.MAIN.2.E\" in the boc");
                Assert.IsTrue(results1.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.G"), $"Expecting usage of \"926P_2024.MAIN.2.G\" in the boc");

                var results2 = bocContext.ExistingResults[2];
                Assert.IsTrue(results2.ComputedPoints.Count == 6, $"{results2.ComputedPoints.Count} computed points instead of 6 in 3th compute");
                Assert.IsTrue(results2.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.A"), $"Expecting usage of \"926P_2024.MAIN.2.A\" in the boc");
                Assert.IsTrue(results2.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.B"), $"Expecting usage of \"926P_2024.MAIN.2.B\" in the boc");
                Assert.IsTrue(results2.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.E"), $"Expecting usage of \"926P_2024.MAIN.2.E\" in the boc");
                Assert.IsTrue(results2.Parameters.PointsToUseAsOBSXYZ.Exists(x => x.Used && x.Point == "926P_2024.MAIN.2.G"), $"Expecting usage of \"926P_2024.MAIN.2.G\" in the boc");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}
