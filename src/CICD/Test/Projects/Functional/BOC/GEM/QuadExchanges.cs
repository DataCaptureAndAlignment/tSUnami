﻿using Functional_Tests;
using MathNet.Numerics.LinearRegression;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Measures;
using TSU.Line.GuidedModules;
using UnitTestGenerator;

namespace Functional_Tests.BOC.GEM
{
    [TestClass]
    public class QuadExchanges
    {
        [TestMethod()]
        public void L2_QE_Step00_dat_file_Test()
        {
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"); // used to fail if the splashscreen test had an error
            string datContent = @"SPS       ;QF.43210.E                      ; 4479.79841; 2604.47317; 4163.75034;2402.27820;402.64103;  .095; -.000174;  .000226;163.9092; 1;A;06-NOV-2024;  ;;;
SPS       ;QF.43210.S                      ; 4482.64368; 2606.00117; 4161.35017;2402.27884;402.64104; 3.085; -.000174;  .000226;163.9092; 1;O;06-NOV-2024;  ;;;
SPS       ;QD.43310.E                      ; 4511.79612; 2621.21009; 4136.48126;2402.28547;402.64113;  .095; -.000174;  .000229;166.0598; 1;A;06-NOV-2024;  ;;;
SPS       ;QD.43310.S                      ; 4514.64205; 2622.65649; 4134.03029;2402.28612;402.64114; 3.085; -.000174;  .000229;166.0598; 1;O;06-NOV-2024;  ;;;
SPS       ;QF.43410.E                      ; 4543.79382; 2637.01589; 4108.66214;2402.29282;402.64120;  .095; -.000174;  .000231;168.2103; 1;A;06-NOV-2024;  ;;;
SPS       ;QF.43410.S                      ; 4546.63695; 2638.37734; 4106.16617;2402.29347;402.64120; 3.085; -.000174;  .000231;168.2103; 1;O;06-NOV-2024;  ;;;

";
            Tools.Artefact.CreateArtefact(fileContent: datContent, artefactName: "L2_QE_Step00_dat_file.dat");
        }

        /// <summary>
        /// start a GEM module and rogrammatically load a dat file (because macro do not record keys in opening and saving windows :(
        /// 
        /// </summary>
            [TestMethod()]
            public void L2_QE_Step01_Start_with_dat_file_Test()
            {
                var datPath = Tools.Artefact.GetArtefactFullPath("L2_QE_Step00_dat_file.dat"); // here first so that the test fails if the file is not existing
           
                var testMethod = MethodBase.GetCurrentMethod();
                F_Test test = new F_Test(testMethod)
                {
                    NewPlacementForMessage = true,
                    MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
    <Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
      IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
      <KeyStrokesAndMouseClicks>
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""234"" Ypos=""281"" Delta=""0"" />
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""234"" Ypos=""281"" Delta=""0"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Guided modules (beginning)"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Guided modules end"" />
	  
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""371"" Ypos=""300"" Delta=""0"" />
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""371"" Ypos=""300"" Delta=""0"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Element alignment (beginning)"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Element alignment end"" />
	  
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""1000"" Button=""Left"" Direction=""Down"" Xpos=""1316"" Ypos=""960"" Delta=""0"" />
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""1316"" Ypos=""960"" Delta=""0"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Next step (beginning)"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Next step end"" />
      </KeyStrokesAndMouseClicks>
    </Macro>"
                };

                test.Execute(testAction: (tsunami) =>
                {
                    Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");


                    var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;
                    Assert.IsTrue(ggm != null, "Guided Group Module object is NULL");

                    var sgm = ggm.SubGuidedModules;
                    Assert.IsTrue(sgm.Count > 0 , $"no sub Guided Group Module found in {ggm}");

                    var gmp = sgm[4] as TSU.Common.Guided.Module;
                    Assert.IsTrue(ggm != null, $"sub Guided Group Module are missing suppossed to be 5 we have: {sgm.Count}");

                    ggm._ElementManager.AddElementsFromFile(new FileInfo(datPath), TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes.CCS, true, TSU.Common.Elements.Point.Types.Reference, updateView: false);
                    tsunami.BOC_Context.SelectedGeodeFilePaths.Add(datPath);
                    test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                    Assert.IsTrue(tsunami != null, "TSUNAMI oject is NULL");
                    Assert.IsTrue(tsunami.Points.Count == 6, "Expecting 6 points in QE_Step1_Start_with_dat_file_Test.tsu");
                });
            }

        /// <summary>
        /// start a GEM module and rogrammatically load a dat file (because macro do not record keys in opening and saving windows :(
        /// 
        /// </summary>
        [TestMethod()]
        public void L2_QE_Step02_Start_with_dat_file_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath("L2_QE_Step01_Start_with_dat_file_Test.tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
    <Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" Name=""{testMethod.Name}"">
<WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""1""/>
<KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""403"" trace=""Open Operation Manager to select one+"" Button=""Left"" Direction=""Down"" Xpos=""750"" Ypos=""432"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""47"" trace=""Open Operation Manager to select one+"" Button=""Left"" Direction=""Up"" Xpos=""750"" Ypos=""432"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Open Operation Manager to select one (beginning)""/>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""326"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""175"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""175"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""316"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""213"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""213"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""316"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""365"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""365"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""341"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""885"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""106"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""885"" Ypos=""925"" Delta=""0"" />
<IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Open Operation Manager to select one end""/>


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""356"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{M}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""31"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{O}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />




<IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""2109"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1324"" Ypos=""933"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""18"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1324"" Ypos=""933"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""2225"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""429"" Ypos=""398"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""429"" Ypos=""398"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""518"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""432"" Ypos=""430"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""41"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""432"" Ypos=""430"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""567"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""433"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""51"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""433"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""1070"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1339"" Ypos=""941"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""59"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1339"" Ypos=""941"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Next step: (press 'n') end""/>
</KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "3 manget should be selected");
                Assert.AreEqual("SPS.QF.43210", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be SPS.QF.43210");
                Assert.AreEqual("SPS.QD.43310", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be SPS.QD.43310");
                Assert.AreEqual("SPS.QF.43410", gmp.MagnetsToBeAligned[2]._Name, "magnet to align 1 must be SPS.QF.43410");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_QE_Step03_Start_with_polar_setup_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath("L2_QE_Step02_Start_with_dat_file_Test.tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
    <Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" Name=""{testMethod.Name}"">
<WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""1""/>
<KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""349"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Down"" Xpos=""1180"" Ypos=""859"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""37"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Up"" Xpos=""1180"" Ypos=""859"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Polar Alignment (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Polar Alignment end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""307"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1012"" Ypos=""256"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""19"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1012"" Ypos=""256"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Free station (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""73"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1067"" Ypos=""649"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""17"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1067"" Ypos=""649"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""MB: Station point name?: OK""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Free station end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""338"" trace=""Select Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""1552"" Ypos=""446"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""98"" trace=""Select Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""1552"" Ypos=""446"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Select Instrument (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""381"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""114"" Ypos=""362"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""09"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""114"" Ypos=""362"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""307"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""144"" Ypos=""432"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""32"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""144"" Ypos=""432"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""314"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""250"" Ypos=""487"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""6"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""250"" Ypos=""487"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""250"" Ypos=""488"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""250"" Ypos=""488"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: Select Instrument end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""346"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""1549"" Ypos=""562"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""105"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""1549"" Ypos=""562"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Change Reflector (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""379"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""160"" Ypos=""486"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""160"" Ypos=""486"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""66"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""160"" Ypos=""486"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""160"" Ypos=""486"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""31"" Delay=""0"" Action=""BB: Change Reflector end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""370"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Down"" Xpos=""1467"" Ypos=""643"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""13"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Up"" Xpos=""1467"" Ypos=""643"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Switch between 1 face and double face (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Switch between 1 face and double face end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""331"" trace=""Number of measurement+"" Button=""Left"" Direction=""Down"" Xpos=""1493"" Ypos=""733"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""11"" trace=""Number of measurement+"" Button=""Left"" Direction=""Up"" Xpos=""1493"" Ypos=""733"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Number of measurement (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""321"" trace=""button4+1"" Button=""Left"" Direction=""Down"" Xpos=""995"" Ypos=""592"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""11"" trace=""button4+1"" Button=""Left"" Direction=""Up"" Xpos=""995"" Ypos=""592"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""MB: Number of measurement: 1""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""42"" Delay=""0"" Action=""BB: Number of measurement end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""369"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1412"" Ypos=""937"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""10"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1412"" Ypos=""938"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""46"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""83"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""428"" Ypos=""423"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""16"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""428"" Ypos=""423"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""436"" Ypos=""447"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""436"" Ypos=""447"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""329"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""435"" Ypos=""480"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""435"" Ypos=""480"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""310"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""434"" Ypos=""512"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""433"" Ypos=""512"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""334"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1372"" Ypos=""918"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1372"" Ypos=""919"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""57"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""58"" Delay=""0"" Action=""BB: Next step end""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1361"" Ypos=""919"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""15"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1361"" Ypos=""919"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""61"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""62"" Delay=""0"" Action=""BB: Next step end""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""373"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""539"" Ypos=""288"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""15"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""539"" Ypos=""288"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""393"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""953"" Ypos=""715"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""9"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""953"" Ypos=""715"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""67"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""342"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""482"" Ypos=""255"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""15"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""482"" Ypos=""255"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""70"" Delay=""68"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""71"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""72"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""73"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""74"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""337"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""786"" Ypos=""948"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""12"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""786"" Ypos=""948"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""77"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""361"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1082"" Ypos=""651"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""10"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1082"" Ypos=""651"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""80"" Delay=""0"" Action=""MB: Point successfully measured: OK!""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""377"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""569"" Ypos=""320"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""82"" Delay=""16"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""569"" Ypos=""320"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""361"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""933"" Ypos=""741"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""10"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""933"" Ypos=""741"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""85"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""451"" Ypos=""278"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""87"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""451"" Ypos=""278"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""88"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""89"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""09"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""92"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""93"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitterPanel""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""94"" Delay=""318"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""845"" Ypos=""944"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""10"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""845"" Ypos=""944"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""96"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""97"" Delay=""89"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""984"" Ypos=""660"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""98"" Delay=""11"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""984"" Ypos=""660"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""99"" Delay=""0"" Action=""MB: Point successfully measured: OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""370"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""562"" Ypos=""367"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""101"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""562"" Ypos=""367"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""102"" Delay=""300"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""966"" Ypos=""712"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""11"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""966"" Ypos=""712"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""104"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""357"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""392"" Ypos=""264"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""392"" Ypos=""264"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""107"" Delay=""265"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""109"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""110"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""111"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""112"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""113"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""114"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""115"" Delay=""17"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""803"" Ypos=""936"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""116"" Delay=""12"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""805"" Ypos=""936"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""117"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""118"" Delay=""302"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""920"" Ypos=""619"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""119"" Delay=""16"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""920"" Ypos=""620"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""120"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""313"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1094"" Ypos=""689"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""122"" Delay=""19"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1094"" Ypos=""689"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""123"" Delay=""0"" Action=""MB: Point successfully measured: OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""346"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""538"" Ypos=""424"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""125"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""538"" Ypos=""424"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""126"" Delay=""310"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""948"" Ypos=""747"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""127"" Delay=""12"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""948"" Ypos=""747"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""128"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""129"" Delay=""324"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""406"" Ypos=""256"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""130"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""406"" Ypos=""256"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""131"" Delay=""101"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""132"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""133"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""134"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""135"" Delay=""61"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""136"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""137"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""138"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""139"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""140"" Delay=""360"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""844"" Ypos=""928"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""141"" Delay=""11"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""845"" Ypos=""928"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""142"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""143"" Delay=""397"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""930"" Ypos=""633"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""144"" Delay=""14"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""930"" Ypos=""633"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""145"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""146"" Delay=""369"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1037"" Ypos=""653"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""147"" Delay=""14"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1037"" Ypos=""653"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""148"" Delay=""0"" Action=""MB: Point successfully measured: OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""149"" Delay=""337"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1457"" Ypos=""957"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""150"" Delay=""13"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1457"" Ypos=""957"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""151"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""152"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""153"" Delay=""394"" trace=""Ctrl+"" Button=""Left"" Direction=""Down"" Xpos=""716"" Ypos=""292"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""154"" Delay=""16"" trace=""Ctrl+"" Button=""Left"" Direction=""Up"" Xpos=""716"" Ypos=""292"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""155"" Delay=""0"" Action=""BB: Control 'opening' (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""156"" Delay=""0"" Action=""BB: Control 'opening' end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""157"" Delay=""310"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""979"" Ypos=""715"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""158"" Delay=""10"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""979"" Ypos=""715"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""159"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""160"" Delay=""372"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""535"" Ypos=""272"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""161"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""535"" Ypos=""272"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""162"" Delay=""374"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""163"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""164"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""165"" Delay=""11"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""166"" Delay=""15"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""167"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""168"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""169"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""170"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""171"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""172"" Delay=""67"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""173"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""174"" Delay=""386"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""890"" Ypos=""947"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""175"" Delay=""18"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""890"" Ypos=""947"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""176"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""177"" Delay=""337"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""924"" Ypos=""606"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""178"" Delay=""15"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""924"" Ypos=""606"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""179"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""180"" Delay=""317"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""941"" Ypos=""605"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""181"" Delay=""18"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""941"" Ypos=""605"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""182"" Delay=""0"" Action=""MB: Closure OK: OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""183"" Delay=""369"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1317"" Ypos=""934"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""184"" Delay=""14"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1317"" Ypos=""934"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""185"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""186"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""187"" Delay=""346"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""1009"" Ypos=""321"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""188"" Delay=""13"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""1009"" Ypos=""321"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""189"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""190"" Delay=""311"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""904"" Ypos=""789"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""191"" Delay=""12"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""904"" Ypos=""789"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""192"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""193"" Delay=""370"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""892"" Ypos=""942"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""194"" Delay=""9"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""892"" Ypos=""942"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""195"" Delay=""0"" Action=""MB: Compensation: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""196"" Delay=""398"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""854"" Ypos=""563"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""197"" Delay=""30"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""854"" Ypos=""563"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""198"" Delay=""0"" Action=""MB: Are you sure?: Yes""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""199"" Delay=""0"" Action=""BB: Computing (LGC2) end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""200"" Delay=""3178"" trace=""Next step blocked+"" Button=""Left"" Direction=""Down"" Xpos=""1233"" Ypos=""933"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""201"" Delay=""80"" trace=""Next step blocked+"" Button=""Left"" Direction=""Up"" Xpos=""1233"" Ypos=""933"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""202"" Delay=""0"" Action=""BB: Next step blocked (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""203"" Delay=""362"" trace=""button2+Force next step"" Button=""Left"" Direction=""Down"" Xpos=""1071"" Ypos=""535"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""204"" Delay=""113"" trace=""button2+Force next step"" Button=""Left"" Direction=""Up"" Xpos=""1071"" Ypos=""535"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""205"" Delay=""0"" Action=""MB: Next step blocked: Force next step""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""206"" Delay=""0"" Action=""BB: Next step blocked end""/>
</KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "3 manget should be selected");
                Assert.AreEqual("SPS.QF.43210", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be SPS.QF.43210");
                Assert.AreEqual("SPS.QD.43310", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be SPS.QD.43310");
                Assert.AreEqual("SPS.QF.43410", gmp.MagnetsToBeAligned[2]._Name, "magnet to align 1 must be SPS.QF.43410");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_QE_Step04_Tilt_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath("L2_QE_Step02_Start_with_dat_file_Test.tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
    <Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" Name=""{testMethod.Name}"">
<WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""1""/>
<KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""363"" trace=""Roll+"" Button=""Left"" Direction=""Down"" Xpos=""598"" Ypos=""850"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""65"" trace=""Roll+"" Button=""Left"" Direction=""Up"" Xpos=""598"" Ypos=""850"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Roll (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Roll end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""395"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""470"" Ypos=""452"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""24"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""470"" Ypos=""452"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""358"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""470"" Ypos=""428"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""470"" Ypos=""428"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""379"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""464"" Ypos=""356"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""466"" Ypos=""356"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""388"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""578"" Ypos=""685"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""578"" Ypos=""685"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""193"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""578"" Ypos=""685"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""578"" Ypos=""685"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""359"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""592"" Ypos=""499"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""592"" Ypos=""499"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""182"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""592"" Ypos=""499"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""592"" Ypos=""499"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""344"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1321"" Ypos=""947"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""154"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1321"" Ypos=""947"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""21"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""381"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1028"" Ypos=""469"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""18"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1028"" Ypos=""469"" Delta=""0""/>
{Tools.GetKeySequence("-0.000170", "SplitContainer")}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""361"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1251"" Ypos=""683"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""33"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1252"" Ypos=""683"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""354"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""2"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1404"" Ypos=""962"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""113"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1404"" Ypos=""962"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""49"" Delay=""0"" Action=""BB: Next step end""/>
</KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "3 manget should be selected");
                Assert.AreEqual("SPS.QF.43210", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be SPS.QF.43210");
                Assert.AreEqual("SPS.QD.43310", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be SPS.QD.43310");
                Assert.AreEqual("SPS.QF.43410", gmp.MagnetsToBeAligned[2]._Name, "magnet to align 1 must be SPS.QF.43410");
                Assert.AreEqual(-0.000174, gmp.MagnetsToBeAligned[1]._Parameters.Tilt, "Tilt should be -0.000174");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_QE_Step05_Wire_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath("L2_QE_Step02_Start_with_dat_file_Test.tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
    <Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" Name=""{testMethod.Name}"">
<WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""1""/>
<KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""347"" trace=""Radial+"" Button=""Left"" Direction=""Down"" Xpos=""1486"" Ypos=""858"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""11"" trace=""Radial+"" Button=""Left"" Direction=""Up"" Xpos=""1486"" Ypos=""858"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Radial (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Radial end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""390"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""529"" Ypos=""394"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""6"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""529"" Ypos=""394"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""212"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""529"" Ypos=""394"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""1"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""529"" Ypos=""394"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""302"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1314"" Ypos=""944"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""11"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1314"" Ypos=""944"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""341"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""847"" Ypos=""401"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""847"" Ypos=""401"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""310"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1376"" Ypos=""934"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""14"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1376"" Ypos=""934"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: Next step: (press 'n') end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""335"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1254"" Ypos=""390"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1254"" Ypos=""390"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""314"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1253"" Ypos=""358"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1253"" Ypos=""358"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""326"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1255"" Ypos=""393"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1255"" Ypos=""393"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""347"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1253"" Ypos=""465"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""1523"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1253"" Ypos=""465"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""318"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1254"" Ypos=""507"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1254"" Ypos=""507"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""370"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""900"" Ypos=""417"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""1073"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""900"" Ypos=""417"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""329"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""905"" Ypos=""345"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""905"" Ypos=""345"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""32"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""39"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""41"" Delay=""23"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""42"" Delay=""34"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""43"" Delay=""23"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""44"" Delay=""66"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""45"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""46"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""47"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""48"" Delay=""334"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""49"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""50"" Delay=""86"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""51"" Delay=""40"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""52"" Delay=""32"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""53"" Delay=""80"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""55"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""56"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""57"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""58"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""59"" Delay=""23"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""60"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""61"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""62"" Delay=""15"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""63"" Delay=""59"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""64"" Delay=""09"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""65"" Delay=""40"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""66"" Delay=""34"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""67"" Delay=""80"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""68"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""69"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""70"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""71"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""72"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""73"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""74"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""75"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""76"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""78"" Delay=""23"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""79"" Delay=""86"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""3161"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1375"" Ypos=""950"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""14"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1375"" Ypos=""950"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""82"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""302"" trace=""button2+Don't"" Button=""Left"" Direction=""Down"" Xpos=""1039"" Ypos=""543"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""16"" trace=""button2+Don't"" Button=""Left"" Direction=""Up"" Xpos=""1039"" Ypos=""543"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""85"" Delay=""0"" Action=""MB: Wire not exported: Don't""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""86"" Delay=""0"" Action=""BB: Next step end""/>
</KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[3] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "3 manget should be selected");
                Assert.AreEqual("SPS.QF.43210", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be SPS.QF.43210");
                Assert.AreEqual("SPS.QD.43310", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be SPS.QD.43310");
                Assert.AreEqual("SPS.QF.43410", gmp.MagnetsToBeAligned[2]._Name, "magnet to align 1 must be SPS.QF.43410");

                Assert.AreEqual(6, gmp.ReceivedMeasures.Count, "6 Measures must have been received");
                Assert.AreEqual("43310", gmp.ReceivedMeasures.ElementAt(2)._Point._Numero, "Number of the point of the 3rd measure must be 43310");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_QE_Step06_Level1_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath("L2_QE_Step02_Start_with_dat_file_Test.tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
    <Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" Name=""{testMethod.Name}"">
<WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""0.8""/>
<KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""387"" trace=""Vertical+"" Button=""Left"" Direction=""Down"" Xpos=""866"" Ypos=""839"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""12"" trace=""Vertical+"" Button=""Left"" Direction=""Up"" Xpos=""866"" Ypos=""839"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Vertical (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Vertical end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""373"" trace=""Select Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""742"" Ypos=""294"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""12"" trace=""Select Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""742"" Ypos=""294"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Select Instrument (beginning)""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""374"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""225"" Ypos=""461"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""225"" Ypos=""461"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""50"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""225"" Ypos=""461"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""225"" Ypos=""461"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Select Instrument end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""381"" trace=""Select Default Main Staff+"" Button=""Left"" Direction=""Down"" Xpos=""677"" Ypos=""327"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""11"" trace=""Select Default Main Staff+"" Button=""Left"" Direction=""Up"" Xpos=""677"" Ypos=""327"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Select Default Main Staff (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""369"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""190"" Ypos=""365"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""190"" Ypos=""365"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""50"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""190"" Ypos=""365"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""190"" Ypos=""365"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""20"" Delay=""0"" Action=""BB: Select Default Main Staff end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""301"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1283"" Ypos=""930"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""12"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1283"" Ypos=""930"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""322"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""573"" Ypos=""397"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""576"" Ypos=""400"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""365"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1380"" Ypos=""935"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""12"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1380"" Ypos=""935"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Next step: (press 'n') end""/>
<!--
<IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""330"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1163"" Ypos=""405"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1163"" Ypos=""405"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""310"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1172"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""22"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1172"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""310"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1172"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""6"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1172"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""317"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1172"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1172"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""361"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1172"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1172"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""332"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1158"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1158"" Ypos=""459"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""43"" Delay=""147"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""44"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""45"" Delay=""32"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""46"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""47"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""48"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""49"" Delay=""43"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""50"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""51"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""52"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""53"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""14"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""55"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""56"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""57"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""58"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""59"" Delay=""23"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""60"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""61"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""62"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""63"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""64"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""65"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""66"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
-->
<IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""333"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""655"" Ypos=""455"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""655"" Ypos=""455"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""362"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""657"" Ypos=""493"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""657"" Ypos=""493"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""321"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""662"" Ypos=""527"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""662"" Ypos=""527"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""305"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""658"" Ypos=""553"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""658"" Ypos=""552"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""382"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1362"" Ypos=""947"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""12"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1363"" Ypos=""947"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""77"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""362"" trace=""button2+No"" Button=""Left"" Direction=""Down"" Xpos=""1013"" Ypos=""554"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""14"" trace=""button2+No"" Button=""Left"" Direction=""Up"" Xpos=""1013"" Ypos=""554"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""80"" Delay=""0"" Action=""MB: Exporting: No""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""81"" Delay=""0"" Action=""BB: Next step end""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""82"" Delay=""314"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1130"" Ypos=""393"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1130"" Ypos=""393"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""354"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1137"" Ypos=""399"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1137"" Ypos=""399"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""86"" Delay=""101"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""87"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""88"" Delay=""61"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""89"" Delay=""32"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""111"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""92"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""93"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""94"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""95"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""96"" Delay=""157"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""97"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""98"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""99"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""100"" Delay=""80"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""101"" Delay=""198"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""102"" Delay=""98"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""103"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""104"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""105"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""389"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1276"" Ypos=""917"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""107"" Delay=""12"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1276"" Ypos=""917"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""108"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""378"" trace=""button2+Yes"" Button=""Left"" Direction=""Down"" Xpos=""800"" Ypos=""533"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""110"" Delay=""10"" trace=""button2+Yes"" Button=""Left"" Direction=""Up"" Xpos=""800"" Ypos=""533"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""111"" Delay=""0"" Action=""MB: Exporting: No""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""112"" Delay=""357"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""988"" Ypos=""576"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""113"" Delay=""11"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""988"" Ypos=""576"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""114"" Delay=""0"" Action=""MB: Rms > 0.07 mm: OK!""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""115"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""116"" Delay=""386"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1108"" Ypos=""404"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""117"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1108"" Ypos=""404"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""118"" Delay=""342"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1108"" Ypos=""404"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""119"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1108"" Ypos=""404"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""120"" Delay=""394"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1133"" Ypos=""398"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1133"" Ypos=""398"" Delta=""0""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""122"" Delay=""378"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""123"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""124"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""125"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""126"" Delay=""96"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""127"" Delay=""184"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""128"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""129"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""130"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""131"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""132"" Delay=""165"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""133"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""134"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""135"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""136"" Delay=""87"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""137"" Delay=""172"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""138"" Delay=""61"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""139"" Delay=""32"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""140"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer""/>
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""141"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
<!--
<IInputActivity xsi:type=""MouseActivity"" Pos.=""142"" Delay=""317"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1465"" Ypos=""943"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""143"" Delay=""143"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1465"" Ypos=""943"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""144"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""145"" Delay=""313"" trace=""button2+Yes"" Button=""Left"" Direction=""Down"" Xpos=""899"" Ypos=""552"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""146"" Delay=""14"" trace=""button2+Yes"" Button=""Left"" Direction=""Up"" Xpos=""899"" Ypos=""552"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""147"" Delay=""0"" Action=""MB: Exporting: No""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""148"" Delay=""0"" Action=""BB: Next step end""/>
-->
</KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[2] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "3 manget should be selected");
                Assert.AreEqual("SPS.QF.43210", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be SPS.QF.43210");
                Assert.AreEqual("SPS.QD.43310", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be SPS.QD.43310");
                Assert.AreEqual("SPS.QF.43410", gmp.MagnetsToBeAligned[2]._Name, "magnet to align 1 must be SPS.QF.43410");

                Assert.AreEqual(6, gmp._ActiveStationModule._Station.MeasuresTaken.Count, "6 Measures must have been received");
                Assert.AreEqual("SPS.QF.43410.S", gmp._ActiveStationModule._Station.MeasuresTaken.ElementAt(5)._PointName, "point name of the last received measure must be SPS.QF.43410.S");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}
