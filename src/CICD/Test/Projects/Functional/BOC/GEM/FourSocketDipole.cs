﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TSU.Common.Compute.Compensations.BeamOffsets;

namespace Functional_Tests.BOC.GEM
{
    [TestClass]
    public class FourSocketDipole
    {
        [TestMethod]
        public void L2_FSD_Step00_dat_file_Test()
        {
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"); // used to fail if the splashscreen test had an error
            string datContent = @"TT24      ;BSM.241105.S                    ;  129.73294;  669.56876; 4568.69155;2441.91510;442.53233;  .450; -.000267; -.000360;  9.9663; 8;A;24-OCT-2024;  ;;;
TT24      ;MBN.241107.E                    ;  130.70394;  669.72016; 4569.65068;2441.94275;442.56032;  .330; -.000267; -.000360;  9.9663; 1;A;24-OCT-2024;  ;;;
TT24      ;MBN.241107.S                    ;  134.50394;  670.31262; 4573.40421;2441.94138;442.56029; 5.000; -.000267; -.000360;  9.9663; 1;O;24-OCT-2024;  ;;;
TT24      ;MCXCD.241119.A                  ;  136.69864;  670.54406; 4576.04516;2442.05370;442.67359;99.999; -.000267; -.000360;  9.9663;24;A;24-OCT-2024; PXMCXCBWWC_RK000007;;Fidu BDVK ;
TT24      ;MCXCD.241119.E                  ;  136.70101;  670.90733; 4575.99021;2442.05343;442.67322; 2.012; -.000267; -.000360;  9.9663;24;O;24-OCT-2024; PXMCXCBWWC_RK000007;;Fidu BDVK ;
TT24      ;MCXCD.241119.B                  ;  136.92974;  670.58210; 4576.27311;2442.05341;442.67338;99.999; -.000267; -.000360;  9.9663;24;O;24-OCT-2024; PXMCXCBWWC_RK000007;;Fidu BDVK ;
TT24      ;MCXCD.241119.S                  ;  136.93216;  670.94432; 4576.21839;2442.05330;442.67317;  .300; -.000267; -.000360;  9.9663;24;O;24-OCT-2024; PXMCXCBWWC_RK000007;;Fidu BDVK ;
TT24      ;MTN.241128.E                    ;  142.60444;  671.57559; 4581.40569;2442.04847;442.67023; 4.771; -.000267; -.000360;  9.9663; 1;A;24-OCT-2024;  ;;;
TT24      ;MTN.241128.S                    ;  145.36744;  672.00637; 4584.13490;2442.04747;442.67020; 3.600; -.000267; -.000360;  9.9663; 1;O;24-OCT-2024;  ;;;
TT24      ;MTN.241135.E                    ;  146.80444;  672.23042; 4585.55432;2442.04695;442.67019;  .600; -.000267; -.000360;  9.9663; 1;A;24-OCT-2024;  ;;;
TT24      ;MTN.241135.S                    ;  149.56744;  672.66120; 4588.28353;2442.04595;442.67017; 3.600; -.000267; -.000360;  9.9663; 1;O;24-OCT-2024;  ;;;
TT24      ;MTN.241142.E                    ;  151.00444;  672.88525; 4589.70296;2442.04544;442.67016;  .600; -.000267; -.000360;  9.9663; 1;A;24-OCT-2024;  ;;;
TT24      ;MTN.241142.S                    ;  153.76744;  673.31603; 4592.43217;2442.04444;442.67014; 3.600; -.000267; -.000360;  9.9663; 1;O;24-OCT-2024;  ;;;
TT24      ;TBIU.241149.E                   ;  154.42053;  673.48874; 4593.52209;2443.21875;443.84484;  .700;  .000593; -.000360;  9.9663; 2;A;24-OCT-2024;  ;;param�tres_TBIU_4.txt  ID 103-TBIU 4  mtortrat;
TT24      ;TBIU.241149.S                   ;  154.70135;  673.53130; 4593.79967;2443.21854;443.84473;  .250;  .000593; -.000360;  9.9663; 2;O;24-OCT-2024;  ;;param�tres_TBIU_4.txt  ID 103-TBIU 4  mtortrat;
TT24      ;T4CENT.1.E                      ;  154.86606;  673.55767; 4593.96225;2443.21857;443.84482;  .400;  .002303; -.000360;  9.9663; 2;A;24-OCT-2024;  ;;param�tres_TBACA_2.txt  TBACA2  mtortrat;
TT24      ;T4CENT.1.S                      ;  155.30582;  673.62591; 4594.39669;2443.21836;443.84477;  .000;  .002303; -.000360;  9.9663; 2;O;24-OCT-2024;  ;;param�tres_TBACA_2.txt  TBACA2  mtortrat;
TT24      ;TBID.241150.E                   ;  155.42158;  673.64432; 4594.51097;2443.21829;443.84474;  .350;  .000273; -.000360;  9.9663; 2;A;24-OCT-2024;  ;;param�tres_TBID_1.txt  ID110TBID1  annaegel;
TT24      ;TBID.241150.S                   ;  155.70030;  673.68700; 4594.78641;2443.21819;443.84474;  .250;  .000273; -.000360;  9.9663; 2;O;24-OCT-2024;  ;;param�tres_TBID_1.txt  ID110TBID1  annaegel;
TT24      ;MTNH.40003.E                    ;  156.85495;  673.86756; 4595.92641;2442.04316;442.67012;  .750; -.000267; -.000360;  9.9663; 1;A;24-OCT-2024;  ;;;
TT24      ;MTNH.40003.S                    ;  159.61795;  674.29835; 4598.65563;2442.04217;442.67011; 3.600; -.000267; -.000360;  9.9663; 1;O;24-OCT-2024;  ;;;
TT24      ;MTNH.40007.E                    ;  161.05495;  674.52239; 4600.07505;2442.04165;442.67010;  .600; -.000267; -.000360;  9.9663; 1;A;24-OCT-2024;  ;;;
TT24      ;MTNH.40007.S                    ;  163.81795;  674.95318; 4602.80426;2442.04065;442.67008; 3.600; -.000267; -.000360;  9.9663; 1;O;24-OCT-2024;  ;;;
BEAM_TT24      ;MTN.241128.E                    ;  141.73594;  671.51031; 4580.99214;2441.57862;442.20023;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;MTN.241128.S                    ;  145.33594;  672.07159; 4584.54811;2441.57732;442.20020;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.MTN.241128    671.510310   4580.992140   2441.578620 0 0    9.966310 1
*FRAME RST_TT24.MTN.241128 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.MTN.241128      0.000600     -0.002340     -0.000650 -.00795775959 0 -.01768391052 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.MTN.241128 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.MTN.241128.E                                           0.000000      0.000000      0.000000   $141.736 114129 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:49
BEAM_TT24.MTN.241128.S                                          -0.000001      3.599994     -0.000003   $145.336 114130 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:49
*OBSXYZ
TT24.MTN.241128.S      0.000000      3.181500      0.470000 0.1 0.1 0.1   $145.367 114060 param�tres RST 24-OCT-2024 09:35:49
TT24.MTN.241128.E      0.000000      0.418500      0.470000 0.1 0.1 0.1   $142.604 114059 param�tres RST 24-OCT-2024 09:35:49
*INCLY Instr_Roll_Theo
TT24.MTN.241128.E -.01697864933 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;MTN.241142.E                    ;  150.13594;  672.81997; 4589.28941;2441.57559;442.20017;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;MTN.241142.S                    ;  153.73594;  673.38125; 4592.84539;2441.57429;442.20014;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.MTN.241142    672.819970   4589.289410   2441.575590 0 0    9.966310 1
*FRAME RST_TT24.MTN.241142 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.MTN.241142      0.000570      0.000850     -0.000110 -.00053051585 0 .00760406052 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.MTN.241142 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.MTN.241142.E                                           0.000000      0.000000      0.000000   $150.136 114133 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:49
BEAM_TT24.MTN.241142.S                                          -0.000003      3.600004     -0.000003   $153.736 114134 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:49
*OBSXYZ
TT24.MTN.241142.E      0.000000      0.418500      0.470000 0.1 0.1 0.1   $151.004 114063 param�tres RST 24-OCT-2024 09:35:49
TT24.MTN.241142.S      0.000000      3.181500      0.470000 0.1 0.1 0.1   $153.767 114064 param�tres RST 24-OCT-2024 09:35:49
*INCLY Instr_Roll_Theo
TT24.MTN.241142.E -.01697864933 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;MTNH.40003.E                    ;  156.43595;  673.80221; 4595.51237;2441.57331;442.20012;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;MTNH.40003.S                    ;  160.03595;  674.36349; 4599.06835;2441.57202;442.20010;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.MTNH.40003    673.802211   4595.512371   2441.573313 0 0    9.966310 1
*FRAME RST_TT24.MTNH.40003 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.MTNH.40003      0.001410     -0.000980     -0.000110 -.08665105134 0 .03165418042 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.MTNH.40003 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.MTNH.40003.E                                           0.000000      0.000000      0.000000   $156.436 772722 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:49
BEAM_TT24.MTNH.40003.S                                           0.000000      3.600000      0.000000   $160.036 772723 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:49
*OBSXYZ
TT24.MTNH.40003.E      0.000000      0.419000      0.470000 0.1 0.1 0.1   $156.855 772754 param�tres RST 24-OCT-2024 09:35:49
TT24.MTNH.40003.S      0.000000      3.182000      0.470000 0.1 0.1 0.1   $159.618 772755 param�tres RST 24-OCT-2024 09:35:49
*INCLY Instr_Roll_Theo
TT24.MTNH.40003.E -.01698004989 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;MTN.241135.E                    ;  145.93594;  672.16514; 4585.14077;2441.57710;442.20019;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;MTN.241135.S                    ;  149.53594;  672.72642; 4588.69675;2441.57580;442.20016;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.MTN.241135    672.165140   4585.140770   2441.577100 0 0    9.966310 1
*FRAME RST_TT24.MTN.241135 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.MTN.241135     -0.000050     -0.000390     -0.000010 .00318309509 0 .00406728818 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.MTN.241135 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.MTN.241135.E                                           0.000000      0.000000      0.000000   $145.936 114131 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:50
BEAM_TT24.MTN.241135.S                                          -0.000003      3.600004     -0.000003   $149.536 114132 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:50
*OBSXYZ
TT24.MTN.241135.E      0.000000      0.418500      0.470000 0.1 0.1 0.1   $146.804 114061 param�tres RST 24-OCT-2024 09:35:50
TT24.MTN.241135.S      0.000000      3.181500      0.470000 0.1 0.1 0.1   $149.567 114062 param�tres RST 24-OCT-2024 09:35:50
*INCLY Instr_Roll_Theo
TT24.MTN.241135.E -.01697864933 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;TBID.241150.E                   ;  155.43594;  673.64630; 4594.52460;2441.57368;442.20013;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;TBID.241150.S                   ;  155.68594;  673.68528; 4594.77154;2441.57359;442.20013;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.TBID.241150    673.646300   4594.524600   2441.573680 0 0    9.966310 1
*FRAME RST_TT24.TBID.241150 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.TBID.241150      0.000170     -0.004390      0.000720 -.0101860132 0 .03055804104 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.TBID.241150 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.TBID.241150.E                                          0.000000      0.000000      0.000000   $155.436 114137 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:50
BEAM_TT24.TBID.241150.S                                          0.000002      0.249998      0.000000   $155.686 114138 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:50
*OBSXYZ
TT24.TBID.241150.E      0.000170     -0.014360      1.644600 0.1 0.1 0.1   $155.422 248475 param�tres RST 24-OCT-2024 09:35:50
TT24.TBID.241150.S     -0.000620      0.264360      1.644610 0.1 0.1 0.1   $155.700 248476 param�tres RST 24-OCT-2024 09:35:50
*INCLY Instr_Roll_Theo
TT24.TBID.241150.E .01739741782 RF .03437746771
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;MBN.241107.E                    ;  129.65394;  669.62659; 4569.05789;2441.58297;442.20033;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;MBN.241107.S                    ;  134.65394;  670.40614; 4573.99674;2441.58117;442.20029;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.MBN.241107    669.626590   4569.057890   2441.582970 0 0    9.966310 1
*FRAME RST_TT24.MBN.241107 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.MBN.241107     -0.000090     -0.002400     -0.001010 .02049918131 0 .00840339141 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.MBN.241107 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.MBN.241107.E                                           0.000000      0.000000      0.000000   $129.654 114127 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:50
BEAM_TT24.MBN.241107.S                                          -0.000008      4.999994      0.000002   $134.654 114128 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:50
*OBSXYZ
TT24.MBN.241107.E      0.000000      0.600000      0.360000 0.1 0.1 0.1   $130.704 114057 param�tres RST 24-OCT-2024 09:35:50
TT24.MBN.241107.S      0.000000      4.400000      0.360000 0.1 0.1 0.1   $134.504 114058 param�tres RST 24-OCT-2024 09:35:50
*INCLY Instr_Roll_Theo
TT24.MBN.241107.E -.01697864933 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;MTNH.40007.E                    ;  160.63595;  674.45704; 4599.66101;2441.57180;442.20010;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;MTNH.40007.S                    ;  164.23595;  675.01832; 4603.21698;2441.57050;442.20008;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.MTNH.40007    674.457040   4599.661009   2441.571800 0 0    9.966310 1
*FRAME RST_TT24.MTNH.40007 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.MTNH.40007      0.001880     -0.001810      0.001120 .00778090835 0 .02069014312 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.MTNH.40007 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.MTNH.40007.E                                           0.000000      0.000000      0.000000   $160.636 772724 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:51
BEAM_TT24.MTNH.40007.S                                           0.000000      3.600000      0.000000   $164.236 772725 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:51
*OBSXYZ
TT24.MTNH.40007.E      0.000000      0.419000      0.470000 0.1 0.1 0.1   $161.055 772756 param�tres RST 24-OCT-2024 09:35:51
TT24.MTNH.40007.S      0.000000      3.182000      0.470000 0.1 0.1 0.1   $163.818 772757 param�tres RST 24-OCT-2024 09:35:51
*INCLY Instr_Roll_Theo
TT24.MTNH.40007.E -.01698004989 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;T4CENT.1.E                      ;  155.08594;  673.59173; 4594.17888;2441.57380;442.20013;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;T4CENT.1.S                      ;  155.08595;  673.59173; 4594.17888;2441.57380;442.20013;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.T4CENT.1    673.591730   4594.178880   2441.573800 0 0    9.966310 1
*FRAME RST_TT24.T4CENT.1 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.T4CENT.1      0.000840     -0.002600      0.000650 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.T4CENT.1 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.T4CENT.1.E                                             0.000000      0.000000      0.000000   $155.086 114139 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:51
BEAM_TT24.T4CENT.1.S                                             0.000000      0.000000      0.000000   $155.086 114140 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:51
*OBSXYZ
TT24.T4CENT.1.E      0.000130     -0.219880      1.644690 0.1 0.1 0.1   $154.866 248463 param�tres RST 24-OCT-2024 09:35:51
TT24.T4CENT.1.S     -0.000200      0.219880      1.644640 0.1 0.1 0.1   $155.306 248464 param�tres RST 24-OCT-2024 09:35:51
*INCLY Instr_Roll_Theo
TT24.T4CENT.1.E .14663123161 RF .1636112815
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;BSM.241105.E                    ;  128.87394;  669.50497; 4568.28743;2441.58325;442.20034;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;BSM.241105.S                    ;  129.32394;  669.57513; 4568.73192;2441.58309;442.20034;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.BSM.241105    669.504970   4568.287430   2441.583250 0 0    9.966310 1
*FRAME RST_TT24.BSM.241105 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.BSM.241105      0.000470      0.000000     -0.000440 -.03253884256 0 -.02263571901 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.BSM.241105 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.BSM.241105.E                                           0.000000      0.000000      0.000000   $128.874 114125 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:51
BEAM_TT24.BSM.241105.S                                           0.000001      0.449993      0.000002   $129.324 114126 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:51
*OBSXYZ
TT24.BSM.241105.E      0.000000      0.041000      0.332000 0.1 0.1 0.1   $129.365 114055 param�tres RST 24-OCT-2024 09:35:51
TT24.BSM.241105.S      0.000000      0.409000      0.332000 0.1 0.1 0.1   $129.733 114056 param�tres RST 24-OCT-2024 09:35:51
*INCLY Instr_Roll_Theo
TT24.BSM.241105.E -.01697864933 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;MCXCD.241119.E                  ;  136.66544;  670.71976; 4575.98364;2441.58045;442.20027;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;MCXCD.241119.S                  ;  136.96544;  670.76653; 4576.27997;2441.58034;442.20027;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.MCXCD.241119    670.719756   4575.983641   2441.580445 0 0    9.966310 1
*FRAME RST_TT24.MCXCD.241119 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.MCXCD.241119      0.000108     -0.002654     -0.000586 -.00509296157 0 -.00806385586 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.MCXCD.241119 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.MCXCD.241119.E                                         0.000000      0.000000      0.000000   $136.665 826155 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:51
BEAM_TT24.MCXCD.241119.S                                         0.000001      0.300000      0.000000   $136.965 826156 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:51
*OBSXYZ
TT24.MCXCD.241119.E      0.184252      0.035566      0.472995 0.1 0.1 0.1   $136.701 826211 param�tres RST 24-OCT-2024 09:35:51
TT24.MCXCD.241119.S      0.185212      0.266722      0.472949 0.1 0.1 0.1   $136.932 826212 param�tres RST 24-OCT-2024 09:35:51
TT24.MCXCD.241119.A     -0.183139      0.033199      0.473271 0.1 0.1 0.1   $136.699 826209 param�tres RST 24-OCT-2024 09:35:51
TT24.MCXCD.241119.B     -0.181100      0.264296      0.473061 0.1 0.1 0.1   $136.930 826210 param�tres RST 24-OCT-2024 09:35:51
*INCLY Instr_Roll_Theo
TT24.MCXCD.241119.E -.01697998623 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_TT24      ;TBIU.241149.S                   ;  154.68594;  673.52937; 4593.78377;2441.57395;442.20014;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
BEAM_TT24      ;TBIU.241149.E                   ;  154.43594;  673.49039; 4593.53683;2441.57404;442.20014;;; -.000360;  9.9663;;;24-OCT-2024; ;;coordonn�es th�oriques dans le syst�me CCS au 24-OCT-2024 09:35:48;
*FRAME RSTI_TT24.TBIU.241149    673.490390   4593.536830   2441.574040 0 0    9.966310 1
*FRAME RST_TT24.TBIU.241149 0 0 0 .0229437766 0 0 1
*FRAME RSTRI_TT24.TBIU.241149     -0.000330     -0.002840      0.000760 .0229185301 0 .06366259697 1 TX TY TZ RX RZ 
*FRAME RSTR_TT24.TBIU.241149 0 0 0 0 0 0 1 RY
*CALA
BEAM_TT24.TBIU.241149.S                                          0.000002      0.249998      0.000000   $154.686 114136 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:52
BEAM_TT24.TBIU.241149.E                                          0.000000      0.000000      0.000000   $154.436 114135 coordonn�es th�oriques dans le syst�me RST au 24-OCT-2024 09:35:52
*OBSXYZ
TT24.TBIU.241149.E      0.000670     -0.015410      1.644700 0.1 0.1 0.1   $154.421 248469 param�tres RST 24-OCT-2024 09:35:52
TT24.TBIU.241149.S     -0.000570      0.265410      1.644600 0.1 0.1 0.1   $154.701 248470 param�tres RST 24-OCT-2024 09:35:52
*INCLY Instr_Roll_Theo
TT24.TBIU.241149.E .03776925053 RF .05474930042
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
";
            Tools.Artefact.CreateArtefact(fileContent: datContent, artefactName: nameof(L2_FSD_Step00_dat_file_Test) + ".dat");
        }

        [TestMethod()]
        public void L2_FSD_Step01_Start_with_dat_file_Test()
        {
            var datPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_FSD_Step00_dat_file_Test) + ".dat"); // here first so that the test fails if the file is not existing

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""234"" Ypos=""281"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""234"" Ypos=""281"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Guided modules (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Guided modules end"" />
	  
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""371"" Ypos=""300"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""371"" Ypos=""300"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Element alignment (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Element alignment end"" />
	  
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""1000"" Button=""Left"" Direction=""Down"" Xpos=""1316"" Ypos=""960"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""1316"" Ypos=""960"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Next step (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Next step end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var mms = tsunami.MeasurementModules;
                Assert.IsTrue(mms.Count > 0, $"No measurement modules");

                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;
                Assert.IsTrue(ggm != null, "Guided Group Module object is NULL");

                var sgm = ggm.SubGuidedModules;
                Assert.IsTrue(sgm.Count > 0, $"no sub Guided Group Module found in {ggm}");

                var gmp = sgm[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(ggm != null, $"sub Guided Group Module are missing suppossed to be 5 we have: {sgm.Count}");

                ggm._ElementManager.AddElementsFromFile(new FileInfo(datPath), TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes.CCS, true, TSU.Common.Elements.Point.Types.Reference, updateView: false);
                tsunami.BOC_Context.SelectedGeodeFilePaths.Add(datPath);
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                Assert.IsTrue(tsunami != null, "TSUNAMI oject is NULL");
                Assert.IsTrue(tsunami.Points.Count == 23, "Expecting 23 points in FSD_Step1_Start_with_dat_file_Test.tsu");
            });
        }

        [TestMethod()]
        public void L2_FSD_Step02_Setup_to_polar_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_FSD_Step01_Start_with_dat_file_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" Name=""{testMethod.Name}"">
<WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""1""/>
<KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""379"" trace=""Open Operation Manager to select one+"" Button=""Left"" Direction=""Down"" Xpos=""741"" Ypos=""423"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""8"" trace=""Open Operation Manager to select one+"" Button=""Left"" Direction=""Up"" Xpos=""741"" Ypos=""423"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Open Operation Manager to select one (beginning)""/>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""326"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""175"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""175"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""316"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""213"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""213"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""316"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""365"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""365"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""341"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""885"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""106"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""885"" Ypos=""925"" Delta=""0"" />
<IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Open Operation Manager to select one end""/>


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""356"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{M}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""31"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{O}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />


<IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""310"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1424"" Ypos=""924"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1424"" Ypos=""924"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""349"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""427"" Ypos=""452"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""427"" Ypos=""452"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""358"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1357"" Ypos=""921"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1357"" Ypos=""921"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""394"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""959"" Ypos=""594"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""11"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""959"" Ypos=""594"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""MB: MCXCD.241119 Magnet serial number check: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""398"" trace=""button1+I know"" Button=""Left"" Direction=""Down"" Xpos=""888"" Ypos=""582"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""14"" trace=""button1+I know"" Button=""Left"" Direction=""Up"" Xpos=""888"" Ypos=""582"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""MB: Wrong serial number: I know""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""373"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Down"" Xpos=""1139"" Ypos=""847"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""9"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Up"" Xpos=""1139"" Ypos=""847"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Polar Alignment (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Polar Alignment end""/>
</KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 1, $"1 manget should be selected: we have: {gmp.MagnetsToBeAligned.Count}");
                Assert.AreEqual("TT24.MCXCD.241119", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be TT24.MCXCD.241119");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_FSD_Step03_Setup_polar_station_Test()
        {

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_FSD_Step02_Setup_to_polar_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" Name=""{testMethod.Name}"">
<WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""1""/>
<KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""361"" trace=""orientationStation+"" Button=""Left"" Direction=""Down"" Xpos=""1555"" Ypos=""292"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""12"" trace=""orientationStation+"" Button=""Left"" Direction=""Up"" Xpos=""1555"" Ypos=""292"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Station Orientation only (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""385"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""77"" Ypos=""319"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""77"" Ypos=""319"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""331"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""819"" Ypos=""953"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""10"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""819"" Ypos=""953"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""MB: Point stationned: Select""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""354"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""860"" Ypos=""573"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""14"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""860"" Ypos=""573"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""MB: Instrument height: OK""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Station Orientation only end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""345"" trace=""Select Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""1511"" Ypos=""463"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""9"" trace=""Select Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""1511"" Ypos=""463"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Select Instrument (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""314"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""107"" Ypos=""366"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""22"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""107"" Ypos=""366"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""377"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""188"" Ypos=""432"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""188"" Ypos=""432"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""382"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""188"" Ypos=""432"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""188"" Ypos=""432"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""340"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""248"" Ypos=""543"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""7"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""248"" Ypos=""543"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""248"" Ypos=""543"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""248"" Ypos=""543"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Select Instrument end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""301"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""1468"" Ypos=""539"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""10"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""1468"" Ypos=""539"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Change Reflector (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""332"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""187"" Ypos=""303"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""187"" Ypos=""303"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""42"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""187"" Ypos=""303"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""187"" Ypos=""302"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Change Reflector end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""397"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Down"" Xpos=""1527"" Ypos=""647"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""8"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Up"" Xpos=""1527"" Ypos=""647"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""BB: Switch between 1 face and double face (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Switch between 1 face and double face end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""317"" trace=""Number of measurement+"" Button=""Left"" Direction=""Down"" Xpos=""1514"" Ypos=""722"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""11"" trace=""Number of measurement+"" Button=""Left"" Direction=""Up"" Xpos=""1514"" Ypos=""722"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Number of measurement (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""357"" trace=""button4+1"" Button=""Left"" Direction=""Down"" Xpos=""1062"" Ypos=""582"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""8"" trace=""button4+1"" Button=""Left"" Direction=""Up"" Xpos=""1062"" Ypos=""582"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""MB: Number of measurement: 1""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""BB: Number of measurement end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""310"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1337"" Ypos=""944"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""13"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1337"" Ypos=""944"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""49"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""385"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""467"" Ypos=""454"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""51"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""467"" Ypos=""454"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""346"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1232"" Ypos=""936"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""13"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1232"" Ypos=""936"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""54"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""55"" Delay=""0"" Action=""BB: Next step: (press 'n') end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""346"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1300"" Ypos=""929"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""10"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1300"" Ypos=""929"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""58"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""59"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""333"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""469"" Ypos=""288"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""469"" Ypos=""288"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""337"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""553"" Ypos=""285"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""553"" Ypos=""285"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""389"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""923"" Ypos=""738"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""8"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""923"" Ypos=""738"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""66"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""329"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""496"" Ypos=""195"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""68"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""496"" Ypos=""195"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""397"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""826"" Ypos=""929"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""10"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""826"" Ypos=""929"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""71"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""313"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""918"" Ypos=""621"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""13"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""918"" Ypos=""621"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""74"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""386"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1022"" Ypos=""656"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""12"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1022"" Ypos=""656"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""77"" Delay=""0"" Action=""MB: Point successfully measured: OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""318"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""622"" Ypos=""292"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""622"" Ypos=""292"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""305"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""907"" Ypos=""758"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""10"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""907"" Ypos=""758"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""82"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""385"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""502"" Ypos=""142"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""502"" Ypos=""142"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""234"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""490"" Ypos=""180"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""490"" Ypos=""182"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""87"" Delay=""338"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""506"" Ypos=""205"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""88"" Delay=""11"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""506"" Ypos=""205"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""89"" Delay=""333"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""821"" Ypos=""958"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""90"" Delay=""10"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""821"" Ypos=""958"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""91"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""369"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1098"" Ypos=""665"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""93"" Delay=""11"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1098"" Ypos=""665"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""94"" Delay=""0"" Action=""MB: Point successfully measured: OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""349"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1271"" Ypos=""948"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""96"" Delay=""10"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1271"" Ypos=""948"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""97"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""98"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""302"" trace=""Ctrl+"" Button=""Left"" Direction=""Down"" Xpos=""626"" Ypos=""284"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""11"" trace=""Ctrl+"" Button=""Left"" Direction=""Up"" Xpos=""626"" Ypos=""284"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""101"" Delay=""0"" Action=""BB: Control 'opening' (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""102"" Delay=""0"" Action=""BB: Control 'opening' end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""103"" Delay=""301"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""908"" Ypos=""744"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""11"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""909"" Ypos=""744"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""105"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""341"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""571"" Ypos=""208"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""107"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""571"" Ypos=""208"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""108"" Delay=""337"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""861"" Ypos=""926"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""9"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""861"" Ypos=""926"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""110"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""111"" Delay=""330"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""976"" Ypos=""624"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""112"" Delay=""10"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""976"" Ypos=""624"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""113"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""114"" Delay=""366"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""976"" Ypos=""624"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""115"" Delay=""16"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""976"" Ypos=""624"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""116"" Delay=""0"" Action=""MB: Closure OK: OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""117"" Delay=""357"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1344"" Ypos=""952"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""118"" Delay=""13"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1344"" Ypos=""952"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""119"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""120"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""333"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""887"" Ypos=""265"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""122"" Delay=""15"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""887"" Ypos=""265"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""123"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""373"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""742"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""125"" Delay=""10"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""742"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""126"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""127"" Delay=""369"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""889"" Ypos=""931"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""128"" Delay=""10"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""889"" Ypos=""931"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""129"" Delay=""0"" Action=""MB: Compensation: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""130"" Delay=""374"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""893"" Ypos=""564"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""131"" Delay=""16"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""893"" Ypos=""564"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""132"" Delay=""0"" Action=""MB: Are you sure?: Yes""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""133"" Delay=""0"" Action=""BB: Computing (LGC2) end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""134"" Delay=""306"" trace=""Next step blocked+"" Button=""Left"" Direction=""Down"" Xpos=""1308"" Ypos=""973"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""135"" Delay=""13"" trace=""Next step blocked+"" Button=""Left"" Direction=""Up"" Xpos=""1308"" Ypos=""973"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""136"" Delay=""0"" Action=""BB: Next step blocked (beginning)""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""137"" Delay=""371"" trace=""button2+Force next step"" Button=""Left"" Direction=""Down"" Xpos=""1070"" Ypos=""567"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""138"" Delay=""9"" trace=""button2+Force next step"" Button=""Left"" Direction=""Up"" Xpos=""1070"" Ypos=""567"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""139"" Delay=""0"" Action=""MB: Next step blocked: Force next step""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""140"" Delay=""0"" Action=""BB: Next step blocked end""/>
</KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;
                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                var station = gmp._ActiveStationModule._Station as TSU.Polar.Station;
                var param2 = station.Parameters2;
                Assert.IsTrue(param2._StationPoint._Name == "TT24.BSM.241105.S");

                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 1, "1 manget should be selected");
                Assert.AreEqual("TT24.MCXCD.241119", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be TT24.MCXCD.241119");

                Assert.IsTrue(param2._IsSetup, "station not setup");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_FSD_Step04_Measure_Test()
        {
            int datagridOffset = 10;

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_FSD_Step03_Setup_polar_station_Test) + ".tsut"),
                //Use Tools.GetKeySequence instead of creating a new line for each character
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" Name=""{testMethod.Name}"">
<WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""1""/>
<KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1184"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1377"" Ypos=""921"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""144"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1377"" Ypos=""921"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""2300"" Delay=""0"" Action=""BB: Next step end""/>

{Tools.ClickInDgvWithYOffset(741,683, datagridOffset, "click on .E")}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""1462"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""616"" Ypos=""348"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""97"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""616"" Ypos=""348"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Goto and measure (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Goto and measure end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""1297"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""939"" Ypos=""711"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""113"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""939"" Ypos=""711"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""1114"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""582"" Ypos=""268"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""177"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""582"" Ypos=""268"" Delta=""0""/>
{Tools.GetKeySequence("326.513671", "SplitContainer")}
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""1888"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
{Tools.GetKeySequence("100.734123", "SplitContainer")}
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""850"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
{Tools.GetKeySequence("2.654215", "SplitContainer")}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""1369"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""954"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""161"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""954"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""MB: Enter values: Valid""/>

{Tools.ClickInDgvWithYOffset(741,710, datagridOffset)}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""1268"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""665"" Ypos=""356"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""114"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""665"" Ypos=""356"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Goto and measure (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Goto and measure end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""4010"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""924"" Ypos=""723"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""105"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""924"" Ypos=""723"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""1114"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""582"" Ypos=""268"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""177"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""582"" Ypos=""268"" Delta=""0""/>
{Tools.GetKeySequence("323.777886", "SplitContainer")}
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""1888"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
{Tools.GetKeySequence("100.799983", "SplitContainer")}
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""850"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
{Tools.GetKeySequence("2.450797", "SplitContainer")}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""1450"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""883"" Ypos=""941"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""97"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""883"" Ypos=""941"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""MB: Enter values: Valid""/>

{Tools.ClickInDgvWithYOffset(741,743, datagridOffset)}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""904"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""567"" Ypos=""301"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""145"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""567"" Ypos=""301"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""58"" Delay=""0"" Action=""BB: Goto and measure (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""59"" Delay=""0"" Action=""BB: Goto and measure end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""1706"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""915"" Ypos=""719"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""98"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""915"" Ypos=""719"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""62"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""937"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""420"" Ypos=""280"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""420"" Ypos=""280"" Delta=""0""/>
{Tools.GetKeySequence("319.099093", "SplitContainer")}
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""66"" Delay=""617"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
{Tools.GetKeySequence("100.694529", "SplitContainer")}
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""68"" Delay=""258"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
{Tools.GetKeySequence("2.835896", "SplitContainer")}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""1522"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""901"" Ypos=""950"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""121"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""901"" Ypos=""950"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""72"" Delay=""0"" Action=""MB: Enter values: Valid""/>

{Tools.ClickInDgvWithYOffset(741,775, datagridOffset)}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""89"" Delay=""902"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""671"" Ypos=""314"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""90"" Delay=""105"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""671"" Ypos=""315"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""91"" Delay=""0"" Action=""BB: Goto and measure (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""92"" Delay=""0"" Action=""BB: Goto and measure end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""93"" Delay=""1122"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""961"" Ypos=""673"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""94"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""961"" Ypos=""674"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""371"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""959"" Ypos=""703"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""96"" Delay=""120"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""959"" Ypos=""713"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""97"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""98"" Delay=""889"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""417"" Ypos=""259"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""421"" Ypos=""260"" Delta=""0""/>
{Tools.GetKeySequence("316.026401", "SplitContainer")}
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""101"" Delay=""170"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
{Tools.GetKeySequence("100.745134", "SplitContainer")}
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""103"" Delay=""146"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer""/>
{Tools.GetKeySequence("2.646276", "SplitContainer")}
<IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""1609"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""837"" Ypos=""929"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""106"" Delay=""195"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""837"" Ypos=""929"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""107"" Delay=""0"" Action=""MB: Enter values: Valid""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""2153"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1124"" Ypos=""431"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""145"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1124"" Ypos=""431"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Compute Beam Offsets end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""108"" Delay=""1488"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1301"" Ypos=""914"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""109"" Delay=""114"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1301"" Ypos=""914"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""110"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""111"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""112"" Delay=""1506"" trace=""Ctrl+"" Button=""Left"" Direction=""Down"" Xpos=""784"" Ypos=""271"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""113"" Delay=""113"" trace=""Ctrl+"" Button=""Left"" Direction=""Up"" Xpos=""783"" Ypos=""271"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""114"" Delay=""0"" Action=""BB: Control 'opening' (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""115"" Delay=""0"" Action=""BB: Control 'opening' end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""116"" Delay=""1090"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""890"" Ypos=""756"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""117"" Delay=""129"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""890"" Ypos=""756"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""118"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""119"" Delay=""953"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""440"" Ypos=""206"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""120"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""440"" Ypos=""206"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""1002"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""849"" Ypos=""922"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""122"" Delay=""161"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""850"" Ypos=""923"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""123"" Delay=""0"" Action=""MB: Enter values: Valid""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""873"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""925"" Ypos=""624"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""125"" Delay=""122"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""925"" Ypos=""624"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""126"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""127"" Delay=""786"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""925"" Ypos=""624"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""128"" Delay=""138"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""925"" Ypos=""624"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""129"" Delay=""0"" Action=""MB: Closure OK: OK!""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""130"" Delay=""1440"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1385"" Ypos=""954"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""131"" Delay=""122"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1385"" Ypos=""954"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""132"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""133"" Delay=""0"" Action=""BB: Next step end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""134"" Delay=""1481"" trace=""_Valid+"" Button=""Left"" Direction=""Down"" Xpos=""600"" Ypos=""451"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""135"" Delay=""18"" trace=""_Valid+"" Button=""Left"" Direction=""Up"" Xpos=""600"" Ypos=""451"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""136"" Delay=""0"" Action=""BB: Validate the pre-alignement (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""137"" Delay=""0"" Action=""BB: Validate the pre-alignement end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""138"" Delay=""102"" trace=""_Valid+"" Button=""Left"" Direction=""Down"" Xpos=""600"" Ypos=""451"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""139"" Delay=""23"" trace=""_Valid+"" Button=""Left"" Direction=""Up"" Xpos=""600"" Ypos=""451"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""140"" Delay=""0"" Action=""BB: Validate the pre-alignement (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""141"" Delay=""0"" Action=""BB: Validate the pre-alignement end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""142"" Delay=""1174"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1368"" Ypos=""961"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""143"" Delay=""137"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1368"" Ypos=""961"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""144"" Delay=""0"" Action=""BB: Next step (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""145"" Delay=""0"" Action=""BB: Next step end""/>
</KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count > 0, "Measurement module not found");

                TSU.Common.Guided.Group.Module ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                Assert.IsNotNull(ggm, "Measurement module is not a TSU.Common.Guided.Group.Module");

                Gathering.GetDataFrom(tsunami.MeasurementModules, "TT24.MCXCD.241119", DateTime.MinValue, out var gatheredmeasures);


                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 1, "1 manget should be selected");
                Assert.AreEqual("TT24.MCXCD.241119", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be TT24.MCXCD.241119");

                Assert.IsTrue(gatheredmeasures != null && gatheredmeasures.Polar != null && gatheredmeasures.Polar.Count > 0, "No polar measure gathered");

                var measuresPolaire = gatheredmeasures.Polar[0].Item2;
                var station = gatheredmeasures.Polar[0];

                Assert.IsTrue(measuresPolaire != null && measuresPolaire.Count > 2, "Not enough polar measures gathered");

                TSU.Polar.Measure measure = measuresPolaire[2].Measure as TSU.Polar.Measure;

                Assert.IsNotNull(measure != null, "The 3rd measure is not a TSU.Polar.Measure");

                Assert.IsTrue(tsunami.BOC_Context.ExistingResults.Count == 1, $"1 BOC results is expected and missing. found {tsunami.BOC_Context.ExistingResults.Count}");
                Assert.IsTrue(tsunami.BOC_Context.ExistingResults[0].BeamPoints[0]._Name == "BEAM_TT24.MCXCD.241119.E", "BOC first point should be BEAM_TT24.MCXCD.241119.E instead of " + tsunami.BOC_Context.ExistingResults[0].BeamPoints[0]._Name);
                Assert.IsTrue(tsunami.BOC_Context.ExistingResults[0].BeamPoints[1]._Name == "BEAM_TT24.MCXCD.241119.S", "BOC first point should be BEAM_TT24.MCXCD.241119.S instead of " + tsunami.BOC_Context.ExistingResults[0].BeamPoints[1]._Name);

                Assert.AreEqual(323.778, measure.Angles.Corrected.Horizontal.Value, 0.001);
                Assert.AreEqual(100.800, measure.Angles.Corrected.Vertical.Value, 0.001);
                Assert.AreEqual(2.451, measure.Distance.Corrected.Value, 0.001);

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }

}
