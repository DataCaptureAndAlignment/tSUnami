﻿using Functional_Tests;
using MathNet.Numerics.LinearRegression;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using TSU;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Elements;
using TSU.Common.Measures;
using TSU.Line.GuidedModules;
using TSU.Views.Message;
using UnitTestGenerator;

namespace Functional_Tests.BOC.GEM
{
    [TestClass]
    public class Elena
    {
        //  315;RE;19-Nov-2021;BDAO      ;26391;Tsunami 1.02.00_beta15;
        //  316;NP;193       ;STL.20211119BDAO03.              ;p;1728.46000;2151.65500;437.17900 ;10;  -999.900;19-Nov-2021;14:21:38;;
        //  316;OB;AT403    ;394303     ;193       ;STL.20211119BDAO03.              ;; 0.00000;;      ;  20.9; 979.1;    36;00;;A;HI_FIX;s0=48.480594;
        //  317;AH;RRR0.5   ;1          ;193       ;NID.LNE15.                       ; 98.917878; 0.000000;0.000000;;;;;;;;;R;01;14:30:23;TSU: 'Good 2F / 2 Fast: => sH=3 cc, sV=1 cc, sD=0.00 mm';
        //  318;AH;RRR0.5   ;1          ;193       ;NID.LNE1.                        ;150.806828; 0.000000;0.000000;;;;;;;;;R;01;14:31:49;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';
        //  319;AH;RRR0.5   ;1          ;193       ;NID.LNE16.                       ;178.193580; 0.000000;0.000000;;;;;;;;;R;01;14:32:21;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=0 cc, sD=0.00 mm';
        //  320;AH;RRR0.5   ;1          ;LNE01     ;ZQNA.0101.E                      ;239.211220; 0.000000;0.000000;;;;;;;;;C;01;14:35:53;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=2 cc, sD=0.00 mm';
        //  321;AH;RRR0.5   ;1          ;193       ;ST.LNA-4.                        ;294.171592; 0.070000;0.000000;;;;;;;;;R;01;14:37:24;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';

        //  334;AH;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.E                     ;120.831662; 0.000000;0.000000;;;;;;;;;C;01;14:48:18;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=0 cc, sD=0.00 mm';
        //  335;AH;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.S                     ;118.901597; 0.000000;0.000000;;;;;;;;;C;01;14:48:43;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=1 cc, sD=0.00 mm';
        //  336;AH;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.T                     ;125.677662; 0.000000;0.000000;;;;;;;;;C;01;14:49:20;TSU: 'Good 2F / 2 Fast: => sH=2 cc, sV=1 cc, sD=0.00 mm';
        //  348;AH;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.E                     ;209.266145; 0.000000;0.000000;;;;;;;;;C;01;15:03:12;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=3 cc, sD=0.00 mm';
        //  349;AH;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.S                     ;193.453568; 0.000000;0.000000;;;;;;;;;C;01;15:03:41;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=4 cc, sD=0.00 mm';
        //  350;AH;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.T                     ;206.844477; 0.000000;0.000000;;;;;;;;;C;01;15:04:05;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=3 cc, sD=0.00 mm';
        //  351;AH;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.E                     ;221.396132; 0.000000;0.000000;;;;;;;;;C;01;15:04:29;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=0 cc, sD=0.00 mm';
        //  352;AH;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.T                     ;224.466274; 0.000000;0.000000;;;;;;;;;C;01;15:05:00;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';
        //  353;AH;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.S                     ;211.917741; 0.000000;0.000000;;;;;;;;;C;01;15:05:23;TSU: 'Good 2F / 2 Fast: => sH=2 cc, sV=2 cc, sD=0.00 mm';
        //% 354;AH;RRR0.5   ;1          ;193       ;NID.LNE15.                       ; 98.917518; 0.000000;0.000000;;;;;;;;; ;01;15:06:16;TSU: 'Control 2F / 2 Fast: => sH=0 cc, sV=0 cc, sD=0.00 mm,Off.: dH=-3.6cc, dV=-0.3cc, dD=0mm' - Was Control according to Tsunami;

        //  355;AV;RRR0.5   ;1          ;193       ;NID.LNE15.                       ;102.955664; 0.000000;0.000000;;;;;;;;;R;01;14:30:23;TSU: 'Good 2F / 2 Fast: => sH=3 cc, sV=1 cc, sD=0.00 mm';
        //  356;AV;RRR0.5   ;1          ;193       ;NID.LNE1.                        ;100.614254; 0.000000;0.000000;;;;;;;;;R;01;14:31:49;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';
        //  357;AV;RRR0.5   ;1          ;193       ;NID.LNE16.                       ;100.759843; 0.000000;0.000000;;;;;;;;;R;01;14:32:21;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=0 cc, sD=0.00 mm';
        //  358;AV;RRR0.5   ;1          ;LNE01     ;ZQNA.0101.E                      ;122.741646; 0.000000;0.000000;;;;;;;;;C;01;14:35:53;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=2 cc, sD=0.00 mm';
        //  359;AV;RRR0.5   ;1          ;193       ;ST.LNA-4.                        ; 98.778398; 0.070000;0.000000;;;;;;;;;R;01;14:37:24;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';

        //  372;AV;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.E                     ;111.354760; 0.000000;0.000000;;;;;;;;;C;01;14:48:18;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=0 cc, sD=0.00 mm';
        //  373;AV;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.S                     ;110.489468; 0.000000;0.000000;;;;;;;;;C;01;14:48:43;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=1 cc, sD=0.00 mm';
        //  374;AV;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.T                     ;110.989492; 0.000000;0.000000;;;;;;;;;C;01;14:49:20;TSU: 'Good 2F / 2 Fast: => sH=2 cc, sV=1 cc, sD=0.00 mm';
        //  386;AV;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.E                     ;128.648249; 0.000000;0.000000;;;;;;;;;C;01;15:03:12;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=3 cc, sD=0.00 mm';
        //  387;AV;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.S                     ;129.080412; 0.000000;0.000000;;;;;;;;;C;01;15:03:41;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=4 cc, sD=0.00 mm';
        //  388;AV;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.T                     ;123.586799; 0.000000;0.000000;;;;;;;;;C;01;15:04:05;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=3 cc, sD=0.00 mm';
        //  389;AV;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.E                     ;117.920037; 0.000000;0.000000;;;;;;;;;C;01;15:04:29;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=0 cc, sD=0.00 mm';
        //  390;AV;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.T                     ;116.220787; 0.000000;0.000000;;;;;;;;;C;01;15:05:00;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';
        //  391;AV;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.S                     ;116.482582; 0.000000;0.000000;;;;;;;;;C;01;15:05:23;TSU: 'Good 2F / 2 Fast: => sH=2 cc, sV=2 cc, sD=0.00 mm';

        //% 392;AV;RRR0.5   ;1          ;193       ;NID.LNE15.                       ;102.955636; 0.000000;0.000000;;;;;;;;; ;01;15:06:16;TSU: 'Control 2F / 2 Fast: => sH=0 cc, sV=0 cc, sD=0.00 mm,Off.: dH=-3.6cc, dV=-0.3cc, dD=0mm' - Was Control according to Tsunami;

        //  393;DD;RRR0.5   ;1          ;193       ;NID.LNE15.                       ;  4.398301; 0.000000;0.000000;;;;;;;;;R;01;14:30:23;TSU: 'Good 2F / 2 Fast: => sH=3 cc, sV=1 cc, sD=0.00 mm';
        //  394;DD;RRR0.5   ;1          ;193       ;NID.LNE1.                        ;  6.418985; 0.000000;0.000000;;;;;;;;;R;01;14:31:49;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';
        //  395;DD;RRR0.5   ;1          ;193       ;NID.LNE16.                       ;  4.690674; 0.000000;0.000000;;;;;;;;;R;01;14:32:21;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=0 cc, sD=0.00 mm';
        //  396;DD;RRR0.5   ;1          ;LNE01     ;ZQNA.0101.E                      ;  2.324880; 0.000000;0.000000;;;;;;;;;C;01;14:35:53;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=2 cc, sD=0.00 mm';
        //  397;DD;RRR0.5   ;1          ;193       ;ST.LNA-4.                        ; 16.225081; 0.070000;0.000000;;;;;;;;;R;01;14:37:24;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';

        //  410;DD;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.E                     ;  4.567162; 0.000000;0.000000;;;;;;;;;C;01;14:48:18;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=0 cc, sD=0.00 mm';
        //  411;DD;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.S                     ;  4.939223; 0.000000;0.000000;;;;;;;;;C;01;14:48:43;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=1 cc, sD=0.00 mm';
        //  412;DD;RRR0.5   ;1          ;LNE01     ;ZDSHR.0132.T                     ;  4.717352; 0.000000;0.000000;;;;;;;;;C;01;14:49:20;TSU: 'Good 2F / 2 Fast: => sH=2 cc, sV=1 cc, sD=0.00 mm';
        //  424;DD;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.E                     ;  1.862047; 0.000000;0.000000;;;;;;;;;C;01;15:03:12;TSU: 'Good 2F / 2 Fast: => sH=0 cc, sV=3 cc, sD=0.00 mm';
        //  425;DD;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.S                     ;  1.836448; 0.000000;0.000000;;;;;;;;;C;01;15:03:41;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=4 cc, sD=0.00 mm';
        //  426;DD;RRR0.5   ;1          ;LNE06     ;ZDSHR.0606.T                     ;  2.236280; 0.000000;0.000000;;;;;;;;;C;01;15:04:05;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=3 cc, sD=0.00 mm';
        //  427;DD;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.E                     ;  2.912772; 0.000000;0.000000;;;;;;;;;C;01;15:04:29;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=0 cc, sD=0.00 mm';
        //  428;DD;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.T                     ;  3.210883; 0.000000;0.000000;;;;;;;;;C;01;15:05:00;TSU: 'Good 2F / 2 Fast: => sH=1 cc, sV=1 cc, sD=0.00 mm';
        //  429;DD;RRR0.5   ;1          ;LNE07     ;ZDSHR.0717.S                     ;  3.159539; 0.000000;0.000000;;;;;;;;;C;01;15:05:23;TSU: 'Good 2F / 2 Fast: => sH=2 cc, sV=2 cc, sD=0.00 mm';

        //% 430;DD;RRR0.5   ;1          ;193       ;NID.LNE15.                       ;  4.398300; 0.000000;0.000000;;;;;;;;; ;01;15:06:16;TSU: 'Control 2F / 2 Fast: => sH=0 cc, sV=0 cc, sD=0.00 mm,Off.: dH=-3.6cc, dV=-0.3cc, dD=0mm' - Was Control according to Tsunami;

        //  431;EC;LNE01     ;ZQNA.0101.E                      ;      0.41;      0.93;      0.11;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:35:53 PM);
        //  432;EC;LNE01     ;ZQNA.0101.S                      ;      0.54;      0.99;     -0.02;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:38:09 PM);
        //  433;EC;LNE00     ;ZQNA.0039.S                      ;      0.63;      2.44;      0.05;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=70.00mm)(t:11/19/2021 2:39:25 PM);
        //  434;EC;LNE00     ;ZQNA.0039.E                      ;      0.72;      2.37;     -0.28;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=70.00mm)(t:11/19/2021 2:39:50 PM);
        //  435;EC;LNE00     ;ZQNA.0032.S                      ;      1.05;      0.54;      0.31;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=70.00mm)(t:11/19/2021 2:40:22 PM);
        //  436;EC;LNE00     ;ZQNA.0032.E                      ;      0.89;      0.52;      0.25;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=70.00mm)(t:11/19/2021 2:40:44 PM);
        //  437;EC;LNE01     ;ZQNA.0107.E                      ;     -0.61;      1.33;      0.16;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:42:36 PM);
        //  438;EC;LNE01     ;ZQNA.0107.S                      ;     -0.48;      1.63;     -0.09;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:42:57 PM);
        //  439;EC;LNE01     ;ZQNA.0114.E                      ;     -0.43;     -0.54;      0.39;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:43:19 PM);
        //  440;EC;LNE01     ;ZQNA.0114.S                      ;     -0.35;     -0.38;      0.38;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:43:38 PM);
        //  441;EC;LNE01     ;ZQNA.0121.S                      ;     -0.61;      0.15;      0.05;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:45:14 PM);
        //  442;EC;LNE01     ;ZDFHR.0127.E                     ;     -1.19;      0.19;     -4.78;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:45:41 PM);
        //  443;EC;LNE01     ;ZDFHR.0127.S                     ;     -0.52;      0.38;     -4.63;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:46:16 PM);
        //  444;EC;LNE01     ;ZDSHR.0132.E                     ;      0.01;      0.89;      0.20;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:48:18 PM);
        //  445;EC;LNE01     ;ZDSHR.0132.S                     ;     -0.06;      0.79;      0.01;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:48:43 PM);
        //  446;EC;LNE01     ;ZDSHR.0132.T                     ;      0.15;      0.82;     -0.08;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:49:20 PM);
        //  447;EC;LNE02     ;ZQNA.0205.S                      ;     -1.23;      1.25;     -0.67;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:51:02 PM);
        //  448;EC;LNE06     ;ZQNA.0612.S                      ;      0.53;      0.62;     -0.13;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:53:04 PM);
        //  449;EC;LNE07     ;ZQNA.0711.E                      ;     -0.41;      1.60;      0.53;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:55:09 PM);
        //  450;EC;LNE07     ;ZQNA.0711.S                      ;     -0.36;      1.68;      0.57;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:55:41 PM);
        //  451;EC;LNE07     ;ZDFHR.0701.E                     ;      0.06;      0.60;     -5.94;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:56:31 PM);
        //  452;EC;LNE07     ;ZDFHR.0701.S                     ;      0.52;      0.54;     -5.25;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:57:13 PM);
        //  453;EC;LNE07     ;ZDSHR.0706.E                     ;     -0.22;      0.62;      0.22;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:58:58 PM);
        //  454;EC;LNE07     ;ZDSHR.0706.S                     ;     -0.31;      0.91;     -0.06;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 2:59:33 PM);
        //  455;EC;LNE00     ;ZQNA.0046.E                        ;      0.10;     -1.23;     -0.36;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 3:01:22 PM);
        //  456;EC;LNE00     ;ZQNA.0046.S                        ;      0.27;     -2.17;     -0.02;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 3:02:19 PM);
        //  457;EC;LNE06     ;ZDSHR.0606.E                     ;     -0.34;      0.66;      0.46;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 3:03:12 PM);
        //  458;EC;LNE06     ;ZDSHR.0606.S                     ;     -0.37;      0.45;      0.34;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 3:03:41 PM);
        //  459;EC;LNE06     ;ZDSHR.0606.T                     ;     -0.50;      0.19;      0.13;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 3:04:05 PM);
        //  460;EC;LNE07     ;ZDSHR.0717.E                     ;      0.10;      0.54;      0.48;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 3:04:29 PM);
        //  461;EC;LNE07     ;ZDSHR.0717.T                     ;     -0.07;      0.68;      0.42;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 3:05:00 PM);
        //  462;EC;LNE07     ;ZDSHR.0717.S                     ;      0.12;      0.45;      0.48;;dr dl dh mm(offset in Beam-Line CS verticalized)(HFT=0.00mm, HR=0.00mm)(t:11/19/2021 3:05:23 PM);

        [TestMethod()]
        public void L2_BGM2_Step00_dat_file_Test()
        {
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"); // used to fail if the splashscreen test had an error
            string datContent = @"193       ;INTER-AS-AS2.1.                 ; -999.99000; 1725.47862; 2162.46750;         0;436.15089;      ;        ;         ;        ;10;O;29-APR-2019;                    ;;Point d'intersection entre LNE05 (ASACUASA) et LNE05 (ASACUSA 2).;
193       ;NID.GBAR-10.                    ; -999.90000; 1750.00000; 2160.00000;         0;440.00000;      ;        ;         ;        ;18;O;01-NOV-2021;                    ;;;
193       ;NID.GBAR-TEMP02.                ; -999.90000; 1750.00000; 2160.00000;         0;440.00000;      ;        ;         ;        ;18;O;01-NOV-2021;                    ;;;
193       ;NID.LNE17.                      ; -999.90000; 1719.11323; 2164.94282;         0;437.02505;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS16.                       ; -999.90000; 1727.85265; 2161.00958;         0;437.76626;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS14.                       ; -999.90000; 1734.90743; 2170.69323;         0;437.92382;      ;        ;         ;        ;18;O;16-MAR-2022;                    ;;Mesure initiale PA;
193       ;NID.AS8-2.                      ; -999.90000; 1732.52116; 2181.18294;         0;437.20322;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE2A5.                     ; -999.90000; 1726.47409; 2186.26393;         0;437.30180;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS5.                        ; -999.90000; 1727.59976; 2167.73218;         0;437.30182;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS7-2.                      ; -999.90000; 1729.79881; 2175.59528;         0;437.21756;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TEMP.52.                    ; -999.90000; 1753.21956; 2171.56477;         0;437.89655;      ;        ;         ;        ;18;O;24-JAN-2024;                    ;;;
193       ;NID.TEMP.54.                    ; -999.90000; 1759.66355; 2178.82054;         0;440.19771;      ;        ;         ;        ;18;O;24-JAN-2024;                    ;;;
193       ;NID.10.                         ; -999.90000; 1761.13334; 2138.92607;         0;435.65250;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.16.                    ; -999.90000; 1758.48977; 2138.45619;         0;441.69818;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.21.                    ; -999.90000; 1770.86821; 2171.97900;         0;441.39122;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.23.                         ; -999.90000; 1750.17938; 2165.43299;         0;437.53041;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.AEGIS-2.                    ; -999.90000; 1720.91008; 2152.96414;         0;436.84020;      ;        ;         ;        ;18;O;06-NOV-2020;                    ;;Réseau AEGIS LS2;
193       ;MPOT.PAG1A.                     ; -999.90000; 1722.89538; 2154.35647;         0;437.08630;      ;        ;         ;        ; 2;O;19-JUN-2024;                    ;;Réseau AEGIS LS2 changement code 7 vers 2;
193       ;NID.PUMA-10.                    ; -999.90000; 1759.17229; 2169.71084;         0;436.90915;      ;        ;         ;        ;18;O;16-FEB-2022;                    ;;Mesure initiale lissage LNE51;
193       ;NID.LNATEMP1.                   ; -999.90000; 1740.94452; 2160.84490;         0;435.95260;      ;        ;         ;        ;18;O;28-JAN-2021;                    ;;Reseau au theo pour tracage et pre alignement LNE51. Cala sur dipoles LNA;
193       ;NID.TEMPALPHA3.                 ; -999.90000; 1727.44299; 2182.69182;         0;434.99329;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BA7.                        ; -999.90000; 1734.34618; 2166.38841;         0;436.80391;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BA10.                       ; -999.90000; 1734.44018; 2158.12055;         0;437.34311;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TEMP.51..                   ; -999.90000; 1748.74665; 2162.67374;         0;438.24332;      ;        ;         ;        ;18;O;23-JAN-2024;                    ;;;
193       ;NID.TEMP.100.                   ; -999.90000; 1771.81643; 2170.64219;         0;439.98663;      ;        ;         ;        ;18;O;24-JAN-2024;                    ;;;
193       ;NID.13.                         ; -999.90000; 1761.17822; 2144.58323;         0;436.98052;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.19.                    ; -999.90000; 1761.53202; 2139.58180;         0;439.96384;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.PUMA-8.                     ; -999.90000; 1756.77791; 2164.87652;         0;437.05853;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.PUMA-2.                     ; -999.90000; 1751.85941; 2158.06924;         0;436.62536;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.PUMA-1.                     ; -999.90000; 1749.27966; 2158.39912;         0;436.90549;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BLINDTEMP2.                 ; -999.90000; 1750.21705; 2157.62540;         0;436.34747;      ;        ;         ;        ;18;O;28-JAN-2021;                    ;;Reseau au theo pour tracage et pre alignement LNE51. Cala sur dipoles LNA;
193       ;MPOT.BA2.                       ; -999.90000; 1732.53397; 2162.64229;         0;436.90364;      ;        ;         ;        ; 2;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS8.                        ; -999.90000; 1731.52243; 2179.12337;         0;437.31330;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS13.                       ; -999.90000; 1738.64333; 2178.23010;         0;437.90580;      ;        ;         ;        ;18;O;16-MAR-2022;                    ;;Mesure initiale PA;
193       ;NID.AS9.                        ; -999.90000; 1734.06285; 2184.33776;         0;437.31507;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID_TEMP.30.                    ; -999.90000; 1757.58466; 2180.83851;         0;439.15334;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.12.                         ; -999.90000; 1758.64813; 2137.33430;         0;436.15596;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.PUMA-7.                     ; -999.90000; 1755.79984; 2162.61262;         0;436.95424;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE2T2.                     ; -999.90000; 1732.74942; 2182.72185;         0;437.24228;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE2A2.                     ; -999.90000; 1729.67320; 2176.40425;         0;437.10441;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BA5.                        ; -999.90000; 1730.83196; 2156.03361;         0;436.91047;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS6.                        ; -999.90000; 1727.48318; 2170.81880;         0;437.32819;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS10.                       ; -999.90000; 1736.15475; 2186.38219;         0;437.30078;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1I11.                    ; -999.90000; 1739.14289; 2179.26545;         0;437.87268;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1T3.                     ; -999.90000; 1737.29717; 2175.53784;         0;437.97133;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1T4.                     ; -999.90000; 1731.98530; 2180.05546;         0;437.32230;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;SS.54S.                         ; -999.90000; 1737.61233; 2140.94576;         0;436.31459;      ;        ;         ;        ; 8;O;24-MAR-2023;                    ;;Initial measurement before removal of QFC54;
193       ;NID_TEMP.28.                    ; -999.90000; 1762.61951; 2181.26597;         0;440.44807;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.17.                    ; -999.90000; 1765.27317; 2147.10898;         0;439.87323;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.33.                         ; -999.90000; 1759.36157; 2184.00324;         0;438.21743;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.TEMP.50..                   ; -999.90000; 1750.42784; 2156.96891;         0;437.97248;      ;        ;         ;        ;18;O;23-JAN-2024;                    ;;;
193       ;NID_TEMP.29.                    ; -999.90000; 1761.47645; 2178.94134;         0;440.47572;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.TEMP.53.                    ; -999.90000; 1753.92046; 2169.05922;         0;437.39422;      ;        ;         ;        ;18;O;24-JAN-2024;                    ;;;
193       ;NID.TEMP.55.                    ; -999.90000; 1765.39697; 2177.63998;         0;441.19218;      ;        ;         ;        ;18;O;24-JAN-2024;                    ;;;
193       ;NID.TEMP.56.                    ; -999.90000; 1767.03857; 2168.01342;         0;440.00193;      ;        ;         ;        ;18;O;24-JAN-2024;                    ;;;
193       ;NID.TEMP.101.                   ; -999.90000; 1766.09781; 2173.38627;         0;439.79931;      ;        ;         ;        ;18;O;24-JAN-2024;                    ;;;
193       ;NID.11.                         ; -999.90000; 1759.62372; 2137.97498;         0;436.67619;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.18.                    ; -999.90000; 1763.15765; 2142.85742;         0;439.95394;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.20.                    ; -999.90000; 1770.69713; 2171.63977;         0;441.39274;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.ADE2A6.                     ; -999.90000; 1724.89056; 2183.46086;         0;437.08159;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS15.                       ; -999.90000; 1732.23288; 2165.27833;         0;437.92088;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS7.                        ; -999.90000; 1729.23651; 2174.44044;         0;437.32536;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS11.                       ; -999.90000; 1739.84957; 2184.53531;         0;437.31692;      ;        ;         ;        ;18;O;16-MAR-2022;                    ;;Mesure initiale PA;
193       ;NID.BA9.                        ; -999.90000; 1735.27203; 2159.78247;         0;437.33801;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1I4.                     ; -999.90000; 1731.98600; 2180.05600;         0;437.32200;      ;        ;         ;        ;18;O;16-NOV-2021;                    ;;;
193       ;NID.31.                         ; -999.90000; 1760.71978; 2184.90689;         0;436.71005;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.32.                         ; -999.90000; 1762.29254; 2184.13840;         0;436.32721;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.14.                    ; -999.90000; 1763.31655; 2148.16237;         0;440.42544;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.22.                         ; -999.90000; 1751.83356; 2168.78236;         0;437.51807;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.PUMA-4.                     ; -999.90000; 1749.94257; 2160.86605;         0;436.96451;      ;        ;         ;        ;18;O;16-FEB-2022;                    ;;Mesure initiale lissage LNE51;
193       ;NID.BA8.                        ; -999.90000; 1735.62727; 2167.76928;         0;436.83104;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1T12.                    ; -999.90000; 1734.63157; 2170.16349;         0;437.93740;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BA4.                        ; -999.90000; 1732.54082; 2155.18279;         0;436.99451;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.PUMA-6.                     ; -999.90000; 1751.35621; 2163.72340;         0;436.80390;      ;        ;         ;        ;18;O;16-FEB-2022;                    ;;Mesure initiale lissage LNE51;
193       ;NID.PUMA-5.                     ; -999.90000; 1754.56468; 2160.11546;         0;436.75463;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TEMPPUMA2.                  ; -999.90000; 1757.78123; 2166.90364;         0;436.64527;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TEMPALPHA4.                 ; -999.90000; 1726.10965; 2186.78283;         0;436.58541;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE18.                      ; -999.90000; 1720.84559; 2167.31484;         0;436.97259;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE19.                      ; -999.90000; 1721.47165; 2166.31322;         0;435.50079;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AL6.                        ; -999.90000; 1726.76419; 2170.37424;         0;437.04697;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID_TEMP.27.                    ; -999.90000; 1763.29549; 2182.64048;         0;440.48376;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.24.                         ; -999.90000; 1769.22411; 2166.83274;         0;439.62636;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.25.                    ; -999.90000; 1738.67761; 2145.46491;         0;441.29003;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID_TEMP.26.                    ; -999.90000; 1736.67891; 2145.50531;         0;441.30099;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.BLINDTEMP1.                 ; -999.90000; 1750.07532; 2157.34241;         0;435.99632;      ;        ;         ;        ;18;O;28-JAN-2021;                    ;;Reseau au theo pour tracage et pre alignement LNE51. Cala sur dipoles LNA;
193       ;NID.PUMA-3.                     ; -999.90000; 1749.23855; 2159.60365;         0;437.08921;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TEMPPUMA1.                  ; -999.90000; 1758.71376; 2168.78637;         0;436.88920;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS12.                       ; -999.90000; 1741.51739; 2181.85963;         0;437.29422;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BA1.                        ; -999.00000; 1733.38111; 2155.96827;         0;436.95967;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.05.                         ; -999.00000; 1754.72732; 2129.22421;         0;435.15342;      ;        ;         ;        ;18;O;03-MAR-2022;                    ;;Calcul global des lignes F16  FTA  DI  DR et FTN à partir des mesures du LS2 et YETS2021-22. Calage alti PSR. Contraintes radiales PSR et AD ring.;
193       ;NID.LNE12.                      ; -999.00000; 1717.79531; 2167.82579;         0;436.89681;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.04.                         ; -999.00000; 1754.93493; 2129.12443;         0;436.80412;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.08.                         ; -999.00000; 1759.88431; 2130.27646;         0;435.91203;      ;        ;         ;        ;18;O;03-MAR-2022;                    ;;Calcul global des lignes F16  FTA  DI  DR et FTN à partir des mesures du LS2 et YETS2021-22. Calage alti PSR. Contraintes radiales PSR et AD ring.;
193       ;NID.LNE14.                      ; -999.00000; 1722.54581; 2157.34259;         0;436.93836;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TEMP.50.                    ; -999.00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ;18;O;23-JAN-2024;                    ;;TSU: 'Good 2F / 2 Fast: => sH=0 cc  sV=0 cc  sD=0.00 mm' [ERR]le point 193.NID.TEMP.50  n'existe pas;
193       ;ATPPOT.4.                       ; -999.00000;     .00000;     .00000;         0;436.36678;      ;        ;         ;        ; 1;O;13-FEB-1986;                    ;;PILIER;
193       ;CRAPO.2900.                     ; -999.00000;     .00000;     .00000;         0;435.29345;      ;        ;         ;        ;10;O;25-FEB-2004;                    ;;;
193       ;ST.LNA-3.                       ; -999.00000; 1748.79102; 2149.59285;         0;437.42530;      ;        ;         ;        ; 1;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BA3.                        ; -999.00000; 1730.63838; 2158.87602;         0;436.98588;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1-9.                     ; -999.00000; 1739.50061; 2184.69507;         0;437.19184;      ;        ;         ;        ;23;O;08-APR-2019;                    ;;Mesure du réseau pour les LT ELENA. Calage avec CHABA sur dipôles theo.;
193       ;NID.AS4.                        ; -999.00000; 1726.13219; 2164.88875;         0;437.02761;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;MPOT.PADE141.                   ; -999.00000; 1727.65292; 2167.71772;         0;437.21045;      ;        ;         ;        ; 2;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.06.                         ; -999.00000; 1755.39422; 2130.67503;         0;437.34001;      ;        ;         ;        ;18;O;03-MAR-2022;                    ;;Calcul global des lignes F16  FTA  DI  DR et FTN à partir des mesures du LS2 et YETS2021-22. Calage alti PSR. Contraintes radiales PSR et AD ring.;

193       ;NID.LNE16.                      ; -999.00000; 1728.70331; 2156.33890;         0;437.12252;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE15.                      ; -999.00000; 1724.37597; 2153.27363;         0;436.97439;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.070401.                     ; -999.00000; 1752.83560; 2157.99739;         0;400.00000;      ;        ;         ;        ;24;O;08-APR-2015;                    ;;;
193       ;ST.2704.                        ; -999.00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ;10;O;28-APR-2015;                    ;;;
193       ;NID.LNA-11.                     ; -999.00000; 1737.08904; 2152.11670;         0;437.69332;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BA2.                        ; -999.00000; 1729.57523; 2156.71622;         0;436.97316;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AL1.                        ; -999.00000; 1725.08191; 2169.10322;         0;437.56896;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE2A3.                     ; -999.00000; 1734.60631; 2187.40156;         0;437.28026;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE2A4.                     ; -999.00000; 1729.35753; 2188.17654;         0;437.46831;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1T2.                     ; -999.00000; 1728.88579; 2173.68692;         0;437.31403;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE5.                       ; -999.00000; 1733.28752; 2188.30488;         0;438.23788;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE4.                       ; -999.00000; 1722.03382; 2164.88522;         0;437.08378;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.03.                         ; -999.00000; 1758.83062; 2127.46223;         0;435.21510;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.TEMP.51.                    ; -999.00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ;18;O;23-JAN-2024;                    ;;TSU: 'Good 2F / 2 Fast: => sH=1 cc  sV=1 cc  sD=0.00 mm' [ERR]le point 193.NID.TEMP.51  n'existe pas;
193       ;POT.AS1.                        ; -999.00000; 1725.77389; 2159.77119;         0;436.67696;      ;        ;         ;        ; 8;O;16-MAR-2022;                    ;;Mesure initiale PA;
193       ;POT.LNE1.                       ; -999.00000; 1728.46048; 2151.65484;         0;436.98881;      ;        ;         ;        ; 8;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;ST.LNE2.                        ; -999.00000; 1739.19563; 2189.39305;         0;440.29908;      ;        ;         ;        ;26;O;07-DEC-2023;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.01.                         ; -999.00000; 1760.81052; 2128.75805;         0;437.26532;      ;        ;         ;        ;18;O;03-MAR-2022;                    ;;Calcul global des lignes F16  FTA  DI  DR et FTN à partir des mesures du LS2 et YETS2021-22. Calage alti PSR. Contraintes radiales PSR et AD ring.;
193       ;NID.02.                         ; -999.00000; 1759.68708; 2127.27173;         0;437.07847;      ;        ;         ;        ;18;O;03-MAR-2022;                    ;;Calcul global des lignes F16  FTA  DI  DR et FTN à partir des mesures du LS2 et YETS2021-22. Calage alti PSR. Contraintes radiales PSR et AD ring.;
193       ;ST.GB-10.                       ; -999.00000; 1749.51835; 2154.39990;         0;437.49860;      ;        ;         ;        ;26;O;17-DEC-2019;                    ;;point de réseau expérience Gbar;
193       ;INTER.2.                        ; -999.00000;     .00000;     .00000;         0;434.98330;      ;        ;         ;        ;10;O;07-MAR-1997;                    ;;CRAPAUD P.MESURES AA;
193       ;NID.070403.                     ; -999.00000; 1741.86968; 2165.41802;         0;400.00000;      ;        ;         ;        ;24;O;08-APR-2015;                    ;;;
193       ;PT.23.                          ; -999.00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ;10;O;28-APR-2015;                    ;;;

193       ;NID.LNA-4.                      ; -999.00000; 1737.02954; 2151.99080;         0;437.49957;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;ST.LNE1.                        ; -999.00000; 1756.78631; 2163.78227;         0;438.29417;      ;        ;         ;        ; 8;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.07.                         ; -999.00000; 1754.65460; 2132.87341;         0;437.13657;      ;        ;         ;        ;18;O;11-DEC-2023;                    ;;Mesure de liaison entre AD et ELENA;
193       ;NID.LNE11.                      ; -999.00000; 1720.44590; 2167.40249;         0;437.03714;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AL5.                        ; -999.00000; 1719.55661; 2173.01956;         0;436.69262;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.070404.                     ; -999.00000; 1748.36986; 2157.51083;         0;400.00000;      ;        ;         ;        ;24;O;08-APR-2015;                    ;;;
193       ;ST.070401.                      ; -999.00000; 1754.93040; 2162.22884;         0;400.00000;      ;        ;         ;        ;10;O;08-APR-2015;                    ;;;
193       ;NID.AL3.                        ; -999.00000; 1718.67159; 2169.23190;         0;436.83474;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;POT.LNE2.                       ; -999.00000; 1724.68815; 2153.70699;         0;436.49900;      ;        ;         ;        ; 8;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;POT.AL1.                        ; -999.00000; 1733.17074; 2184.57800;         0;437.04528;      ;        ;         ;        ; 8;O;08-APR-2019;                    ;;Mesure du réseau pour les LT ELENA. Calage avec CHABA sur dipôles theo.;
193       ;NID.070402.                     ; -999.00000; 1735.64021; 2159.13950;         0;400.00000;      ;        ;         ;        ;24;O;08-APR-2015;                    ;;;
193       ;PT.61.                          ; -999.00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ;10;O;28-APR-2015;                    ;;;
193       ;NID.LNA-10.                     ; -999.00000; 1739.81909; 2144.63153;         0;437.68104;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AL2.                        ; -999.00000; 1721.51603; 2167.82546;         0;437.56640;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE2T1.                     ; -999.00000; 1726.26655; 2169.37062;         0;437.32312;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS3.                        ; -999.00000; 1724.49228; 2162.87208;         0;436.90692;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;POTEX.LNE8.                     ; -999.00000; 1718.77533; 2166.58219;         0;436.46673;      ;        ;         ;        ; 8;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE6.                       ; -999.00000; 1741.86492; 2187.57258;         0;439.54552;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE-111.                    ; -999.00000; 1728.62249; 2151.06539;         0;437.03466;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;PT.56.                          ; -999.00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ;10;O;28-APR-2015;                    ;;;
193       ;NID.LNA-15.                     ; -999.00000; 1736.80175; 2161.89539;         0;437.30241;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1I8.                     ; -999.00000; 1736.34039; 2186.27404;         0;437.21627;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE13.                      ; -999.00000; 1720.80891; 2161.21328;         0;436.85691;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;INTER.1.                        ; -999.00000;     .00000;     .00000;         0;435.05553;      ;        ;         ;        ;10;O;29-MAY-1997;                    ;;CRAPAUD P. MESURES AA/ACOL 97;
193       ;ST.070402.                      ; -999.00000; 1740.56330; 2157.90653;         0;400.00000;      ;        ;         ;        ;10;O;08-APR-2015;                    ;;;
193       ;NID.270403.                     ; -999.00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ;24;O;28-APR-2015;                    ;;;
193       ;ST.LNA-4.                       ; -999.00000; 1743.94458; 2146.81784;         0;437.48989;      ;        ;         ;        ; 1;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;ST.LNA-5.                       ; -999.00000; 1740.81435; 2160.86768;         0;437.43804;      ;        ;         ;        ; 1;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE1I7.                     ; -999.00000; 1734.02994; 2184.23801;         0;437.31909;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AS1.                        ; -999.00000; 1731.13634; 2160.91172;         0;437.23697;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE1.                       ; -999.00000; 1726.08921; 2157.61950;         0;437.11660;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE2.                       ; -999.00000; 1724.57367; 2159.26450;         0;437.13243;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.ADE2T4.                     ;   -1.00000; 1723.31436; 2166.96912;         0;437.33194;      ;        ;         ;        ;23;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE-113.                    ;   -1.00000; 1719.28754; 2155.70306;         0;439.66132;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE-115.                    ;   -1.00000; 1723.27562; 2152.13596;         0;439.03397;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE-114.                    ;   -1.00000; 1721.46242; 2153.73111;         0;439.44235;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-12.                    ;   -1.00000; 1753.64019; 2156.32892;         0;437.43250;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;POT.PAG2A.                      ;   -1.00000; 1714.24545; 2163.56682;         0;438.84916;      ;        ;         ;        ;26;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TAG3.                       ;   -1.00000; 1715.23075; 2166.04457;         0;436.96856;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-TEMP03.                ;   -1.00000; 1752.61175; 2151.89010;         0;436.66556;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AEGIS-3.                    ;   -1.00000; 1719.65943; 2154.34241;         0;436.51006;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-07.                    ;   -1.00000; 1755.21125; 2151.48243;         0;436.94183;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-08.                    ;   -1.00000; 1753.74785; 2151.71440;         0;437.76774;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TAG5.                       ;   -1.00000; 1722.01989; 2155.36849;         0;438.28250;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.LNE-112.                    ;   -1.00000; 1722.07218; 2155.29163;         0;439.98902;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AEGIS-5.                    ;   -1.00000; 1721.98011; 2156.52220;         0;436.64180;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.BA6.                        ;   -1.00000; 1733.14119; 2163.95209;         0;436.79673;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.PUMA-9.                     ;   -1.00000; 1755.10372; 2171.28768;         0;436.99278;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-02.                    ;   -1.00000; 1753.77767; 2156.37567;         0;436.50475;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-03.                    ;   -1.00000; 1757.16633; 2162.97708;         0;436.92059;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-06.                    ;   -1.00000; 1764.67978; 2150.19604;         0;437.61698;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-09.                    ;   -1.00000; 1749.67296; 2153.55729;         0;437.07899;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-11.                    ;   -1.00000; 1759.09362; 2150.85555;         0;436.99100;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-13.                    ;   -1.00000; 1758.64402; 2162.79326;         0;437.23308;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-01.                    ;   -1.00000; 1753.62083; 2156.33813;         0;436.96528;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-TEMP01.                ;   -1.00000; 1750.13582; 2154.80004;         0;436.83312;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;TRIPOD.LNA-2.                   ;   -1.00000; 1742.95852; 2153.24186;         0;436.04401;      ;        ;         ;        ;26;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AEGIS-1.                    ;   -1.00000; 1722.26021; 2152.04315;         0;436.86382;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;POTEX.AEGIS-1.                  ;   -1.00000; 1719.24773; 2155.35977;         0;436.98406;      ;        ;         ;        ; 8;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TAG6.                       ;   -1.00000; 1717.69450; 2159.55089;         0;436.93207;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-05.                    ;   -1.00000; 1766.54903; 2153.96060;         0;437.19640;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;POTEX.2A.                       ;   -1.00000; 1715.07138; 2169.79475;         0;437.04392;      ;        ;         ;        ;26;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TAG2.                       ;   -1.00000; 1715.53014; 2160.84422;         0;437.16936;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.GBAR-04.                    ;   -1.00000; 1759.72743; 2159.84798;         0;436.87597;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;POT.PAG3.                       ;   -1.00000; 1716.41677; 2168.64330;         0;437.15990;      ;        ;         ;        ;26;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.TAG4.                       ;   -1.00000; 1718.49460; 2163.24760;         0;438.14397;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;POT.LNE3.                       ;   -1.00000; 1719.51692; 2165.13687;         0;436.72982;      ;        ;         ;        ; 8;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;NID.AEGIS-4.                    ;   -1.00000; 1719.34152; 2155.33243;         0;438.20508;      ;        ;         ;        ;18;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;ASC.0.                          ;     .00000; 1724.67908; 2155.47015;         0;400.00000;      ;        ;         ;        ;13;O;12-MAR-1999;                    ;;;
193       ;TEMP.2709-2.                    ;     .00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ; 0;O;27-SEP-2013;                    ;;;
193       ;TEMP.2709-3.                    ;     .00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ; 0;O;27-SEP-2013;                    ;;;
193       ;DEM.10S.                        ;     .00000; 1721.90459; 2152.99188;         0;434.96000;      ;        ;         ;        ; 5;O;02-APR-2004;                    ;;;
193       ;TEMP.2709-1.                    ;     .00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ; 0;O;27-SEP-2013;                    ;;;
193       ;INTER-AE-BA.2.                  ;     .00000; 1730.30682; 2154.03565;         0;436.15095;      ;        ;         ;        ;10;O;25-APR-2019;                    ;;Point d'intersection entre LNE01 (AEGIS) et LNE06 (BASE) deuxième virage;
193       ;PAA.0.                          ;     .00000; 1732.83226; 2168.45516;         0;437.26940;      ;        ;         ;        ; 1;O;27-MAY-1997;                    ;;PILIER CENTRAL DU HALL 193;
193       ;ATC.0.                          ;     .00000; 1719.30640; 2167.32941;         0;400.00000;      ;        ;         ;        ;13;O;12-MAR-1999;                    ;;;
193       ;RV_BASE.1.                      ;    1.00000; 1730.00000; 2160.00000;         0;434.95668;      ;        ;         ;        ;10;O;17-OCT-2013;                    ;;rivet de nivellement pour l'expérience BASE;
193       ;CITR.1.                         ;    1.00000; 1779.80084; 2116.21082;         0;436.01963;      ;        ;         ;        ; 1;O;30-JAN-1987;                    ;;PILIER;
193       ;F_BASE.1.                       ;    1.00000; 1732.80100; 2159.14500;         0;434.93800;      ;        ;         ;        ;10;O;17-OCT-2013;                    ;;point à implanter pour expérience;
193       ;POT_BASE.1.                     ;    1.00000; 1730.00000; 2160.00000;         0;434.00000;      ;        ;         ;        ; 1;O;07-OCT-2013;                    ;;potence pour l'expérience BASE;
193       ;POTEX.1.                        ;    1.00000; 1719.24721; 2155.35934;         0;436.91288;      ;        ;         ;        ; 1;O;02-MAR-1999;                    ;;;
193       ;ATPPOT.1.                       ;    1.00000; 1703.82490; 2152.76588;         0;435.84652;      ;        ;         ;        ; 1;O;13-FEB-1986;                    ;;PILIER;
193       ;QN40_S.1.                       ;    1.00000; 1732.54000; 2158.60500;         0;436.13800;      ;        ;         ;        ;10;O;17-OCT-2013;                    ;;point S  du DE5QN40  pour avoir un 3ème point dans la zone.....;
193       ;R_BASE.1.                       ;    1.00000; 1730.00000; 2160.00000;         0;434.95668;      ;        ;         ;        ;10;O;07-OCT-2013;                    ;;;
193       ;DEMQN.10E.                      ;    1.00000; 1730.80377; 2146.61525;         0;436.68803;      ;        ;         ;        ; 8;O;27-MAR-2007;                    ;;;
193       ;AE.1.                           ;    1.00000; 1724.66394; 2155.46678;         0;436.54500;      ;        ;         ;        ; 1;O;05-FEB-1999;                    ;;;
193       ;P_BASE.1.                       ;    1.00000; 1731.49938; 2160.54840;         0;436.86375;      ;        ;         ;        ; 1;O;17-OCT-2013;                    ;;potence pour BASE;
193       ;POT_BASE.2.                     ;    2.00000; 1730.00000; 2160.00000;         0;434.00000;      ;        ;         ;        ; 1;O;07-OCT-2013;                    ;;potence pour l'expérience BASE;
193       ;CITR.2.                         ;    2.00000; 1772.50568; 2122.69223;         0;436.01774;      ;        ;         ;        ; 1;O;30-JAN-1987;                    ;;PILIER;
193       ;AE.2.                           ;    2.00000; 1719.26419; 2167.34805;         0;436.55400;      ;        ;         ;        ; 1;O;05-FEB-1999;                    ;;;
193       ;DEMQN.10S.                      ;    2.00000; 1730.59209; 2146.76661;         0;436.68796;      ;        ;         ;        ; 8;O;27-MAR-2007;                    ;;;
193       ;R_BASE.2.                       ;    2.00000; 1730.00000; 2160.00000;         0;434.95401;      ;        ;         ;        ;10;O;07-OCT-2013;                    ;;;
193       ;P_BASE.2.                       ;    2.00000; 1732.53054; 2162.64601;         0;436.89119;      ;        ;         ;        ; 1;O;08-OCT-2013;                    ;;;
193       ;POTEX.2.                        ;    2.00000; 1714.26095; 2169.39456;         0;436.91477;      ;        ;         ;        ; 1;O;02-MAR-1999;                    ;;;
193       ;RV_BASE.2.                      ;    2.00000; 1730.00000; 2160.00000;         0;434.95401;      ;        ;         ;        ;10;O;17-OCT-2013;                    ;;;
193       ;F_BASE.2.                       ;    2.00000; 1734.56800; 2162.75700;         0;434.93800;      ;        ;         ;        ;10;O;17-OCT-2013;                    ;;point à implanter pour l'expérience;
193       ;INTER-AE-AS.1.                  ;    2.53187; 1729.23323; 2153.20010;         0;436.15096;      ;        ;         ;        ;10;O;25-APR-2019;                    ;;Point d'intersection entre LNE01 (AEGIS) et LNE06 (ASACUSA);
193       ;ATPPOT.3.                       ;    3.00000; 1702.84399; 2152.22556;         0;435.60378;      ;        ;         ;        ; 1;O;13-FEB-1986;                    ;;PILIER;
193       ;P_BASE.3.                       ;    3.00000; 1735.20874; 2163.37224;         0;436.93350;      ;        ;         ;        ; 1;O;08-OCT-2013;                    ;;;
193       ;STDEM.01.                       ;    3.00000; 1721.02532; 2153.62170;         0;436.74959;      ;        ;         ;        ; 8;O;27-MAR-2007;                    ;;;
193       ;R_BASE.3.                       ;    3.00000;     .00000;     .00000;         0;434.96023;      ;        ;         ;        ; 0;O;07-OCT-2013;                    ;;;
193       ;POTEX.3.                        ;    3.00000; 1724.14699; 2186.03143;         0;436.90364;      ;        ;         ;        ; 1;O;02-MAR-1999;                    ;;;
193       ;CITR.3.                         ;    3.00000; 1763.54153; 2127.24451;         0;436.64224;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;RV_BASE.3.                      ;    3.00000; 1730.00000; 2160.00000;         0;434.96023;      ;        ;         ;        ;10;O;17-OCT-2013;                    ;;rivet de nivellement pour l'expérience BASE;
193       ;POT_BASE.3.                     ;    3.00000; 1730.00000; 2160.00000;         0;434.00000;      ;        ;         ;        ; 1;O;07-OCT-2013;                    ;;potence pour l'expérience BASE;
193       ;CITR.4.                         ;    4.00000; 1759.15460; 2129.95378;         0;436.65554;      ;        ;         ;        ; 8;O;07-MAY-1987;                    ;;PILIER;
193       ;POT_BASE.4.                     ;    4.00000; 1730.00000; 2160.00000;         0;434.00000;      ;        ;         ;        ; 1;O;07-OCT-2013;                    ;;potence pour l'expérience BASE;
193       ;POTEX.4.                        ;    4.00000; 1735.73662; 2189.23826;         0;436.94118;      ;        ;         ;        ; 1;O;02-MAR-1999;                    ;;;
193       ;R_BASE.4.                       ;    4.00000; 1730.00000; 2160.00000;         0;434.96523;      ;        ;         ;        ;10;O;07-OCT-2013;                    ;;;
193       ;RV_BASE.4.                      ;    4.00000; 1730.00000; 2160.00000;         0;434.96523;      ;        ;         ;        ;10;O;17-OCT-2013;                    ;;rivet de nivellement pour l'expérience BASE;
193       ;P_BASE.4.                       ;    4.00000; 1736.07360; 2161.53466;         0;436.85044;      ;        ;         ;        ; 1;O;08-OCT-2013;                    ;;;
193       ;INTER-AT-AL.1.                  ;    4.28800; 1719.30553; 2167.32740;         0;436.15091;      ;        ;         ;        ;10;O;25-APR-2019;                    ;;Point d'intersection entre LNE04 (ATRAP) et LNE04 (ALPHA);
193       ;POTEX.5.                        ;    5.00000; 1731.28032; 2162.05276;         0;436.42297;      ;        ;         ;        ; 1;O;13-JUL-1998;                    ;;;
193       ;CITR.5.                         ;    5.00000; 1749.93255; 2134.87742;         0;436.01565;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;BS.5.                           ;    5.00000; 1717.35072; 2151.39978;         0;436.78253;      ;        ;         ;        ; 1;O;26-MAR-2001;                    ;;;
193       ;POT.AD5.                        ;    5.00000; 1718.61086; 2147.80258;         0;436.40557;      ;        ;         ;        ; 1;O;10-JAN-2001;                    ;;;
193       ;CIPOTA.6.                       ;    6.00000; 1778.92734; 2113.80490;         0;436.58800;      ;        ;         ;        ; 5;O;13-JUL-1987;                    ;;PILIER;
193       ;POTEX.6.                        ;    6.00000; 1733.94891; 2167.44660;         0;436.41823;      ;        ;         ;        ; 1;O;02-MAR-1999;                    ;;;
193       ;INTER-AS-AS1.1.                 ;    6.83494; 1726.66674; 2159.53489;         0;436.15091;      ;        ;         ;        ;10;O;26-APR-2019;                    ;;Point d'intersection entre LNE06 (ASACUSA) côté expérience et LNE06 vers ASACUSA 1;
193       ;INTER-AE-AT.1.                  ;    6.96000; 1725.01638; 2154.72325;         0;436.15099;      ;        ;         ;        ;10;O;25-APR-2019;                    ;;Point d'intersection entre LNE01 vers LNE 02 (AEGIS) et LNE01 vers LNE03 (ATRAP);
193       ;POTEX.7.                        ;    7.00000; 1724.25511; 2154.45997;         0;436.38348;      ;        ;         ;        ; 1;O;02-FEB-1999;                    ;;;
193       ;CIPOT.7.                        ;    7.00000; 1776.46125; 2122.60988;         0;436.30595;      ;        ;         ;        ; 1;O;03-AUG-1987;                    ;;PILIER;
193       ;INTER-AE-BA.1.                  ;    7.73050; 1730.88744; 2152.60258;         0;436.15096;      ;        ;         ;        ;10;O;25-APR-2019;                    ;;Point d'intersection entre LNE01 (AEGIS) et LNE07 (BASE) premier virage;
193       ;CIPOT.8.                        ;    8.00000; 1773.80859; 2124.65384;         0;436.30225;      ;        ;         ;        ; 1;O;03-AUG-1987;                    ;;PILIER;
193       ;POTEX.8.                        ;    8.00000; 1718.77457; 2166.58167;         0;436.39115;      ;        ;         ;        ; 1;O;02-FEB-1999;                    ;;;
193       ;CIPOT.9.                        ;    9.00000; 1771.34286; 2126.53441;         0;436.30993;      ;        ;         ;        ; 1;O;03-AUG-1987;                    ;;PILIER;
193       ;AS.10.                          ;   10.00000; 1724.03928; 2155.66725;         0;434.95869;      ;        ;         ;        ;10;O;19-AUG-1998;                    ;;*modif 25-0ct-2004-old_curr_day= 19-AUG-1998 00:00:01*;
193       ;ST.2709-1.                      ;   10.00000; 1730.00000; 2160.00000;         0;435.00000;      ;        ;         ;        ; 6;O;07-OCT-2013;                    ;;station AT401 pour reseau BASE;
193       ;AS.11.                          ;   11.00000; 1725.30876; 2154.78392;         0;434.95769;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;BS.11.                          ;   11.00000; 1710.48670; 2168.56351;         0;436.78286;      ;        ;         ;        ; 1;O;10-FEB-2001;                    ;;;
193       ;AS.12.                          ;   12.00000; 1729.52816; 2163.25180;         0;434.96735;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;AS.13.                          ;   13.00000; 1732.49145; 2169.20762;         0;434.96752;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;AS.14.                          ;   14.00000; 1728.88710; 2166.68560;         0;434.97115;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;BE.17.                          ;   17.00000; 1717.83936; 2185.34777;         0;436.78254;      ;        ;         ;        ; 1;O;01-FEB-2001;                    ;;;
193       ;BE.18.                          ;   18.00000; 1720.10344; 2188.01013;         0;400.00000;      ;        ;         ;        ; 1;O;13-JUL-1998;                    ;;;
193       ;ST.2709-2.                      ;   20.00000; 1735.00000; 2165.00000;         0;435.00000;      ;        ;         ;        ; 6;O;07-OCT-2013;                    ;;station AT401 pour reseau BASE;
193       ;AT.20.                          ;   20.00000; 1720.36895; 2163.76941;         0;434.96455;      ;        ;         ;        ;10;O;19-AUG-1998;                    ;;*modif 25-0ct-2004-old_curr_day= 19-AUG-1998 00:00:01*;
193       ;AT.21.                          ;   21.00000; 1718.22869; 2160.37795;         0;434.96408;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;*modif 25-0ct-2004-old_curr_day= 13-JUL-1998 00:00:01*;
193       ;AT.22.                          ;   22.00000; 1720.75420; 2164.86956;         0;434.97217;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;AT.23.                          ;   23.00000; 1723.19379; 2170.40857;         0;434.96504;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;BBS.23.                         ;   23.00000; 1735.81400; 2193.32400;         0;400.00000;      ;        ;         ;        ; 1;O;06-JUL-1998;                    ;;;
193       ;BE.23.                          ;   23.00000; 1734.43749; 2193.38310;         0;436.78312;      ;        ;         ;        ; 1;O;14-JUL-1998;                    ;;*modif 25-0ct-2004-old_curr_day= 13-JUL-1998 00:00:01*;
193       ;BS.24.                          ;   24.00000; 1739.23215; 2192.58199;         0;400.00000;      ;        ;         ;        ; 1;O;13-JUL-1998;                    ;;;
193       ;AT.24.                          ;   24.00000; 1725.89199; 2176.01676;         0;434.95389;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;POT.AD24.                       ;   24.00000; 1738.62343; 2195.22713;         0;436.82162;      ;        ;         ;        ; 1;O;01-JUN-2016;                    ;;;
193       ;AT.25.                          ;   25.00000; 1719.35349; 2168.91647;         0;434.97260;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;AT.26.                          ;   26.00000; 1722.28884; 2174.71873;         0;434.98476;      ;        ;         ;        ;10;O;13-JUL-1998;                    ;;;
193       ;DE.31.                          ;   31.00000; 1722.06462; 2158.15802;         0;437.98852;      ;        ;         ;        ;10;O;02-MAR-1999;                    ;;;
193       ;DE.32.                          ;   32.00000; 1720.89251; 2160.68301;         0;437.98584;      ;        ;         ;        ;10;O;02-MAR-1999;                    ;;;
193       ;BE.33.                          ;   33.00000; 1764.43145; 2180.12122;         0;436.78294;      ;        ;         ;        ; 1;O;13-JUL-1998;                    ;;;
193       ;POT.AD33.                       ;   33.00000; 1767.62171; 2181.09970;         0;436.76749;      ;        ;         ;        ; 1;O;01-JUN-2016;                    ;;;
193       ;DE.33.                          ;   33.00000; 1722.57070; 2161.34022;         0;437.98335;      ;        ;         ;        ;10;O;02-MAR-1999;                    ;;;
193       ;DE.34.                          ;   34.00000; 1723.83059; 2158.91578;         0;437.98881;      ;        ;         ;        ;10;O;02-MAR-1999;                    ;;;
193       ;BS.39.                          ;   39.00000; 1772.40753; 2162.14715;         0;400.00000;      ;        ;         ;        ; 1;O;13-JUL-1998;                    ;;;
193       ;BE.39.                          ;   39.00000; 1772.46664; 2163.52263;         0;436.78293;      ;        ;         ;        ; 1;O;01-FEB-2001;                    ;;;
193       ;DE.41.                          ;   41.00000; 1720.02047; 2162.43665;         0;437.98505;      ;        ;         ;        ;10;O;02-MAR-1999;                    ;;;
193       ;DE.42.                          ;   42.00000; 1718.82712; 2165.36257;         0;437.98342;      ;        ;         ;        ;10;O;02-MAR-1999;                    ;;;
193       ;DE.43.                          ;   43.00000; 1720.37606; 2165.89165;         0;437.98293;      ;        ;         ;        ;10;O;02-MAR-1999;                    ;;;
193       ;DE.44.                          ;   44.00000; 1721.81766; 2163.18356;         0;437.98362;      ;        ;         ;        ;10;O;02-MAR-1999;                    ;;;
193       ;BE.46.                          ;   46.00000; 1762.79412; 2142.70059;         0;436.78258;      ;        ;         ;        ; 1;O;01-FEB-2001;                    ;;;
193       ;POT.AD49.                       ;   49.00000; 1754.15520; 2135.40108;         0;436.79688;      ;        ;         ;        ; 1;O;01-JUN-2016;                    ;;;
193       ;BS.52.                          ;   52.00000; 1743.66774; 2138.12577;         0;436.78300;      ;        ;         ;        ; 1;O;01-FEB-2001;                    ;;;
193       ;POTATP.1.                       ;   89.00000;     .00000;     .00000;         0;400.00000;      ;        ;         ;        ; 8;O;23-MAY-2016;                    ;;;
193       ;TRIPOD.LNA-1.                   ;  101.51008; 1754.92833; 2162.23118;         0;436.68609;      ;        ;         ;        ;26;O;01-JUN-2016;                    ;;;
193       ;ST.10.                          ;  123.50874; 1749.51760; 2154.39725;         0;437.42894;      ;        ;         ;        ; 1;O;26-OCT-2017;                    ;;;
193       ;NID.LNA-1.                      ;  123.50874; 1739.51044; 2144.78584;         0;437.69033;      ;        ;         ;        ;24;O;01-AUG-2016;                    ;;;
193       ;NID.LNA-3.                      ;  123.50874; 1750.67041; 2162.25494;         0;437.40294;      ;        ;         ;        ;24;O;01-AUG-2016;                    ;;;
193       ;NID.LNA-2.                      ;  123.50900; 1738.06536; 2156.97794;         0;437.44205;      ;        ;         ;        ;24;O;05-FEB-2022;                    ;;Mesure globale Hall ELENA calage sur LNR dipole;
193       ;PAA C.206.                      ;  206.00000; 1723.24454; 2150.51081;         0;437.19683;      ;        ;         ;        ; 1;O;27-MAY-1997;                    ;;PILIER;
193       ;PAA C.504.                      ;  504.00000; 1715.41669; 2167.19329;         0;437.23256;      ;        ;         ;        ; 1;O;27-MAY-1997;                    ;;PILIER;
193       ;DRPE.507.                       ;  507.00000;     .00000;     .00000;         0;436.78286;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;;
193       ;PAC E.507.                      ;  507.00000; 1718.46464; 2150.58246;         0;436.77284;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;DRPS.507.                       ;  507.00000; 1717.35050; 2151.39831;         0;436.78301;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;PAA C.608.                      ;  608.00000; 1715.53580; 2175.74479;         0;437.23893;      ;        ;         ;        ; 1;O;22-MAR-1994;                    ;;PILIER;
193       ;PAA C.704.                      ;  704.00000; 1717.45069; 2179.62881;         0;437.24112;      ;        ;         ;        ; 1;O;22-MAR-1994;                    ;;PILIER;
193       ;PAC S.807.                      ;  807.00000; 1711.76358; 2159.12140;         0;400.00000;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;04-02-2022 modif H=400 au lieu de 0 - PILIER;
193       ;PAA C.808.                      ;  808.00000; 1724.19898; 2184.92736;         0;437.23264;      ;        ;         ;        ; 1;O;27-MAY-1997;                    ;;PILIER;
193       ;DRPE.907.                       ;  907.00000;     .00000;     .00000;         0;436.88390;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;;
193       ;DRPS.907.                       ;  907.00000;     .00000;     .00000;         0;436.88382;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;;
193       ;PAA C.1106.                     ; 1106.00000; 1742.25175; 2189.23663;         0;437.19764;      ;        ;         ;        ; 1;O;16-APR-1997;                    ;;PILIER;
193       ;DRPE.1107.                      ; 1107.00000;     .00000;     .00000;         0;436.78472;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;;
193       ;DRPS.1107.                      ; 1107.00000; 1710.48480; 2168.56211;         0;436.78332;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;PAC E.1207.                     ; 1207.00000; 1710.83602; 2170.65462;         0;436.77216;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;PAA C.1406.                     ; 1406.00000; 1759.66942; 2180.21241;         0;437.19764;      ;        ;         ;        ; 1;O;27-MAY-1997;                    ;;PILIER;
193       ;PAA C.1704.                     ; 1704.00000; 1767.56097; 2163.54203;         0;437.22731;      ;        ;         ;        ; 1;O;27-MAY-1997;                    ;;PILIER;
193       ;DRPE.1707.                      ; 1707.00000; 1717.83885; 2185.34774;         0;400.00000;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;PAC S.1707.                     ; 1707.00000; 1718.65519; 2186.45998;         0;436.77260;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;DRPE.1807.                      ; 1807.00000;     .00000;     .00000;         0;436.78244;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;PAA C.1808.                     ; 1808.00000; 1767.56934; 2154.87984;         0;437.23667;      ;        ;         ;        ; 1;O;22-MAR-1994;                    ;;PILIER;
193       ;PAA C.1904.                     ; 1904.00000; 1765.62223; 2150.93264;         0;437.23555;      ;        ;         ;        ; 1;O;22-MAR-1994;                    ;;PILIER;
193       ;PAC S.2007.                     ; 2007.00000; 1726.37659; 2192.04376;         0;400.00000;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;04-02-2022 modif H=400 au lieu de 0 - PILIER;
193       ;PAA C.2008.                     ; 2008.00000; 1758.72070; 2145.73574;         0;437.22847;      ;        ;         ;        ; 1;O;27-MAY-1997;                    ;;PILIER;
193       ;PAA C.2306.                     ; 2306.00000; 1740.66445; 2141.67738;         0;437.21988;      ;        ;         ;        ; 1;O;27-MAY-1997;                    ;;PILIER;
193       ;DRPS.2307.                      ; 2307.00000; 1735.81591; 2193.32500;         0;436.78320;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;DRPE.2307.                      ; 2307.00000;     .00000;     .00000;         0;436.78356;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;;
193       ;PAC E.2407.                     ; 2407.00000; 1737.90522; 2192.97470;         0;436.77265;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;PAC S.3307.                     ; 3307.00000; 1765.54300; 2179.30764;         0;436.77298;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;DRPE.3307.                      ; 3307.00000; 1764.43353; 2180.12180;         0;400.00000;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;DRPS.3407.                      ; 3407.00000;     .00000;     .00000;         0;436.78259;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;DRPE.3407.                      ; 3407.00000;     .00000;     .00000;         0;436.78244;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;PAC S.3607.                     ; 3607.00000; 1771.12908; 2171.58708;         0;400.00000;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;04-02-2022 modif H=400 au lieu de 0 - PILIER;
193       ;DRPS.3907.                      ; 3907.00000; 1772.40588; 2162.15376;         0;436.78229;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;;
193       ;DRPE.3907.                      ; 3907.00000;     .00000;     .00000;         0;436.78286;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;PAC E.4007.                     ; 4007.00000; 1772.05810; 2160.05483;         0;436.77245;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;PAC E.4507.                     ; 4507.00000; 1765.05460; 2145.36115;         0;436.77281;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
193       ;DRPS.4607.                      ; 4607.00000;     .00000;     .00000;         0;436.78130;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;;
193       ;DRPE.4607.                      ; 4607.00000; 1762.79388; 2142.70119;         0;436.78102;      ;        ;         ;        ; 1;O;07-APR-1998;                    ;;PILIER DIPOLE AD;
193       ;PAC S.4807.                     ; 4807.00000; 1756.52292; 2138.66705;         0;400.00000;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;04-02-2022 modif H=400 au lieu de 0 - PILIER;
193       ;DRPS.5207.                      ; 5207.00000; 1743.66620; 2138.12542;         0;436.78187;      ;        ;         ;        ; 8;O;06-MAY-1998;                    ;;;
193       ;DRPE.5207.                      ; 5207.00000;     .00000;     .00000;         0;436.78185;      ;        ;         ;        ; 1;O;20-APR-1998;                    ;;;
193       ;PAC S.5207.                     ; 5207.00000; 1743.67018; 2138.12573;         0;436.77312;      ;        ;         ;        ; 1;O;07-MAY-1987;                    ;;PILIER;
LNR       ;BPMEB.0110.E                    ;     .67798; 1738.64190; 2151.33199;2436.19602;436.20082;  .806; -.000001; -.000009;  2.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0105.E                    ;     .72098; 1738.73239; 2151.37114;2436.35012;436.35491;  .806; -.000001; -.000009;  2.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0105.S                    ;     .89098; 1738.73971; 2151.54098;2436.35012;436.35491;  .000; -.000001; -.000009;  2.7390;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEB.0110.T                    ;     .93398; 1738.66339; 2151.58730;2436.11636;436.12116;99.999; -.000001; -.000009;  2.7390;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEB.0110.S                    ;     .93398; 1738.60856; 2151.58967;2436.22886;436.23366;  .000; -.000001; -.000009;  2.7390;24;O;24-OCT-2024;  ;;;
LNR       ;MSMIE.0115.E                    ;    2.78638; 1738.51284; 2153.44790;2436.33809;436.34291; 2.109; -.000001; -.000009;  2.7390; 1;A;24-OCT-2024;  ;;;
LNR       ;MSMIE.0115.S                    ;    3.06549; 1738.54719; 2153.72579;2436.33809;436.34291;  .000; -.000001; -.000009;  2.7390; 1;O;24-OCT-2024;  ;;;
LNR       ;LBTV.0117.E                     ;    3.27955; 1738.68258; 2153.93421;2436.46681;436.47163;  .258; -.000001; -.000009;  2.7390; 1;A;24-OCT-2024;  ;;;
LNR       ;LBTV.0117.S                     ;    3.66303; 1738.69908; 2154.31734;2436.46681;436.47163;  .231; -.000001; -.000009;  2.7390; 1;O;24-OCT-2024;  ;;;
LNR       ;MKKFH.0120.E                    ;    3.75198; 1738.70289; 2154.40622;2436.72890;436.73372;  .065; -.000001; -.000009;  2.7390; 1;A;24-OCT-2024;  ;;;
LNR       ;MKKFH.0120.S                    ;    4.14198; 1738.71967; 2154.79586;2436.72917;436.73399;  .432; -.000001; -.000009;  2.7390; 1;O;24-OCT-2024;  ;;;
LNR       ;BPMEB.0125.E                    ;    4.53600; 1738.80784; 2155.18645;2436.19598;436.20080;  .372; -.000001; -.000009;  2.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0130.E                    ;    4.57900; 1738.89833; 2155.22559;2436.35009;436.35491;  .372; -.000001; -.000009;  2.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0130.S                    ;    4.74900; 1738.90564; 2155.39543;2436.35008;436.35490;  .000; -.000001; -.000009;  2.7390;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEB.0125.S                    ;    4.79200; 1738.77450; 2155.44412;2436.22882;436.23365;  .000; -.000001; -.000009;  2.7390;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEB.0125.T                    ;    4.79200; 1738.82932; 2155.44176;2436.11632;436.12114;99.999; -.000001; -.000009;  2.7390;24;O;24-OCT-2024;  ;;;
LNR       ;MBHEK.0135.E                    ;    5.21312; 1739.16189; 2155.65026;2436.73109;436.73590;  .321;  .000000;  .000011; 36.0724; 1;A;24-OCT-2024;  ;;;
LNR       ;MBHEK.0135.S                    ;    5.68312; 1739.41418; 2156.04681;2436.73110;436.73591;  .485;  .000000;  .000011; 36.0724; 1;O;24-OCT-2024;  ;;;
LNR       ;MXNAD.0201.E                    ;    6.14527; 1739.52032; 2156.31888;2436.35119;436.35599;  .200;  .000001;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MXNAD.0201.S                    ;    6.31532; 1739.67106; 2156.39756;2436.35109;436.35589;  .150;  .000001;  .000030; 69.4057;24;O;24-OCT-2024;  ;;;
LNR       ;MQNLG.0205.E                    ;    6.49500; 1739.83045; 2156.48051;2436.49110;436.49589;  .150;  .000981;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0205.S                    ;    6.66500; 1739.98119; 2156.55910;2436.49111;436.49590;  .250;  .000981;  .000030; 69.4057;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0215.E                    ;    6.87920; 1740.10566; 2156.78372;2436.19602;436.20080;  .125;  .000001;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0210.E                    ;    6.96820; 1740.25002; 2156.69933;2436.49122;436.49600;  .223; -.000619;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0210.S                    ;    7.13820; 1740.40076; 2156.77792;2436.49123;436.49600;  .125; -.000619;  .000030; 69.4057;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0215.T                    ;    7.22720; 1740.41909; 2156.93531;2436.11637;436.12114;99.999;  .000001;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEA.0215.S                    ;    7.22720; 1740.39372; 2156.98396;2436.22887;436.23364;  .000;  .000001;  .000030; 69.4057;24;O;24-OCT-2024;  ;;;
LNR       ;MXNAD.0220.E                    ;    7.35825; 1740.59590; 2156.87962;2436.35120;436.35596;  .190;  .000001;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MXNAD.0220.S                    ;    7.52826; 1740.74654; 2156.95844;2436.35121;436.35597;  .150;  .000001;  .000030; 69.4057;24;O;24-OCT-2024;  ;;;
LNR       ;MCCAY.0230.E                    ;    8.95390; 1741.98637; 2157.66416;2436.35018;436.35490;  .472;  .000001;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0230.S                    ;    9.12390; 1742.13711; 2157.74275;2436.35019;436.35490;  .000;  .000001;  .000030; 69.4057;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0240.E                    ;    9.25240; 1742.21005; 2157.88083;2436.19609;436.20080;  .125;  .000001;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0235.E                    ;    9.34140; 1742.35443; 2157.79638;2436.49118;436.49588;  .262;  .000331;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0235.S                    ;    9.51140; 1742.50518; 2157.87497;2436.49118;436.49588;  .125;  .000331;  .000030; 69.4057;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0240.T                    ;    9.60040; 1742.52348; 2158.03241;2436.11644;436.12114;99.999;  .000001;  .000030; 69.4057;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEA.0240.S                    ;    9.60040; 1742.49811; 2158.08107;2436.22894;436.23364;  .000;  .000001;  .000030; 69.4057;24;O;24-OCT-2024;  ;;;
LNR       ;MBHEK.0245.E                    ;   10.07952; 1742.92174; 2157.87545;2436.73122;436.73590;  .300;  .000002;  .000035;102.7390; 1;A;24-OCT-2024;  ;;;
LNR       ;MBHEK.0245.S                    ;   10.54952; 1743.39131; 2157.85524;2436.73124;436.73590;  .485;  .000002;  .000035;102.7390; 1;O;24-OCT-2024;  ;;;
LNR       ;MQNLG.0305.E                    ;   11.16140; 1743.80630; 2157.81895;2436.49121;436.49585;  .300;  .000462;  .000040;136.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0305.S                    ;   11.33140; 1743.94973; 2157.72769;2436.49122;436.49586;  .250;  .000462;  .000040;136.0723;24;O;24-OCT-2024;  ;;;
LNR       ;ZDFHL.0310.E                    ;   11.75640; 1744.42261; 2157.67920;2436.40428;436.40890;  .315;  .000002;  .000040;136.0723; 1;A;24-OCT-2024;  ;;;
LNR       ;ZDFHL.0310.S                    ;   12.11640; 1744.72635; 2157.48596;2436.40430;436.40890;  .500;  .000002;  .000040;136.0723; 1;O;24-OCT-2024;  ;;;
LNR       ;BQKMA.0312.E                    ;   12.69470; 1745.19187; 2157.14031;2436.21863;436.22321;  .469;  .000002;  .000040;136.0723;24;A;24-OCT-2024;  ;;;
LNR       ;BQKMA.0312.A                    ;   12.69470; 1745.23656; 2157.21055;2436.21863;436.22321;99.999;  .000002;  .000040;136.0723;24;O;24-OCT-2024;  ;;;
LNR       ;BQKMA.0312.B                    ;   12.96500; 1745.46461; 2157.06545;2436.21864;436.22321;99.999;  .000002;  .000040;136.0723;24;O;24-OCT-2024;  ;;;
LNR       ;BQKMA.0312.S                    ;   12.96500; 1745.42006; 2156.99543;2436.21864;436.22321;  .350;  .000002;  .000040;136.0723;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0320.E                    ;   13.44560; 1745.80957; 2156.71233;2436.19625;436.20080;  .125;  .000002;  .000040;136.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0315.E                    ;   13.53460; 1745.80856; 2156.54495;2436.49136;436.49591;  .490; -.000448;  .000040;136.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0315.S                    ;   13.70460; 1745.95199; 2156.45370;2436.49137;436.49591;  .125; -.000448;  .000040;136.0723;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0320.S                    ;   13.79360; 1746.12701; 2156.56298;2436.22910;436.23364;  .000;  .000002;  .000040;136.0723;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEA.0320.T                    ;   13.79360; 1746.09756; 2156.51668;2436.11660;436.12114;99.999;  .000002;  .000040;136.0723;24;O;24-OCT-2024;  ;;;
LNR       ;MQNLG.0325.E                    ;   14.00780; 1746.20779; 2156.29092;2436.49138;436.49591;  .223;  .001012;  .000040;136.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0325.S                    ;   14.17780; 1746.35122; 2156.19967;2436.49138;436.49591;  .250;  .001012;  .000040;136.0723;24;O;24-OCT-2024;  ;;;
LNR       ;MCCAY.0330.E                    ;   14.34230; 1746.51850; 2156.15613;2436.35038;436.35490;  .210;  .000002;  .000040;136.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0330.S                    ;   14.51230; 1746.66193; 2156.06488;2436.35039;436.35490;  .000;  .000002;  .000040;136.0723;24;O;24-OCT-2024;  ;;;
LNR       ;MBHEK.0335.E                    ;   14.94592; 1746.72874; 2155.73192;2436.73139;436.73590;  .290;  .000001;  .000025;169.4057; 1;A;24-OCT-2024;  ;;;
LNR       ;MBHEK.0335.S                    ;   15.41592; 1746.94601; 2155.31515;2436.73141;436.73590;  .485;  .000001;  .000025;169.4057; 1;O;24-OCT-2024;  ;;;
LNR       ;MCCAY.0405.E                    ;   15.87155; 1747.18169; 2155.09145;2436.35042;436.35490;  .268;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEB.0407.E                    ;   15.88080; 1747.26994; 2155.07839;2436.19632;436.20080;  .052;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0405.S                    ;   16.04155; 1747.17437; 2154.92160;2436.35042;436.35490;  .000;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEB.0407.S                    ;   16.13680; 1747.30328; 2154.82071;2436.22917;436.23365;  .000;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEB.0407.T                    ;   16.13680; 1747.24846; 2154.82307;2436.11667;436.12115;99.999;  .000000;  .000009;202.7390;24;O;24-OCT-2024;  ;;;
LNR       ;MLNAF.0410.E                    ;   16.32780; 1747.10797; 2154.63795;2436.41131;436.41579;  .224;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MLNAF.0410.S                    ;   16.49780; 1747.10066; 2154.46810;2436.41131;436.41579;  .360;  .000000;  .000009;202.7390;24;O;24-OCT-2024;  ;;;
LNR       ;VANNE.0425.E                    ;   16.73030; 1747.20469; 2154.23091;2436.41343;436.41790;  .138;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0420.E                    ;   16.83898; 1747.45978; 2154.11115;2436.35044;436.35490;  .194;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0420.S                    ;   17.00898; 1747.45247; 2153.94130;2436.35044;436.35490;  .000;  .000000;  .000009;202.7390;24;O;24-OCT-2024;  ;;;
LNR       ;LNTML.0430.E                    ;   17.53780; 1747.26986; 2153.41986;2436.50744;436.51191;  .432;  .000000;  .000009;202.7390; 1;A;24-OCT-2024;  ;;;
LNR       ;LNTML.0430.S                    ;   18.33780; 1747.23546; 2152.62060;2436.50745;436.51191;  .000;  .000000;  .000009;202.7390; 1;O;24-OCT-2024;  ;;;
LNR       ;MCCAY.0440.E                    ;   18.86662; 1747.05287; 2152.09915;2436.35045;436.35491;  .144;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0440.S                    ;   19.03662; 1747.04555; 2151.92930;2436.35045;436.35491;  .000;  .000000;  .000009;202.7390;24;O;24-OCT-2024;  ;;;
LNR       ;VANNE.0435.S                    ;   19.14531; 1747.10082; 2151.81814;2436.41345;436.41791;  .000;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MLNAF.0450.E                    ;   19.37780; 1746.97679; 2151.59077;2436.41134;436.41580;  .137;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MLNAF.0450.S                    ;   19.54780; 1746.96948; 2151.42093;2436.41134;436.41580;  .360;  .000000;  .000009;202.7390;24;O;24-OCT-2024;  ;;;
LNR       ;MCCAY.0460.E                    ;   19.72955; 1747.01575; 2151.23702;2436.35046;436.35492;  .172;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEB.0457.E                    ;   19.73880; 1747.10401; 2151.22396;2436.19636;436.20081;  .052;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0460.S                    ;   19.89955; 1747.00844; 2151.06717;2436.35046;436.35492;  .000;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEB.0457.T                    ;   19.99480; 1747.08253; 2150.96864;2436.11670;436.12115;99.999;  .000000;  .000009;202.7390;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEB.0457.S                    ;   19.99480; 1747.13734; 2150.96628;2436.22920;436.23365;  .000;  .000000;  .000009;202.7390;24;O;24-OCT-2024;  ;;;
LNR       ;MBHEK.0470.E                    ;   20.41592; 1746.74992; 2150.76015;2436.73145;436.73591;  .321; -.000002; -.000011;236.0724; 1;A;24-OCT-2024;  ;;;
LNR       ;MBHEK.0470.S                    ;   20.88592; 1746.49763; 2150.36360;2436.73145;436.73592;  .485; -.000002; -.000011;236.0724; 1;O;24-OCT-2024;  ;;;
LNR       ;MXNAD.0505.E                    ;   21.34802; 1746.39152; 2150.09164;2436.35141;436.35588;  .200; -.000003; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MXNAD.0505.S                    ;   21.51801; 1746.24078; 2150.01307;2436.35132;436.35580;  .150; -.000003; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;MQNLG.0510.E                    ;   21.69780; 1746.08140; 2149.92984;2436.49142;436.49590;  .150;  .000127; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0510.S                    ;   21.86780; 1745.93066; 2149.85126;2436.49142;436.49591;  .250;  .000127; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0520.E                    ;   22.08200; 1745.80619; 2149.62668;2436.19633;436.20082;  .125; -.000003; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0515.E                    ;   22.17100; 1745.66178; 2149.71113;2436.49143;436.49593;  .223;  .000747; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0515.S                    ;   22.34100; 1745.51104; 2149.63254;2436.49142;436.49592;  .125;  .000747; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0520.S                    ;   22.43000; 1745.51812; 2149.42644;2436.22916;436.23366;  .000; -.000003; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEA.0520.T                    ;   22.43000; 1745.49276; 2149.47509;2436.11666;436.12116;99.999; -.000003; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;MXNAD.0525.E                    ;   22.56097; 1745.31602; 2149.53077;2436.35138;436.35589;  .190; -.000003; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MXNAD.0525.S                    ;   22.73091; 1745.16526; 2149.45235;2436.35133;436.35585;  .150; -.000003; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;ACW02.0530.E                    ;   22.86000; 1745.14925; 2149.20382;2436.47140;436.47591;  .084; -.000003; -.000030;269.4057; 1;A;24-OCT-2024;  ;;;
LNR       ;ACW02.0530.S                    ;   23.05000; 1744.98077; 2149.11599;2436.47140;436.47592;  .300; -.000003; -.000030;269.4057; 1;O;24-OCT-2024;  ;;;
LNR       ;MCCAY.0535.E                    ;   23.37000; 1744.62305; 2149.10993;2436.35038;436.35491;  .350; -.000003; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0535.S                    ;   23.54000; 1744.47231; 2149.03134;2436.35038;436.35492;  .000; -.000003; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;MQSAB.0540.E                    ;   23.65500; 1744.34591; 2149.02503;2436.49142;436.49597;  .210; -.000123; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQSAB.0540.S                    ;   23.82500; 1744.19516; 2148.94644;2436.49142;436.49597;  .150; -.000123; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;BSSHV.0545.E                    ;   24.05295; 1743.99328; 2148.84059;2436.52359;436.52815;  .048; -.000003; -.000030;269.4057; 1;A;24-OCT-2024;  ;;;
LNR       ;BSSHV.0545.S                    ;   24.22295; 1743.84253; 2148.76200;2436.52461;436.52917;  .550; -.000003; -.000030;269.4057; 1;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0555.E                    ;   24.45520; 1743.70180; 2148.52957;2436.19625;436.20082;  .125; -.000003; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0550.E                    ;   24.54420; 1743.55739; 2148.61404;2436.49130;436.49587;  .091; -.000383; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0550.S                    ;   24.71420; 1743.40664; 2148.53545;2436.49129;436.49587;  .125; -.000383; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0555.S                    ;   24.80320; 1743.41374; 2148.32934;2436.22909;436.23367;  .000; -.000003; -.000030;269.4057;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEA.0555.T                    ;   24.80320; 1743.38838; 2148.37799;2436.11658;436.12116;99.999; -.000003; -.000030;269.4057;24;O;24-OCT-2024;  ;;;
LNR       ;MBHEK.0560.E                    ;   25.28232; 1742.99006; 2148.53496;2436.73133;436.73593;  .300; -.000003; -.000035;302.7390; 1;A;24-OCT-2024;  ;;;
LNR       ;MBHEK.0560.S                    ;   25.75232; 1742.52050; 2148.55518;2436.73131;436.73592;  .485; -.000003; -.000035;302.7390; 1;O;24-OCT-2024;  ;;;
LNR       ;MQNLG.0605.E                    ;   26.36420; 1742.10558; 2148.59156;2436.49120;436.49583;  .300;  .001097; -.000040;336.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0605.S                    ;   26.53420; 1741.96215; 2148.68281;2436.49119;436.49583;  .250;  .001097; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;ZDFHL.0610.E                    ;   26.95920; 1741.48922; 2148.73121;2436.40426;436.40892;  .315; -.000003; -.000040;336.0723; 1;A;24-OCT-2024;  ;;;
LNR       ;ZDFHL.0610.S                    ;   27.31920; 1741.18548; 2148.92445;2436.40425;436.40892;  .500; -.000003; -.000040;336.0723; 1;O;24-OCT-2024;  ;;;
LNR       ;BQKMA.0612.E                    ;   27.89750; 1740.71984; 2149.26988;2436.21854;436.22323;  .469; -.000003; -.000040;336.0723;24;A;24-OCT-2024;  ;;;
LNR       ;BQKMA.0612.A                    ;   27.89750; 1740.67529; 2149.19985;2436.21854;436.22323;99.999; -.000003; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;BQKMA.0612.S                    ;   28.16780; 1740.49178; 2149.41498;2436.21853;436.22323;  .350; -.000003; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;BQKMA.0612.B                    ;   28.16780; 1740.44723; 2149.34495;2436.21853;436.22324;99.999; -.000003; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;MCCAY.0615.E                    ;   28.32970; 1740.41879; 2149.60186;2436.35021;436.35492;  .207; -.000003; -.000040;336.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MCCAY.0615.S                    ;   28.49970; 1740.27536; 2149.69312;2436.35020;436.35492;  .000; -.000003; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0625.E                    ;   28.64840; 1740.10227; 2149.69807;2436.19610;436.20082;  .125; -.000003; -.000040;336.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0620.E                    ;   28.73740; 1740.10320; 2149.86536;2436.49121;436.49593;  .283; -.000193; -.000040;336.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0620.S                    ;   28.90740; 1739.95977; 2149.95661;2436.49121;436.49594;  .125; -.000193; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;BPMEA.0625.S                    ;   28.99640; 1739.78483; 2149.84742;2436.22892;436.23366;  .000; -.000003; -.000040;336.0723;24;A;24-OCT-2024;  ;;;
LNR       ;BPMEA.0625.T                    ;   28.99640; 1739.81429; 2149.89372;2436.11642;436.12116;99.999; -.000003; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;MQNLG.0630.E                    ;   29.21060; 1739.70392; 2150.11930;2436.49125;436.49599;  .223; -.000163; -.000040;336.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQNLG.0630.S                    ;   29.38060; 1739.56049; 2150.21056;2436.49124;436.49599;  .250; -.000163; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;MQSAB.0635.E                    ;   29.53020; 1739.43431; 2150.29094;2436.49110;436.49585;  .120; -.000013; -.000040;336.0723;24;A;24-OCT-2024;  ;;;
LNR       ;MQSAB.0635.S                    ;   29.70020; 1739.29088; 2150.38219;2436.49110;436.49586;  .150; -.000013; -.000040;336.0723;24;O;24-OCT-2024;  ;;;
LNR       ;MBHEK.0640.E                    ;   30.14872; 1739.18307; 2150.67850;2436.73115;436.73592;  .230; -.000002; -.000025;369.4057; 1;A;24-OCT-2024;  ;;;
LNR       ;MBHEK.0640.S                    ;   30.61872; 1738.96579; 2151.09526;2436.73114;436.73592;  .485; -.000002; -.000025;369.4057; 1;O;24-OCT-2024;  ;;;

LNE01     ;ZQNA.0101.E                     ;    1.39144; 1730.30592; 2152.81291;2436.36079;436.36594; 1.316;  .000620; -.000038;322.0667; 1;A;24-OCT-2024; PXZQNA_001-CR000049;;Calculé avec le PXZQNA_001-CR000049  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0101.S                     ;    1.63143; 1730.08009; 2152.89416;2436.36088;436.36604;  .023;  .000620; -.000038;322.0667; 1;O;24-OCT-2024; PXZQNA_001-CR000049;;Calculé avec le PXZQNA_001-CR000049  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0107.E                     ;    3.20925; 1728.59625; 2153.43053;2436.36086;436.36609; 1.375;  .000712; -.000038;322.0667; 1;A;24-OCT-2024; PXZQNA_001-CR000051;;Calculé avec le PXZQNA_001-CR000051  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0107.S                     ;    3.44915; 1728.37027; 2153.51109;2436.36091;436.36616;  .023;  .000712; -.000038;322.0667; 1;O;24-OCT-2024; PXZQNA_001-CR000051;;Calculé avec le PXZQNA_001-CR000051  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;BSGWA.0109.A                    ;    3.52367; 1728.30041; 2153.53704;2436.52012;436.52537;99.999; -.000015; -.000038;322.0667;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0109.F                    ;    3.59892; 1728.27391; 2153.68519;2436.52012;436.52537;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0109.B                    ;    3.59892; 1728.18535; 2153.44002;2436.52012;436.52537;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0109.E                    ;    3.74942; 1728.13236; 2153.73632;2436.52011;436.52537;  .000; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0109.C                    ;    3.74942; 1728.04381; 2153.49115;2436.52012;436.52537;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0109.D                    ;    3.82467; 1728.01731; 2153.63930;2436.52011;436.52537;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;ZQNA.0114.E                     ;    4.56866; 1727.31774; 2153.89249;2436.36078;436.36608;  .509; -.001437; -.000038;322.0667; 1;A;24-OCT-2024; PXZQNA_001-CR000052;;Calculé avec le PXZQNA_001-CR000052  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0114.S                     ;    4.80864; 1727.09168; 2153.97305;2436.36087;436.36618;  .023; -.001437; -.000038;322.0667; 1;O;24-OCT-2024; PXZQNA_001-CR000052;;Calculé avec le PXZQNA_001-CR000052  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;BSGWA.0120.A                    ;    5.49917; 1726.44240; 2154.20817;2436.52005;436.52538;99.999; -.000015; -.000038;322.0667;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0120.B                    ;    5.57442; 1726.32735; 2154.11114;2436.52005;436.52538;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0120.F                    ;    5.57442; 1726.41591; 2154.35632;2436.52004;436.52538;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0120.E                    ;    5.72492; 1726.27436; 2154.40745;2436.52004;436.52538;  .616; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0120.C                    ;    5.72492; 1726.18580; 2154.16227;2436.52004;436.52538;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0120.D                    ;    5.80017; 1726.15930; 2154.31042;2436.52004;436.52538;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;ZQNA.0121.E                     ;    5.87451; 1726.08925; 2154.33529;2436.36056;436.36591;  .300;  .000549; -.000038;322.0667; 1;A;24-OCT-2024; PXZQNA_001-CR000053;;Calculé avec le PXZQNA_001-CR000053  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0121.S                     ;    6.11447; 1725.86399; 2154.41802;2436.36082;436.36618;  .023;  .000549; -.000038;322.0667; 1;O;24-OCT-2024; PXZQNA_001-CR000053;;Calculé avec le PXZQNA_001-CR000053  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZDFHR.0127.E                    ;    6.38277; 1725.61124; 2154.50838;2436.39861;436.40399;  .100; -.000015; -.000039;329.0695; 1;A;24-OCT-2024;  ;;Param T mis a jour selon default de construction les dessins vont suivre;
LNE01     ;ZDFHR.0127.S                    ;    6.74070; 1725.27256; 2154.63073;2436.39860;436.40399;  .400; -.000015; -.000039;329.0695; 1;O;24-OCT-2024;  ;;Param T mis a jour selon default de construction les dessins vont suivre;
LNE01     ;ZDSHR.0132.E                    ;    6.85243; 1725.08624; 2154.62349;2436.36312;436.36852;  .213; -.000016; -.000036;354.4946;24;A;24-OCT-2024;  ;;;
LNE01     ;ZDSHR.0132.T                    ;    7.03766; 1725.21535; 2154.98082;2436.36342;436.36881;99.999; -.000016; -.000036;354.4946;24;O;24-OCT-2024;  ;;;
LNE01     ;ZDSHR.0132.S                    ;    7.20472; 1724.70674; 2154.76060;2436.36344;436.36885;  .347; -.000016; -.000036;354.4946;24;O;24-OCT-2024;  ;;;
LNE01     ;BSGWA.0137.A                    ;    7.57777; 1724.76284; 2155.28281;2436.51997;436.52538;99.999; -.000014; -.000033;372.9168;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0137.F                    ;    7.65302; 1724.85051; 2155.40514;2436.51997;436.52538;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0137.B                    ;    7.65302; 1724.61306; 2155.29756;2436.51997;436.52539;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0137.C                    ;    7.80352; 1724.55095; 2155.43464;2436.51996;436.52539;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0137.E                    ;    7.80352; 1724.78839; 2155.54223;2436.51997;436.52538;  .256; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0137.D                    ;    7.87877; 1724.63862; 2155.55698;2436.51996;436.52538;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;ZQNA.0138.E                     ;    7.95321; 1724.60830; 2155.62496;2436.36033;436.36575;  .000;  .001810; -.000033;372.9168; 1;A;24-OCT-2024; PXZQNA_001-CR000056;;Calculé avec le PXZQNA_001-CR000056  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0138.S                     ;    8.19319; 1724.50853; 2155.84322;2436.36045;436.36588;  .024;  .001810; -.000033;372.9168; 1;O;24-OCT-2024; PXZQNA_001-CR000056;;Calculé avec le PXZQNA_001-CR000056  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;BSGWA.0144.A                    ;    8.60937; 1724.33709; 2156.22245;2436.51994;436.52538;99.999; -.000014; -.000033;372.9168;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0144.B                    ;    8.68462; 1724.18732; 2156.23720;2436.51993;436.52538;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0144.F                    ;    8.68462; 1724.42476; 2156.34479;2436.51994;436.52538;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0144.E                    ;    8.83512; 1724.36265; 2156.48187;2436.51993;436.52537;  .182; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0144.C                    ;    8.83512; 1724.12520; 2156.37429;2436.51993;436.52538;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0144.D                    ;    8.91037; 1724.21287; 2156.49662;2436.51993;436.52538;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;ZQNA.0145.E                     ;    8.98501; 1724.18147; 2156.56433;2436.36077;436.36622;  .000;  .001794; -.000033;372.9168; 1;A;24-OCT-2024; PXZQNA_001-CR000060;;Calculé avec le PXZQNA_001-CR000060  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0145.S                     ;    9.22493; 1724.08263; 2156.78295;2436.36072;436.36618;  .024;  .001794; -.000033;372.9168; 1;O;24-OCT-2024; PXZQNA_001-CR000060;;Calculé avec le PXZQNA_001-CR000060  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;BSGWA.0151.A                    ;    9.79045; 1723.84966; 2157.29826;2436.51990;436.52537;99.999; -.000014; -.000033;372.9168;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0151.B                    ;    9.86570; 1723.69988; 2157.31301;2436.51989;436.52537;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0151.F                    ;    9.86570; 1723.93732; 2157.42059;2436.51990;436.52537;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0151.C                    ;   10.01620; 1723.63777; 2157.45009;2436.51989;436.52537;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0151.E                    ;   10.01620; 1723.87521; 2157.55768;2436.51989;436.52537;  .491; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0151.D                    ;   10.09145; 1723.72543; 2157.57243;2436.51989;436.52537;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;ZQNA.0152.E                     ;   10.16590; 1723.69472; 2157.64024;2436.36043;436.36591;  .000;  .000215; -.000033;372.9168; 1;A;24-OCT-2024; PXZQNA_001-CR000041;;Calculé avec le PXZQNA_001-CR000041  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0152.S                     ;   10.40586; 1723.59561; 2157.85877;2436.36054;436.36603;  .024;  .000215; -.000033;372.9168; 1;O;24-OCT-2024; PXZQNA_001-CR000041;;Calculé avec le PXZQNA_001-CR000041  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;BSGWA.0158.A                    ;   11.09645; 1723.31066; 2158.48785;2436.51985;436.52536;99.999; -.000014; -.000033;372.9168;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0158.F                    ;   11.17170; 1723.39833; 2158.61018;2436.51985;436.52536;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0158.B                    ;   11.17170; 1723.16088; 2158.50260;2436.51985;436.52536;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0158.C                    ;   11.32220; 1723.09877; 2158.63968;2436.51984;436.52536;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0158.E                    ;   11.32220; 1723.33622; 2158.74727;2436.51985;436.52536;  .616; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;BSGWA.0158.D                    ;   11.39745; 1723.18644; 2158.76202;2436.51984;436.52536;99.999; -.000014; -.000033;372.9168;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE01     ;ZQNA.0159.E                     ;   11.47184; 1723.15594; 2158.82986;2436.36054;436.36605;  .000; -.002510; -.000033;372.9168; 1;A;24-OCT-2024; PXZQNA_001-CR000050;;Calculé avec le PXZQNA_001-CR000050  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZQNA.0159.S                     ;   11.71181; 1723.05718; 2159.04857;2436.36075;436.36627;  .024; -.002510; -.000033;372.9168; 1;O;24-OCT-2024; PXZQNA_001-CR000050;;Calculé avec le PXZQNA_001-CR000050  inséré avec apex_warm_sock par CVENDEUV;
LNE01     ;ZDFV.0165.E                     ;   12.06538; 1722.92364; 2159.34203;2436.43042;436.43596;  .100; -.000014;  .109967;372.9167; 1;A;24-OCT-2024;  ;;MAJ tilt pi/2;
LNE01     ;ZDFV.0165.S                     ;   12.42321; 1722.77507; 2159.66994;2436.43041;436.43595;  .400; -.000014;  .109967;372.9167; 1;O;24-OCT-2024;  ;;MAJ tilt pi/2;
LNE01     ;ZDSV.0170.T                     ;   13.15631; 1722.76925; 2160.12650;2436.76003;436.76557;99.999; 1.570774;  .895321;367.2203;24;A;24-OCT-2024;  ;;;
LNE01     ;ZDSV.0170.E                     ;   13.15718; 1722.43658; 2159.97678;2436.76041;436.76597;  .209; 1.570774;  .895321;367.2203;24;O;24-OCT-2024;  ;;;
LNE01     ;ZDSV.0170.S                     ;   13.36750; 1722.46406; 2160.35829;2436.76057;436.76613;  .675; 1.570774;  .895321;367.2203;24;O;24-OCT-2024;  ;;;
LNE01     ;ZQNA.17582.A                    ;   13.60793; 1722.32737; 2160.13736;2437.03277;437.03834;99.999; -.150518; 1.570675;361.5240; 1;A;24-OCT-2024;  ;;;
LNE01     ;ZQNA.17582.B                    ;   13.84800; 1722.32668; 2160.13858;2437.27284;437.27841;99.999; -.150518; 1.570675;361.5240; 1;O;24-OCT-2024;  ;;;
LNE01     ;ZQNA.17582.F                    ;   13.84817; 1722.50261; 2160.38870;2437.27300;437.27856;99.999; -.150518; 1.570675;361.5240;24;O;24-OCT-2024;  ;;;
LNE01     ;ZQNA.17582.E                    ;   13.84834; 1722.63143; 2160.10435;2437.27320;437.27876;  .236; -.150518; 1.570675;361.5240;24;O;24-OCT-2024;  ;;;
LNE01     ;BSGWA.0181.C                    ;   13.89030; 1722.55432; 2160.06313;2437.31516;437.32072;99.999; -.150520; 1.570675;361.5240;24;A;24-OCT-2024;  ;;Parametres standard peu fiable pour SEM horizontal;
LNE01     ;BSGWA.0181.B                    ;   13.90812; 1722.65684; 2160.10958;2437.33299;437.33854;99.999; -.150520; 1.570675;361.5240;24;O;24-OCT-2024;  ;;Parametres standard peu fiable pour SEM horizontal;
LNE01     ;BSGWA.0181.D                    ;   13.97087; 1722.48092; 2160.02988;2437.39573;437.40129;99.999; -.150520; 1.570675;361.5240;24;O;24-OCT-2024;  ;;Parametres standard peu fiable pour SEM horizontal;
LNE01     ;BSGWA.0181.A                    ;   14.00966; 1722.70395; 2160.13094;2437.43453;437.44008;99.999; -.150520; 1.570675;361.5240;24;O;24-OCT-2024;  ;;Parametres standard peu fiable pour SEM horizontal;
LNE01     ;BSGWA.0181.E                    ;   14.08342; 1722.49715; 2160.03725;2437.50828;437.51384;  .024; -.150520; 1.570675;361.5240;24;O;24-OCT-2024;  ;;Parametres standard peu fiable pour SEM horizontal;
LNE01     ;BSGWA.0181.F                    ;   14.13516; 1722.58963; 2160.07916;2437.56002;437.56558;99.999; -.150520; 1.570675;361.5240;24;O;24-OCT-2024;  ;;Parametres standard peu fiable pour SEM horizontal;
LNE01     ;ZQNA.17582.G                    ;   14.16968; 1722.63124; 2160.10470;2437.59454;437.60009;99.999; -.150518; 1.570675;361.5240;24;A;24-OCT-2024;  ;;;
LNE01     ;ZQNA.17582.H                    ;   14.16973; 1722.50254; 2160.38900;2437.59456;437.60012;99.999; -.150518; 1.570675;361.5240;24;O;24-OCT-2024;  ;;;
LNE01     ;ZQNA.17582.C                    ;   14.16984; 1722.32708; 2160.13783;2437.59468;437.60025;99.999; -.150518; 1.570675;361.5240; 1;O;24-OCT-2024;  ;;;
LNE01     ;ZQNA.17582.D                    ;   14.40981; 1722.32691; 2160.13833;2437.83465;437.84022;99.999; -.150518; 1.570675;361.5240; 1;O;24-OCT-2024;  ;;;
LNE02     ;ZQNA.0205.E                     ;    1.64058; 1724.18113; 2155.02412;2436.36049;436.36593;  .085;  .003697; -.000038;322.0667; 1;A;24-OCT-2024; PXZQNA_001-CR000039;;Calculé avec le PXZQNA_001-CR000039  inséré avec apex_warm_sock par CVENDEUV;
LNE02     ;ZQNA.0205.S                     ;    1.88053; 1723.95538; 2155.10542;2436.36058;436.36603;  .023;  .003697; -.000038;322.0667; 1;O;24-OCT-2024; PXZQNA_001-CR000039;;Calculé avec le PXZQNA_001-CR000039  inséré avec apex_warm_sock par CVENDEUV;
LNE02     ;BSGWA.0207.A                    ;    1.95509; 1723.88558; 2155.13170;2436.51994;436.52540;99.999; -.000015; -.000038;322.0667;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE02     ;BSGWA.0207.B                    ;    2.03034; 1723.77053; 2155.03468;2436.51994;436.52540;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE02     ;BSGWA.0207.F                    ;    2.03034; 1723.85909; 2155.27986;2436.51994;436.52539;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE02     ;BSGWA.0207.C                    ;    2.18084; 1723.62898; 2155.08581;2436.51994;436.52540;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE02     ;BSGWA.0207.E                    ;    2.18084; 1723.71754; 2155.33099;2436.51993;436.52539;  .000; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE02     ;BSGWA.0207.D                    ;    2.25609; 1723.60248; 2155.23396;2436.51993;436.52540;99.999; -.000015; -.000038;322.0667;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE02     ;ZQNA.0208.E                     ;    3.78611; 1722.16351; 2155.75386;2436.36075;436.36628; 1.455; -.000648; -.000038;322.0667; 1;A;24-OCT-2024; PXZQNA_001-CR000044;;Calculé avec le PXZQNA_001-CR000044  inséré avec apex_warm_sock par CVENDEUV;
LNE02     ;ZQNA.0208.S                     ;    4.02606; 1721.93775; 2155.83516;2436.36093;436.36647;  .024; -.000648; -.000038;322.0667; 1;O;24-OCT-2024; PXZQNA_001-CR000044;;Calculé avec le PXZQNA_001-CR000044  inséré avec apex_warm_sock par CVENDEUV;
LNE02     ;ZQNA.0214.E                     ;    4.29614; 1721.68376; 2155.92699;2436.36074;436.36629;  .120;  .002037; -.000038;322.0667; 1;A;24-OCT-2024; PXZQNA_001-CR000046;;Calculé avec le PXZQNA_001-CR000046  inséré avec apex_warm_sock par CVENDEUV;
LNE02     ;ZQNA.0214.S                     ;    4.53612; 1721.45801; 2156.00841;2436.36073;436.36629;  .024;  .002037; -.000038;322.0667; 1;O;24-OCT-2024; PXZQNA_001-CR000046;;Calculé avec le PXZQNA_001-CR000046  inséré avec apex_warm_sock par CVENDEUV;
LNE02     ;ZDSHR.0220.E                    ;    4.78177; 1721.16315; 2156.01709;2436.36319;436.36877;  .221; -.000016; -.000036;347.4945;24;A;24-OCT-2024;  ;;LS2 2020  LT ELENA  LNE02 AEGIS;
LNE02     ;ZDSHR.0220.T                    ;    4.91000; 1721.27513; 2156.32717;2436.36316;436.36874;99.999; -.000016; -.000036;347.4945;24;O;24-OCT-2024;  ;;LS2 2020  LT ELENA  LNE02 AEGIS;
LNE02     ;ZDSHR.0220.S                    ;    5.23498; 1720.70034; 2156.18405;2436.36331;436.36891;  .479; -.000016; -.000036;347.4945;24;O;24-OCT-2024;  ;;LS2 2020  LT ELENA  LNE02 AEGIS;
LNE02     ;AEGIS.0228.E                    ;    7.67275; 1719.86281; 2158.57696;2436.14534;436.15100;  .019; -.000015; -.000033;372.9223; 5;A;24-OCT-2024;  ;;;
LNE02     ;AEGIS.0228.S                    ;   12.47275; 1717.88220; 2162.94928;2436.14518;436.15097; 4.800; -.000015; -.000033;372.9223; 5;O;24-OCT-2024;  ;;point faisceau experience;
LNE06     ;ZDFHR.0601.E                    ;     .12019; 1729.84716; 2152.97835;2436.39879;436.40396;  .100; -.000015; -.000039;329.0695; 1;A;24-OCT-2024;  ;;Param T mis a jour selon default de construction les dessins vont suivre;
LNE06     ;ZDFHR.0601.S                    ;     .47801; 1729.50857; 2153.10065;2436.39877;436.40397;  .400; -.000015; -.000039;329.0695; 1;O;24-OCT-2024;  ;;Param T mis a jour selon default de construction les dessins vont suivre;
LNE06     ;ZDSHR.0606.E                    ;     .58679; 1729.32212; 2153.09243;2436.36341;436.36861;  .213; -.000015; -.000036;355.7835;24;A;24-OCT-2024;  ;;;
LNE06     ;ZDSHR.0606.T                    ;     .79620; 1729.46260; 2153.48200;2436.36397;436.36916;99.999; -.000015; -.000036;355.7835;24;O;24-OCT-2024;  ;;;
LNE06     ;ZDSHR.0606.S                    ;     .94337; 1728.93377; 2153.23302;2436.36344;436.36866;  .372; -.000015; -.000036;355.7835;24;O;24-OCT-2024;  ;;;
LNE06     ;BSGWA.0611.A                    ;    1.42140; 1728.96788; 2153.85504;2436.52014;436.52536;99.999; -.000013; -.000032;375.4946;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0611.B                    ;    1.49665; 1728.81882; 2153.87584;2436.52013;436.52536;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0611.F                    ;    1.49665; 1729.06043; 2153.97373;2436.52014;436.52536;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0611.C                    ;    1.64715; 1728.76231; 2154.01533;2436.52013;436.52536;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0611.E                    ;    1.64715; 1729.00391; 2154.11321;2436.52014;436.52536;  .141; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0611.D                    ;    1.72240; 1728.85486; 2154.13402;2436.52013;436.52536;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;ZQNA.0612.E                     ;    1.79693; 1728.82563; 2154.20259;2436.36085;436.36608;  .000;  .002580; -.000032;375.4946; 1;A;24-OCT-2024; PXZQNA_001-CR000023;;Calculé avec le PXZQNA_001-CR000023  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;ZQNA.0612.S                     ;    2.03697; 1728.73635; 2154.42541;2436.36081;436.36605;  .024;  .002580; -.000032;375.4946; 1;O;24-OCT-2024; PXZQNA_001-CR000023;;Calculé avec le PXZQNA_001-CR000023  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;ZQNA.0618.E                     ;    2.91547; 1728.40588; 2155.23939;2436.36051;436.36577;  .729;  .003374; -.000032;375.4946; 1;A;24-OCT-2024; PXZQNA_001-CR000025;;Calculé avec le PXZQNA_001-CR000025  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;ZQNA.0618.S                     ;    3.15545; 1728.31622; 2155.46198;2436.36044;436.36570;  .024;  .003374; -.000032;375.4946; 1;O;24-OCT-2024; PXZQNA_001-CR000025;;Calculé avec le PXZQNA_001-CR000025  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;BSGWA.0624.A                    ;    3.55890; 1728.16526; 2155.83613;2436.52007;436.52534;99.999; -.000013; -.000032;375.4946;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0624.F                    ;    3.63415; 1728.25780; 2155.95482;2436.52007;436.52534;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0624.B                    ;    3.63415; 1728.01620; 2155.85693;2436.52007;436.52535;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0624.E                    ;    3.78465; 1728.20129; 2156.09430;2436.52007;436.52534;  .329; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0624.C                    ;    3.78465; 1727.95969; 2155.99642;2436.52006;436.52534;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0624.D                    ;    3.85990; 1728.05223; 2156.11510;2436.52006;436.52534;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;ZQNA.0625.E                     ;    3.93437; 1728.02335; 2156.18375;2436.36050;436.36579;  .000; -.000114; -.000032;375.4946; 1;A;24-OCT-2024; PXZQNA_001-CR000026;;Calculé avec le PXZQNA_001-CR000026  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;ZQNA.0625.S                     ;    4.17433; 1727.93377; 2156.40637;2436.36061;436.36589;  .024; -.000114; -.000032;375.4946; 1;O;24-OCT-2024; PXZQNA_001-CR000026;;Calculé avec le PXZQNA_001-CR000026  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;BSGWA.0631.A                    ;    6.02060; 1727.24090; 2158.11769;2436.51999;436.52533;99.999; -.000013; -.000032;375.4946;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0631.B                    ;    6.09585; 1727.09184; 2158.13850;2436.51999;436.52533;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0631.F                    ;    6.09585; 1727.33345; 2158.23638;2436.51999;436.52532;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0631.C                    ;    6.24635; 1727.03533; 2158.27798;2436.51998;436.52533;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0631.E                    ;    6.24635; 1727.27694; 2158.37587;2436.51999;436.52532; 1.772; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0631.D                    ;    6.32160; 1727.12788; 2158.39667;2436.51998;436.52532;99.999; -.000013; -.000032;375.4946;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;ZQNA.0632.E                     ;    6.39614; 1727.09980; 2158.46572;2436.36053;436.36587;  .000; -.000944; -.000032;375.4946; 1;A;24-OCT-2024; PXZQNA_001-CR000062;;Calculé avec le PXZQNA_001-CR000062  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;ZQNA.0632.S                     ;    6.63607; 1727.01055; 2158.68843;2436.36055;436.36590;  .024; -.000944; -.000032;375.4946; 1;O;24-OCT-2024; PXZQNA_001-CR000062;;Calculé avec le PXZQNA_001-CR000062  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;ZDFHR.0638.E                    ;    6.89682; 1726.91185; 2158.92991;2436.39856;436.40392;  .100; -.000012; -.000029;382.4974; 1;A;24-OCT-2024;  ;;Param T mis a jour selon default de construction les dessins vont suivre;
LNE06     ;ZDFHR.0638.S                    ;    7.25464; 1726.77667; 2159.26356;2436.39855;436.40392;  .400; -.000012; -.000029;382.4974; 1;O;24-OCT-2024;  ;;Param T mis a jour selon default de construction les dessins vont suivre;
LNE06     ;ZDSHR.0643.E                    ;    7.36295; 1726.64524; 2159.39646;2436.36361;436.36899;  .213; -.000008; -.000015;  9.2113;24;A;24-OCT-2024;  ;;;
LNE06     ;ZDSHR.0643.T                    ;    7.57252; 1727.03010; 2159.55216;2436.36351;436.36887;99.999; -.000008; -.000015;  9.2113;24;O;24-OCT-2024;  ;;;
LNE06     ;ZDSHR.0643.S                    ;    7.71990; 1726.49122; 2159.77962;2436.36359;436.36897;  .372; -.000008; -.000015;  9.2113;24;O;24-OCT-2024;  ;;;
LNE06     ;ZQNA.0648.E                     ;    8.20353; 1726.98027; 2160.17432;2436.36054;436.36590;  .268;  .000035; -.000004; 28.9224; 1;A;24-OCT-2024; PXZQNA_001-CR000014;;Calculé avec le PXZQNA_001-CR000014  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;ZQNA.0648.S                     ;    8.44348; 1727.08331; 2160.39104;2436.36064;436.36600;  .023;  .000035; -.000004; 28.9224; 1;O;24-OCT-2024; PXZQNA_001-CR000014;;Calculé avec le PXZQNA_001-CR000014  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;BSGWA.0650.A                    ;    8.51802; 1727.11727; 2160.45741;2436.51994;436.52530;99.999; -.000004; -.000004; 28.9224;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0650.B                    ;    8.59327; 1727.03318; 2160.58222;2436.51993;436.52530;99.999; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0650.F                    ;    8.59327; 1727.26741; 2160.46783;2436.51994;436.52530;99.999; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0650.E                    ;    8.74377; 1727.33346; 2160.60306;2436.51994;436.52530;  .000; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0650.C                    ;    8.74377; 1727.09922; 2160.71746;2436.51993;436.52530;99.999; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0650.D                    ;    8.81902; 1727.24936; 2160.72788;2436.51994;436.52530;99.999; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;ZQNA.0655.E                     ;    9.03358; 1727.34364; 2160.92062;2436.36052;436.36588;  .140;  .001345; -.000004; 28.9224; 1;A;24-OCT-2024; PXZQNA_001-CR000035;;Calculé avec le PXZQNA_001-CR000035  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;ZQNA.0655.S                     ;    9.27358; 1727.44811; 2161.13668;2436.36050;436.36586;  .023;  .001345; -.000004; 28.9224; 1;O;24-OCT-2024; PXZQNA_001-CR000035;;Calculé avec le PXZQNA_001-CR000035  inséré avec apex_warm_sock par CVENDEUV;
LNE06     ;BSGWA.0657.A                    ;    9.34802; 1727.48151; 2161.20322;2436.51994;436.52529;99.999; -.000004; -.000004; 28.9224;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0657.F                    ;    9.42327; 1727.63165; 2161.21363;2436.51994;436.52529;99.999; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0657.B                    ;    9.42327; 1727.39742; 2161.32803;2436.51993;436.52529;99.999; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0657.C                    ;    9.57377; 1727.46346; 2161.46326;2436.51993;436.52529;99.999; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0657.E                    ;    9.57377; 1727.69770; 2161.34887;2436.51994;436.52529;  .000; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;BSGWA.0657.D                    ;    9.64902; 1727.61360; 2161.47368;2436.51993;436.52529;99.999; -.000004; -.000004; 28.9224;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE06     ;AS.1.E                          ;   10.44852; 1727.96447; 2162.19208;2436.14554;436.15088;  .610; -.000004; -.000004; 28.9224; 5;A;24-OCT-2024;  ;;;
LNE06     ;AS.1.S                          ;   15.54852; 1730.20257; 2166.77475;2436.14552;436.15081; 4.530; -.000004; -.000004; 28.9224; 5;O;24-OCT-2024;  ;;point faisceau experience;
LNE07     ;ZDFHR.0701.E                    ;     .12019; 1731.50136; 2152.38084;2436.39886;436.40396;  .100; -.000015; -.000039;329.0695; 1;A;24-OCT-2024;  ;;Param T mis a jour selon default de construction les dessins vont suivre;
LNE07     ;ZDFHR.0701.S                    ;     .47801; 1731.16277; 2152.50314;2436.39884;436.40396;  .400; -.000015; -.000039;329.0695; 1;O;24-OCT-2024;  ;;Param T mis a jour selon default de construction les dessins vont suivre;
LNE07     ;ZDSHR.0706.E                    ;     .58708; 1730.97601; 2152.49505;2436.36350;436.36863;  .213; -.000015; -.000036;355.7835;24;A;24-OCT-2024;  ;;;
LNE07     ;ZDSHR.0706.T                    ;     .79625; 1731.11678; 2152.88455;2436.36359;436.36871;99.999; -.000015; -.000036;355.7835;24;O;24-OCT-2024;  ;;;
LNE07     ;ZDSHR.0706.S                    ;     .94335; 1730.58794; 2152.63547;2436.36422;436.36936;  .372; -.000015; -.000036;355.7835;24;O;24-OCT-2024;  ;;;
LNE07     ;ZQNA.0711.E                     ;    1.46177; 1730.60615; 2153.29464;2436.36070;436.36585;  .106;  .002822; -.000032;375.4946; 1;A;24-OCT-2024; PXZQNA_001-CR000016;;Calculé avec le PXZQNA_001-CR000016  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;ZQNA.0711.S                     ;    1.70178; 1730.51603; 2153.51709;2436.36077;436.36593;  .024;  .002822; -.000032;375.4946; 1;O;24-OCT-2024; PXZQNA_001-CR000016;;Calculé avec le PXZQNA_001-CR000016  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;ZDSHR.0717.E                    ;    1.94162; 1730.32708; 2153.73868;2436.36424;436.36940;  .216; -.000009; -.000018;  2.2168;24;A;24-OCT-2024;  ;;;
LNE07     ;ZDSHR.0717.T                    ;    2.07998; 1730.64229; 2153.86614;2436.36414;436.36929;99.999; -.000009; -.000018;  2.2168;24;O;24-OCT-2024;  ;;;
LNE07     ;ZDSHR.0717.S                    ;    2.40272; 1730.13796; 2154.20665;2436.36442;436.36960;  .504; -.000009; -.000018;  2.2168;24;O;24-OCT-2024;  ;;;
LNE07     ;BSGWA.0722.A                    ;    2.78590; 1730.55127; 2154.53589;2436.52018;436.52534;99.999; -.000003; -.000004; 28.9390;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0722.F                    ;    2.86115; 1730.70141; 2154.54627;2436.52018;436.52534;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0722.B                    ;    2.86115; 1730.46720; 2154.66073;2436.52017;436.52534;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0722.E                    ;    3.01165; 1730.76749; 2154.68149;2436.52018;436.52533;  .289; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0722.C                    ;    3.01165; 1730.53328; 2154.79595;2436.52017;436.52534;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0722.D                    ;    3.08690; 1730.68343; 2154.80633;2436.52018;436.52533;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;ZQNA.0723.E                     ;    3.16125; 1730.71648; 2154.87292;2436.36056;436.36571;  .000;  .001769; -.000004; 28.9390; 1;A;24-OCT-2024; PXZQNA_001-CR000018;;Calculé avec le PXZQNA_001-CR000018  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;ZQNA.0723.S                     ;    3.40125; 1730.82135; 2155.08880;2436.36071;436.36586;  .024;  .001769; -.000004; 28.9390; 1;O;24-OCT-2024; PXZQNA_001-CR000018;;Calculé avec le PXZQNA_001-CR000018  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;ZQNA.0729.E                     ;    4.31141; 1731.22073; 2155.90666;2436.36078;436.36592;  .760;  .003680; -.000004; 28.9390; 1;A;24-OCT-2024; PXZQNA_001-CR000040;;Calculé avec le PXZQNA_001-CR000040  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;ZQNA.0729.S                     ;    4.55145; 1731.32502; 2156.12285;2436.36064;436.36578;  .024;  .003680; -.000004; 28.9390; 1;O;24-OCT-2024; PXZQNA_001-CR000040;;Calculé avec le PXZQNA_001-CR000040  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;ZQNA.0735.E                     ;    5.35480; 1731.68024; 2156.84341;2436.36090;436.36604;  .653; -.002673; -.000004; 28.9390; 1;A;24-OCT-2024; PXZQNA_001-CR000054;;Calculé avec le PXZQNA_001-CR000054  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;ZQNA.0735.S                     ;    5.59478; 1731.78538; 2157.05914;2436.36103;436.36616;  .000; -.002673; -.000004; 28.9390; 1;O;24-OCT-2024; PXZQNA_001-CR000054;;Calculé avec le PXZQNA_001-CR000054  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;BSGWA.0737.A                    ;    5.66933; 1731.81732; 2157.12651;2436.52017;436.52530;99.999; -.000003; -.000004; 28.9390;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0737.B                    ;    5.74458; 1731.73325; 2157.25134;2436.52016;436.52530;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0737.F                    ;    5.74458; 1731.96746; 2157.13688;2436.52017;436.52529;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0737.E                    ;    5.89508; 1732.03354; 2157.27210;2436.52017;436.52529;  .024; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0737.C                    ;    5.89508; 1731.79934; 2157.38656;2436.52016;436.52529;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0737.D                    ;    5.97033; 1731.94948; 2157.39694;2436.52016;436.52529;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;ZQNA.0742.E                     ;    6.72174; 1732.27993; 2158.07179;2436.36068;436.36580;  .677; -.000220; -.000004; 28.9390; 1;A;24-OCT-2024; PXZQNA_001-CR000020;;Calculé avec le PXZQNA_001-CR000020  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;ZQNA.0742.S                     ;    6.96172; 1732.38501; 2158.28754;2436.36071;436.36582;  .000; -.000220; -.000004; 28.9390; 1;O;24-OCT-2024; PXZQNA_001-CR000020;;Calculé avec le PXZQNA_001-CR000020  inséré avec apex_warm_sock par CVENDEUV;
LNE07     ;BSGWA.0744.A                    ;    7.03626; 1732.41751; 2158.35462;2436.52016;436.52528;99.999; -.000003; -.000004; 28.9390;24;A;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0744.B                    ;    7.11151; 1732.33344; 2158.47946;2436.52015;436.52528;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0744.F                    ;    7.11151; 1732.56765; 2158.36500;2436.52016;436.52527;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0744.C                    ;    7.26201; 1732.39952; 2158.61468;2436.52015;436.52527;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0744.E                    ;    7.26201; 1732.63373; 2158.50022;2436.52016;436.52527;  .024; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BSGWA.0744.D                    ;    7.33726; 1732.54967; 2158.62505;2436.52016;436.52527;99.999; -.000003; -.000004; 28.9390;24;O;24-OCT-2024;  ;;Parametre standard selon dessin AD_BSGWA0006 AD_BSGWA0014;
LNE07     ;BASE.0750.E                     ;    7.73640; 1732.72494; 2158.98365;2436.14576;436.15087;  .210; -.000003; -.000004; 28.9390; 5;A;24-OCT-2024;  ;;;
LNE07     ;BASE.0750.S                     ;   10.53640; 1733.95436; 2161.49931;2436.14575;436.15083;  .875; -.000003; -.000004; 28.9390; 5;O;24-OCT-2024;  ;;point faisceau experience;
BEAM_LNE00     ;ZQMF.0013.S                     ;    3.74800; 1738.09298; 2149.99990;2436.14612;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0013.E                      ;    3.77800; 1738.06477; 2150.01009;2436.14612;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0013.E                      ;    3.82500; 1738.02056; 2150.02606;2436.14612;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0013.S                      ;    3.86200; 1737.98576; 2150.03863;2436.14612;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQMD.0014.E                     ;    3.89200; 1737.95755; 2150.04882;2436.14612;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQMD.0014.S                     ;    3.99200; 1737.86349; 2150.08279;2436.14611;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0013.E                     ;    3.62550; 1738.20820; 2149.95829;2436.14613;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0013.S                     ;    4.01550; 1737.84139; 2150.09078;2436.14611;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQMF.0013.E                     ;    3.64800; 1738.18704; 2149.96593;2436.14613;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0013.S                      ;    3.81500; 1738.02997; 2150.02266;2436.14612;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.ZQNA.0013   1738.208197   2149.958286   2436.146128 0 0  322.066700 1
*FRAME RST_LNE00.ZQNA.0013 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.ZQNA.0013     -0.000685      0.001630     -0.000155 -.00114265073 0 .2317953745 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.ZQNA.0013 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.ZQMF.0013.S                                           0.000000      0.122500      0.000000   $3.748 774798 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZCV.0013.E                                            0.000000      0.152500      0.000000   $3.778 774799 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZCH.0013.E                                            0.000000      0.199500      0.000000   $3.825 774801 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZCH.0013.S                                            0.000000      0.236500      0.000000   $3.862 774802 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZQMD.0014.E                                           0.000000      0.266500      0.000000   $3.892 774803 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZQMD.0014.S                                           0.000000      0.366500      0.000000   $3.992 774804 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZQNA.0013.E                                           0.000000      0.000000      0.000000   $3.626 776392 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZQNA.0013.S                                           0.000000      0.390000      0.000001   $4.016 776393 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZQMF.0013.E                                           0.000000      0.022500      0.000000   $3.648 774797 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
BEAM_LNE00.ZCV.0013.S                                            0.000000      0.189500      0.000000   $3.815 774800 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:47
*OBSXYZ
LNE00.ZQNA.0013.E     -0.002117      0.075109      0.216946 0.1 0.1 0.1   $3.701 776394 paramètres RST 24-OCT-2024 09:40:47
LNE00.ZQNA.0013.S     -0.000632      0.314728      0.217020 0.1 0.1 0.1   $3.940 776409 paramètres RST 24-OCT-2024 09:40:47
*INCLY Instr_Roll_Theo
LNE00.ZQNA.0013.E .15407197984 RF .15501691457
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;ZQFF.0026.S                     ;    5.87510; 1736.09239; 2150.72253;2436.14604;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0026.E                      ;    5.90510; 1736.06418; 2150.73272;2436.14604;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0026.S                      ;    5.94210; 1736.02938; 2150.74529;2436.14604;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0026.E                      ;    5.95210; 1736.01997; 2150.74869;2436.14604;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0026.S                      ;    5.98910; 1735.98517; 2150.76126;2436.14604;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQ.0027.E                       ;    6.01910; 1735.95696; 2150.77145;2436.14604;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQ.0027.S                       ;    6.11910; 1735.86290; 2150.80542;2436.14603;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQFF.0026.E                     ;    5.77510; 1736.18644; 2150.68856;2436.14605;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0026.E                     ;    5.75260; 1736.20761; 2150.68091;2436.14605;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0026.S                     ;    6.14260; 1735.84080; 2150.81340;2436.14603;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.ZQNA.0026   1736.207605   2150.680911   2436.146047 0 0  322.066700 1
*FRAME RST_LNE00.ZQNA.0026 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.ZQNA.0026     -0.000281     -0.002582     -0.001092 -.10234914912 0 -.16127775856 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.ZQNA.0026 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.ZQFF.0026.S                                           0.000000      0.122500      0.000000   $5.875 774814 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZCV.0026.E                                            0.000000      0.152500      0.000000   $5.905 774815 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZCV.0026.S                                            0.000000      0.189500      0.000000   $5.942 774816 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZCH.0026.E                                            0.000000      0.199500      0.000000   $5.952 774817 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZCH.0026.S                                            0.000000      0.236500      0.000000   $5.989 774818 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZQ.0027.E                                             0.000000      0.266500      0.000000   $6.019 774819 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZQ.0027.S                                             0.000000      0.366500      0.000000   $6.119 774820 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZQFF.0026.E                                           0.000000      0.022500      0.000000   $5.775 774813 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZQNA.0026.E                                           0.000000      0.000000      0.000000   $5.753 776395 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
BEAM_LNE00.ZQNA.0026.S                                           0.000000      0.389999      0.000000   $6.143 776396 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:48
*OBSXYZ
LNE00.ZQNA.0026.S     -0.001444      0.314840      0.215018 0.1 0.1 0.1   $6.067 776408 paramètres RST 24-OCT-2024 09:40:48
LNE00.ZQNA.0026.E      0.000360      0.074923      0.214858 0.1 0.1 0.1   $5.828 776407 paramètres RST 24-OCT-2024 09:40:48
*INCLY Instr_Roll_Theo
LNE00.ZQNA.0026.E .25020156547 RF .2511465002
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZDFHR.0127.E                    ;    6.36258; 1725.63035; 2154.50148;2436.14562;436.15098;;; -.000039;329.0695;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZDFHR.0127.S                    ;    6.76258; 1725.27206; 2154.67749;2436.14560;436.15099;;; -.000039;329.0695;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZDFHR.0127   1725.630354   2154.501478   2436.145615 0 0  322.066700 1
*FRAME RST_LNE01.ZDFHR.0127 0 0 0 .00241915513 -.00054647441 7.00281749604 1
*FRAME RSTRI_LNE01.ZDFHR.0127      0.001044     -0.000189     -0.000242 -.02057245069 0 -.1183315328 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZDFHR.0127 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZDFHR.0127.E                                          0.000000      0.000000      0.000000   $6.363 787754 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:49
BEAM_LNE01.ZDFHR.0127.S                                          0.000000      0.399194      0.000000   $6.763 787755 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:49
*OBSXYZ
LNE01.ZDFHR.0127.E     -0.002229      0.020188      0.253000 0.1 0.1 0.1   $6.383 792597 paramètres RST 24-OCT-2024 09:40:49
LNE01.ZDFHR.0127.S     -0.041750      0.378120      0.253000 0.1 0.1 0.1   $6.741 792598 paramètres RST 24-OCT-2024 09:40:49
*INCLY Instr_Roll_Theo
LNE01.ZDFHR.0127.E -.00097784797 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZDSHR.0132.E                    ;    6.97519; 1725.09268; 2154.79162;2436.14559;436.15099;;; -.000036;354.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZDSHR.0132.S                    ;    7.32244; 1724.86823; 2155.05022;2436.14558;436.15099;;; -.000036;354.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZDSHR.0132   1725.092677   2154.791616   2436.145591 0 0  336.072300 1
*FRAME RST_LNE01.ZDSHR.0132 0 0 0 .00248281711 .00000006366 18.42222222345 1
*FRAME RSTRI_LNE01.ZDSHR.0132      0.000114      0.000385     -0.000347 .03550982709 0 .00018591534 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZDSHR.0132 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZDSHR.0132.E                                          0.000000      0.000000      0.000000   $6.975 787756 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:49
BEAM_LNE01.ZDSHR.0132.S                                          0.000000      0.342425      0.000000   $7.322 787757 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:49
*OBSXYZ
LNE01.ZDSHR.0132.E     -0.115062     -0.122763      0.217530 0.1 0.1 0.1   $6.852 792594 paramètres RST 24-OCT-2024 09:40:49
LNE01.ZDSHR.0132.S     -0.311795      0.229534      0.217862 0.1 0.1 0.1   $7.205 792595 paramètres RST 24-OCT-2024 09:40:49
LNE01.ZDSHR.0132.T      0.216664      0.062471      0.217828 0.1 0.1 0.1   $7.038 792596 paramètres RST 24-OCT-2024 09:40:49
*INCLY Instr_Roll_Theo
LNE01.ZDSHR.0132.E -.00099268121 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZQ.0121.E                       ;    5.82217; 1726.13862; 2154.31789;2436.14564;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0121.S                       ;    5.92217; 1726.04457; 2154.35186;2436.14563;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0121.S                      ;    6.03617; 1725.93735; 2154.39059;2436.14563;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQFD.0122.E                     ;    6.06617; 1725.90913; 2154.40078;2436.14563;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQFD.0122.S                     ;    6.16617; 1725.81508; 2154.43475;2436.14562;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0121.E                     ;    5.79967; 1726.15979; 2154.31025;2436.14564;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0121.E                      ;    5.99917; 1725.97215; 2154.37802;2436.14563;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0121.E                      ;    5.95217; 1726.01635; 2154.36205;2436.14563;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0121.S                     ;    6.18967; 1725.79298; 2154.44274;2436.14562;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0121.S                      ;    5.98917; 1725.98156; 2154.37462;2436.14563;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.0121   1726.159785   2154.310245   2436.145637 0 0  322.066700 1
*FRAME RST_LNE01.ZQNA.0121 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE01.ZQNA.0121     -0.000054     -0.000258     -0.000690 -.05092958081 0 .05452078749 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.0121 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZQ.0121.E                                             0.000000      0.022500      0.000000   $5.822 787744 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZQ.0121.S                                             0.000000      0.122500      0.000000   $5.922 787745 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZCH.0121.S                                            0.000000      0.236500      0.000000   $6.036 787749 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZQFD.0122.E                                           0.000000      0.266500      0.000000   $6.066 787750 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZQFD.0122.S                                           0.000000      0.366500      0.000000   $6.166 787751 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZQNA.0121.E                                           0.000000      0.000000      0.000000   $5.800 792369 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZCH.0121.E                                            0.000000      0.199500      0.000000   $5.999 787748 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZCV.0121.E                                            0.000000      0.152500      0.000000   $5.952 787746 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZQNA.0121.S                                           0.000000      0.390000      0.000000   $6.190 792370 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE01.ZCV.0121.S                                            0.000000      0.189500      0.000000   $5.989 787747 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
*OBSXYZ
LNE01.ZQNA.0121.E     -0.000414      0.074844      0.214930 0.1 0.1 0.1   $5.875 792393 paramètres RST 24-OCT-2024 09:40:50
LNE01.ZQNA.0121.S      0.000871      0.314805      0.215192 0.1 0.1 0.1   $6.114 792394 paramètres RST 24-OCT-2024 09:40:50
*INCLY Instr_Roll_Theo
LNE01.ZQNA.0121.E .0349604841 RF .03590535516
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;BSGWA.0657.S                    ;    9.64852; 1727.61340; 2161.47323;2436.14553;436.15088;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;BSGWA.0657.E                    ;    9.34852; 1727.48174; 2161.20366;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.BSGWA.0657   1727.481743   2161.203656   2436.145535 0 0   28.922400 1
*FRAME RST_LNE06.BSGWA.0657 0 0 0 .00025464791 .00248848303 0 1
*FRAME RSTRI_LNE06.BSGWA.0657      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.BSGWA.0657 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.BSGWA.0657.S                                          0.000000      0.300000      0.000000   $9.649 788147 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
BEAM_LNE06.BSGWA.0657.E                                          0.000000      0.000000      0.000000   $9.349 788146 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:50
*OBSXYZ
LNE06.BSGWA.0657.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $9.348 816608 paramètres RST 24-OCT-2024 09:40:50
LNE06.BSGWA.0657.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $9.423 816609 paramètres RST 24-OCT-2024 09:40:50
LNE06.BSGWA.0657.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $9.574 816610 paramètres RST 24-OCT-2024 09:40:50
LNE06.BSGWA.0657.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $9.649 816611 paramètres RST 24-OCT-2024 09:40:50
LNE06.BSGWA.0657.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $9.574 816612 paramètres RST 24-OCT-2024 09:40:50
LNE06.BSGWA.0657.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $9.423 816613 paramètres RST 24-OCT-2024 09:40:50
*INCLY Instr_Roll_Theo
LNE06.BSGWA.0657.E -.0002362496 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZDFHR.0638.E                    ;    6.87663; 1726.91948; 2158.91108;2436.14556;436.15092;;; -.000029;382.4974;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZDFHR.0638.S                    ;    7.27663; 1726.81111; 2159.29528;2436.14555;436.15092;;; -.000029;382.4974;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZDFHR.0638   1726.919479   2158.911075   2436.145565 0 0  375.494600 1
*FRAME RST_LNE06.ZDFHR.0638 0 0 0 .00203718327 .00145353026 7.00281749604 1
*FRAME RSTRI_LNE06.ZDFHR.0638     -0.002267      0.000506      0.001877 .14209355409 0 .05374367333 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZDFHR.0638 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZDFHR.0638.E                                          0.000000      0.000000      0.000000   $6.877 788124 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:51
BEAM_LNE06.ZDFHR.0638.S                                          0.000000      0.399194      0.000000   $7.277 788125 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:51
*OBSXYZ
LNE06.ZDFHR.0638.E     -0.002229      0.020188      0.253000 0.1 0.1 0.1   $6.897 791933 paramètres RST 24-OCT-2024 09:40:51
LNE06.ZDFHR.0638.S     -0.041750      0.378012      0.253000 0.1 0.1 0.1   $7.255 791934 paramètres RST 24-OCT-2024 09:40:51
*INCLY Instr_Roll_Theo
LNE06.ZDFHR.0638.E -.00079189133 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEB.0125.E                    ;    4.66400; 1738.74214; 2155.31739;2436.14608;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEB.0125.S                    ;    4.66401; 1738.74214; 2155.31739;2436.14608;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEB.0125   1738.742143   2155.317393   2436.146079 0 0    2.739000 1
*FRAME RST_LNR.BPMEB.0125 0 0 0 .0005729578 .00257818275 0 1
*FRAME RSTRI_LNR.BPMEB.0125      0.001560     -0.002210      0.001980 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEB.0125 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEB.0125.E                                            0.000000      0.000000      0.000000   $4.664 775496 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:51
BEAM_LNR.BPMEB.0125.S                                            0.000000      0.000000      0.000000   $4.664 775497 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:51
*OBSXYZ
LNR.BPMEB.0125.E      0.071270     -0.128000      0.049900 0.1 0.1 0.1   $4.536 778410 paramètres RST 24-OCT-2024 09:40:51
LNR.BPMEB.0125.S      0.026880      0.128000      0.082740 0.1 0.1 0.1   $4.792 778412 paramètres RST 24-OCT-2024 09:40:51
LNR.BPMEB.0125.T      0.081750      0.128000     -0.029760 0.1 0.1 0.1   $4.792 778414 paramètres RST 24-OCT-2024 09:40:51
*INCLY Instr_Roll_Theo
LNR.BPMEB.0125.E -.00005455831 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BQKMA.0612.S                    ;   28.20780; 1740.43576; 2149.40143;2436.14621;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BQKMA.0612.E                    ;   27.85780; 1740.73106; 2149.21356;2436.14623;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BQKMA.0612   1740.731064   2149.213558   2436.146228 0 0  336.072300 1
*FRAME RST_LNR.BQKMA.0612 0 0 0 .00254647909 .0007661719 0 1
*FRAME RSTRI_LNR.BQKMA.0612      0.000830     -0.000560     -0.000450 .01273241131 0 -.19098645866 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BQKMA.0612 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BQKMA.0612.S                                            0.000000      0.350000      0.000000   $28.208 775637 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:51
BEAM_LNR.BQKMA.0612.E                                            0.000000      0.000000      0.000000   $27.858 775671 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:51
*OBSXYZ
LNR.BQKMA.0612.A     -0.041500      0.039700      0.072310 0.1 0.1 0.1   $27.898 775896 paramètres RST 24-OCT-2024 09:40:51
LNR.BQKMA.0612.B     -0.041500      0.310000      0.072310 0.1 0.1 0.1   $28.168 775897 paramètres RST 24-OCT-2024 09:40:51
LNR.BQKMA.0612.E      0.041500      0.039700      0.072310 0.1 0.1 0.1   $27.898 775894 paramètres RST 24-OCT-2024 09:40:51
LNR.BQKMA.0612.S      0.041500      0.310000      0.072310 0.1 0.1 0.1   $28.168 775895 paramètres RST 24-OCT-2024 09:40:51
*INCLY Instr_Roll_Theo
LNR.BQKMA.0612.E -.00019047664 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MBHEK.0335.E                    ;   14.71742; 1746.92089; 2156.08976;2436.14640;436.15090;;;  .000025;169.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MBHEK.0335.S                    ;   15.68818; 1747.34943; 2155.26777;2436.14643;436.15090;;;  .000025;169.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MBHEK.0335   1746.920891   2156.089764   2436.146398 0 0  136.072300 1
*FRAME RST_LNR.MBHEK.0335 0 0 0 -.00254647909 -.0007661719 33.33333335891 1
*FRAME RSTRI_LNR.MBHEK.0335      0.000061      0.000319     -0.000054 .00542534663 0 -.00096145384 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MBHEK.0335 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MBHEK.0335.E                                            0.000000      0.000000      0.000000   $14.717 775558 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:52
BEAM_LNR.MBHEK.0335.S                                            0.000000      0.927000      0.000000   $15.688 775559 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:52
*OBSXYZ
LNR.MBHEK.0335.E      0.335800      0.228500      0.585000 0.1 0.1 0.1   $14.946 775754 paramètres RST 24-OCT-2024 09:40:52
LNR.MBHEK.0335.S      0.335800      0.698500      0.585000 0.1 0.1 0.1   $15.416 775755 paramètres RST 24-OCT-2024 09:40:52
*INCLY Instr_Roll_Theo
LNR.MBHEK.0335.E .00003959775 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MLNAF.0410.E                    ;   16.23280; 1747.32601; 2154.72365;2436.14643;436.15090;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MLNAF.0410.S                    ;   16.59280; 1747.31053; 2154.36398;2436.14643;436.15090;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MLNAF.0410   1747.326008   2154.723646   2436.146430 0 0  202.739000 1
*FRAME RST_LNR.MLNAF.0410 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.MLNAF.0410     -0.000492      0.000244     -0.000121 .03359936882 0 .0031830984 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MLNAF.0410 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MLNAF.0410.E                                            0.000000      0.000000      0.000000   $16.233 775568 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:52
BEAM_LNR.MLNAF.0410.S                                           -0.000001      0.360000      0.000000   $16.593 775569 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:52
*OBSXYZ
LNR.MLNAF.0410.S      0.214140      0.265000      0.264890 0.1 0.1 0.1   $16.498 775887 paramètres RST 24-OCT-2024 09:40:52
LNR.MLNAF.0410.E      0.214140      0.095000      0.264890 0.1 0.1 0.1   $16.328 775886 paramètres RST 24-OCT-2024 09:40:52
*INCLY Instr_Roll_Theo
LNR.MLNAF.0410.E -.00002922085 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0515.E                    ;   22.13100; 1745.79569; 2149.54083;2436.14643;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0515.S                    ;   22.38100; 1745.57400; 2149.42526;2436.14642;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0515   1745.795686   2149.540828   2436.146427 0 0  269.405700 1
*FRAME RST_LNR.MQNLG.0515 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.MQNLG.0515     -0.000400      0.000177      0.000032 .02979374929 0 .00585689132 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0515 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0515.E                                            0.000000      0.000000      0.000000   $22.131 775616 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:53
BEAM_LNR.MQNLG.0515.S                                            0.000000      0.250000      0.000000   $22.381 775621 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:53
*OBSXYZ
LNR.MQNLG.0515.E      0.212900      0.040000      0.345010 0.1 0.1 0.1   $22.171 775776 paramètres RST 24-OCT-2024 09:40:53
LNR.MQNLG.0515.S      0.212900      0.210000      0.345010 0.1 0.1 0.1   $22.341 775777 paramètres RST 24-OCT-2024 09:40:53
*INCLY Instr_Roll_Theo
LNR.MQNLG.0515.E .04756931164 RF .04774648293
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0630.S                    ;   29.42060; 1739.41250; 2150.05245;2436.14617;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0630.E                    ;   29.17060; 1739.62343; 2149.91825;2436.14618;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0630   1739.623431   2149.918251   2436.146175 0 0  336.072300 1
*FRAME RST_LNR.MQNLG.0630 0 0 0 .00254647909 .0007661719 0 1
*FRAME RSTRI_LNR.MQNLG.0630     -0.000108      0.000213     -0.000094 .02750196811 0 .03004844962 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0630 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0630.S                                            0.000000      0.250000      0.000000   $29.421 775649 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:53
BEAM_LNR.MQNLG.0630.E                                            0.000000      0.000000      0.000000   $29.171 775646 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:53
*OBSXYZ
LNR.MQNLG.0630.E      0.212840      0.040000      0.345070 0.1 0.1 0.1   $29.211 775770 paramètres RST 24-OCT-2024 09:40:53
LNR.MQNLG.0630.S      0.212840      0.210000      0.345070 0.1 0.1 0.1   $29.381 775771 paramètres RST 24-OCT-2024 09:40:53
*INCLY Instr_Roll_Theo
LNR.MQNLG.0630.E -.01037467412 RF -.01018591636
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MSMIE.0115.E                    ;    2.91498; 1738.66692; 2153.56999;2436.14610;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MSMIE.0115.S                    ;    2.91499; 1738.66692; 2153.56999;2436.14610;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MSMIE.0115   1738.666916   2153.569988   2436.146096 0 0    2.739000 1
*FRAME RST_LNR.MSMIE.0115 0 0 0 .0005729578 .00257818275 0 1
*FRAME RSTRI_LNR.MSMIE.0115      0.000120     -0.000290     -0.000060 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MSMIE.0115 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MSMIE.0115.E                                            0.000000      0.000000      0.000000   $2.915 775488 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:53
BEAM_LNR.MSMIE.0115.S                                            0.000000      0.000000      0.000000   $2.915 775489 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:53
*OBSXYZ
LNR.MSMIE.0115.E     -0.148670     -0.128600      0.192000 0.1 0.1 0.1   $2.786 775736 paramètres RST 24-OCT-2024 09:40:53
LNR.MSMIE.0115.S     -0.126310      0.150510      0.192000 0.1 0.1 0.1   $3.065 775737 paramètres RST 24-OCT-2024 09:40:53
*INCLY Instr_Roll_Theo
LNR.MSMIE.0115.E -.00005602254 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;ZDFHL.0610.E                    ;   26.88920; 1741.54829; 2148.69363;2436.14627;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;ZDFHL.0610.S                    ;   27.38920; 1741.12643; 2148.96202;2436.14625;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.ZDFHL.0610   1741.548290   2148.693628   2436.146266 0 0  336.072300 1
*FRAME RST_LNR.ZDFHL.0610 0 0 0 .00254647909 .0007661719 0 1
*FRAME RSTRI_LNR.ZDFHL.0610      0.000268      0.000424     -0.000284 -.01833465634 0 .00585690428 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.ZDFHL.0610 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.ZDFHL.0610.E                                            0.000000      0.000000      0.000000   $26.889 775669 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:54
BEAM_LNR.ZDFHL.0610.S                                            0.000001      0.500000      0.000000   $27.389 775670 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:54
*OBSXYZ
LNR.ZDFHL.0610.E      0.000000      0.070000      0.258000 0.1 0.1 0.1   $26.959 775694 paramètres RST 24-OCT-2024 09:40:54
LNR.ZDFHL.0610.S      0.000000      0.430000      0.258000 0.1 0.1 0.1   $27.319 775695 paramètres RST 24-OCT-2024 09:40:54
*INCLY Instr_Roll_Theo
LNR.ZDFHL.0610.E -.00019092227 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;BSGWA.0025.S                    ;    5.75260; 1736.20761; 2150.68091;2436.14605;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;BSGWA.0025.E                    ;    5.45260; 1736.48976; 2150.57899;2436.14606;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.BSGWA.0025   1736.489763   2150.578994   2436.146058 0 0  322.066700 1
*FRAME RST_LNE00.BSGWA.0025 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.BSGWA.0025      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.BSGWA.0025 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.BSGWA.0025.S                                          0.000000      0.300000      0.000000   $5.753 774812 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:54
BEAM_LNE00.BSGWA.0025.E                                          0.000000      0.000000      0.000000   $5.453 774811 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:54
*OBSXYZ
LNE00.BSGWA.0025.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $5.452 801816 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0025.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $5.527 801817 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0025.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $5.678 801818 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0025.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $5.753 801819 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0025.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $5.678 801820 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0025.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $5.527 801821 paramètres RST 24-OCT-2024 09:40:54
*INCLY Instr_Roll_Theo
LNE00.BSGWA.0025.E -.00094493473 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;BSGWA.0038.E                    ;    8.55260; 1733.57413; 2151.63214;2436.14594;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;BSGWA.0038.S                    ;    8.85260; 1733.29198; 2151.73405;2436.14593;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.BSGWA.0038   1733.574134   2151.632137   2436.145939 0 0  322.066700 1
*FRAME RST_LNE00.BSGWA.0038 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.BSGWA.0038      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.BSGWA.0038 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.BSGWA.0038.E                                          0.000000      0.000000      0.000000   $8.553 774848 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:54
BEAM_LNE00.BSGWA.0038.S                                          0.000000      0.300000      0.000000   $8.853 774849 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:54
*OBSXYZ
LNE00.BSGWA.0038.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $8.552 801823 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0038.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $8.627 801824 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0038.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $8.778 801825 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0038.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $8.853 801826 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0038.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $8.778 801822 paramètres RST 24-OCT-2024 09:40:54
LNE00.BSGWA.0038.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $8.627 801827 paramètres RST 24-OCT-2024 09:40:54
*INCLY Instr_Roll_Theo
LNE00.BSGWA.0038.E -.00094487107 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;BSGWA.0045.E                    ;    9.85860; 1732.34581; 2152.07582;2436.14589;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;BSGWA.0045.S                    ;   10.15860; 1732.06365; 2152.17773;2436.14588;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.BSGWA.0045   1732.345808   2152.075816   2436.145889 0 0  322.066700 1
*FRAME RST_LNE00.BSGWA.0045 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.BSGWA.0045      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.BSGWA.0045 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.BSGWA.0045.E                                          0.000000      0.000000      0.000000   $9.859 774835 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.BSGWA.0045.S                                          0.000000      0.300000      0.000000   $10.159 774836 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
*OBSXYZ
LNE00.BSGWA.0045.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $9.858 801828 paramètres RST 24-OCT-2024 09:40:55
LNE00.BSGWA.0045.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $9.933 801829 paramètres RST 24-OCT-2024 09:40:55
LNE00.BSGWA.0045.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $10.084 801830 paramètres RST 24-OCT-2024 09:40:55
LNE00.BSGWA.0045.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $10.159 801831 paramètres RST 24-OCT-2024 09:40:55
LNE00.BSGWA.0045.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $10.084 801832 paramètres RST 24-OCT-2024 09:40:55
LNE00.BSGWA.0045.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $9.933 801833 paramètres RST 24-OCT-2024 09:40:55
*INCLY Instr_Roll_Theo
LNE00.BSGWA.0045.E -.00094487107 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;ZQFD.0032.E                     ;    7.32510; 1734.72863; 2151.21513;2436.14599;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0032.E                      ;    7.50210; 1734.56216; 2151.27526;2436.14598;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0032.S                      ;    7.53910; 1734.52736; 2151.28783;2436.14598;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQ.0033.E                       ;    7.56910; 1734.49914; 2151.29802;2436.14598;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0032.S                      ;    7.49210; 1734.57156; 2151.27186;2436.14598;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0032.E                     ;    7.30260; 1734.74979; 2151.20748;2436.14599;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0032.S                     ;    7.69260; 1734.38299; 2151.33998;2436.14597;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQFD.0032.S                     ;    7.42510; 1734.63458; 2151.24910;2436.14598;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQ.0033.S                       ;    7.66910; 1734.40509; 2151.33199;2436.14597;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0032.E                      ;    7.45510; 1734.60636; 2151.25929;2436.14598;436.15094;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.ZQNA.0032   1734.749791   2151.207483   2436.145987 0 0  322.066700 1
*FRAME RST_LNE00.ZQNA.0032 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.ZQNA.0032     -0.001370      0.000644     -0.000268 -.02203683594 0 -.02921921429 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.ZQNA.0032 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.ZQFD.0032.E                                           0.000000      0.022500      0.000000   $7.325 774823 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZCH.0032.E                                            0.000000      0.199500      0.000000   $7.502 774827 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZCH.0032.S                                            0.000000      0.236500      0.000000   $7.539 774828 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZQ.0033.E                                             0.000000      0.266500      0.000000   $7.569 774829 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZCV.0032.S                                            0.000000      0.189500      0.000000   $7.492 774826 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZQNA.0032.E                                           0.000000      0.000000      0.000000   $7.303 776397 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZQNA.0032.S                                           0.000000      0.390000      0.000000   $7.693 776398 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZQFD.0032.S                                           0.000000      0.122500      0.000000   $7.425 774824 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZQ.0033.S                                             0.000000      0.366500      0.000000   $7.669 774830 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
BEAM_LNE00.ZCV.0032.E                                            0.000000      0.152500      0.000000   $7.455 774825 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:55
*OBSXYZ
LNE00.ZQNA.0032.E      0.000108      0.074949      0.214923 0.1 0.1 0.1   $7.378 776410 paramètres RST 24-OCT-2024 09:40:55
LNE00.ZQNA.0032.S     -0.001587      0.314879      0.215059 0.1 0.1 0.1   $7.617 776411 paramètres RST 24-OCT-2024 09:40:55
*INCLY Instr_Roll_Theo
LNE00.ZQNA.0032.E .23925176905 RF .24019664011
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;BSGWA.0158.E                    ;   11.09695; 1723.31047; 2158.48829;2436.14545;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;BSGWA.0158.S                    ;   11.39695; 1723.18666; 2158.76155;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.BSGWA.0158   1723.310468   2158.488294   2436.145454 0 0  372.916800 1
*FRAME RST_LNE01.BSGWA.0158 0 0 0 .00210084525 .00136981476 0 1
*FRAME RSTRI_LNE01.BSGWA.0158      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.BSGWA.0158 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.BSGWA.0158.E                                          0.000000      0.000000      0.000000   $11.097 787776 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.BSGWA.0158.S                                          0.000000      0.300000      0.000000   $11.397 787777 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
*OBSXYZ
LNE01.BSGWA.0158.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $11.096 816518 paramètres RST 24-OCT-2024 09:40:56
LNE01.BSGWA.0158.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $11.172 816519 paramètres RST 24-OCT-2024 09:40:56
LNE01.BSGWA.0158.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $11.322 816520 paramètres RST 24-OCT-2024 09:40:56
LNE01.BSGWA.0158.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $11.397 816521 paramètres RST 24-OCT-2024 09:40:56
LNE01.BSGWA.0158.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $11.322 816522 paramètres RST 24-OCT-2024 09:40:56
LNE01.BSGWA.0158.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $11.172 816523 paramètres RST 24-OCT-2024 09:40:56
*INCLY Instr_Roll_Theo
LNE01.BSGWA.0158.E -.00089807951 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZQFF.0101.E                     ;    1.33897; 1730.35519; 2152.79484;2436.14581;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQFF.0101.S                     ;    1.43897; 1730.26113; 2152.82881;2436.14580;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0101.E                      ;    1.46897; 1730.23292; 2152.83900;2436.14580;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0101.S                      ;    1.50597; 1730.19812; 2152.85157;2436.14580;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0101.E                      ;    1.51597; 1730.18871; 2152.85497;2436.14580;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0101.S                      ;    1.55297; 1730.15391; 2152.86754;2436.14580;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0102.E                       ;    1.58297; 1730.12570; 2152.87773;2436.14580;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0102.S                       ;    1.68297; 1730.03165; 2152.91171;2436.14579;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0101.E                     ;    1.31647; 1730.37635; 2152.78720;2436.14581;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0101.S                     ;    1.70647; 1730.00954; 2152.91969;2436.14579;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.0101   1730.376348   2152.787197   2436.145809 0 0  322.066700 1
*FRAME RST_LNE01.ZQNA.0101 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE01.ZQNA.0101      0.000005      0.000613     -0.000326 .03689129705 0 -.03754424674 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.0101 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZQFF.0101.E                                           0.000000      0.022500      0.000000   $1.339 787712 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQFF.0101.S                                           0.000000      0.122500      0.000000   $1.439 787713 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZCV.0101.E                                            0.000000      0.152500      0.000000   $1.469 787714 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZCV.0101.S                                            0.000000      0.189500      0.000000   $1.506 787715 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZCH.0101.E                                            0.000000      0.199500      0.000000   $1.516 787716 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZCH.0101.S                                            0.000000      0.236500      0.000000   $1.553 787717 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQ.0102.E                                             0.000000      0.266500      0.000000   $1.583 787718 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQ.0102.S                                             0.000000      0.366500      0.000000   $1.683 787719 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQNA.0101.E                                           0.000000      0.000000      0.000000   $1.316 792363 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQNA.0101.S                                           0.000000      0.390000      0.000000   $1.706 792364 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
*OBSXYZ
LNE01.ZQNA.0101.E      0.000254      0.074968      0.214981 0.1 0.1 0.1   $1.391 792387 paramètres RST 24-OCT-2024 09:40:56
LNE01.ZQNA.0101.S     -0.000048      0.314963      0.215081 0.1 0.1 0.1   $1.631 792388 paramètres RST 24-OCT-2024 09:40:56
*INCLY Instr_Roll_Theo
LNE01.ZQNA.0101.E .03948048448 RF .04042535555
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZQFD.0152.E                     ;   10.11345; 1723.71636; 2157.59246;2436.14549;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQFD.0152.S                     ;   10.21345; 1723.67509; 2157.68354;2436.14548;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0152.S                      ;   10.28045; 1723.64744; 2157.74457;2436.14548;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0152.E                      ;   10.29045; 1723.64331; 2157.75368;2436.14548;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0152.S                      ;   10.32745; 1723.62804; 2157.78738;2436.14548;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0153.S                       ;   10.45745; 1723.57439; 2157.90580;2436.14547;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0152.E                     ;   10.09095; 1723.72565; 2157.57196;2436.14549;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0152.S                     ;   10.48095; 1723.56469; 2157.92720;2436.14547;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0153.E                       ;   10.35745; 1723.61566; 2157.81471;2436.14548;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0152.E                      ;   10.24345; 1723.66271; 2157.71087;2436.14548;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.0152   1723.725649   2157.571964   2436.145487 0 0  372.916800 1
*FRAME RST_LNE01.ZQNA.0152 0 0 0 .00210084525 .00136981476 0 1
*FRAME RSTRI_LNE01.ZQNA.0152     -0.000121     -0.000543     -0.000703 .00652943704 0 .03933986083 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.0152 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZQFD.0152.E                                           0.000000      0.022500      0.000000   $10.113 787768 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQFD.0152.S                                           0.000000      0.122500      0.000000   $10.213 787769 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZCV.0152.S                                            0.000000      0.189500      0.000000   $10.280 787771 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZCH.0152.E                                            0.000000      0.199500      0.000000   $10.290 787772 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZCH.0152.S                                            0.000000      0.236500      0.000000   $10.327 787773 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQ.0153.S                                             0.000000      0.366500      0.000000   $10.457 787775 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQNA.0152.E                                           0.000000      0.000000      0.000000   $10.091 792375 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQNA.0152.S                                           0.000000      0.390000      0.000000   $10.481 792376 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZQ.0153.E                                             0.000000      0.266500      0.000000   $10.357 787774 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
BEAM_LNE01.ZCV.0152.E                                            0.000000      0.152500      0.000000   $10.243 787770 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:56
*OBSXYZ
LNE01.ZQNA.0152.S     -0.000080      0.314906      0.215064 0.1 0.1 0.1   $10.406 792400 paramètres RST 24-OCT-2024 09:40:56
LNE01.ZQNA.0152.E      0.000007      0.074953      0.214947 0.1 0.1 0.1   $10.166 792399 paramètres RST 24-OCT-2024 09:40:56
*INCLY Instr_Roll_Theo
LNE01.ZQNA.0152.E .01368051327 RF .01457859279
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE02     ;ZDSHR.0220.E                    ;    4.83177; 1721.18001; 2156.10897;2436.14543;436.15101;;; -.000036;347.4945;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZDSHR.0220.S                    ;    5.31107; 1720.83730; 2156.42571;2436.14542;436.15101;;; -.000036;347.4945;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE02.ZDSHR.0220   1721.180007   2156.108970   2436.145433 0 0  322.066696 1
*FRAME RST_LNE02.ZDSHR.0220 0 0 0 .00241915513 -.00054647441 25.42777782114 1
*FRAME RSTRI_LNE02.ZDSHR.0220      0.000011     -0.000079     -0.000234 -.01868965825 0 .00313767994 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE02.ZDSHR.0220 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE02.ZDSHR.0220.E                                          0.000000      0.000000      0.000000   $4.832 787860 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZDSHR.0220.S                                          0.000001      0.466659      0.000000   $5.311 787861 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
*OBSXYZ
LNE02.ZDSHR.0220.E     -0.078914     -0.049994      0.217756 0.1 0.1 0.1   $4.782 806358 paramètres RST 24-OCT-2024 09:40:57
LNE02.ZDSHR.0220.S     -0.270431      0.403211      0.217896 0.1 0.1 0.1   $5.235 806359 paramètres RST 24-OCT-2024 09:40:57
LNE02.ZDSHR.0220.T      0.224803      0.078234      0.217728 0.1 0.1 0.1   $4.910 806360 paramètres RST 24-OCT-2024 09:40:57
*INCLY Instr_Roll_Theo
LNE02.ZDSHR.0220.E -.00102565812 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE02     ;ZQMD.0214.E                     ;    4.24359; 1721.73320; 2155.90915;2436.14546;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCV.0214.E                      ;    4.37359; 1721.61093; 2155.95332;2436.14545;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCV.0214.S                      ;    4.41059; 1721.57613; 2155.96589;2436.14545;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCH.0214.E                      ;    4.42059; 1721.56673; 2155.96928;2436.14545;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCH.0214.S                      ;    4.45759; 1721.53193; 2155.98185;2436.14545;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQMF.0215.E                     ;    4.48759; 1721.50371; 2155.99205;2436.14545;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQMF.0215.S                     ;    4.58759; 1721.40966; 2156.02602;2436.14544;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQNA.0214.E                     ;    4.22109; 1721.75436; 2155.90151;2436.14546;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQNA.0214.S                     ;    4.61109; 1721.38756; 2156.03400;2436.14544;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQMD.0214.S                     ;    4.34359; 1721.63915; 2155.94313;2436.14545;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE02.ZQNA.0214   1721.754360   2155.901510   2436.145460 0 0  322.066696 1
*FRAME RST_LNE02.ZQNA.0214 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE02.ZQNA.0214     -0.000122      0.000090     -0.000286 .02171066388 0 -.01436495122 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE02.ZQNA.0214 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE02.ZQMD.0214.E                                           0.000001      0.022496     -0.000003   $4.244 787852 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZCV.0214.E                                            0.000001      0.152496     -0.000003   $4.374 787854 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZCV.0214.S                                            0.000001      0.189496     -0.000003   $4.411 787855 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZCH.0214.E                                            0.000001      0.199496     -0.000003   $4.421 787856 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZCH.0214.S                                            0.000001      0.236496     -0.000003   $4.458 787857 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZQMF.0215.E                                           0.000001      0.266496     -0.000003   $4.488 787858 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZQMF.0215.S                                           0.000001      0.366496     -0.000003   $4.588 787859 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZQNA.0214.E                                           0.000000      0.000000      0.000000   $4.221 805991 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZQNA.0214.S                                           0.000000      0.389995     -0.000005   $4.611 805992 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
BEAM_LNE02.ZQMD.0214.S                                           0.000001      0.122496     -0.000003   $4.344 787853 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:57
*OBSXYZ
LNE02.ZQNA.0214.E     -0.000021      0.075046      0.215282 0.1 0.1 0.1   $4.296 806093 paramètres RST 24-OCT-2024 09:40:57
LNE02.ZQNA.0214.S     -0.000141      0.315032      0.215279 0.1 0.1 0.1   $4.536 806094 paramètres RST 24-OCT-2024 09:40:57
*INCLY Instr_Roll_Theo
LNE02.ZQNA.0214.E .12968950622 RF .13063437729
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZDSHR.0643.E                    ;    7.48934; 1726.77618; 2159.50510;2436.14555;436.15091;;; -.000015;  9.2113;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZDSHR.0643.S                    ;    7.86089; 1726.82890; 2159.86692;2436.14554;436.15091;;; -.000015;  9.2113;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZDSHR.0643   1726.776182   2159.505104   2436.145547 0 0  389.500200 1
*FRAME RST_LNE06.ZDSHR.0643 0 0 0 .00165521141 .0018635134 19.71111115543 1
*FRAME RSTRI_LNE06.ZDSHR.0643     -0.001162      0.000281     -0.000574 .03064374629 0 .0536265665 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZDSHR.0643 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZDSHR.0643.E                                          0.000000      0.000000      0.000000   $7.489 788126 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZDSHR.0643.S                                          0.000000      0.365638      0.000000   $7.861 788127 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
*OBSXYZ
LNE06.ZDSHR.0643.E     -0.113899     -0.126387      0.218069 0.1 0.1 0.1   $7.363 791838 paramètres RST 24-OCT-2024 09:40:58
LNE06.ZDSHR.0643.S     -0.321556      0.230561      0.218056 0.1 0.1 0.1   $7.720 791839 paramètres RST 24-OCT-2024 09:40:58
LNE06.ZDSHR.0643.T      0.244490      0.083177      0.217959 0.1 0.1 0.1   $7.573 791840 paramètres RST 24-OCT-2024 09:40:58
*INCLY Instr_Roll_Theo
LNE06.ZDSHR.0643.E -.00051114202 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZCV.0612.E                      ;    1.87440; 1728.79779; 2154.27489;2436.14573;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0612.S                      ;    1.91140; 1728.78390; 2154.30918;2436.14572;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0612.E                      ;    1.92140; 1728.78014; 2154.31845;2436.14572;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0612.S                      ;    1.95840; 1728.76625; 2154.35274;2436.14572;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMF.0613.E                     ;    1.98840; 1728.75499; 2154.38054;2436.14572;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMF.0613.S                     ;    2.08840; 1728.71744; 2154.47323;2436.14572;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0612.E                     ;    1.72190; 1728.85505; 2154.13355;2436.14573;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0612.S                     ;    2.11190; 1728.70861; 2154.49501;2436.14572;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMD.0612.E                     ;    1.74440; 1728.84661; 2154.15440;2436.14573;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMD.0612.S                     ;    1.84440; 1728.80906; 2154.24708;2436.14573;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZQNA.0612   1728.855054   2154.133546   2436.145730 0 0  375.494600 1
*FRAME RST_LNE06.ZQNA.0612 0 0 0 .00203718327 .00145353026 0 1
*FRAME RSTRI_LNE06.ZQNA.0612     -0.000464      0.000314     -0.000534 .01860890996 0 -.05043994688 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZQNA.0612 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZCV.0612.E                                            0.000000      0.152499      0.000000   $1.874 788088 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZCV.0612.S                                            0.000000      0.189499      0.000000   $1.911 788089 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZCH.0612.E                                            0.000000      0.199499      0.000000   $1.921 788090 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZCH.0612.S                                            0.000000      0.236499      0.000000   $1.958 788091 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZQMF.0613.E                                           0.000000      0.266499      0.000000   $1.988 788092 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZQMF.0613.S                                           0.000000      0.366499      0.000000   $2.088 788093 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZQNA.0612.E                                           0.000000      0.000000      0.000000   $1.722 792471 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZQNA.0612.S                                           0.000000      0.389999     -0.000001   $2.112 792472 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZQMD.0612.E                                           0.000000      0.022499      0.000000   $1.744 788086 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
BEAM_LNE06.ZQMD.0612.S                                           0.000000      0.122499      0.000000   $1.844 788087 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:58
*OBSXYZ
LNE06.ZQNA.0612.E     -0.001346      0.075030      0.215118 0.1 0.1 0.1   $1.797 792479 paramètres RST 24-OCT-2024 09:40:58
LNE06.ZQNA.0612.S     -0.000423      0.315074      0.215092 0.1 0.1 0.1   $2.037 792480 paramètres RST 24-OCT-2024 09:40:58
*INCLY Instr_Roll_Theo
LNE06.ZQNA.0612.E .1642294393 RF .16507550697
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;BSGWA.0722.E                    ;    2.78640; 1730.55150; 2154.53633;2436.14578;436.15094;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;BSGWA.0722.S                    ;    3.08640; 1730.68322; 2154.80587;2436.14578;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.BSGWA.0722   1730.551498   2154.536334   2436.145778 0 0   28.939000 1
*FRAME RST_LNE07.BSGWA.0722 0 0 0 .00025464791 .00248854669 0 1
*FRAME RSTRI_LNE07.BSGWA.0722      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.BSGWA.0722 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.BSGWA.0722.E                                          0.000000      0.000000      0.000000   $2.786 788170 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:59
BEAM_LNE07.BSGWA.0722.S                                          0.000000      0.300000      0.000000   $3.086 788171 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:59
*OBSXYZ
LNE07.BSGWA.0722.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $3.012 801838 paramètres RST 24-OCT-2024 09:40:59
LNE07.BSGWA.0722.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $2.786 801834 paramètres RST 24-OCT-2024 09:40:59
LNE07.BSGWA.0722.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $2.861 801835 paramètres RST 24-OCT-2024 09:40:59
LNE07.BSGWA.0722.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $3.012 801836 paramètres RST 24-OCT-2024 09:40:59
LNE07.BSGWA.0722.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $3.087 801837 paramètres RST 24-OCT-2024 09:40:59
LNE07.BSGWA.0722.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $2.861 801839 paramètres RST 24-OCT-2024 09:40:59
*INCLY Instr_Roll_Theo
LNE07.BSGWA.0722.E -.00017927213 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;ZDSHR.0706.E                    ;     .71271; 1730.98270; 2152.66413;2436.14583;436.15096;;; -.000036;355.7835;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZDSHR.0706.S                    ;    1.08426; 1730.74868; 2152.94506;2436.14582;436.15096;;; -.000036;355.7835;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.ZDSHR.0706   1730.982703   2152.664127   2436.145832 0 0  336.072300 1
*FRAME RST_LNE07.ZDSHR.0706 0 0 0 .00248281711 .00000006366 19.71111115543 1
*FRAME RSTRI_LNE07.ZDSHR.0706      0.000560      0.000257     -0.000360 .04979609083 0 .03325543958 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.ZDSHR.0706 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.ZDSHR.0706.E                                          0.000000      0.000000      0.000000   $0.713 788156 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:59
BEAM_LNE07.ZDSHR.0706.S                                          0.000000      0.365638      0.000000   $1.084 788157 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:59
*OBSXYZ
LNE07.ZDSHR.0706.E     -0.113353     -0.125634      0.217667 0.1 0.1 0.1   $0.587 792540 paramètres RST 24-OCT-2024 09:40:59
LNE07.ZDSHR.0706.S     -0.321648      0.230639      0.218396 0.1 0.1 0.1   $0.943 792541 paramètres RST 24-OCT-2024 09:40:59
LNE07.ZDSHR.0706.T      0.244098      0.083532      0.217757 0.1 0.1 0.1   $0.796 792542 paramètres RST 24-OCT-2024 09:40:59
*INCLY Instr_Roll_Theo
LNE07.ZDSHR.0706.E -.00095779445 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BQKMA.0312.E                    ;   12.65500; 1745.18079; 2157.19684;2436.14632;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BQKMA.0312.S                    ;   13.00500; 1745.47609; 2157.00897;2436.14633;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BQKMA.0312   1745.180786   2157.196843   2436.146316 0 0  136.072300 1
*FRAME RST_LNR.BQKMA.0312 0 0 0 -.00254647909 -.0007661719 0 1
*FRAME RSTRI_LNR.BQKMA.0312     -0.000790      0.000210      0.000320 .00909457948 0 .15460800413 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BQKMA.0312 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BQKMA.0312.E                                            0.000000      0.000000      0.000000   $12.655 775544 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:59
BEAM_LNR.BQKMA.0312.S                                            0.000000      0.350000      0.000000   $13.005 775545 coordonnées théoriques dans le système RST au 24-OCT-2024 09:40:59
*OBSXYZ
LNR.BQKMA.0312.E      0.041750      0.039700      0.072310 0.1 0.1 0.1   $12.695 775890 paramètres RST 24-OCT-2024 09:40:59
LNR.BQKMA.0312.A     -0.041500      0.039700      0.072310 0.1 0.1 0.1   $12.695 775892 paramètres RST 24-OCT-2024 09:40:59
LNR.BQKMA.0312.B     -0.041500      0.310000      0.072310 0.1 0.1 0.1   $12.965 775893 paramètres RST 24-OCT-2024 09:40:59
LNR.BQKMA.0312.S      0.041500      0.310000      0.072310 0.1 0.1 0.1   $12.965 775891 paramètres RST 24-OCT-2024 09:40:59
*INCLY Instr_Roll_Theo
LNR.BQKMA.0312.E .00010014029 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0420.E                    ;   16.92398; 1747.29628; 2154.03311;2436.14644;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0420.S                    ;   16.92399; 1747.29628; 2154.03311;2436.14644;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0420   1747.296280   2154.033105   2436.146436 0 0  202.739000 1
*FRAME RST_LNR.MCCAY.0420 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.MCCAY.0420      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0420 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0420.E                                            0.000000      0.000000      0.000000   $16.924 775570 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:00
BEAM_LNR.MCCAY.0420.S                                            0.000000      0.000000      0.000000   $16.924 775571 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:00
*OBSXYZ
LNR.MCCAY.0420.E     -0.160000     -0.085000      0.204000 0.1 0.1 0.1   $16.839 776153 paramètres RST 24-OCT-2024 09:41:00
LNR.MCCAY.0420.S     -0.160000      0.085000      0.204000 0.1 0.1 0.1   $17.009 776163 paramètres RST 24-OCT-2024 09:41:00
*INCLY Instr_Roll_Theo
LNR.MCCAY.0420.E -.00003138535 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0535.E                    ;   23.45500; 1744.62166; 2148.92876;2436.14639;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0535.S                    ;   23.45501; 1744.62166; 2148.92876;2436.14639;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0535   1744.621656   2148.928758   2436.146387 0 0  269.405700 1
*FRAME RST_LNR.MCCAY.0535 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.MCCAY.0535      0.000174      0.000199     -0.000344 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0535 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0535.E                                            0.000000      0.000000      0.000000   $23.455 775626 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:00
BEAM_LNR.MCCAY.0535.S                                            0.000000      0.000000      0.000000   $23.455 775627 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:00
*OBSXYZ
LNR.MCCAY.0535.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $23.540 776166 paramètres RST 24-OCT-2024 09:41:00
LNR.MCCAY.0535.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $23.370 776156 paramètres RST 24-OCT-2024 09:41:00
*INCLY Instr_Roll_Theo
LNR.MCCAY.0535.E -.00017768058 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0620.E                    ;   28.69740; 1740.02268; 2149.66424;2436.14619;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0620.S                    ;   28.94740; 1739.81175; 2149.79844;2436.14618;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0620   1740.022678   2149.664244   2436.146194 0 0  336.072300 1
*FRAME RST_LNR.MQNLG.0620 0 0 0 .00254647909 .0007661719 0 1
*FRAME RSTRI_LNR.MQNLG.0620      0.000091      0.000934      0.000007 .04711001461 0 .01375103391 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0620 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0620.E                                            0.000000      0.000000      0.000000   $28.697 775640 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:01
BEAM_LNR.MQNLG.0620.S                                            0.000000      0.249999      0.000000   $28.947 775645 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:01
*OBSXYZ
LNR.MQNLG.0620.S      0.212910      0.210000      0.345020 0.1 0.1 0.1   $28.907 775767 paramètres RST 24-OCT-2024 09:41:01
LNR.MQNLG.0620.E      0.212910      0.040000      0.345020 0.1 0.1 0.1   $28.737 775766 paramètres RST 24-OCT-2024 09:41:01
*INCLY Instr_Roll_Theo
LNR.MQNLG.0620.E -.01228453344 RF -.01209577567
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MXNAD.0505.S                    ;   21.50780; 1746.34830; 2149.82893;2436.14645;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MXNAD.0505.E                    ;   21.35780; 1746.48131; 2149.89827;2436.14645;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MXNAD.0505   1746.481305   2149.898270   2436.146450 0 0  269.405700 1
*FRAME RST_LNR.MXNAD.0505 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.MXNAD.0505      0.000076     -0.000208     -0.000392 -.0101858911 0 -.03565062103 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MXNAD.0505 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MXNAD.0505.S                                            0.000000      0.150000      0.000001   $21.508 775611 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:01
BEAM_LNR.MXNAD.0505.E                                            0.000000      0.000000      0.000000   $21.358 775610 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:01
*OBSXYZ
LNR.MXNAD.0505.E      0.212967     -0.009779      0.204968 0.1 0.1 0.1   $21.348 775840 paramètres RST 24-OCT-2024 09:41:01
LNR.MXNAD.0505.S      0.212987      0.160206      0.204876 0.1 0.1 0.1   $21.518 775841 paramètres RST 24-OCT-2024 09:41:01
*INCLY Instr_Roll_Theo
LNR.MXNAD.0505.E -.00017710762 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;ZDFHL.0310.E                    ;   11.68640; 1744.36356; 2157.71677;2436.14628;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;ZDFHL.0310.S                    ;   12.18640; 1744.78542; 2157.44838;2436.14630;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.ZDFHL.0310   1744.363560   2157.716774   2436.146278 0 0  136.072300 1
*FRAME RST_LNR.ZDFHL.0310 0 0 0 -.00254647909 -.0007661719 0 1
*FRAME RSTRI_LNR.ZDFHL.0310     -0.000275      0.000039     -0.000067 .00993127209 0 .02368226495 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.ZDFHL.0310 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.ZDFHL.0310.E                                            0.000000      0.000000      0.000000   $11.686 775542 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:01
BEAM_LNR.ZDFHL.0310.S                                            0.000001      0.500000      0.000000   $12.186 775543 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:01
*OBSXYZ
LNR.ZDFHL.0310.E      0.000000      0.070000      0.258000 0.1 0.1 0.1   $11.756 775692 paramètres RST 24-OCT-2024 09:41:01
LNR.ZDFHL.0310.S      0.000000      0.430000      0.258000 0.1 0.1 0.1   $12.116 775693 paramètres RST 24-OCT-2024 09:41:01
*INCLY Instr_Roll_Theo
LNR.ZDFHL.0310.E .00009975832 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;BSGWA.0137.S                    ;    7.87827; 1724.63883; 2155.55651;2436.14556;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;BSGWA.0137.E                    ;    7.57827; 1724.76265; 2155.28325;2436.14557;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.BSGWA.0137   1724.762646   2155.283253   2436.145570 0 0  372.916800 1
*FRAME RST_LNE01.BSGWA.0137 0 0 0 .00210084525 .00136981476 0 1
*FRAME RSTRI_LNE01.BSGWA.0137      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.BSGWA.0137 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.BSGWA.0137.S                                          0.000000      0.300000      0.000000   $7.878 787759 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:02
BEAM_LNE01.BSGWA.0137.E                                          0.000000      0.000000      0.000000   $7.578 787758 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:02
*OBSXYZ
LNE01.BSGWA.0137.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $7.578 801873 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0137.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $7.653 801874 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0137.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $7.804 801875 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0137.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $7.879 801876 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0137.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $7.804 801877 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0137.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $7.653 801878 paramètres RST 24-OCT-2024 09:41:02
*INCLY Instr_Roll_Theo
LNE01.BSGWA.0137.E -.00089807951 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;BSGWA.0151.E                    ;    9.79095; 1723.84946; 2157.29870;2436.14550;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;BSGWA.0151.S                    ;   10.09095; 1723.72565; 2157.57196;2436.14549;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.BSGWA.0151   1723.849461   2157.298704   2436.145497 0 0  372.916800 1
*FRAME RST_LNE01.BSGWA.0151 0 0 0 .00210084525 .00136981476 0 1
*FRAME RSTRI_LNE01.BSGWA.0151      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.BSGWA.0151 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.BSGWA.0151.E                                          0.000000      0.000000      0.000000   $9.791 787766 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:02
BEAM_LNE01.BSGWA.0151.S                                          0.000000      0.300000      0.000000   $10.091 787767 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:02
*OBSXYZ
LNE01.BSGWA.0151.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $10.016 816516 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0151.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $9.790 816512 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0151.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $9.866 816513 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0151.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $10.016 816514 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0151.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $10.091 816515 paramètres RST 24-OCT-2024 09:41:02
LNE01.BSGWA.0151.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $9.866 816517 paramètres RST 24-OCT-2024 09:41:02
*INCLY Instr_Roll_Theo
LNE01.BSGWA.0151.E -.00089807951 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;BSGWA.0181.E                    ;   13.92393; 1722.52284; 2160.22656;2437.34877;437.35433;;; 1.570675;361.5240;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;BSGWA.0181.S                    ;   14.09538; 1722.52283; 2160.22658;2437.52022;437.52578;;; 1.570675;361.5240;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.BSGWA.0181   1722.522845   2160.226559   2437.348774 0 0  361.524000 1
*FRAME RST_LNE01.BSGWA.0181 0 0 0 -99.99227609635 11.39273449698 0 1
*FRAME RSTRI_LNE01.BSGWA.0181      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.BSGWA.0181 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.BSGWA.0181.E                                          0.000000      0.000000      0.000000   $13.924 787814 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.BSGWA.0181.S                                          0.000000      0.171450      0.000000   $14.095 787815 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
*OBSXYZ
LNE01.BSGWA.0181.A      0.125500      0.085730      0.161850 0.1 0.1 0.1   $14.010 816524 paramètres RST 24-OCT-2024 09:41:03
LNE01.BSGWA.0181.B      0.073770     -0.015810      0.161850 0.1 0.1 0.1   $13.908 816525 paramètres RST 24-OCT-2024 09:41:03
LNE01.BSGWA.0181.C     -0.038780     -0.033630      0.161850 0.1 0.1 0.1   $13.890 816526 paramètres RST 24-OCT-2024 09:41:03
LNE01.BSGWA.0181.D     -0.119360      0.046940      0.161850 0.1 0.1 0.1   $13.971 816527 paramètres RST 24-OCT-2024 09:41:03
LNE01.BSGWA.0181.E     -0.101530      0.159490      0.161850 0.1 0.1 0.1   $14.083 816528 paramètres RST 24-OCT-2024 09:41:03
LNE01.BSGWA.0181.F      0.000000      0.211230      0.161850 0.1 0.1 0.1   $14.135 816529 paramètres RST 24-OCT-2024 09:41:03
*INCLY Instr_Roll_Theo
LNE01.BSGWA.0181.E -9.58237929593 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZQFD.0107.E                     ;    3.15667; 1728.64559; 2153.41236;2436.14574;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQFD.0107.S                     ;    3.25667; 1728.55154; 2153.44633;2436.14573;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0107.E                      ;    3.28667; 1728.52333; 2153.45652;2436.14573;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0107.S                      ;    3.32367; 1728.48853; 2153.46909;2436.14573;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0107.E                      ;    3.33367; 1728.47912; 2153.47249;2436.14573;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0108.S                       ;    3.50067; 1728.32205; 2153.52922;2436.14572;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0107.S                     ;    3.52417; 1728.29995; 2153.53720;2436.14572;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0108.E                       ;    3.40067; 1728.41611; 2153.49525;2436.14573;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0107.S                      ;    3.37067; 1728.44432; 2153.48506;2436.14573;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0107.E                     ;    3.13417; 1728.66676; 2153.40471;2436.14574;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.0107   1728.666755   2153.404712   2436.145739 0 0  322.066700 1
*FRAME RST_LNE01.ZQNA.0107 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE01.ZQNA.0107      0.000248      0.001095     -0.000249 .06578404657 0 -.03607513708 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.0107 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZQFD.0107.E                                           0.000000      0.022500      0.000000   $3.157 787722 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZQFD.0107.S                                           0.000000      0.122500      0.000000   $3.257 787723 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZCV.0107.E                                            0.000000      0.152500      0.000000   $3.287 787724 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZCV.0107.S                                            0.000000      0.189500      0.000000   $3.324 787725 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZCH.0107.E                                            0.000000      0.199500      0.000000   $3.334 787726 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZQ.0108.S                                             0.000000      0.366500      0.000000   $3.501 787729 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZQNA.0107.S                                           0.000000      0.390000      0.000000   $3.524 792366 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZQ.0108.E                                             0.000000      0.266500      0.000000   $3.401 787728 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZCH.0107.S                                            0.000000      0.236500      0.000000   $3.371 787727 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
BEAM_LNE01.ZQNA.0107.E                                           0.000000      0.000000      0.000000   $3.134 792365 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:03
*OBSXYZ
LNE01.ZQNA.0107.E      0.000328      0.075079      0.215120 0.1 0.1 0.1   $3.209 792389 paramètres RST 24-OCT-2024 09:41:03
LNE01.ZQNA.0107.S     -0.000674      0.314979      0.215187 0.1 0.1 0.1   $3.449 792390 paramètres RST 24-OCT-2024 09:41:03
*INCLY Instr_Roll_Theo
LNE01.ZQNA.0107.E .04533732272 RF .04628225745
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZCV.0145.S                      ;    9.09937; 1724.13488; 2156.66877;2436.14552;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0145.E                      ;    9.10937; 1724.13075; 2156.67788;2436.14552;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0145.S                      ;    9.14637; 1724.11548; 2156.71158;2436.14552;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQMF.0146.E                     ;    9.17637; 1724.10310; 2156.73891;2436.14552;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQMF.0146.S                     ;    9.27637; 1724.06183; 2156.82999;2436.14551;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQMD.0145.S                     ;    9.03237; 1724.16253; 2156.60774;2436.14552;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0145.E                     ;    8.90987; 1724.21309; 2156.49616;2436.14553;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0145.S                     ;    9.29987; 1724.05213; 2156.85140;2436.14551;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0145.E                      ;    9.06237; 1724.15015; 2156.63507;2436.14552;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQMD.0145.E                     ;    8.93237; 1724.20380; 2156.51665;2436.14553;436.15097;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.0145   1724.213087   2156.496160   2436.145526 0 0  372.916800 1
*FRAME RST_LNE01.ZQNA.0145 0 0 0 .00210084525 .00136981476 0 1
*FRAME RSTRI_LNE01.ZQNA.0145     -0.000495     -0.001660     -0.000708 -.01469123345 0 .05762229121 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.0145 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZCV.0145.S                                            0.000000      0.189500      0.000000   $9.099 787761 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZCH.0145.E                                            0.000000      0.199500      0.000000   $9.109 787762 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZCH.0145.S                                            0.000000      0.236500      0.000000   $9.146 787763 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZQMF.0146.E                                           0.000000      0.266500      0.000000   $9.176 787764 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZQMF.0146.S                                           0.000000      0.366500      0.000000   $9.276 787765 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZQMD.0145.S                                           0.000000      0.122500      0.000000   $9.032 787810 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZQNA.0145.E                                           0.000000      0.000000      0.000000   $8.910 792373 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZQNA.0145.S                                           0.000000      0.390000      0.000000   $9.300 792374 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZCV.0145.E                                            0.000000      0.152500      0.000000   $9.062 787811 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE01.ZQMD.0145.E                                           0.000000      0.022500      0.000000   $8.932 787809 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
*OBSXYZ
LNE01.ZQNA.0145.E     -0.000663      0.075135      0.215244 0.1 0.1 0.1   $8.985 792397 paramètres RST 24-OCT-2024 09:41:04
LNE01.ZQNA.0145.S     -0.000460      0.315062      0.215206 0.1 0.1 0.1   $9.225 792398 paramètres RST 24-OCT-2024 09:41:04
*INCLY Instr_Roll_Theo
LNE01.ZQNA.0145.E .11420277533 RF .11510085484
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZDFHR.0601.E                    ;     .10000; 1729.86627; 2152.97144;2436.14579;436.15096;;; -.000039;329.0695;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZDFHR.0601.S                    ;     .50000; 1729.50798; 2153.14745;2436.14577;436.15096;;; -.000039;329.0695;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZDFHR.0601   1729.866272   2152.971439   2436.145788 0 0  322.066700 1
*FRAME RST_LNE06.ZDFHR.0601 0 0 0 .00241915513 -.00054647441 7.00281749604 1
*FRAME RSTRI_LNE06.ZDFHR.0601      0.000923     -0.000028     -0.000554 -.00542219622 0 .01483130159 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZDFHR.0601 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZDFHR.0601.E                                          0.000000      0.000000      0.000000   $0.100 788078 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
BEAM_LNE06.ZDFHR.0601.S                                          0.000000      0.399194      0.000000   $0.500 788079 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:04
*OBSXYZ
LNE06.ZDFHR.0601.E     -0.002229      0.020188      0.253000 0.1 0.1 0.1   $0.120 791931 paramètres RST 24-OCT-2024 09:41:04
LNE06.ZDFHR.0601.S     -0.041750      0.378012      0.253000 0.1 0.1 0.1   $0.478 791932 paramètres RST 24-OCT-2024 09:41:04
*INCLY Instr_Roll_Theo
LNE06.ZDFHR.0601.E -.00097281867 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;BASE.0750.S                     ;   10.53640; 1733.95435; 2161.49931;2436.14575;436.15083;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;BASE.0750.E                     ;    7.73640; 1732.72494; 2158.98365;2436.14576;436.15087;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.BASE.0750   1732.724935   2158.983650   2436.145756 0 0   28.939000 1
*FRAME RST_LNE07.BASE.0750 0 0 0 .00025464791 .00248854669 0 1
*FRAME RSTRI_LNE07.BASE.0750     -0.000010     -0.000649     -0.000056 -.00086398391 0 -.00004547284 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.BASE.0750 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.BASE.0750.S                                          -0.000001      2.800000      0.000000   $10.536 817908 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.BASE.0750.E                                           0.000000      0.000000      0.000000   $7.736 817907 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
*OBSXYZ
LNE07.BASE.0750.E      0.000000      0.000000      0.000000 0.1 0.1 0.1   $7.736 817909 paramètres RST 24-OCT-2024 09:41:05
LNE07.BASE.0750.S      0.000000      2.800000      0.000000 0.1 0.1 0.1   $10.536 817910 paramètres RST 24-OCT-2024 09:41:05
*INCLY Instr_Roll_Theo
LNE07.BASE.0750.E -.00017927213 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;BSGWA.0737.E                    ;    5.66983; 1731.81755; 2157.12695;2436.14577;436.15090;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;BSGWA.0737.S                    ;    5.96983; 1731.94927; 2157.39648;2436.14576;436.15089;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.BSGWA.0737   1731.817549   2157.126947   2436.145765 0 0   28.939000 1
*FRAME RST_LNE07.BSGWA.0737 0 0 0 .00025464791 .00248854669 0 1
*FRAME RSTRI_LNE07.BSGWA.0737      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.BSGWA.0737 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.BSGWA.0737.E                                          0.000000      0.000000      0.000000   $5.670 788196 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.BSGWA.0737.S                                          0.000000      0.300000      0.000000   $5.970 788197 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
*OBSXYZ
LNE07.BSGWA.0737.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $5.669 801840 paramètres RST 24-OCT-2024 09:41:05
LNE07.BSGWA.0737.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $5.745 801841 paramètres RST 24-OCT-2024 09:41:05
LNE07.BSGWA.0737.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $5.895 801842 paramètres RST 24-OCT-2024 09:41:05
LNE07.BSGWA.0737.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $5.970 801843 paramètres RST 24-OCT-2024 09:41:05
LNE07.BSGWA.0737.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $5.895 801844 paramètres RST 24-OCT-2024 09:41:05
LNE07.BSGWA.0737.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $5.745 801845 paramètres RST 24-OCT-2024 09:41:05
*INCLY Instr_Roll_Theo
LNE07.BSGWA.0737.E -.00017920847 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;ZQFD.0729.E                     ;    4.25890; 1731.19804; 2155.85930;2436.14577;436.15092;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQFD.0729.S                     ;    4.35890; 1731.24195; 2155.94914;2436.14577;436.15091;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0729.E                      ;    4.38890; 1731.25512; 2155.97610;2436.14577;436.15091;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0729.E                      ;    4.43590; 1731.27576; 2156.01832;2436.14577;436.15091;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQ.0730.E                       ;    4.50290; 1731.30518; 2156.07852;2436.14577;436.15091;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQ.0730.S                       ;    4.60290; 1731.34908; 2156.16837;2436.14577;436.15091;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0729.E                     ;    4.23640; 1731.18816; 2155.83908;2436.14577;436.15092;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0729.S                      ;    4.42590; 1731.27137; 2156.00934;2436.14577;436.15091;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0729.S                     ;    4.62640; 1731.35940; 2156.18948;2436.14577;436.15091;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0729.S                      ;    4.47290; 1731.29200; 2156.05157;2436.14577;436.15091;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.ZQNA.0729   1731.188160   2155.839080   2436.145770 0 0   28.939000 1
*FRAME RST_LNE07.ZQNA.0729 0 0 0 .00025464791 .00248854669 0 1
*FRAME RSTRI_LNE07.ZQNA.0729     -0.000446     -0.000030     -0.000217 -.03787030638 0 -.16715190888 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.ZQNA.0729 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.ZQFD.0729.E                                          -0.000001      0.022505      0.000001   $4.259 788180 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZQFD.0729.S                                          -0.000001      0.122505      0.000001   $4.359 788181 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZCV.0729.E                                           -0.000001      0.152505      0.000001   $4.389 788182 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZCH.0729.E                                           -0.000001      0.199505      0.000001   $4.436 788184 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZQ.0730.E                                            -0.000001      0.266505      0.000001   $4.503 788186 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZQ.0730.S                                            -0.000001      0.366505      0.000001   $4.603 788187 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZQNA.0729.E                                           0.000000      0.000000      0.000000   $4.236 791631 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZCV.0729.S                                           -0.000001      0.189505      0.000001   $4.426 788183 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZQNA.0729.S                                          -0.000003      0.390004      0.000002   $4.626 791632 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
BEAM_LNE07.ZCH.0729.S                                           -0.000001      0.236505      0.000001   $4.473 788185 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:05
*OBSXYZ
LNE07.ZQNA.0729.E     -0.000399      0.075013      0.215009 0.1 0.1 0.1   $4.311 791637 paramètres RST 24-OCT-2024 09:41:05
LNE07.ZQNA.0729.S     -0.001627      0.315046      0.214869 0.1 0.1 0.1   $4.551 791638 paramètres RST 24-OCT-2024 09:41:05
*INCLY Instr_Roll_Theo
LNE07.ZQNA.0729.E .2342878537 RF .23446706216
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;ZCV.0735.E                      ;    5.43233; 1731.71327; 2156.91357;2436.14577;436.15090;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0735.S                      ;    5.46933; 1731.72951; 2156.94681;2436.14577;436.15090;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0735.E                      ;    5.47933; 1731.73390; 2156.95579;2436.14577;436.15090;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0735.S                      ;    5.51633; 1731.75015; 2156.98904;2436.14577;436.15090;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0735.E                     ;    5.27983; 1731.64631; 2156.77655;2436.14577;436.15090;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0735.S                     ;    5.66983; 1731.81755; 2157.12695;2436.14577;436.15090;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.ZQNA.0735   1731.646310   2156.776550   2436.145770 0 0   28.939000 1
*FRAME RST_LNE07.ZQNA.0735 0 0 0 .00025464791 .00248854669 0 1
*FRAME RSTRI_LNE07.ZQNA.0735     -0.000706      0.000143      0.000017 -.00277497918 0 .06333482951 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.ZQNA.0735 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.ZCV.0735.E                                           -0.000002      0.152501     -0.000003   $5.432 788190 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:06
BEAM_LNE07.ZCV.0735.S                                           -0.000002      0.189501     -0.000003   $5.469 788191 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:06
BEAM_LNE07.ZCH.0735.E                                           -0.000002      0.199501     -0.000003   $5.479 788192 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:06
BEAM_LNE07.ZCH.0735.S                                           -0.000002      0.236501     -0.000003   $5.516 788193 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:06
BEAM_LNE07.ZQNA.0735.E                                           0.000000      0.000000      0.000000   $5.280 791633 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:06
BEAM_LNE07.ZQNA.0735.S                                          -0.000003      0.390004      0.000002   $5.670 791634 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:06
*OBSXYZ
LNE07.ZQNA.0735.E      0.001136      0.074971      0.215134 0.1 0.1 0.1   $5.355 791639 paramètres RST 24-OCT-2024 09:41:06
LNE07.ZQNA.0735.S      0.000872      0.314954      0.215257 0.1 0.1 0.1   $5.595 791640 paramètres RST 24-OCT-2024 09:41:06
*INCLY Instr_Roll_Theo
LNE07.ZQNA.0735.E -.17015668769 RF -.16997747922
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;ZCH.0742.E                      ;    6.84626; 1732.33409; 2158.18391;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMF.0742.E                     ;    6.66926; 1732.25638; 2158.02488;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMF.0742.S                     ;    6.76926; 1732.30028; 2158.11473;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0742.S                      ;    6.88326; 1732.35034; 2158.21715;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMD.0743.S                     ;    7.01326; 1732.40742; 2158.33395;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0742.E                     ;    6.64676; 1732.24650; 2158.00467;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0742.S                     ;    7.03676; 1732.41774; 2158.35506;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0742.E                      ;    6.79926; 1732.31346; 2158.14168;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0742.S                      ;    6.83626; 1732.32970; 2158.17492;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMD.0743.E                     ;    6.91326; 1732.36351; 2158.24410;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.ZQNA.0742   1732.246500   2158.004670   2436.145760 0 0   28.939000 1
*FRAME RST_LNE07.ZQNA.0742 0 0 0 .00025464791 .00248854669 0 1
*FRAME RSTRI_LNE07.ZQNA.0742     -0.000013     -0.001279     -0.000071 .03656527595 0 -.00032647571 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.ZQNA.0742 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.ZCH.0742.E                                           -0.000001      0.199497      0.000001   $6.846 788202 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZQMF.0742.E                                          -0.000001      0.022497      0.000001   $6.669 788198 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZQMF.0742.S                                          -0.000001      0.122497      0.000001   $6.769 788199 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZCH.0742.S                                           -0.000001      0.236497      0.000001   $6.883 788204 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZQMD.0743.S                                          -0.000001      0.366497      0.000001   $7.013 788206 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZQNA.0742.E                                           0.000000      0.000000      0.000000   $6.647 791635 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZQNA.0742.S                                           0.000002      0.389995      0.000002   $7.037 791636 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZCV.0742.E                                           -0.000001      0.152497      0.000001   $6.799 788200 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZCV.0742.S                                           -0.000001      0.189497      0.000001   $6.836 788201 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNE07.ZQMD.0743.E                                          -0.000001      0.266497      0.000001   $6.913 788205 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
*OBSXYZ
LNE07.ZQNA.0742.S      0.000253      0.314962      0.214946 0.1 0.1 0.1   $6.962 791642 paramètres RST 24-OCT-2024 09:41:07
LNE07.ZQNA.0742.E      0.000568      0.074983      0.214924 0.1 0.1 0.1   $6.722 791641 paramètres RST 24-OCT-2024 09:41:07
*INCLY Instr_Roll_Theo
LNE07.ZQNA.0742.E -.01399392119 RF -.01381464906
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEA.0520.E                    ;   22.25600; 1745.68484; 2149.48304;2436.14642;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEA.0520.S                    ;   22.25601; 1745.68484; 2149.48304;2436.14642;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEA.0520   1745.684844   2149.483042   2436.146423 0 0  269.405700 1
*FRAME RST_LNR.BPMEA.0520 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.BPMEA.0520     -0.000290      0.002380      0.000690 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEA.0520 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEA.0520.E                                            0.000000      0.000000      0.000000   $22.256 775618 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNR.BPMEA.0520.S                                            0.000000      0.000000      0.000000   $22.256 775619 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
*OBSXYZ
LNR.BPMEA.0520.S      0.026880      0.174000      0.082740 0.1 0.1 0.1   $22.430 778400 paramètres RST 24-OCT-2024 09:41:07
LNR.BPMEA.0520.E      0.071270     -0.174000      0.049900 0.1 0.1 0.1   $22.082 778394 paramètres RST 24-OCT-2024 09:41:07
LNR.BPMEA.0520.T      0.081750      0.174000     -0.029760 0.1 0.1 0.1   $22.430 778406 paramètres RST 24-OCT-2024 09:41:07
*INCLY Instr_Roll_Theo
LNR.BPMEA.0520.E -.00017927213 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MBHEK.0640.E                    ;   29.92022; 1738.99096; 2150.32064;2436.14615;436.15092;;; -.000025;369.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MBHEK.0640.S                    ;   30.89098; 1738.56242; 2151.14264;2436.14612;436.15092;;; -.000025;369.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MBHEK.0640   1738.990959   2150.320638   2436.146145 0 0  336.072300 1
*FRAME RST_LNR.MBHEK.0640 0 0 0 .00254647909 .0007661719 33.33333335891 1
*FRAME RSTRI_LNR.MBHEK.0640     -0.000154     -0.000101      0.000015 .00068675274 0 -.00226628403 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MBHEK.0640 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MBHEK.0640.E                                            0.000000      0.000000      0.000000   $29.920 775652 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
BEAM_LNR.MBHEK.0640.S                                            0.000000      0.927000      0.000001   $30.891 775653 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:07
*OBSXYZ
LNR.MBHEK.0640.E      0.335800      0.228500      0.585000 0.1 0.1 0.1   $30.149 775760 paramètres RST 24-OCT-2024 09:41:07
LNR.MBHEK.0640.S      0.335800      0.698500      0.585000 0.1 0.1 0.1   $30.619 775761 paramètres RST 24-OCT-2024 09:41:07
*INCLY Instr_Roll_Theo
LNR.MBHEK.0640.E -.00012980677 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0615.E                    ;   28.41470; 1740.26120; 2149.51249;2436.14621;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0615.S                    ;   28.41471; 1740.26120; 2149.51249;2436.14621;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0615   1740.261197   2149.512494   2436.146205 0 0  336.072300 1
*FRAME RST_LNR.MCCAY.0615 0 0 0 .00254647909 .0007661719 0 1
*FRAME RSTRI_LNR.MCCAY.0615      0.000072      0.000229     -0.000013 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0615 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0615.E                                            0.000000      0.000000      0.000000   $28.415 775638 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:08
BEAM_LNR.MCCAY.0615.S                                            0.000000      0.000000      0.000000   $28.415 775639 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:08
*OBSXYZ
LNR.MCCAY.0615.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $28.330 776157 paramètres RST 24-OCT-2024 09:41:08
LNR.MCCAY.0615.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $28.500 776167 paramètres RST 24-OCT-2024 09:41:08
*INCLY Instr_Roll_Theo
LNR.MCCAY.0615.E -.00018933072 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MKKFH.0120.S                    ;    4.29198; 1738.72614; 2154.94571;2436.14608;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MKKFH.0120.E                    ;    3.85998; 1738.70756; 2154.51411;2436.14609;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MKKFH.0120   1738.707561   2154.514113   2436.146087 0 0    2.739000 1
*FRAME RST_LNR.MKKFH.0120 0 0 0 .0005729578 .00257818275 0 1
*FRAME RSTRI_LNR.MKKFH.0120     -0.000124      0.004389      0.000378 -.00206312066 0 -.02107330431 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MKKFH.0120 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MKKFH.0120.S                                            0.000000      0.432000      0.000000   $4.292 775495 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:08
BEAM_LNR.MKKFH.0120.E                                            0.000000      0.000000      0.000000   $3.860 775494 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:08
*OBSXYZ
LNR.MKKFH.0120.E      0.000000     -0.108000      0.582810 0.1 0.1 0.1   $3.752 775790 paramètres RST 24-OCT-2024 09:41:08
LNR.MKKFH.0120.S      0.000000      0.282000      0.583090 0.1 0.1 0.1   $4.142 775791 paramètres RST 24-OCT-2024 09:41:08
*INCLY Instr_Roll_Theo
LNR.MKKFH.0120.E -.00005455831 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0605.E                    ;   26.32420; 1742.02499; 2148.39034;2436.14629;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0605.S                    ;   26.57420; 1741.81406; 2148.52454;2436.14628;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0605   1742.024991   2148.390344   2436.146289 0 0  336.072300 1
*FRAME RST_LNR.MQNLG.0605 0 0 0 .00254647909 .0007661719 0 1
*FRAME RSTRI_LNR.MQNLG.0605     -0.000131     -0.000020     -0.000321 -.10415136654 0 -.01171385614 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0605 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0605.E                                            0.000000      0.000000      0.000000   $26.324 775665 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:09
BEAM_LNR.MQNLG.0605.S                                            0.000000      0.249999      0.000000   $26.574 775668 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:09
*OBSXYZ
LNR.MQNLG.0605.E      0.213030      0.040000      0.344910 0.1 0.1 0.1   $26.364 775782 paramètres RST 24-OCT-2024 09:41:09
LNR.MQNLG.0605.S      0.213030      0.210000      0.344910 0.1 0.1 0.1   $26.534 775783 paramètres RST 24-OCT-2024 09:41:09
*INCLY Instr_Roll_Theo
LNR.MQNLG.0605.E .0698394172 RF .07002817496
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQSAB.0540.S                    ;   23.81500; 1744.30243; 2148.76233;2436.14638;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQSAB.0540.E                    ;   23.66500; 1744.43544; 2148.83168;2436.14638;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQSAB.0540   1744.435443   2148.831677   2436.146380 0 0  269.405700 1
*FRAME RST_LNR.MQSAB.0540 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.MQSAB.0540     -0.000097     -0.000194     -0.000223 .01909854602 0 -.02588914165 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQSAB.0540 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQSAB.0540.S                                            0.000000      0.150000      0.000001   $23.815 775629 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:09
BEAM_LNR.MQSAB.0540.E                                            0.000000      0.000000      0.000000   $23.665 775628 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:09
*OBSXYZ
LNR.MQSAB.0540.E      0.212830     -0.010000      0.345050 0.1 0.1 0.1   $23.655 775786 paramètres RST 24-OCT-2024 09:41:09
LNR.MQSAB.0540.S      0.212830      0.160000      0.345050 0.1 0.1 0.1   $23.825 775787 paramètres RST 24-OCT-2024 09:41:09
*INCLY Instr_Roll_Theo
LNR.MQSAB.0540.E -.00781660855 RF -.00763943727
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;VANNE.0435.E                    ;   19.14531; 1747.20074; 2151.81383;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;VANNE.0435.S                    ;   19.14532; 1747.20074; 2151.81382;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.VANNE.0435   1747.200739   2151.813831   2436.146456 0 0  202.739000 1
*FRAME RST_LNR.VANNE.0435 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.VANNE.0435     -0.029705     -0.024474      0.000286 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.VANNE.0435 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.VANNE.0435.E                                            0.000000      0.000000      0.000000   $19.145 778365 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNR.VANNE.0435.S                                            0.000000      0.000010      0.000000   $19.145 778366 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
*OBSXYZ
LNR.VANNE.0435.S      0.100000      0.000000      0.267000 0.1 0.1 0.1   $19.145 778368 paramètres RST 24-OCT-2024 09:41:10
*INCLY Instr_Roll_Theo
LNR.VANNE.0435.S -.00003036676 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;BSGWA.0008.S                    ;    3.06550; 1738.73489; 2149.76804;2436.14615;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;BSGWA.0008.E                    ;    2.76550; 1739.01705; 2149.66612;2436.14616;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.BSGWA.0008   1739.017049   2149.666123   2436.146161 0 0  322.066700 1
*FRAME RST_LNE00.BSGWA.0008 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.BSGWA.0008      0.000010     -0.001140      0.000830 .36711943689 0 .12732615648 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.BSGWA.0008 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.BSGWA.0008.S                                          0.000000      0.300000      0.000000   $3.066 774796 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.BSGWA.0008.E                                          0.000000      0.000000      0.000000   $2.766 774795 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
*OBSXYZ
LNE00.BSGWA.0008.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $2.765 778747 paramètres RST 24-OCT-2024 09:41:10
LNE00.BSGWA.0008.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $2.840 778748 paramètres RST 24-OCT-2024 09:41:10
LNE00.BSGWA.0008.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $2.991 778749 paramètres RST 24-OCT-2024 09:41:10
LNE00.BSGWA.0008.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $3.066 778750 paramètres RST 24-OCT-2024 09:41:10
LNE00.BSGWA.0008.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $2.991 778751 paramètres RST 24-OCT-2024 09:41:10
LNE00.BSGWA.0008.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $2.840 778752 paramètres RST 24-OCT-2024 09:41:10
*INCLY Instr_Roll_Theo
LNE00.BSGWA.0008.E -.00094493473 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;ZCV.0006.E                      ;    2.52800; 1739.24042; 2149.58544;2436.14617;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0006.S                      ;    2.56500; 1739.20562; 2149.59801;2436.14617;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0006.E                      ;    2.57500; 1739.19622; 2149.60141;2436.14617;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0006.S                      ;    2.61200; 1739.16142; 2149.61398;2436.14617;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQMD.0007.E                     ;    2.64200; 1739.13320; 2149.62417;2436.14617;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQMD.0007.S                     ;    2.74200; 1739.03915; 2149.65814;2436.14616;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQMF.0006.S                     ;    2.49800; 1739.26864; 2149.57525;2436.14617;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQMF.0006.E                     ;    2.39800; 1739.36269; 2149.54128;2436.14618;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0006.E                     ;    2.37550; 1739.38385; 2149.53363;2436.14618;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0006.S                     ;    2.76550; 1739.01705; 2149.66612;2436.14616;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.ZQNA.0006   1739.383854   2149.533631   2436.146176 0 0  322.066700 1
*FRAME RST_LNE00.ZQNA.0006 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.ZQNA.0006      0.000447      0.000712     -0.000205 .04423691038 0 -.09288123612 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.ZQNA.0006 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.ZCV.0006.E                                            0.000000      0.152500      0.000000   $2.528 774789 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZCV.0006.S                                            0.000000      0.189500      0.000000   $2.565 774790 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZCH.0006.E                                            0.000000      0.199500      0.000000   $2.575 774791 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZCH.0006.S                                            0.000000      0.236500      0.000000   $2.612 774792 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZQMD.0007.E                                           0.000000      0.266500      0.000000   $2.642 774793 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZQMD.0007.S                                           0.000000      0.366500      0.000000   $2.742 774794 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZQMF.0006.S                                           0.000000      0.122500      0.000000   $2.498 774788 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZQMF.0006.E                                           0.000000      0.022500      0.000000   $2.398 774787 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZQNA.0006.E                                           0.000000      0.000000      0.000000   $2.376 776384 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
BEAM_LNE00.ZQNA.0006.S                                           0.000000      0.390000      0.000001   $2.766 776385 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:10
*OBSXYZ
LNE00.ZQNA.0006.E     -0.000854      0.075220      0.215510 0.1 0.1 0.1   $2.451 776386 paramètres RST 24-OCT-2024 09:41:10
LNE00.ZQNA.0006.S      0.000902      0.314651      0.215844 0.1 0.1 0.1   $2.690 776387 paramètres RST 24-OCT-2024 09:41:10
*INCLY Instr_Roll_Theo
LNE00.ZQNA.0006.E -.04824578382 RF -.04730084909
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZDSV.0170.E                     ;   12.62262; 1722.68422; 2159.87044;2436.23492;436.24047;;;  .895321;367.2203;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZDSV.0170.S                     ;   13.29797; 1722.52289; 2160.22650;2436.72282;436.72837;;;  .895321;367.2203;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZDSV.0170   1722.684224   2159.870443   2436.234923 0 0  372.916500 1
*FRAME RST_LNE01.ZDSV.0170 0 0 0 -14.00353414684 100.00140363233 42.99444444068 1
*FRAME RSTRI_LNE01.ZDSV.0170      0.002450     -0.000493     -0.001701 -.14154482377 0 -.35284613929 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZDSV.0170 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZDSV.0170.E                                           0.000000      0.000000      0.000000   $12.623 787790 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZDSV.0170.S                                           0.000000      0.625175      0.000000   $13.298 787791 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
*OBSXYZ
LNE01.ZDSV.0170.E      0.173226      0.534565      0.181675 0.1 0.1 0.1   $13.157 793699 paramètres RST 24-OCT-2024 09:41:11
LNE01.ZDSV.0170.S     -0.089025      0.744880     -0.000803 0.1 0.1 0.1   $13.367 793700 paramètres RST 24-OCT-2024 09:41:11
LNE01.ZDSV.0170.T      0.173696      0.533686     -0.183137 0.1 0.1 0.1   $13.156 793701 paramètres RST 24-OCT-2024 09:41:11
*INCLY Instr_Roll_Theo
LNE01.ZDSV.0170.E 99.99856360786 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZQFF.0114.E                     ;    4.51617; 1727.36695; 2153.87421;2436.14569;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQFF.0114.S                     ;    4.61617; 1727.27290; 2153.90818;2436.14568;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0114.E                      ;    4.64617; 1727.24468; 2153.91837;2436.14568;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0114.S                      ;    4.68317; 1727.20988; 2153.93094;2436.14568;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0114.E                      ;    4.69317; 1727.20048; 2153.93434;2436.14568;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0114.S                      ;    4.73017; 1727.16568; 2153.94691;2436.14568;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0115.E                       ;    4.76017; 1727.13746; 2153.95710;2436.14568;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0115.S                       ;    4.86017; 1727.04341; 2153.99107;2436.14567;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0114.E                     ;    4.49367; 1727.38811; 2153.86657;2436.14569;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0114.S                     ;    4.88367; 1727.02131; 2153.99906;2436.14567;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.0114   1727.388111   2153.866566   2436.145687 0 0  322.066700 1
*FRAME RST_LNE01.ZQNA.0114 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE01.ZQNA.0114      0.000861     -0.000845     -0.000078 .00506031038 0 -.02122065687 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.0114 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZQFF.0114.E                                           0.000000      0.022500      0.000000   $4.516 787734 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZQFF.0114.S                                           0.000000      0.122500      0.000000   $4.616 787735 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZCV.0114.E                                            0.000000      0.152500      0.000000   $4.646 787736 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZCV.0114.S                                            0.000000      0.189500      0.000000   $4.683 787737 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZCH.0114.E                                            0.000000      0.199500      0.000000   $4.693 787738 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZCH.0114.S                                            0.000000      0.236500      0.000000   $4.730 787739 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZQ.0115.E                                             0.000000      0.266500      0.000000   $4.760 787740 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZQ.0115.S                                             0.000000      0.366500      0.000000   $4.860 787741 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZQNA.0114.E                                           0.000000      0.000000      0.000000   $4.494 792367 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
BEAM_LNE01.ZQNA.0114.S                                           0.000000      0.390000      0.000000   $4.884 792368 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:11
*OBSXYZ
LNE01.ZQNA.0114.E      0.000477      0.074987      0.215100 0.1 0.1 0.1   $4.569 792391 paramètres RST 24-OCT-2024 09:41:11
LNE01.ZQNA.0114.S     -0.000552      0.314967      0.215199 0.1 0.1 0.1   $4.809 792392 paramètres RST 24-OCT-2024 09:41:11
*INCLY Instr_Roll_Theo
LNE01.ZQNA.0114.E -.09147226636 RF -.09052733163
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE02     ;AEGIS.0228.E                    ;    7.67275; 1719.86281; 2158.57696;2436.14534;436.15100;;; -.000033;372.9223;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;AEGIS.0228.S                    ;   12.47275; 1717.88220; 2162.94928;2436.14518;436.15097;;; -.000033;372.9223;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE02.AEGIS.0228   1719.862810   2158.576960   2436.145338 0 0  372.922251 1
*FRAME RST_LNE02.AEGIS.0228 0 0 0 .00210084525 .00136994209 0 1
*FRAME RSTRI_LNE02.AEGIS.0228      0.000003     -0.000043     -0.000669 -.00899225467 0 -.00054377942 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE02.AEGIS.0228 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE02.AEGIS.0228.E                                          0.000000      0.000000      0.000000   $7.673 817831 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.AEGIS.0228.S                                          0.000000      4.800000      0.000000   $12.473 817832 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
*OBSXYZ
LNE02.AEGIS.0228.E      0.000000      0.000000      0.000000 0.1 0.1 0.1   $7.673 817835 paramètres RST 24-OCT-2024 09:41:12
LNE02.AEGIS.0228.S      0.000000      4.800000      0.000000 0.1 0.1 0.1   $12.473 817836 paramètres RST 24-OCT-2024 09:41:12
*INCLY Instr_Roll_Theo
LNE02.AEGIS.0228.E -.00092914656 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE02     ;ZCH.0205.E                      ;    1.76509; 1724.06429; 2155.06715;2436.14555;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCH.0205.S                      ;    1.80209; 1724.02949; 2155.07972;2436.14555;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQNA.0205.E                     ;    1.56559; 1724.25193; 2154.99937;2436.14556;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQNA.0205.S                     ;    1.95559; 1723.88512; 2155.13187;2436.14555;436.15100;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCV.0205.E                      ;    1.71809; 1724.10850; 2155.05118;2436.14555;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQFF.0205.E                     ;    1.58809; 1724.23077; 2155.00702;2436.14556;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQFF.0205.S                     ;    1.68809; 1724.13672; 2155.04099;2436.14556;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQ.0206.S                       ;    1.93209; 1723.90723; 2155.12388;2436.14554;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQ.0206.E                       ;    1.83209; 1724.00128; 2155.08991;2436.14555;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCV.0205.S                      ;    1.75509; 1724.07370; 2155.06375;2436.14555;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE02.ZQNA.0205   1724.251929   2154.999374   2436.145560 0 0  322.066696 1
*FRAME RST_LNE02.ZQNA.0205 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE02.ZQNA.0205     -0.000156      0.000872     -0.001009 .03183098594 0 .0504398795 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE02.ZQNA.0205 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE02.ZCH.0205.E                                            0.000000      0.199500     -0.000001   $1.765 801807 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZCH.0205.S                                            0.000000      0.236500     -0.000001   $1.802 801808 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZQNA.0205.E                                           0.000000      0.000000      0.000000   $1.566 801774 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZQNA.0205.S                                           0.000000      0.390000      0.000000   $1.956 801792 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZCV.0205.E                                            0.000000      0.152500      0.000000   $1.718 801781 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZQFF.0205.E                                           0.000000      0.022500      0.000000   $1.588 801777 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZQFF.0205.S                                           0.000000      0.122500      0.000000   $1.688 801791 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZQ.0206.S                                             0.000000      0.366500     -0.000001   $1.932 801810 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZQ.0206.E                                             0.000000      0.266500     -0.000001   $1.832 801809 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
BEAM_LNE02.ZCV.0205.S                                            0.000001      0.189500     -0.000001   $1.755 801790 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:12
*OBSXYZ
LNE02.ZQNA.0205.E     -0.000783      0.074987      0.214936 0.1 0.1 0.1   $1.641 801814 paramètres RST 24-OCT-2024 09:41:12
LNE02.ZQNA.0205.S     -0.001006      0.314936      0.215035 0.1 0.1 0.1   $1.881 801815 paramètres RST 24-OCT-2024 09:41:12
*INCLY Instr_Roll_Theo
LNE02.ZQNA.0205.E .23536838844 RF .2363132595
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE02     ;ZQMD.0208.E                     ;    3.73359; 1722.21287; 2155.73589;2436.14548;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQMD.0208.S                     ;    3.83359; 1722.11882; 2155.76987;2436.14547;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCV.0208.E                      ;    3.86359; 1722.09060; 2155.78006;2436.14547;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCV.0208.S                      ;    3.90059; 1722.05580; 2155.79263;2436.14547;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCH.0208.E                      ;    3.91059; 1722.04640; 2155.79603;2436.14547;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZCH.0208.S                      ;    3.94759; 1722.01160; 2155.80860;2436.14547;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQMF.0209.E                     ;    3.97759; 1721.98338; 2155.81879;2436.14547;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQMF.0209.S                     ;    4.07759; 1721.88933; 2155.85276;2436.14546;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQNA.0208.E                     ;    3.71109; 1722.23403; 2155.72825;2436.14548;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;ZQNA.0208.S                     ;    4.10109; 1721.86723; 2155.86074;2436.14546;436.15101;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE02.ZQNA.0208   1722.234030   2155.728250   2436.145480 0 0  322.066696 1
*FRAME RST_LNE02.ZQNA.0208 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE02.ZQNA.0208      0.000643     -0.000761     -0.000450 -.06219371499 0 -.04342135101 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE02.ZQNA.0208 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE02.ZQMD.0208.E                                           0.000001      0.022498     -0.000003   $3.734 787844 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZQMD.0208.S                                           0.000002      0.122498     -0.000003   $3.834 787845 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZCV.0208.E                                            0.000001      0.152498     -0.000003   $3.864 787846 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZCV.0208.S                                            0.000001      0.189499     -0.000004   $3.901 787847 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZCH.0208.E                                            0.000001      0.199498     -0.000003   $3.911 787848 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZCH.0208.S                                            0.000001      0.236498     -0.000004   $3.948 787849 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZQMF.0209.E                                           0.000001      0.266498     -0.000004   $3.978 787850 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZQMF.0209.S                                           0.000001      0.366499     -0.000004   $4.078 787851 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZQNA.0208.E                                           0.000000      0.000000      0.000000   $3.711 805989 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE02.ZQNA.0208.S                                           0.000000      0.389995     -0.000005   $4.101 805990 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
*OBSXYZ
LNE02.ZQNA.0208.E      0.000131      0.075017      0.215274 0.1 0.1 0.1   $3.786 806091 paramètres RST 24-OCT-2024 09:41:13
LNE02.ZQNA.0208.S     -0.000109      0.314972      0.215464 0.1 0.1 0.1   $4.026 806092 paramètres RST 24-OCT-2024 09:41:13
*INCLY Instr_Roll_Theo
LNE02.ZQNA.0208.E -.04124290266 RF -.04029803159
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;BSGWA.0611.E                    ;    1.42190; 1728.96770; 2153.85550;2436.14574;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;BSGWA.0611.S                    ;    1.72190; 1728.85505; 2154.13354;2436.14573;436.15096;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.BSGWA.0611   1728.967703   2153.855497   2436.145739 0 0  375.494600 1
*FRAME RST_LNE06.BSGWA.0611 0 0 0 .00203718327 .00145353026 0 1
*FRAME RSTRI_LNE06.BSGWA.0611      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.BSGWA.0611 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.BSGWA.0611.E                                          0.000000      0.000000      0.000000   $1.422 788084 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
BEAM_LNE06.BSGWA.0611.S                                          0.000000      0.300000      0.000000   $1.722 788085 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:13
*OBSXYZ
LNE06.BSGWA.0611.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $1.421 816584 paramètres RST 24-OCT-2024 09:41:13
LNE06.BSGWA.0611.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $1.497 816585 paramètres RST 24-OCT-2024 09:41:13
LNE06.BSGWA.0611.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $1.647 816586 paramètres RST 24-OCT-2024 09:41:13
LNE06.BSGWA.0611.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $1.722 816587 paramètres RST 24-OCT-2024 09:41:13
LNE06.BSGWA.0611.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $1.647 816588 paramètres RST 24-OCT-2024 09:41:13
LNE06.BSGWA.0611.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $1.497 816589 paramètres RST 24-OCT-2024 09:41:13
*INCLY Instr_Roll_Theo
LNE06.BSGWA.0611.E -.00084606768 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;BSGWA.0631.E                    ;    6.02110; 1727.24073; 2158.11815;2436.14559;436.15093;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;BSGWA.0631.S                    ;    6.32110; 1727.12808; 2158.39620;2436.14558;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.BSGWA.0631   1727.240727   2158.118148   2436.145592 0 0  375.494600 1
*FRAME RST_LNE06.BSGWA.0631 0 0 0 .00203718327 .00145353026 0 1
*FRAME RSTRI_LNE06.BSGWA.0631      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.BSGWA.0631 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.BSGWA.0631.E                                          0.000000      0.000000      0.000000   $6.021 788112 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.BSGWA.0631.S                                          0.000000      0.300000      0.000000   $6.321 788113 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
*OBSXYZ
LNE06.BSGWA.0631.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $6.021 816596 paramètres RST 24-OCT-2024 09:41:14
LNE06.BSGWA.0631.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $6.246 816598 paramètres RST 24-OCT-2024 09:41:14
LNE06.BSGWA.0631.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $6.322 816599 paramètres RST 24-OCT-2024 09:41:14
LNE06.BSGWA.0631.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $6.246 816600 paramètres RST 24-OCT-2024 09:41:14
LNE06.BSGWA.0631.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $6.096 816601 paramètres RST 24-OCT-2024 09:41:14
LNE06.BSGWA.0631.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $6.096 816597 paramètres RST 24-OCT-2024 09:41:14
*INCLY Instr_Roll_Theo
LNE06.BSGWA.0631.E -.00084606768 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZQ.0625.E                       ;    3.88190; 1728.04399; 2156.13549;2436.14566;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQ.0625.S                       ;    3.98190; 1728.00644; 2156.22817;2436.14566;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0625.E                      ;    4.01190; 1727.99517; 2156.25597;2436.14566;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0625.S                      ;    4.04890; 1727.98128; 2156.29027;2436.14566;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0625.E                      ;    4.05890; 1727.97752; 2156.29953;2436.14566;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0625.S                      ;    4.09590; 1727.96363; 2156.33383;2436.14565;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0625.E                     ;    3.85940; 1728.05243; 2156.11463;2436.14566;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQFD.0626.S                     ;    4.22590; 1727.91482; 2156.45431;2436.14565;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQFD.0626.E                     ;    4.12590; 1727.95236; 2156.36163;2436.14565;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0625.S                     ;    4.24940; 1727.90599; 2156.47610;2436.14565;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZQNA.0625   1728.052434   2156.114633   2436.145661 0 0  375.494600 1
*FRAME RST_LNE06.ZQNA.0625 0 0 0 .00203718327 .00145353026 0 1
*FRAME RSTRI_LNE06.ZQNA.0625     -0.000368     -0.001018     -0.001169 -.13107832926 0 .0548473055 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZQNA.0625 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZQ.0625.E                                             0.000000      0.022499      0.000000   $3.882 788104 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZQ.0625.S                                             0.000000      0.122499      0.000000   $3.982 788105 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZCV.0625.E                                            0.000000      0.152499      0.000000   $4.012 788106 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZCV.0625.S                                            0.000000      0.189499      0.000000   $4.049 788107 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZCH.0625.E                                            0.000000      0.199499      0.000000   $4.059 788108 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZCH.0625.S                                            0.000000      0.236499      0.000000   $4.096 788109 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZQNA.0625.E                                           0.000000      0.000000      0.000000   $3.859 792475 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZQFD.0626.S                                           0.000000      0.366499      0.000000   $4.226 788111 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZQFD.0626.E                                           0.000000      0.266499      0.000000   $4.126 788110 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
BEAM_LNE06.ZQNA.0625.S                                           0.000000      0.390000      0.000000   $4.249 792476 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:14
*OBSXYZ
LNE06.ZQNA.0625.E     -0.001004      0.074972      0.214845 0.1 0.1 0.1   $3.934 792483 paramètres RST 24-OCT-2024 09:41:14
LNE06.ZQNA.0625.S     -0.000432      0.314936      0.214954 0.1 0.1 0.1   $4.174 792484 paramètres RST 24-OCT-2024 09:41:14
*INCLY Instr_Roll_Theo
LNE06.ZQNA.0625.E -.00727592738 RF -.0064298597
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEA.0555.E                    ;   24.62920; 1743.58046; 2148.38594;2436.14635;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEA.0555.S                    ;   24.62921; 1743.58046; 2148.38594;2436.14635;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEA.0555   1743.580458   2148.385938   2436.146351 0 0  269.405700 1
*FRAME RST_LNR.BPMEA.0555 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.BPMEA.0555      0.000450      0.004420     -0.001530 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEA.0555 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEA.0555.E                                            0.000000      0.000000      0.000000   $24.629 775634 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:15
BEAM_LNR.BPMEA.0555.S                                            0.000000      0.000000      0.000000   $24.629 775635 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:15
*OBSXYZ
LNR.BPMEA.0555.S      0.026880      0.174000      0.082740 0.1 0.1 0.1   $24.803 778401 paramètres RST 24-OCT-2024 09:41:15
LNR.BPMEA.0555.E      0.071270     -0.174000      0.049900 0.1 0.1 0.1   $24.455 778395 paramètres RST 24-OCT-2024 09:41:15
LNR.BPMEA.0555.T      0.081750      0.174000     -0.029760 0.1 0.1 0.1   $24.803 778407 paramètres RST 24-OCT-2024 09:41:15
*INCLY Instr_Roll_Theo
LNR.BPMEA.0555.E -.00017933579 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEB.0110.E                    ;     .80598; 1738.57621; 2151.46294;2436.14612;436.15092;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEB.0110.S                    ;     .80599; 1738.57621; 2151.46294;2436.14612;436.15092;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEB.0110   1738.576206   2151.462939   2436.146116 0 0    2.739000 1
*FRAME RST_LNR.BPMEB.0110 0 0 0 .0005729578 .00257818275 0 1
*FRAME RSTRI_LNR.BPMEB.0110     -0.002970     -0.001990      0.001820 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEB.0110 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEB.0110.E                                            0.000000      0.000000      0.000000   $0.806 775486 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:15
BEAM_LNR.BPMEB.0110.S                                            0.000000      0.000000      0.000000   $0.806 775487 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:15
*OBSXYZ
LNR.BPMEB.0110.E      0.071270     -0.128000      0.049900 0.1 0.1 0.1   $0.678 778409 paramètres RST 24-OCT-2024 09:41:15
LNR.BPMEB.0110.S      0.026880      0.128000      0.082740 0.1 0.1 0.1   $0.934 778411 paramètres RST 24-OCT-2024 09:41:15
LNR.BPMEB.0110.T      0.081750      0.128000     -0.029760 0.1 0.1 0.1   $0.934 778413 paramètres RST 24-OCT-2024 09:41:15
*INCLY Instr_Roll_Theo
LNR.BPMEB.0110.E -.00005455831 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEB.0407.E                    ;   16.00880; 1747.33564; 2154.94744;2436.14643;436.15090;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEB.0407.S                    ;   16.00881; 1747.33564; 2154.94744;2436.14643;436.15090;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEB.0407   1747.335643   2154.947438   2436.146428 0 0  202.739000 1
*FRAME RST_LNR.BPMEB.0407 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.BPMEB.0407     -0.003300      0.003120      0.001370 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEB.0407 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEB.0407.E                                            0.000000      0.000000      0.000000   $16.009 775566 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:16
BEAM_LNR.BPMEB.0407.S                                            0.000000      0.000000      0.000000   $16.009 775567 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:16
*OBSXYZ
LNR.BPMEB.0407.E      0.071270     -0.128000      0.049900 0.1 0.1 0.1   $15.881 778357 paramètres RST 24-OCT-2024 09:41:16
LNR.BPMEB.0407.T      0.081750      0.128000     -0.029760 0.1 0.1 0.1   $16.137 778359 paramètres RST 24-OCT-2024 09:41:16
LNR.BPMEB.0407.S      0.026880      0.128000      0.082740 0.1 0.1 0.1   $16.137 778358 paramètres RST 24-OCT-2024 09:41:16
*INCLY Instr_Roll_Theo
LNR.BPMEB.0407.E -.00003068507 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MBHEK.0560.S                    ;   26.02458; 1742.27779; 2148.22951;2436.14630;436.15092;;; -.000035;302.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MBHEK.0560.E                    ;   25.05382; 1743.20393; 2148.18964;2436.14634;436.15092;;; -.000035;302.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MBHEK.0560   1743.203932   2148.189639   2436.146338 0 0  269.405700 1
*FRAME RST_LNR.MBHEK.0560 0 0 0 .00190985932 -.0018119472 33.33333335891 1
*FRAME RSTRI_LNR.MBHEK.0560     -0.000035      0.000291      0.000141 .01655074363 0 .01284227863 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MBHEK.0560 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MBHEK.0560.S                                           -0.000001      0.927000      0.000000   $26.025 775660 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:16
BEAM_LNR.MBHEK.0560.E                                            0.000000      0.000000      0.000000   $25.054 775659 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:16
*OBSXYZ
LNR.MBHEK.0560.S      0.335800      0.698500      0.585000 0.1 0.1 0.1   $25.752 775759 paramètres RST 24-OCT-2024 09:41:16
LNR.MBHEK.0560.E      0.335800      0.228500      0.585000 0.1 0.1 0.1   $25.282 775758 paramètres RST 24-OCT-2024 09:41:16
*INCLY Instr_Roll_Theo
LNR.MBHEK.0560.E -.0002181696 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0130.S                    ;    4.66401; 1738.74214; 2155.31739;2436.14608;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0130.E                    ;    4.66400; 1738.74214; 2155.31739;2436.14608;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0130   1738.742143   2155.317393   2436.146079 0 0    2.739000 1
*FRAME RST_LNR.MCCAY.0130 0 0 0 .0005729578 .00257818275 0 1
*FRAME RSTRI_LNR.MCCAY.0130     -0.000045     -0.000184      0.000595 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0130 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0130.S                                            0.000000      0.000000      0.000000   $4.664 775499 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:17
BEAM_LNR.MCCAY.0130.E                                            0.000000      0.000000      0.000000   $4.664 775498 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:17
*OBSXYZ
LNR.MCCAY.0130.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $4.749 776159 paramètres RST 24-OCT-2024 09:41:17
LNR.MCCAY.0130.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $4.579 776149 paramètres RST 24-OCT-2024 09:41:17
*INCLY Instr_Roll_Theo
LNR.MCCAY.0130.E -.00005296677 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0460.S                    ;   19.81456; 1747.17196; 2151.14521;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0460.E                    ;   19.81455; 1747.17196; 2151.14521;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0460   1747.171955   2151.145210   2436.146464 0 0  202.739000 1
*FRAME RST_LNR.MCCAY.0460 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.MCCAY.0460      0.000340      0.053460     -0.000665 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0460 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0460.S                                            0.000000      0.000000      0.000000   $19.815 775601 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:17
BEAM_LNR.MCCAY.0460.E                                            0.000000      0.000000      0.000000   $19.815 775600 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:17
*OBSXYZ
LNR.MCCAY.0460.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $19.730 776155 paramètres RST 24-OCT-2024 09:41:17
LNR.MCCAY.0460.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $19.900 776165 paramètres RST 24-OCT-2024 09:41:17
*INCLY Instr_Roll_Theo
LNR.MCCAY.0460.E -.00002979381 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0550.E                    ;   24.50420; 1743.69130; 2148.44372;2436.14636;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0550.S                    ;   24.75420; 1743.46962; 2148.32815;2436.14635;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0550   1743.691299   2148.443724   2436.146355 0 0  269.405700 1
*FRAME RST_LNR.MQNLG.0550 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.MQNLG.0550      0.000010     -0.000147      0.000683 .14260318057 0 .00662087304 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0550 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0550.E                                            0.000000      0.000000      0.000000   $24.504 775632 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:18
BEAM_LNR.MQNLG.0550.S                                            0.000000      0.250000     -0.000001   $24.754 775658 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:18
*OBSXYZ
LNR.MQNLG.0550.S      0.212920      0.210000      0.344950 0.1 0.1 0.1   $24.714 775769 paramètres RST 24-OCT-2024 09:41:18
LNR.MQNLG.0550.E      0.212920      0.040000      0.344950 0.1 0.1 0.1   $24.544 775768 paramètres RST 24-OCT-2024 09:41:18
*INCLY Instr_Roll_Theo
LNR.MQNLG.0550.E -.02436872263 RF -.02419155135
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MXNAD.0201.S                    ;    6.30500; 1739.56356; 2156.58148;2436.14610;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MXNAD.0201.E                    ;    6.15500; 1739.43055; 2156.51213;2436.14609;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MXNAD.0201   1739.430546   2156.512131   2436.146094 0 0   69.405700 1
*FRAME RST_LNR.MXNAD.0201 0 0 0 -.00190985932 .0018119472 0 1
*FRAME RSTRI_LNR.MXNAD.0201     -0.000106      0.000404      0.000334 .01103474659 0 .00636620003 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MXNAD.0201 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MXNAD.0201.S                                           -0.000001      0.150000      0.000000   $6.305 775507 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:18
BEAM_LNR.MXNAD.0201.E                                            0.000000      0.000000      0.000000   $6.155 775506 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:18
*OBSXYZ
LNR.MXNAD.0201.S      0.212785      0.160316      0.204985 0.1 0.1 0.1   $6.315 775837 paramètres RST 24-OCT-2024 09:41:18
LNR.MXNAD.0201.E      0.212871     -0.009725      0.205091 0.1 0.1 0.1   $6.145 775836 paramètres RST 24-OCT-2024 09:41:18
*INCLY Instr_Roll_Theo
LNR.MXNAD.0201.E .00009020902 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MXNAD.0220.E                    ;    7.36820; 1740.50633; 2157.07298;2436.14613;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MXNAD.0220.S                    ;    7.51820; 1740.63934; 2157.14232;2436.14614;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MXNAD.0220   1740.506326   2157.072980   2436.146130 0 0   69.405700 1
*FRAME RST_LNR.MXNAD.0220 0 0 0 -.00190985932 .0018119472 0 1
*FRAME RSTRI_LNR.MXNAD.0220     -0.000162      0.000101      0.000081 -.00254647997 0 -.01315681328 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MXNAD.0220 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MXNAD.0220.E                                            0.000000      0.000000      0.000000   $7.368 775518 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNR.MXNAD.0220.S                                           -0.000001      0.150000      0.000001   $7.518 775519 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
*OBSXYZ
LNR.MXNAD.0220.E      0.212870     -0.009951      0.205062 0.1 0.1 0.1   $7.358 775838 paramètres RST 24-OCT-2024 09:41:19
LNR.MXNAD.0220.S      0.212619      0.160059      0.205068 0.1 0.1 0.1   $7.528 775839 paramètres RST 24-OCT-2024 09:41:19
*INCLY Instr_Roll_Theo
LNR.MXNAD.0220.E .00009020902 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;VANNE.0425.E                    ;   16.73030; 1747.30461; 2154.22661;2436.14643;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;VANNE.0425.S                    ;   16.73031; 1747.30461; 2154.22660;2436.14643;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.VANNE.0425   1747.304611   2154.226606   2436.146434 0 0  202.739000 1
*FRAME RST_LNR.VANNE.0425 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.VANNE.0425     -0.030142      0.028306      0.003370 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.VANNE.0425 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.VANNE.0425.E                                            0.000000      0.000000      0.000000   $16.730 778363 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNR.VANNE.0425.S                                            0.000000      0.000010      0.000000   $16.730 778364 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
*OBSXYZ
LNR.VANNE.0425.E      0.100000      0.000000      0.267000 0.1 0.1 0.1   $16.730 778367 paramètres RST 24-OCT-2024 09:41:19
*INCLY Instr_Roll_Theo
LNR.VANNE.0425.E -.00003036676 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;ZQFF.0039.E                     ;    8.87510; 1733.27082; 2151.74170;2436.14593;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQFF.0039.S                     ;    8.97510; 1733.17676; 2151.77567;2436.14592;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0039.E                      ;    9.00510; 1733.14855; 2151.78586;2436.14592;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0039.S                      ;    9.04210; 1733.11375; 2151.79843;2436.14592;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0039.E                      ;    9.05210; 1733.10434; 2151.80183;2436.14592;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0039.S                      ;    9.08910; 1733.06954; 2151.81440;2436.14592;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQ.0040.E                       ;    9.11910; 1733.04133; 2151.82459;2436.14592;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQ.0040.S                       ;    9.21910; 1732.94727; 2151.85856;2436.14591;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0039.E                     ;    8.85260; 1733.29198; 2151.73405;2436.14593;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0039.S                     ;    9.24260; 1732.92517; 2151.86655;2436.14591;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.ZQNA.0039   1733.291977   2151.734054   2436.145928 0 0  322.066700 1
*FRAME RST_LNE00.ZQNA.0039 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.ZQNA.0039     -0.000521      0.002556     -0.000867 -.08700471835 0 .02660746404 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.ZQNA.0039 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.ZQFF.0039.E                                           0.000000      0.022500      0.000000   $8.875 774850 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZQFF.0039.S                                           0.000000      0.122500      0.000000   $8.975 774851 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZCV.0039.E                                            0.000000      0.152500      0.000000   $9.005 774852 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZCV.0039.S                                            0.000000      0.189500      0.000000   $9.042 774853 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZCH.0039.E                                            0.000000      0.199500      0.000000   $9.052 774831 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZCH.0039.S                                            0.000000      0.236500      0.000000   $9.089 774832 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZQ.0040.E                                             0.000000      0.266500      0.000000   $9.119 774833 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZQ.0040.S                                             0.000000      0.366500      0.000000   $9.219 774834 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZQNA.0039.E                                           0.000000      0.000000      0.000000   $8.853 776399 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
BEAM_LNE00.ZQNA.0039.S                                           0.000000      0.390000      0.000000   $9.243 776400 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:19
*OBSXYZ
LNE00.ZQNA.0039.E     -0.000017      0.074916      0.214762 0.1 0.1 0.1   $8.928 776412 paramètres RST 24-OCT-2024 09:41:19
LNE00.ZQNA.0039.S     -0.000535      0.314913      0.214831 0.1 0.1 0.1   $9.168 776413 paramètres RST 24-OCT-2024 09:41:19
*INCLY Instr_Roll_Theo
LNE00.ZQNA.0039.E .02451985617 RF .02546479089
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;BSGWA.0109.S                    ;    3.82417; 1728.01779; 2153.63912;2436.14571;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;BSGWA.0109.E                    ;    3.52417; 1728.29995; 2153.53720;2436.14572;436.15097;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.BSGWA.0109   1728.299950   2153.537204   2436.145724 0 0  322.066700 1
*FRAME RST_LNE01.BSGWA.0109 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE01.BSGWA.0109      0.131849      0.002491     -0.000699 .18865193536 0 -67.82387279982 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.BSGWA.0109 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.BSGWA.0109.S                                          0.000000      0.300000      0.000000   $3.824 787731 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:20
BEAM_LNE01.BSGWA.0109.E                                          0.000000      0.000000      0.000000   $3.524 787730 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:20
*OBSXYZ
LNE01.BSGWA.0109.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $3.524 801856 paramètres RST 24-OCT-2024 09:41:20
LNE01.BSGWA.0109.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $3.599 801857 paramètres RST 24-OCT-2024 09:41:20
LNE01.BSGWA.0109.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $3.749 801858 paramètres RST 24-OCT-2024 09:41:20
LNE01.BSGWA.0109.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $3.825 801859 paramètres RST 24-OCT-2024 09:41:20
LNE01.BSGWA.0109.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $3.749 801860 paramètres RST 24-OCT-2024 09:41:20
LNE01.BSGWA.0109.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $3.599 801861 paramètres RST 24-OCT-2024 09:41:20
*INCLY Instr_Roll_Theo
LNE01.BSGWA.0109.E -.00094493473 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;BSGWA.0120.E                    ;    5.49967; 1726.44194; 2154.20833;2436.14565;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;BSGWA.0120.S                    ;    5.79967; 1726.15978; 2154.31024;2436.14564;436.15098;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.BSGWA.0120   1726.441942   2154.208328   2436.145648 0 0  322.066700 1
*FRAME RST_LNE01.BSGWA.0120 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE01.BSGWA.0120      0.130566     -0.001067     -0.001933 -.71451459149 0 -68.41859681296 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.BSGWA.0120 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.BSGWA.0120.E                                          0.000000      0.000000      0.000000   $5.500 787742 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:21
BEAM_LNE01.BSGWA.0120.S                                          0.000000      0.300000      0.000000   $5.800 787743 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:21
*OBSXYZ
LNE01.BSGWA.0120.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $5.499 801862 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0120.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $5.574 801863 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0120.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $5.800 801865 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0120.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $5.725 801866 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0120.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $5.574 801867 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0120.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $5.725 801864 paramètres RST 24-OCT-2024 09:41:21
*INCLY Instr_Roll_Theo
LNE01.BSGWA.0120.E -.00094487107 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;BSGWA.0144.E                    ;    8.60987; 1724.33690; 2156.22290;2436.14554;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;BSGWA.0144.S                    ;    8.90987; 1724.21309; 2156.49616;2436.14553;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.BSGWA.0144   1724.336899   2156.222901   2436.145536 0 0  372.916800 1
*FRAME RST_LNE01.BSGWA.0144 0 0 0 .00210084525 .00136981476 0 1
*FRAME RSTRI_LNE01.BSGWA.0144      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.BSGWA.0144 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.BSGWA.0144.E                                          0.000000      0.000000      0.000000   $8.610 787807 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:21
BEAM_LNE01.BSGWA.0144.S                                          0.000000      0.300000      0.000000   $8.910 787808 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:21
*OBSXYZ
LNE01.BSGWA.0144.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $8.609 801880 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0144.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $8.685 801881 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0144.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $8.835 801882 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0144.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $8.910 801883 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0144.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $8.835 801884 paramètres RST 24-OCT-2024 09:41:21
LNE01.BSGWA.0144.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $8.685 801885 paramètres RST 24-OCT-2024 09:41:21
*INCLY Instr_Roll_Theo
LNE01.BSGWA.0144.E -.00089807951 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZDFV.0165.E                     ;   12.01322; 1722.93232; 2159.32289;2436.14542;436.15096;;;  .109967;372.9167;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZDFV.0165.S                     ;   12.41322; 1722.76856; 2159.68431;2436.18923;436.19478;;;  .109967;372.9167;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZDFV.0165   1722.932317   2159.322892   2436.145424 0 0  372.916800 1
*FRAME RST_LNE01.ZDFV.0165 0 0 0 .00210084525 100.00136982782 7.00281749604 1
*FRAME RSTRI_LNE01.ZDFV.0165      0.000459     -0.000163     -0.003431 -.17223467813 0 .08835025474 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZDFV.0165 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZDFV.0165.E                                           0.000000      0.000000      0.000000   $12.013 787788 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZDFV.0165.S                                           0.000000      0.399194      0.000000   $12.413 787789 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
*OBSXYZ
LNE01.ZDFV.0165.E      0.280972      0.052160      0.000000 0.1 0.1 0.1   $12.065 793717 paramètres RST 24-OCT-2024 09:41:22
LNE01.ZDFV.0165.S      0.241452      0.409984      0.000000 0.1 0.1 0.1   $12.423 793718 paramètres RST 24-OCT-2024 09:41:22
*INCLY Instr_Roll_Theo
LNE01.ZDFV.0165.E -.00090374543 RF -100.00000020404
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZQ.0159.E                       ;   11.41945; 1723.17737; 2158.78205;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQ.0159.S                       ;   11.51945; 1723.13610; 2158.87313;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0159.E                      ;   11.54945; 1723.12372; 2158.90046;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0159.S                      ;   11.58645; 1723.10845; 2158.93416;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0159.E                      ;   11.59645; 1723.10432; 2158.94327;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0159.S                      ;   11.63345; 1723.08905; 2158.97697;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQFF.0160.E                     ;   11.66345; 1723.07667; 2159.00430;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQFF.0160.S                     ;   11.76345; 1723.03540; 2159.09538;2436.14543;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0159.E                     ;   11.39695; 1723.18666; 2158.76155;2436.14544;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0159.S                     ;   11.78695; 1723.02570; 2159.11679;2436.14543;436.15096;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.0159   1723.186656   2158.761553   2436.145444 0 0  372.916800 1
*FRAME RST_LNE01.ZQNA.0159 0 0 0 .00210084525 .00136981476 0 1
*FRAME RSTRI_LNE01.ZQNA.0159      0.000711     -0.000793     -0.000983 -.07949591653 0 -.06986504474 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.0159 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZQ.0159.E                                             0.000000      0.022500      0.000000   $11.419 787778 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZQ.0159.S                                             0.000000      0.122500      0.000000   $11.519 787779 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZCV.0159.E                                            0.000000      0.152500      0.000000   $11.549 787780 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZCV.0159.S                                            0.000000      0.189500      0.000000   $11.586 787781 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZCH.0159.E                                            0.000000      0.199500      0.000000   $11.596 787782 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZCH.0159.S                                            0.000000      0.236500      0.000000   $11.633 787783 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZQFF.0160.E                                           0.000000      0.266500      0.000000   $11.663 787784 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZQFF.0160.S                                           0.000000      0.366500      0.000000   $11.763 787785 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZQNA.0159.E                                           0.000000      0.000000      0.000000   $11.397 792377 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
BEAM_LNE01.ZQNA.0159.S                                           0.000000      0.390000      0.000000   $11.787 792378 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:22
*OBSXYZ
LNE01.ZQNA.0159.E      0.000215      0.074892      0.215094 0.1 0.1 0.1   $11.472 792401 paramètres RST 24-OCT-2024 09:41:22
LNE01.ZQNA.0159.S      0.000524      0.314857      0.215312 0.1 0.1 0.1   $11.712 792402 paramètres RST 24-OCT-2024 09:41:22
*INCLY Instr_Roll_Theo
LNE01.ZQNA.0159.E -.1597983747 RF -.15890029518
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;BSGWA.0744.S                    ;    7.33676; 1732.54946; 2158.62460;2436.14576;436.15087;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;BSGWA.0744.E                    ;    7.03676; 1732.41774; 2158.35506;2436.14576;436.15088;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.BSGWA.0744   1732.417738   2158.355063   2436.145759 0 0   28.939000 1
*FRAME RST_LNE07.BSGWA.0744 0 0 0 .00025464791 .00248854669 0 1
*FRAME RSTRI_LNE07.BSGWA.0744      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.BSGWA.0744 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.BSGWA.0744.S                                          0.000000      0.300000      0.000000   $7.337 788208 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:23
BEAM_LNE07.BSGWA.0744.E                                          0.000000      0.000000      0.000000   $7.037 788207 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:23
*OBSXYZ
LNE07.BSGWA.0744.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $7.036 801846 paramètres RST 24-OCT-2024 09:41:23
LNE07.BSGWA.0744.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $7.112 801847 paramètres RST 24-OCT-2024 09:41:23
LNE07.BSGWA.0744.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $7.262 801848 paramètres RST 24-OCT-2024 09:41:23
LNE07.BSGWA.0744.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $7.337 801849 paramètres RST 24-OCT-2024 09:41:23
LNE07.BSGWA.0744.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $7.262 801850 paramètres RST 24-OCT-2024 09:41:23
LNE07.BSGWA.0744.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $7.112 801851 paramètres RST 24-OCT-2024 09:41:23
*INCLY Instr_Roll_Theo
LNE07.BSGWA.0744.E -.00017920847 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEA.0240.E                    ;    9.42640; 1742.33139; 2158.02446;2436.14619;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEA.0240.S                    ;    9.42641; 1742.33139; 2158.02446;2436.14619;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEA.0240   1742.331392   2158.024464   2436.146193 0 0   69.405700 1
*FRAME RST_LNR.BPMEA.0240 0 0 0 -.00190985932 .0018119472 0 1
*FRAME RSTRI_LNR.BPMEA.0240      0.001420      0.006480     -0.000970 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEA.0240 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEA.0240.E                                            0.000000      0.000000      0.000000   $9.426 775528 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:23
BEAM_LNR.BPMEA.0240.S                                            0.000000      0.000000      0.000000   $9.426 775529 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:23
*OBSXYZ
LNR.BPMEA.0240.S      0.026880      0.174000      0.082740 0.1 0.1 0.1   $9.600 778398 paramètres RST 24-OCT-2024 09:41:23
LNR.BPMEA.0240.E      0.071270     -0.174000      0.049900 0.1 0.1 0.1   $9.252 778392 paramètres RST 24-OCT-2024 09:41:23
LNR.BPMEA.0240.T      0.081750      0.174000     -0.029760 0.1 0.1 0.1   $9.600 778404 paramètres RST 24-OCT-2024 09:41:23
*INCLY Instr_Roll_Theo
LNR.BPMEA.0240.E .00008810818 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEB.0457.E                    ;   19.86680; 1747.16971; 2151.09301;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEB.0457.S                    ;   19.86681; 1747.16971; 2151.09301;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEB.0457   1747.169708   2151.093008   2436.146464 0 0  202.739000 1
*FRAME RST_LNR.BPMEB.0457 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.BPMEB.0457      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEB.0457 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEB.0457.E                                            0.000000      0.000000      0.000000   $19.867 775602 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:24
BEAM_LNR.BPMEB.0457.S                                            0.000000      0.000000      0.000000   $19.867 775603 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:24
*OBSXYZ
LNR.BPMEB.0457.S      0.026880      0.128000      0.082740 0.1 0.1 0.1   $19.995 778361 paramètres RST 24-OCT-2024 09:41:24
LNR.BPMEB.0457.T      0.081750      0.128000     -0.029760 0.1 0.1 0.1   $19.995 778362 paramètres RST 24-OCT-2024 09:41:24
LNR.BPMEB.0457.E      0.071270     -0.128000      0.049900 0.1 0.1 0.1   $19.739 778360 paramètres RST 24-OCT-2024 09:41:24
*INCLY Instr_Roll_Theo
LNR.BPMEB.0457.E -.00003068507 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MBHEK.0470.E                    ;   20.18742; 1747.15592; 2150.77268;2436.14647;436.15091;;; -.000011;236.0724;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MBHEK.0470.S                    ;   21.15818; 1746.65832; 2149.99055;2436.14646;436.15092;;; -.000011;236.0724;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MBHEK.0470   1747.155917   2150.772681   2436.146467 0 0  202.739000 1
*FRAME RST_LNR.MBHEK.0470 0 0 0 -.0005729578 -.00257818275 33.33333335891 1
*FRAME RSTRI_LNR.MBHEK.0470     -0.000243     -0.000238     -0.000561 -.01030129443 0 -.01318565708 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MBHEK.0470 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MBHEK.0470.E                                            0.000000      0.000000      0.000000   $20.187 775604 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:24
BEAM_LNR.MBHEK.0470.S                                            0.000000      0.927000      0.000001   $21.158 775605 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:24
*OBSXYZ
LNR.MBHEK.0470.E      0.335800      0.228500      0.585000 0.1 0.1 0.1   $20.416 775756 paramètres RST 24-OCT-2024 09:41:24
LNR.MBHEK.0470.S      0.335800      0.698500      0.585000 0.1 0.1 0.1   $20.886 775757 paramètres RST 24-OCT-2024 09:41:24
*INCLY Instr_Roll_Theo
LNR.MBHEK.0470.E -.00010020395 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0330.E                    ;   14.42730; 1746.67611; 2156.24550;2436.14639;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0330.S                    ;   14.42731; 1746.67611; 2156.24550;2436.14639;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0330   1746.676108   2156.245498   2436.146387 0 0  136.072300 1
*FRAME RST_LNR.MCCAY.0330 0 0 0 -.00254647909 -.0007661719 0 1
*FRAME RSTRI_LNR.MCCAY.0330      0.000181     -0.000237      0.000158 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0330 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0330.E                                            0.000000      0.000000      0.000000   $14.427 775556 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:25
BEAM_LNR.MCCAY.0330.S                                            0.000000      0.000000      0.000000   $14.427 775557 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:25
*OBSXYZ
LNR.MCCAY.0330.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $14.342 776151 paramètres RST 24-OCT-2024 09:41:25
LNR.MCCAY.0330.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $14.512 776161 paramètres RST 24-OCT-2024 09:41:25
*INCLY Instr_Roll_Theo
LNR.MCCAY.0330.E .00010134987 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0440.E                    ;   18.95162; 1747.20907; 2152.00734;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0440.S                    ;   18.95163; 1747.20907; 2152.00734;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0440   1747.209070   2152.007342   2436.146456 0 0  202.739000 1
*FRAME RST_LNR.MCCAY.0440 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.MCCAY.0440      0.000185      0.002442      0.000589 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0440 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0440.E                                            0.000000      0.000000      0.000000   $18.952 775596 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:25
BEAM_LNR.MCCAY.0440.S                                            0.000000      0.000000      0.000000   $18.952 775597 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:25
*OBSXYZ
LNR.MCCAY.0440.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $18.867 776154 paramètres RST 24-OCT-2024 09:41:25
LNR.MCCAY.0440.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $19.037 776164 paramètres RST 24-OCT-2024 09:41:25
*INCLY Instr_Roll_Theo
LNR.MCCAY.0440.E -.00002979381 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0305.S                    ;   11.37140; 1744.09779; 2157.88586;2436.14627;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0305.E                    ;   11.12140; 1743.88686; 2158.02006;2436.14626;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0305   1743.886859   2158.020058   2436.146255 0 0  136.072300 1
*FRAME RST_LNR.MQNLG.0305 0 0 0 -.00254647909 -.0007661719 0 1
*FRAME RSTRI_LNR.MQNLG.0305     -0.000238     -0.000327      0.000020 -.04736440183 0 .03565063348 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0305 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0305.S                                            0.000000      0.250001      0.000000   $11.371 775541 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:26
BEAM_LNR.MQNLG.0305.E                                            0.000000      0.000000      0.000000   $11.121 775538 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:26
*OBSXYZ
LNR.MQNLG.0305.E      0.212920      0.040000      0.344960 0.1 0.1 0.1   $11.161 775778 paramètres RST 24-OCT-2024 09:41:26
LNR.MQNLG.0305.S      0.212920      0.210000      0.344960 0.1 0.1 0.1   $11.331 775779 paramètres RST 24-OCT-2024 09:41:26
*INCLY Instr_Roll_Theo
LNR.MQNLG.0305.E .02938636869 RF .02928450953
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZQNA.17582.E                    ;   13.53393; 1722.52287; 2160.22652;2436.95877;436.96433;;; 1.570675;361.5240;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.17582.S                    ;   14.48538; 1722.52281; 2160.22662;2437.91022;437.91578;;; 1.570675;361.5240;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.17582   1722.522871   2160.226520   2436.958774 0 0  361.524000 1
*FRAME RST_LNE01.ZQNA.17582 0 0 0 -99.99227609635 11.39273449698 0 1
*FRAME RSTRI_LNE01.ZQNA.17582     -0.093230     -0.000305     -0.000906 0 0 -.02555980428 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.17582 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZQNA.17582.E                                          0.000000      0.000000      0.000000   $13.534 793421 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:26
BEAM_LNE01.ZQNA.17582.S                                          0.000001      0.951450      0.000000   $14.485 793422 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:26
*OBSXYZ
LNE01.ZQNA.17582.G      0.048450      0.635750      0.155760 0.1 0.1 0.1   $14.170 793429 paramètres RST 24-OCT-2024 09:41:26
LNE01.ZQNA.17582.A     -0.214870      0.074000      0.000540 0.1 0.1 0.1   $13.608 793423 paramètres RST 24-OCT-2024 09:41:26
LNE01.ZQNA.17582.B     -0.214990      0.314070     -0.000830 0.1 0.1 0.1   $13.848 793424 paramètres RST 24-OCT-2024 09:41:26
LNE01.ZQNA.17582.C     -0.214930      0.635910      0.000060 0.1 0.1 0.1   $14.170 793425 paramètres RST 24-OCT-2024 09:41:26
LNE01.ZQNA.17582.D     -0.214870      0.875880     -0.000440 0.1 0.1 0.1   $14.410 793426 paramètres RST 24-OCT-2024 09:41:26
LNE01.ZQNA.17582.E      0.048470      0.314410      0.156120 0.1 0.1 0.1   $13.848 793427 paramètres RST 24-OCT-2024 09:41:26
LNE01.ZQNA.17582.F      0.048480      0.314240     -0.156050 0.1 0.1 0.1   $13.848 793428 paramètres RST 24-OCT-2024 09:41:26
LNE01.ZQNA.17582.H      0.048550      0.635800     -0.156310 0.1 0.1 0.1   $14.170 793430 paramètres RST 24-OCT-2024 09:41:26
*INCLY Instr_Roll_Theo
LNE01.ZQNA.17582.E -9.58228329367 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;BSGWA.0650.E                    ;    8.51852; 1727.11750; 2160.45785;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;BSGWA.0650.S                    ;    8.81852; 1727.24916; 2160.72742;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.BSGWA.0650   1727.117502   2160.457849   2436.145539 0 0   28.922400 1
*FRAME RST_LNE06.BSGWA.0650 0 0 0 .00025464791 .00248848303 0 1
*FRAME RSTRI_LNE06.BSGWA.0650      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.BSGWA.0650 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.BSGWA.0650.E                                          0.000000      0.000000      0.000000   $8.519 788136 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:27
BEAM_LNE06.BSGWA.0650.S                                          0.000000      0.300000      0.000000   $8.819 788137 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:27
*OBSXYZ
LNE06.BSGWA.0650.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $8.518 816602 paramètres RST 24-OCT-2024 09:41:27
LNE06.BSGWA.0650.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $8.819 816605 paramètres RST 24-OCT-2024 09:41:27
LNE06.BSGWA.0650.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $8.744 816606 paramètres RST 24-OCT-2024 09:41:27
LNE06.BSGWA.0650.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $8.593 816607 paramètres RST 24-OCT-2024 09:41:27
LNE06.BSGWA.0650.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $8.593 816603 paramètres RST 24-OCT-2024 09:41:27
LNE06.BSGWA.0650.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $8.744 816604 paramètres RST 24-OCT-2024 09:41:27
*INCLY Instr_Roll_Theo
LNE06.BSGWA.0650.E -.0002362496 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;ZQMF.0723.E                     ;    3.10890; 1730.69310; 2154.82608;2436.14578;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMF.0723.S                     ;    3.20890; 1730.73701; 2154.91593;2436.14578;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0723.E                      ;    3.23890; 1730.75018; 2154.94288;2436.14578;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0723.S                      ;    3.27590; 1730.76643; 2154.97612;2436.14578;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0723.S                      ;    3.32290; 1730.78706; 2155.01835;2436.14578;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMD.0724.S                     ;    3.45290; 1730.84414; 2155.13515;2436.14577;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0723.E                     ;    3.08640; 1730.68322; 2154.80587;2436.14578;436.15094;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0723.S                     ;    3.47640; 1730.85446; 2155.15626;2436.14577;436.15092;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0723.E                      ;    3.28590; 1730.77082; 2154.98511;2436.14578;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMD.0724.E                     ;    3.35290; 1730.80024; 2155.04530;2436.14578;436.15093;;; -.000004; 28.9390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.ZQNA.0723   1730.683220   2154.805870   2436.145780 0 0   28.939000 1
*FRAME RST_LNE07.ZQNA.0723 0 0 0 .00025464791 .00248854669 0 1
*FRAME RSTRI_LNE07.ZQNA.0723     -0.000333      0.000483     -0.000148 .05223611147 0 -.02987253405 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.ZQNA.0723 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.ZQMF.0723.E                                           0.000002      0.022499     -0.000004   $3.109 788172 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZQMF.0723.S                                           0.000002      0.122499     -0.000004   $3.209 788173 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZCV.0723.E                                            0.000002      0.152499     -0.000004   $3.239 788174 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZCV.0723.S                                            0.000002      0.189499     -0.000004   $3.276 788175 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZCH.0723.S                                            0.000002      0.236499     -0.000004   $3.323 788177 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZQMD.0724.S                                           0.000002      0.366499     -0.000004   $3.453 788179 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZQNA.0723.E                                           0.000000      0.000000      0.000000   $3.086 791627 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZQNA.0723.S                                           0.000002      0.389995     -0.000008   $3.476 791628 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZCH.0723.E                                            0.000002      0.199499     -0.000004   $3.286 788176 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNE07.ZQMD.0724.E                                           0.000002      0.266499     -0.000004   $3.353 788178 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
*OBSXYZ
LNE07.ZQNA.0723.E      0.000452      0.074845      0.214776 0.1 0.1 0.1   $3.161 791629 paramètres RST 24-OCT-2024 09:41:28
LNE07.ZQNA.0723.S     -0.000118      0.314846      0.214927 0.1 0.1 0.1   $3.401 791630 paramètres RST 24-OCT-2024 09:41:28
*INCLY Instr_Roll_Theo
LNE07.ZQNA.0723.E .1126298152 RF .11280902366
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEA.0625.E                    ;   28.82240; 1739.91721; 2149.73134;2436.14619;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEA.0625.S                    ;   28.82241; 1739.91721; 2149.73134;2436.14619;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEA.0625   1739.917213   2149.731342   2436.146189 0 0  336.072300 1
*FRAME RST_LNR.BPMEA.0625 0 0 0 .00254647909 .0007661719 0 1
*FRAME RSTRI_LNR.BPMEA.0625     -0.000960      0.004150      0.000530 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEA.0625 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEA.0625.E                                            0.000000      0.000000      0.000000   $28.822 775642 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
BEAM_LNR.BPMEA.0625.S                                            0.000000      0.000000      0.000000   $28.822 775643 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:28
*OBSXYZ
LNR.BPMEA.0625.S      0.026880      0.174000      0.082740 0.1 0.1 0.1   $28.996 778402 paramètres RST 24-OCT-2024 09:41:28
LNR.BPMEA.0625.E      0.071270     -0.174000      0.049900 0.1 0.1 0.1   $28.648 778396 paramètres RST 24-OCT-2024 09:41:28
LNR.BPMEA.0625.T      0.081750      0.174000     -0.029760 0.1 0.1 0.1   $28.996 778408 paramètres RST 24-OCT-2024 09:41:28
*INCLY Instr_Roll_Theo
LNR.BPMEA.0625.E -.00019098593 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BSSHV.0545.E                    ;   23.86295; 1744.25992; 2148.74017;2436.14637;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BSSHV.0545.S                    ;   24.41295; 1743.77221; 2148.48591;2436.14636;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BSSHV.0545   1744.259915   2148.740167   2436.146374 0 0  269.405700 1
*FRAME RST_LNR.BSSHV.0545 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.BSSHV.0545     -0.000228      0.000076      0.000035 -.00266222427 0 .03206244146 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BSSHV.0545 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BSSHV.0545.E                                            0.000000      0.000000      0.000000   $23.863 775630 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:29
BEAM_LNR.BSSHV.0545.S                                            0.000000      0.550001      0.000001   $24.413 775631 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:29
*OBSXYZ
LNR.BSSHV.0545.E      0.212300      0.190000      0.377230 0.1 0.1 0.1   $24.053 775738 paramètres RST 24-OCT-2024 09:41:29
LNR.BSSHV.0545.S      0.212300      0.360000      0.378250 0.1 0.1 0.1   $24.223 775739 paramètres RST 24-OCT-2024 09:41:29
*INCLY Instr_Roll_Theo
LNR.BSSHV.0545.E -.00017717128 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;LNTML.0430.S                    ;   17.93781; 1747.25268; 2153.02022;2436.14645;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCHAE.0425.E                    ;   17.06800; 1747.29009; 2153.88922;2436.14644;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCHAE.0425.S                    ;   17.06801; 1747.29009; 2153.88922;2436.14644;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCHAE.0435.E                    ;   18.80760; 1747.21527; 2152.15124;2436.14645;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCHAE.0435.S                    ;   18.80760; 1747.21527; 2152.15124;2436.14645;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;LNTML.0430.E                    ;   17.93780; 1747.25268; 2153.02022;2436.14645;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.LNTML.0430   1747.252675   2153.020223   2436.146446 0 0  202.739000 1
*FRAME RST_LNR.LNTML.0430 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.LNTML.0430      0.002451      0.001146     -0.000479 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.LNTML.0430 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.LNTML.0430.S                                            0.000000      0.000000      0.000000   $17.938 775585 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:29
BEAM_LNR.MCHAE.0425.E                                            0.000000     -0.869800      0.000000   $17.068 775572 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:29
BEAM_LNR.MCHAE.0425.S                                            0.000000     -0.869800      0.000000   $17.068 775573 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:29
BEAM_LNR.MCHAE.0435.E                                            0.000000      0.869790      0.000000   $18.808 775594 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:29
BEAM_LNR.MCHAE.0435.S                                            0.000000      0.869790      0.000000   $18.808 775595 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:29
BEAM_LNR.LNTML.0430.E                                            0.000000      0.000000      0.000000   $17.938 775584 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:29
*OBSXYZ
LNR.LNTML.0430.E      0.000000     -0.400000      0.361000 0.1 0.1 0.1   $17.538 778320 paramètres RST 24-OCT-2024 09:41:29
LNR.LNTML.0430.S      0.000000      0.400000      0.361000 0.1 0.1 0.1   $18.338 778321 paramètres RST 24-OCT-2024 09:41:29
*INCLY Instr_Roll_Theo
LNR.LNTML.0430.E -.00003138535 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0230.E                    ;    9.03890; 1741.98778; 2157.84533;2436.14618;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0230.S                    ;    9.03891; 1741.98778; 2157.84533;2436.14618;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0230   1741.987784   2157.845327   2436.146181 0 0   69.405700 1
*FRAME RST_LNR.MCCAY.0230 0 0 0 -.00190985932 .0018119472 0 1
*FRAME RSTRI_LNR.MCCAY.0230     -0.000027      0.000375      0.000313 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0230 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0230.E                                            0.000000      0.000000      0.000000   $9.039 775524 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:30
BEAM_LNR.MCCAY.0230.S                                            0.000000      0.000000      0.000000   $9.039 775525 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:30
*OBSXYZ
LNR.MCCAY.0230.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $8.954 776150 paramètres RST 24-OCT-2024 09:41:30
LNR.MCCAY.0230.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $9.124 776160 paramètres RST 24-OCT-2024 09:41:30
*INCLY Instr_Roll_Theo
LNR.MCCAY.0230.E .00008969973 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MLNAF.0450.S                    ;   19.64280; 1747.17934; 2151.31680;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MLNAF.0450.E                    ;   19.28280; 1747.19483; 2151.67647;2436.14646;436.15091;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MLNAF.0450   1747.194826   2151.676468   2436.146459 0 0  202.739000 1
*FRAME RST_LNR.MLNAF.0450 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.MLNAF.0450      0.000260      0.000048     -0.000327 -.01114084155 0 .00353677513 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MLNAF.0450 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MLNAF.0450.S                                            0.000000      0.360000      0.000000   $19.643 775599 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:30
BEAM_LNR.MLNAF.0450.E                                            0.000000      0.000000      0.000000   $19.283 775598 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:30
*OBSXYZ
LNR.MLNAF.0450.S      0.214140      0.265000      0.264890 0.1 0.1 0.1   $19.548 775889 paramètres RST 24-OCT-2024 09:41:30
LNR.MLNAF.0450.E      0.214140      0.095000      0.264890 0.1 0.1 0.1   $19.378 775888 paramètres RST 24-OCT-2024 09:41:30
*INCLY Instr_Roll_Theo
LNR.MLNAF.0450.E -.00002922085 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0210.E                    ;    6.92820; 1740.11617; 2156.86957;2436.14612;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0210.S                    ;    7.17820; 1740.33785; 2156.98515;2436.14613;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0210   1740.116165   2156.869573   2436.146117 0 0   69.405700 1
*FRAME RST_LNR.MQNLG.0210 0 0 0 -.00190985932 .0018119472 0 1
*FRAME RSTRI_LNR.MQNLG.0210     -0.000324      0.000783      0.000264 .04099838296 0 .05245757151 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0210 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0210.E                                            0.000000      0.000000      0.000000   $6.928 775512 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:31
BEAM_LNR.MQNLG.0210.S                                            0.000000      0.250000      0.000001   $7.178 775517 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:31
*OBSXYZ
LNR.MQNLG.0210.E      0.212850      0.040000      0.345100 0.1 0.1 0.1   $6.968 775780 paramètres RST 24-OCT-2024 09:41:31
LNR.MQNLG.0210.S      0.212850      0.210000      0.345100 0.1 0.1 0.1   $7.138 775781 paramètres RST 24-OCT-2024 09:41:31
*INCLY Instr_Roll_Theo
LNR.MQNLG.0210.E -.03938021687 RF -.03947042589
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0325.E                    ;   13.96780; 1746.28842; 2156.49215;2436.14637;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0325.S                    ;   14.21780; 1746.49935; 2156.35796;2436.14638;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0325   1746.288420   2156.492151   2436.146369 0 0  136.072300 1
*FRAME RST_LNR.MQNLG.0325 0 0 0 -.00254647909 -.0007661719 0 1
*FRAME RSTRI_LNR.MQNLG.0325     -0.000079      0.000502     -0.000006 -.02775670966 0 .00458367712 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0325 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0325.E                                            0.000000      0.000000      0.000000   $13.968 775552 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:31
BEAM_LNR.MQNLG.0325.S                                            0.000000      0.249999     -0.000001   $14.218 775555 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:31
*OBSXYZ
LNR.MQNLG.0325.E      0.213060      0.040000      0.345010 0.1 0.1 0.1   $14.008 775784 paramètres RST 24-OCT-2024 09:41:31
LNR.MQNLG.0325.S      0.213060      0.210000      0.345010 0.1 0.1 0.1   $14.178 775785 paramètres RST 24-OCT-2024 09:41:31
*INCLY Instr_Roll_Theo
LNR.MQNLG.0325.E .06440045617 RF .06429859701
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQSAB.0635.E                    ;   29.54020; 1739.31159; 2150.11665;2436.14616;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQSAB.0635.S                    ;   29.69020; 1739.18503; 2150.19716;2436.14616;436.15092;;; -.000040;336.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQSAB.0635   1739.311592   2150.116646   2436.146161 0 0  336.072300 1
*FRAME RST_LNR.MQSAB.0635 0 0 0 .00254647909 .0007661719 0 1
*FRAME RSTRI_LNR.MQSAB.0635      0.000172      0.000422     -0.000054 .01612767393 0 -.07978956732 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQSAB.0635 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQSAB.0635.E                                            0.000000      0.000000      0.000000   $29.540 775650 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:32
BEAM_LNR.MQSAB.0635.S                                            0.000000      0.150000      0.000000   $29.690 775651 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:32
*OBSXYZ
LNR.MQSAB.0635.S      0.212930      0.160000      0.344940 0.1 0.1 0.1   $29.700 775789 paramètres RST 24-OCT-2024 09:41:32
LNR.MQSAB.0635.E      0.212930     -0.010000      0.344940 0.1 0.1 0.1   $29.530 775788 paramètres RST 24-OCT-2024 09:41:32
*INCLY Instr_Roll_Theo
LNR.MQSAB.0635.E -.00082537753 RF -.00063661977
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;BSGWA.0015.E                    ;    4.01550; 1737.84139; 2150.09078;2436.14611;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;BSGWA.0015.S                    ;    4.31550; 1737.55923; 2150.19269;2436.14610;436.15093;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.BSGWA.0015   1737.841392   2150.090778   2436.146114 0 0  322.066700 1
*FRAME RST_LNE00.BSGWA.0015 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.BSGWA.0015      0.010840     -0.017510      0.000760 3.38629173871 0 .23375839003 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.BSGWA.0015 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.BSGWA.0015.E                                          0.000000      0.000000      0.000000   $4.016 774805 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:32
BEAM_LNE00.BSGWA.0015.S                                          0.000000      0.300000      0.000000   $4.316 774806 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:32
*OBSXYZ
LNE00.BSGWA.0015.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $4.015 778753 paramètres RST 24-OCT-2024 09:41:32
LNE00.BSGWA.0015.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $4.090 778754 paramètres RST 24-OCT-2024 09:41:32
LNE00.BSGWA.0015.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $4.241 778755 paramètres RST 24-OCT-2024 09:41:32
LNE00.BSGWA.0015.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $4.316 778756 paramètres RST 24-OCT-2024 09:41:32
LNE00.BSGWA.0015.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $4.090 778758 paramètres RST 24-OCT-2024 09:41:32
LNE00.BSGWA.0015.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $4.241 778757 paramètres RST 24-OCT-2024 09:41:32
*INCLY Instr_Roll_Theo
LNE00.BSGWA.0015.E -.00094487107 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE02     ;BSGWA.0207.E                    ;    1.95559; 1723.88512; 2155.13187;2436.14554;436.15099;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE02     ;BSGWA.0207.S                    ;    2.25559; 1723.60297; 2155.23378;2436.14553;436.15100;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE02.BSGWA.0207   1723.885124   2155.131866   2436.145544 0 0  322.066696 1
*FRAME RST_LNE02.BSGWA.0207 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE02.BSGWA.0207      0.130728      0.000650     -0.000688 -.15194006311 0 -67.6937647158 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE02.BSGWA.0207 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE02.BSGWA.0207.E                                          0.000000      0.000000      0.000000   $1.956 801811 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:33
BEAM_LNE02.BSGWA.0207.S                                          0.000000      0.300000      0.000000   $2.256 801812 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:33
*OBSXYZ
LNE02.BSGWA.0207.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $1.955 808954 paramètres RST 24-OCT-2024 09:41:33
LNE02.BSGWA.0207.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $2.030 808955 paramètres RST 24-OCT-2024 09:41:33
LNE02.BSGWA.0207.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $2.181 808956 paramètres RST 24-OCT-2024 09:41:33
LNE02.BSGWA.0207.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $2.256 808957 paramètres RST 24-OCT-2024 09:41:33
LNE02.BSGWA.0207.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $2.030 808959 paramètres RST 24-OCT-2024 09:41:33
LNE02.BSGWA.0207.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $2.181 808958 paramètres RST 24-OCT-2024 09:41:33
*INCLY Instr_Roll_Theo
LNE02.BSGWA.0207.E -.00094487107 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;BSGWA.0624.E                    ;    3.55940; 1728.16508; 2155.83658;2436.14567;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;BSGWA.0624.S                    ;    3.85940; 1728.05243; 2156.11463;2436.14566;436.15094;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.BSGWA.0624   1728.165083   2155.836585   2436.145671 0 0  375.494600 1
*FRAME RST_LNE06.BSGWA.0624 0 0 0 .00203718327 .00145353026 0 1
*FRAME RSTRI_LNE06.BSGWA.0624      0.000000      0.000000      0.000000 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.BSGWA.0624 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.BSGWA.0624.E                                          0.000000      0.000000      0.000000   $3.559 788102 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:33
BEAM_LNE06.BSGWA.0624.S                                          0.000000      0.300000      0.000000   $3.859 788103 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:33
*OBSXYZ
LNE06.BSGWA.0624.F      0.130340      0.074750      0.374400 0.1 0.1 0.1   $3.634 816595 paramètres RST 24-OCT-2024 09:41:33
LNE06.BSGWA.0624.A      0.000000     -0.000500      0.374400 0.1 0.1 0.1   $3.559 816590 paramètres RST 24-OCT-2024 09:41:33
LNE06.BSGWA.0624.B     -0.130340      0.074750      0.374400 0.1 0.1 0.1   $3.634 816591 paramètres RST 24-OCT-2024 09:41:33
LNE06.BSGWA.0624.C     -0.130340      0.225250      0.374400 0.1 0.1 0.1   $3.785 816592 paramètres RST 24-OCT-2024 09:41:33
LNE06.BSGWA.0624.D      0.000000      0.300500      0.374400 0.1 0.1 0.1   $3.860 816593 paramètres RST 24-OCT-2024 09:41:33
LNE06.BSGWA.0624.E      0.130340      0.225250      0.374400 0.1 0.1 0.1   $3.785 816594 paramètres RST 24-OCT-2024 09:41:33
*INCLY Instr_Roll_Theo
LNE06.BSGWA.0624.E -.00084606768 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZQMD.0618.S                     ;    2.96290; 1728.38907; 2155.28373;2436.14569;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0618.S                      ;    3.02990; 1728.36391; 2155.34583;2436.14569;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0618.E                      ;    3.03990; 1728.36015; 2155.35510;2436.14569;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0618.S                      ;    3.07690; 1728.34626; 2155.38939;2436.14569;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMF.0619.E                     ;    3.10690; 1728.33499; 2155.41720;2436.14569;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMF.0619.S                     ;    3.20690; 1728.29744; 2155.50988;2436.14568;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0618.E                     ;    2.84040; 1728.43506; 2155.17020;2436.14569;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMD.0618.E                     ;    2.86290; 1728.42661; 2155.19105;2436.14569;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0618.S                     ;    3.23040; 1728.28862; 2155.53166;2436.14568;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0618.E                      ;    2.99290; 1728.37780; 2155.31154;2436.14569;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZQNA.0618   1728.435063   2155.170199   2436.145694 0 0  375.494600 1
*FRAME RST_LNE06.ZQNA.0618 0 0 0 .00203718327 .00145353026 0 1
*FRAME RSTRI_LNE06.ZQNA.0618     -0.000964     -0.000118     -0.000087 .03068837859 0 .05092965895 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZQNA.0618 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZQMD.0618.S                                           0.000000      0.122499      0.000000   $2.963 788095 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZCV.0618.S                                            0.000000      0.189499      0.000000   $3.030 788097 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZCH.0618.E                                            0.000000      0.199499      0.000000   $3.040 788098 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZCH.0618.S                                            0.000000      0.236499      0.000000   $3.077 788099 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZQMF.0619.E                                           0.000000      0.266499      0.000000   $3.107 788100 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZQMF.0619.S                                           0.000000      0.366499      0.000000   $3.207 788101 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZQNA.0618.E                                           0.000000      0.000000      0.000000   $2.840 792473 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZQMD.0618.E                                           0.000000      0.022499      0.000000   $2.863 788094 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZQNA.0618.S                                           0.000000      0.389999      0.000000   $3.230 792474 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
BEAM_LNE06.ZCV.0618.E                                            0.000000      0.152499      0.000000   $2.993 788096 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:34
*OBSXYZ
LNE06.ZQNA.0618.E     -0.001063      0.075076      0.214816 0.1 0.1 0.1   $2.915 792481 paramètres RST 24-OCT-2024 09:41:34
LNE06.ZQNA.0618.S     -0.000581      0.315052      0.214751 0.1 0.1 0.1   $3.155 792482 paramètres RST 24-OCT-2024 09:41:34
*INCLY Instr_Roll_Theo
LNE06.ZQNA.0618.E .21477704922 RF .2156231169
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZQMD.0648.E                     ;    8.15102; 1726.95623; 2160.12763;2436.14554;436.15091;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0648.S                      ;    8.31802; 1727.02951; 2160.27769;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0648.E                      ;    8.32802; 1727.03390; 2160.28667;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0648.S                      ;    8.36502; 1727.05014; 2160.31992;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMD.0649.E                     ;    8.39502; 1727.06331; 2160.34688;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0648.E                     ;    8.12852; 1726.94635; 2160.10741;2436.14554;436.15091;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0648.S                     ;    8.51852; 1727.11750; 2160.45785;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0648.E                      ;    8.28102; 1727.01328; 2160.24444;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMD.0649.S                     ;    8.49502; 1727.10719; 2160.43673;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMD.0648.S                     ;    8.25102; 1727.00011; 2160.21748;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZQNA.0648   1726.946350   2160.107410   2436.145540 0 0   28.922400 1
*FRAME RST_LNE06.ZQNA.0648 0 0 0 .00025464791 .00248848303 0 1
*FRAME RSTRI_LNE06.ZQNA.0648     -0.000369      0.000400     -0.000460 .09386048223 0 -.03770745804 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZQNA.0648 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZQMD.0648.E                                           0.000003      0.022501      0.000000   $8.151 788128 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZCV.0648.S                                            0.000003      0.189501      0.000000   $8.318 788131 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZCH.0648.E                                            0.000003      0.199501      0.000000   $8.328 788132 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZCH.0648.S                                            0.000003      0.236501      0.000000   $8.365 788133 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQMD.0649.E                                           0.000003      0.266501      0.000000   $8.395 788134 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQNA.0648.E                                           0.000000      0.000000      0.000000   $8.129 791555 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQNA.0648.S                                           0.000001      0.390001      0.000002   $8.519 791556 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZCV.0648.E                                            0.000003      0.152501      0.000000   $8.281 788130 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQMD.0649.S                                           0.000003      0.366501      0.000000   $8.495 788135 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQMD.0648.S                                           0.000003      0.122501      0.000000   $8.251 788129 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
*OBSXYZ
LNE06.ZQNA.0648.E      0.001119      0.075008      0.214999 0.1 0.1 0.1   $8.204 791557 paramètres RST 24-OCT-2024 09:41:35
LNE06.ZQNA.0648.S     -0.001394      0.314959      0.215102 0.1 0.1 0.1   $8.443 791558 paramètres RST 24-OCT-2024 09:41:35
*INCLY Instr_Roll_Theo
LNE06.ZQNA.0648.E .00224656751 RF .00248281711
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZQMF.0655.E                     ;    8.98102; 1727.32047; 2160.87343;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMF.0655.S                     ;    9.08102; 1727.36435; 2160.96329;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0655.E                      ;    9.11102; 1727.37752; 2160.99025;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0655.S                      ;    9.14802; 1727.39376; 2161.02349;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0655.E                      ;    9.15802; 1727.39814; 2161.03248;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0655.S                      ;    9.19502; 1727.41438; 2161.06573;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMD.0656.E                     ;    9.22502; 1727.42755; 2161.09268;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQMD.0656.S                     ;    9.32502; 1727.47143; 2161.18254;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0655.E                     ;    8.95852; 1727.31059; 2160.85322;2436.14554;436.15090;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0655.S                     ;    9.34852; 1727.48174; 2161.20366;2436.14554;436.15089;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZQNA.0655   1727.310590   2160.853220   2436.145540 0 0   28.922400 1
*FRAME RST_LNE06.ZQNA.0655 0 0 0 .00025464791 .00248848303 0 1
*FRAME RSTRI_LNE06.ZQNA.0655     -0.001633     -0.000542     -0.000473 .25268931368 0 .06855948942 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZQNA.0655 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZQMF.0655.E                                           0.000005      0.022499     -0.000003   $8.981 788138 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQMF.0655.S                                           0.000005      0.122499     -0.000003   $9.081 788139 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZCV.0655.E                                            0.000005      0.152499     -0.000003   $9.111 788140 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZCV.0655.S                                            0.000005      0.189499     -0.000003   $9.148 788141 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZCH.0655.E                                            0.000005      0.199499     -0.000003   $9.158 788142 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZCH.0655.S                                            0.000005      0.236499     -0.000003   $9.195 788143 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQMD.0656.E                                           0.000005      0.266499     -0.000003   $9.225 788144 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQMD.0656.S                                           0.000005      0.366499     -0.000003   $9.325 788145 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQNA.0655.E                                           0.000000      0.000000      0.000000   $8.959 791559 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
BEAM_LNE06.ZQNA.0655.S                                           0.000001      0.390001      0.000002   $9.349 791560 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:35
*OBSXYZ
LNE06.ZQNA.0655.S     -0.000817      0.315059      0.214965 0.1 0.1 0.1   $9.274 791562 paramètres RST 24-OCT-2024 09:41:35
LNE06.ZQNA.0655.E      0.000128      0.075061      0.214980 0.1 0.1 0.1   $9.034 791561 paramètres RST 24-OCT-2024 09:41:35
*INCLY Instr_Roll_Theo
LNE06.ZQNA.0655.E .08564375769 RF .08588000729
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;ZDFHR.0701.E                    ;     .10000; 1731.52047; 2152.37393;2436.14586;436.15096;;; -.000039;329.0695;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZDFHR.0701.S                    ;     .50000; 1731.16217; 2152.54995;2436.14584;436.15096;;; -.000039;329.0695;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.ZDFHR.0701   1731.520468   2152.373934   2436.145856 0 0  322.066700 1
*FRAME RST_LNE07.ZDFHR.0701 0 0 0 .00241915513 -.00054647441 7.00281749604 1
*FRAME RSTRI_LNE07.ZDFHR.0701      0.000143      0.000187     -0.001460 -.12263739604 0 -.08707428081 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.ZDFHR.0701 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.ZDFHR.0701.E                                          0.000000      0.000000      0.000000   $0.100 788154 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:36
BEAM_LNE07.ZDFHR.0701.S                                          0.000000      0.399194      0.000000   $0.500 788155 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:36
*OBSXYZ
LNE07.ZDFHR.0701.E     -0.002229      0.020188      0.253000 0.1 0.1 0.1   $0.120 792543 paramètres RST 24-OCT-2024 09:41:36
LNE07.ZDFHR.0701.S     -0.041750      0.378012      0.253000 0.1 0.1 0.1   $0.478 792544 paramètres RST 24-OCT-2024 09:41:36
*INCLY Instr_Roll_Theo
LNE07.ZDFHR.0701.E -.00097090881 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;ZDSHR.0717.E                    ;    1.99322; 1730.40737; 2153.78751;2436.14579;436.15095;;; -.000018;  2.2168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZDSHR.0717.S                    ;    2.49692; 1730.42439; 2154.27625;2436.14578;436.15094;;; -.000018;  2.2168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.ZDSHR.0717   1730.407369   2153.787510   2436.145789 0 0  375.494600 1
*FRAME RST_LNE07.ZDSHR.0717 0 0 0 .00203718327 .00145353026 26.72222221556 1
*FRAME RSTRI_LNE07.ZDSHR.0717      0.000435      0.000317     -0.000004 -.00130177325 0 -.00820117149 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.ZDSHR.0717 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.ZDSHR.0717.E                                          0.000000      0.000000      0.000000   $1.993 788168 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:36
BEAM_LNE07.ZDSHR.0717.S                                          0.000000      0.489040      0.000000   $2.497 788169 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:36
*OBSXYZ
LNE07.ZDSHR.0717.S     -0.283832      0.409498      0.218649 0.1 0.1 0.1   $2.403 792538 paramètres RST 24-OCT-2024 09:41:36
LNE07.ZDSHR.0717.T      0.232048      0.086760      0.218349 0.1 0.1 0.1   $2.080 792539 paramètres RST 24-OCT-2024 09:41:36
LNE07.ZDSHR.0717.E     -0.078532     -0.051604      0.218451 0.1 0.1 0.1   $1.942 792537 paramètres RST 24-OCT-2024 09:41:36
*INCLY Instr_Roll_Theo
LNE07.ZDSHR.0717.E -.00055398653 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MBHEK.0245.E                    ;    9.85102; 1742.70792; 2158.22076;2436.14621;436.15090;;;  .000035;102.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MBHEK.0245.S                    ;   10.82178; 1743.63406; 2158.18089;2436.14624;436.15090;;;  .000035;102.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MBHEK.0245   1742.707918   2158.220762   2436.146206 0 0   69.405700 1
*FRAME RST_LNR.MBHEK.0245 0 0 0 -.00190985932 .0018119472 33.33333335891 1
*FRAME RSTRI_LNR.MBHEK.0245     -0.000039      0.000057      0.000005 .00109880341 0 -.00041205128 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MBHEK.0245 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MBHEK.0245.E                                            0.000000      0.000000      0.000000   $9.851 775532 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:37
BEAM_LNR.MBHEK.0245.S                                           -0.000002      0.927001      0.000000   $10.822 775533 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:37
*OBSXYZ
LNR.MBHEK.0245.E      0.335800      0.228500      0.585000 0.1 0.1 0.1   $10.080 775752 paramètres RST 24-OCT-2024 09:41:37
LNR.MBHEK.0245.S      0.335800      0.698500      0.585000 0.1 0.1 0.1   $10.550 775753 paramètres RST 24-OCT-2024 09:41:37
*INCLY Instr_Roll_Theo
LNR.MBHEK.0245.E .00012496846 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0105.S                    ;     .80599; 1738.57621; 2151.46294;2436.14612;436.15092;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0105.E                    ;     .80598; 1738.57621; 2151.46294;2436.14612;436.15092;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0105   1738.576206   2151.462939   2436.146116 0 0    2.739000 1
*FRAME RST_LNR.MCCAY.0105 0 0 0 .0005729578 .00257818275 0 1
*FRAME RSTRI_LNR.MCCAY.0105     -0.000010     -0.000819      0.000190 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0105 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0105.S                                            0.000000      0.000000      0.000000   $0.806 775485 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:37
BEAM_LNR.MCCAY.0105.E                                            0.000000      0.000000      0.000000   $0.806 775484 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:37
*OBSXYZ
LNR.MCCAY.0105.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $0.721 776148 paramètres RST 24-OCT-2024 09:41:37
LNR.MCCAY.0105.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $0.891 776158 paramètres RST 24-OCT-2024 09:41:37
*INCLY Instr_Roll_Theo
LNR.MCCAY.0105.E -.00005296677 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MCCAY.0405.E                    ;   15.95655; 1747.33789; 2154.99964;2436.14643;436.15090;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MCCAY.0405.S                    ;   15.95656; 1747.33789; 2154.99964;2436.14643;436.15090;;;  .000009;202.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MCCAY.0405   1747.337890   2154.999640   2436.146427 0 0  202.739000 1
*FRAME RST_LNR.MCCAY.0405 0 0 0 -.0005729578 -.00257818275 0 1
*FRAME RSTRI_LNR.MCCAY.0405     -0.000165      0.055726     -0.000162 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MCCAY.0405 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MCCAY.0405.E                                            0.000000      0.000000      0.000000   $15.957 775564 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:38
BEAM_LNR.MCCAY.0405.S                                            0.000000      0.000000      0.000000   $15.957 775565 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:38
*OBSXYZ
LNR.MCCAY.0405.E      0.160000     -0.085000      0.204000 0.1 0.1 0.1   $15.872 776152 paramètres RST 24-OCT-2024 09:41:38
LNR.MCCAY.0405.S      0.160000      0.085000      0.204000 0.1 0.1 0.1   $16.042 776162 paramètres RST 24-OCT-2024 09:41:38
*INCLY Instr_Roll_Theo
LNR.MCCAY.0405.E -.00002979381 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0205.E                    ;    6.45500; 1739.69656; 2156.65082;2436.14610;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0205.S                    ;    6.70500; 1739.91825; 2156.76639;2436.14611;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0205   1739.696564   2156.650818   2436.146103 0 0   69.405700 1
*FRAME RST_LNR.MQNLG.0205 0 0 0 -.00190985932 .0018119472 0 1
*FRAME RSTRI_LNR.MQNLG.0205     -0.000484     -0.000380      0.000834 .16552100987 0 .02342764241 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0205 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0205.E                                            0.000000      0.000000      0.000000   $6.455 775508 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:38
BEAM_LNR.MQNLG.0205.S                                            0.000000      0.250000     -0.000001   $6.705 775511 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:38
*OBSXYZ
LNR.MQNLG.0205.S      0.212920      0.210000      0.344990 0.1 0.1 0.1   $6.665 775773 paramètres RST 24-OCT-2024 09:41:38
LNR.MQNLG.0205.E      0.212920      0.040000      0.344990 0.1 0.1 0.1   $6.495 775772 paramètres RST 24-OCT-2024 09:41:38
*INCLY Instr_Roll_Theo
LNR.MQNLG.0205.E .06247894671 RF .06238873769
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0235.E                    ;    9.30140; 1742.22055; 2157.96668;2436.14619;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0235.S                    ;    9.55140; 1742.44223; 2158.08225;2436.14620;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0235   1742.220551   2157.966677   2436.146189 0 0   69.405700 1
*FRAME RST_LNR.MQNLG.0235 0 0 0 -.00190985932 .0018119472 0 1
*FRAME RSTRI_LNR.MQNLG.0235     -0.000017      0.000004      0.000058 -.00687549204 0 .01247774494 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0235 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0235.E                                            0.000000      0.000000      0.000000   $9.301 775526 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:39
BEAM_LNR.MQNLG.0235.S                                           -0.000001      0.250000      0.000001   $9.551 775531 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:39
*OBSXYZ
LNR.MQNLG.0235.S      0.212910      0.210000      0.344980 0.1 0.1 0.1   $9.511 775763 paramètres RST 24-OCT-2024 09:41:39
LNR.MQNLG.0235.E      0.212910      0.040000      0.344980 0.1 0.1 0.1   $9.341 775762 paramètres RST 24-OCT-2024 09:41:39
*INCLY Instr_Roll_Theo
LNR.MQNLG.0235.E .02109866151 RF .02100845249
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MXNAD.0525.E                    ;   22.57100; 1745.40553; 2149.33742;2436.14641;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MXNAD.0525.S                    ;   22.72100; 1745.27252; 2149.26808;2436.14641;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MXNAD.0525   1745.405525   2149.337421   2436.146413 0 0  269.405700 1
*FRAME RST_LNR.MXNAD.0525 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.MXNAD.0525     -0.000024     -0.000396      0.000015 -.01103471537 0 .0127323641 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MXNAD.0525 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MXNAD.0525.E                                            0.000000      0.000000      0.000000   $22.571 775622 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNR.MXNAD.0525.S                                            0.000000      0.150000      0.000001   $22.721 775623 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
*OBSXYZ
LNR.MXNAD.0525.E      0.212818     -0.010027      0.204975 0.1 0.1 0.1   $22.561 775842 paramètres RST 24-OCT-2024 09:41:40
LNR.MXNAD.0525.S      0.212978      0.159909      0.204931 0.1 0.1 0.1   $22.731 775843 paramètres RST 24-OCT-2024 09:41:40
*INCLY Instr_Roll_Theo
LNR.MXNAD.0525.E -.00017717128 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE00     ;ZQ.0046.E                       ;   10.18110; 1732.04249; 2152.18538;2436.14588;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQ.0046.S                       ;   10.28110; 1731.94844; 2152.21935;2436.14587;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0046.E                      ;   10.31110; 1731.92022; 2152.22954;2436.14587;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCV.0046.S                      ;   10.34810; 1731.88542; 2152.24211;2436.14587;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0046.E                      ;   10.35810; 1731.87602; 2152.24551;2436.14587;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZCH.0046.S                      ;   10.39510; 1731.84122; 2152.25808;2436.14587;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQFD.0047.S                     ;   10.52510; 1731.71895; 2152.30224;2436.14586;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0046.E                     ;   10.15860; 1732.06365; 2152.17773;2436.14588;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQNA.0046.S                     ;   10.54860; 1731.69685; 2152.31023;2436.14586;436.15096;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE00     ;ZQFD.0047.E                     ;   10.42510; 1731.81300; 2152.26827;2436.14587;436.15095;;; -.000038;322.0667;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE00.ZQNA.0046   1732.063651   2152.177733   2436.145878 0 0  322.066700 1
*FRAME RST_LNE00.ZQNA.0046 0 0 0 .00241915513 -.00054647441 0 1
*FRAME RSTRI_LNE00.ZQNA.0046     -0.000064     -0.002123     -0.000929 -.08814737016 0 -.04782814517 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE00.ZQNA.0046 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE00.ZQ.0046.E                                             0.000000      0.022500      0.000000   $10.181 774837 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZQ.0046.S                                             0.000000      0.122500      0.000000   $10.281 774838 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZCV.0046.E                                            0.000000      0.152500      0.000000   $10.311 774839 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZCV.0046.S                                            0.000000      0.189500      0.000000   $10.348 774840 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZCH.0046.E                                            0.000000      0.199500      0.000000   $10.358 774841 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZCH.0046.S                                            0.000000      0.236500      0.000000   $10.395 774842 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZQFD.0047.S                                           0.000000      0.366500      0.000000   $10.525 774844 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZQNA.0046.E                                           0.000000      0.000000      0.000000   $10.159 776401 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZQNA.0046.S                                           0.000000      0.390000      0.000000   $10.549 776402 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
BEAM_LNE00.ZQFD.0047.E                                           0.000000      0.266500      0.000000   $10.425 774843 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:40
*OBSXYZ
LNE00.ZQNA.0046.E      0.000065      0.075086      0.214973 0.1 0.1 0.1   $10.234 776414 paramètres RST 24-OCT-2024 09:41:40
LNE00.ZQNA.0046.S      0.000170      0.315030      0.214951 0.1 0.1 0.1   $10.474 776415 paramètres RST 24-OCT-2024 09:41:40
*INCLY Instr_Roll_Theo
LNE00.ZQNA.0046.E .00395703752 RF .00490197225
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE01     ;ZQMF.0138.E                     ;    7.90077; 1724.62955; 2155.57701;2436.14556;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQMF.0138.S                     ;    8.00077; 1724.58828; 2155.66809;2436.14556;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0138.E                      ;    8.03077; 1724.57590; 2155.69542;2436.14555;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCV.0138.S                      ;    8.06777; 1724.56063; 2155.72912;2436.14555;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0138.E                      ;    8.07777; 1724.55650; 2155.73823;2436.14555;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZCH.0138.S                      ;    8.11477; 1724.54123; 2155.77193;2436.14555;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQMD.0139.E                     ;    8.14477; 1724.52885; 2155.79926;2436.14555;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQMD.0139.S                     ;    8.24477; 1724.48758; 2155.89034;2436.14555;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0138.S                     ;    8.26827; 1724.47788; 2155.91175;2436.14555;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE01     ;ZQNA.0138.E                     ;    7.87827; 1724.63883; 2155.55651;2436.14556;436.15098;;; -.000033;372.9168;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE01.ZQNA.0138   1724.638834   2155.556512   2436.145560 0 0  372.916800 1
*FRAME RST_LNE01.ZQNA.0138 0 0 0 .00210084525 .00136981476 0 1
*FRAME RSTRI_LNE01.ZQNA.0138     -0.000394     -0.001167     -0.000616 -.00897797595 0 -.01256916649 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE01.ZQNA.0138 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE01.ZQMF.0138.E                                           0.000000      0.022500      0.000000   $7.901 787760 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZQMF.0138.S                                           0.000000      0.122500      0.000000   $8.001 787798 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZCV.0138.E                                            0.000000      0.152500      0.000000   $8.031 787799 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZCV.0138.S                                            0.000000      0.189500      0.000000   $8.068 787800 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZCH.0138.E                                            0.000000      0.199500      0.000000   $8.078 787801 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZCH.0138.S                                            0.000000      0.236500      0.000000   $8.115 787802 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZQMD.0139.E                                           0.000000      0.266500      0.000000   $8.145 787803 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZQMD.0139.S                                           0.000000      0.366500      0.000000   $8.245 787804 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZQNA.0138.S                                           0.000000      0.390000      0.000000   $8.268 792372 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE01.ZQNA.0138.E                                           0.000000      0.000000      0.000000   $7.878 792371 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
*OBSXYZ
LNE01.ZQNA.0138.E      0.000439      0.074941      0.214767 0.1 0.1 0.1   $7.953 792395 paramètres RST 24-OCT-2024 09:41:41
LNE01.ZQNA.0138.S     -0.000365      0.314920      0.214900 0.1 0.1 0.1   $8.193 792396 paramètres RST 24-OCT-2024 09:41:41
*INCLY Instr_Roll_Theo
LNE01.ZQNA.0138.E .11522136697 RF .11611944648
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;AS.1.E                          ;   10.44852; 1727.96447; 2162.19208;2436.14554;436.15088;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;AS.1.S                          ;   15.54852; 1730.20257; 2166.77475;2436.14552;436.15081;;; -.000004; 28.9224;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.AS.1   1727.964469   2162.192079   2436.145536 0 0   28.922400 1
*FRAME RST_LNE06.AS.1 0 0 0 .00025464791 .00248848303 0 1
*FRAME RSTRI_LNE06.AS.1     -0.001095     -0.000359     -0.000119 -.00081137805 0 -.00003744822 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.AS.1 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.AS.1.E                                                0.000000      0.000000      0.000000   $10.449 817456 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
BEAM_LNE06.AS.1.S                                               -0.000001      5.100001     -0.000001   $15.549 817457 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:41
*OBSXYZ
LNE06.AS.1.E      0.000000      0.000000      0.000000 0.1 0.1 0.1   $10.449 817460 paramètres RST 24-OCT-2024 09:41:41
LNE06.AS.1.S      0.000000      5.100000      0.000000 0.1 0.1 0.1   $15.549 817461 paramètres RST 24-OCT-2024 09:41:41
*INCLY Instr_Roll_Theo
LNE06.AS.1.E -.0002362496 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZDSHR.0606.E                    ;     .71271; 1729.32851; 2153.26163;2436.14576;436.15096;;; -.000036;355.7835;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZDSHR.0606.S                    ;    1.08426; 1729.09448; 2153.54257;2436.14575;436.15096;;; -.000036;355.7835;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZDSHR.0606   1729.328507   2153.261633   2436.145764 0 0  336.072300 1
*FRAME RST_LNE06.ZDSHR.0606 0 0 0 .00248281711 .00000006366 19.71111115543 1
*FRAME RSTRI_LNE06.ZDSHR.0606      0.000606      0.000038     -0.000069 .02106757513 0 -.01445131252 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZDSHR.0606 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZDSHR.0606.E                                          0.000000      0.000000      0.000000   $0.713 788080 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZDSHR.0606.S                                          0.000000      0.365638      0.000000   $1.084 788081 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
*OBSXYZ
LNE06.ZDSHR.0606.E     -0.113197     -0.125924      0.217646 0.1 0.1 0.1   $0.587 791835 paramètres RST 24-OCT-2024 09:41:42
LNE06.ZDSHR.0606.S     -0.321598      0.230657      0.217691 0.1 0.1 0.1   $0.943 791836 paramètres RST 24-OCT-2024 09:41:42
LNE06.ZDSHR.0606.T      0.244081      0.083485      0.218202 0.1 0.1 0.1   $0.796 791837 paramètres RST 24-OCT-2024 09:41:42
*INCLY Instr_Roll_Theo
LNE06.ZDSHR.0606.E -.00096664346 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE06     ;ZQFF.0633.S                     ;    6.68760; 1726.99046; 2158.73588;2436.14557;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQ.0632.E                       ;    6.34360; 1727.11963; 2158.41705;2436.14558;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQ.0632.S                       ;    6.44360; 1727.08208; 2158.50973;2436.14558;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0632.E                      ;    6.47360; 1727.07082; 2158.53754;2436.14558;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCV.0632.S                      ;    6.51060; 1727.05692; 2158.57183;2436.14558;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0632.E                      ;    6.52060; 1727.05317; 2158.58110;2436.14558;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQFF.0633.E                     ;    6.58760; 1727.02801; 2158.64319;2436.14557;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0632.S                     ;    6.71110; 1726.98164; 2158.75766;2436.14557;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZQNA.0632.E                     ;    6.32110; 1727.12808; 2158.39620;2436.14558;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE06     ;ZCH.0632.S                      ;    6.55760; 1727.03927; 2158.61539;2436.14557;436.15092;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE06.ZQNA.0632   1727.128080   2158.396200   2436.145580 0 0  375.494600 1
*FRAME RST_LNE06.ZQNA.0632 0 0 0 .00203718327 .00145353026 0 1
*FRAME RSTRI_LNE06.ZQNA.0632     -0.001251     -0.000637     -0.000870 -.1116541033 0 .02775031817 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE06.ZQNA.0632 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE06.ZQFF.0633.S                                          -0.000003      0.366497      0.000003   $6.688 788121 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZQ.0632.E                                            -0.000003      0.022497      0.000003   $6.344 788114 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZQ.0632.S                                            -0.000003      0.122497      0.000003   $6.444 788115 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZCV.0632.E                                           -0.000003      0.152497      0.000003   $6.474 788116 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZCV.0632.S                                           -0.000003      0.189497      0.000003   $6.511 788117 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZCH.0632.E                                           -0.000003      0.199497      0.000003   $6.521 788118 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZQFF.0633.E                                          -0.000003      0.266497      0.000003   $6.588 788120 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZQNA.0632.S                                           0.000002      0.389997      0.000002   $6.711 791552 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZQNA.0632.E                                           0.000000      0.000000      0.000000   $6.321 791551 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
BEAM_LNE06.ZCH.0632.S                                           -0.000003      0.236497      0.000003   $6.558 788119 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:42
*OBSXYZ
LNE06.ZQNA.0632.E     -0.000101      0.075040      0.214948 0.1 0.1 0.1   $6.396 791553 paramètres RST 24-OCT-2024 09:41:42
LNE06.ZQNA.0632.S      0.000811      0.314973      0.214979 0.1 0.1 0.1   $6.636 791554 paramètres RST 24-OCT-2024 09:41:42
*INCLY Instr_Roll_Theo
LNE06.ZQNA.0632.E -.06011536848 RF -.05926930081
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNE07     ;ZQMF.0711.E                     ;    1.40930; 1730.62663; 2153.24631;2436.14581;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMF.0711.S                     ;    1.50930; 1730.58908; 2153.33900;2436.14580;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0711.E                      ;    1.53930; 1730.57782; 2153.36680;2436.14580;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCV.0711.S                      ;    1.57630; 1730.56392; 2153.40109;2436.14580;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0711.E                      ;    1.58630; 1730.56017; 2153.41036;2436.14580;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZCH.0711.S                      ;    1.62330; 1730.54627; 2153.44465;2436.14580;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMD.0712.S                     ;    1.75330; 1730.49746; 2153.56514;2436.14580;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0711.E                     ;    1.38680; 1730.63508; 2153.22546;2436.14581;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQNA.0711.S                     ;    1.77680; 1730.48864; 2153.58692;2436.14580;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNE07     ;ZQMD.0712.E                     ;    1.65330; 1730.53501; 2153.47246;2436.14580;436.15095;;; -.000032;375.4946;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNE07.ZQNA.0711   1730.635078   2153.225461   2436.145808 0 0  375.494600 1
*FRAME RST_LNE07.ZQNA.0711 0 0 0 .00203718327 .00145353026 0 1
*FRAME RSTRI_LNE07.ZQNA.0711      0.000054      0.001461      0.000037 -.01126327159 0 -.01926182727 1 TX TY TZ RX RZ 
*FRAME RSTR_LNE07.ZQNA.0711 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNE07.ZQMF.0711.E                                           0.000000      0.022499      0.000000   $1.409 788160 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZQMF.0711.S                                           0.000000      0.122499      0.000000   $1.509 788161 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZCV.0711.E                                            0.000000      0.152499      0.000000   $1.539 788162 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZCV.0711.S                                            0.000000      0.189499      0.000000   $1.576 788163 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZCH.0711.E                                            0.000000      0.199499      0.000000   $1.586 788164 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZCH.0711.S                                            0.000000      0.236499      0.000000   $1.623 788165 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZQMD.0712.S                                           0.000000      0.366499      0.000000   $1.753 788167 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZQNA.0711.E                                           0.000000      0.000000      0.000000   $1.387 792477 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZQNA.0711.S                                           0.000001      0.390000      0.000000   $1.777 792478 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
BEAM_LNE07.ZQMD.0712.E                                           0.000000      0.266499      0.000000   $1.653 788166 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:43
*OBSXYZ
LNE07.ZQNA.0711.E     -0.000832      0.074974      0.214893 0.1 0.1 0.1   $1.462 792485 paramètres RST 24-OCT-2024 09:41:43
LNE07.ZQNA.0711.S     -0.000832      0.314981      0.214975 0.1 0.1 0.1   $1.702 792486 paramètres RST 24-OCT-2024 09:41:43
*INCLY Instr_Roll_Theo
LNE07.ZQNA.0711.E .17964875216 RF .18048170547
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;ACW02.0530.S                    ;   23.10500; 1744.93201; 2149.09056;2436.14640;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;ACW02.0530.E                    ;   22.80500; 1745.19803; 2149.22925;2436.14641;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.ACW02.0530   1745.198030   2149.229246   2436.146406 0 0  269.405700 1
*FRAME RST_LNR.ACW02.0530 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.ACW02.0530      0.000122      0.000083     -0.000183 -.0059417782 0 .00169765092 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.ACW02.0530 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.ACW02.0530.S                                            0.000000      0.300000      0.000000   $23.105 775625 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:44
BEAM_LNR.ACW02.0530.E                                            0.000000      0.000000      0.000000   $22.805 775624 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:44
*OBSXYZ
LNR.ACW02.0530.E      0.000000      0.055000      0.325000 0.1 0.1 0.1   $22.860 775748 paramètres RST 24-OCT-2024 09:41:44
LNR.ACW02.0530.S      0.000000      0.245000      0.325000 0.1 0.1 0.1   $23.050 775749 paramètres RST 24-OCT-2024 09:41:44
*INCLY Instr_Roll_Theo
LNR.ACW02.0530.E -.00017927213 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEA.0215.E                    ;    7.05320; 1740.22701; 2156.92736;2436.14612;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEA.0215.S                    ;    7.05321; 1740.22701; 2156.92736;2436.14612;436.15090;;;  .000030; 69.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEA.0215   1740.227006   2156.927359   2436.146121 0 0   69.405700 1
*FRAME RST_LNR.BPMEA.0215 0 0 0 -.00190985932 .0018119472 0 1
*FRAME RSTRI_LNR.BPMEA.0215     -0.000360      0.005430      0.000110 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEA.0215 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEA.0215.E                                            0.000000      0.000000      0.000000   $7.053 775514 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:44
BEAM_LNR.BPMEA.0215.S                                            0.000000      0.000000      0.000000   $7.053 775515 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:44
*OBSXYZ
LNR.BPMEA.0215.T      0.081750      0.174000     -0.029760 0.1 0.1 0.1   $7.227 778403 paramètres RST 24-OCT-2024 09:41:44
LNR.BPMEA.0215.S      0.026880      0.174000      0.082740 0.1 0.1 0.1   $7.227 778397 paramètres RST 24-OCT-2024 09:41:44
LNR.BPMEA.0215.E      0.071270     -0.174000      0.049900 0.1 0.1 0.1   $6.879 778391 paramètres RST 24-OCT-2024 09:41:44
*INCLY Instr_Roll_Theo
LNR.BPMEA.0215.E .00008810818 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;BPMEA.0320.E                    ;   13.61960; 1745.99464; 2156.67906;2436.14636;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BPMEA.0320.S                    ;   13.61961; 1745.99464; 2156.67906;2436.14636;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.BPMEA.0320   1745.994637   2156.679060   2436.146355 0 0  136.072300 1
*FRAME RST_LNR.BPMEA.0320 0 0 0 -.00254647909 -.0007661719 0 1
*FRAME RSTRI_LNR.BPMEA.0320      0.000360      0.001030     -0.000040 0 0 0 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.BPMEA.0320 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.BPMEA.0320.E                                            0.000000      0.000000      0.000000   $13.620 775548 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:45
BEAM_LNR.BPMEA.0320.S                                            0.000000      0.000000      0.000000   $13.620 775549 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:45
*OBSXYZ
LNR.BPMEA.0320.S      0.026880      0.174000      0.082740 0.1 0.1 0.1   $13.794 778399 paramètres RST 24-OCT-2024 09:41:45
LNR.BPMEA.0320.E      0.071270     -0.174000      0.049900 0.1 0.1 0.1   $13.446 778393 paramètres RST 24-OCT-2024 09:41:45
LNR.BPMEA.0320.T      0.081750      0.174000     -0.029760 0.1 0.1 0.1   $13.794 778405 paramètres RST 24-OCT-2024 09:41:45
*INCLY Instr_Roll_Theo
LNR.BPMEA.0320.E .00009975832 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;LBTV.0117.E                     ;    3.17279; 1738.67800; 2153.82755;2436.14609;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;LBTV.0117.S                     ;    3.79479; 1738.70476; 2154.44897;2436.14609;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BTV.0117.S                      ;    3.39380; 1738.68751; 2154.04835;2436.14609;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BTV.0118.E                      ;    3.56329; 1738.69480; 2154.21769;2436.14609;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BTV.0118.S                      ;    3.56330; 1738.69480; 2154.21770;2436.14609;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;BTV.0117.E                      ;    3.39379; 1738.68751; 2154.04834;2436.14609;436.15091;;; -.000009;  2.7390;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.LBTV.0117   1738.678004   2153.827549   2436.146094 0 0    2.739000 1
*FRAME RST_LNR.LBTV.0117 0 0 0 .0005729578 .00257818275 0 1
*FRAME RSTRI_LNR.LBTV.0117      0.000110     -0.004330     -0.000140 -.02456410757 0 -.08188038521 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.LBTV.0117 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.LBTV.0117.E                                             0.000000      0.000000      0.000000   $3.173 775882 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:45
BEAM_LNR.LBTV.0117.S                                             0.000000      0.622000      0.000000   $3.795 775883 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:45
BEAM_LNR.BTV.0117.S                                              0.000000      0.221010      0.000000   $3.394 775491 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:45
BEAM_LNR.BTV.0118.E                                              0.000000      0.390500      0.000000   $3.563 775492 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:45
BEAM_LNR.BTV.0118.S                                              0.000000      0.390510      0.000000   $3.563 775493 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:45
BEAM_LNR.BTV.0117.E                                              0.000000      0.221000      0.000000   $3.394 775490 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:45
*OBSXYZ
LNR.LBTV.0117.E      0.000000      0.106760      0.320720 0.1 0.1 0.1   $3.280 775884 paramètres RST 24-OCT-2024 09:41:45
LNR.LBTV.0117.S      0.000000      0.490240      0.320720 0.1 0.1 0.1   $3.663 775885 paramètres RST 24-OCT-2024 09:41:45
*INCLY Instr_Roll_Theo
LNR.LBTV.0117.E -.00005455831 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MBHEK.0135.E                    ;    4.98462; 1738.75593; 2155.63772;2436.14608;436.15090;;;  .000011; 36.0724;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MBHEK.0135.S                    ;    5.95538; 1739.25353; 2156.41985;2436.14609;436.15090;;;  .000011; 36.0724;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MBHEK.0135   1738.755933   2155.637720   2436.146076 0 0    2.739000 1
*FRAME RST_LNR.MBHEK.0135 0 0 0 .0005729578 .00257818275 33.33333335891 1
*FRAME RSTRI_LNR.MBHEK.0135     -0.000081      0.000197      0.000292 -.00652414718 0 .00254098365 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MBHEK.0135 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MBHEK.0135.E                                            0.000000      0.000000      0.000000   $4.985 775500 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:46
BEAM_LNR.MBHEK.0135.S                                           -0.000001      0.927001      0.000000   $5.955 775501 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:46
*OBSXYZ
LNR.MBHEK.0135.S      0.335800      0.698500      0.585000 0.1 0.1 0.1   $5.683 775751 paramètres RST 24-OCT-2024 09:41:46
LNR.MBHEK.0135.E      0.335800      0.228500      0.585000 0.1 0.1 0.1   $5.213 775750 paramètres RST 24-OCT-2024 09:41:46
*INCLY Instr_Roll_Theo
LNR.MBHEK.0135.E .00000999493 RF 0
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0315.E                    ;   13.49460; 1745.88917; 2156.74616;2436.14635;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0315.S                    ;   13.74460; 1746.10010; 2156.61196;2436.14636;436.15090;;;  .000040;136.0723;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0315   1745.889172   2156.746158   2436.146350 0 0  136.072300 1
*FRAME RST_LNR.MQNLG.0315 0 0 0 -.00254647909 -.0007661719 0 1
*FRAME RSTRI_LNR.MQNLG.0315      0.000099     -0.000765     -0.000329 -.30226812766 0 -.00611161718 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0315 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0315.E                                            0.000000      0.000000      0.000000   $13.495 775546 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:46
BEAM_LNR.MQNLG.0315.S                                            0.000000      0.250000      0.000000   $13.745 775551 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:46
*OBSXYZ
LNR.MQNLG.0315.E      0.213030      0.040000      0.345010 0.1 0.1 0.1   $13.535 775764 paramètres RST 24-OCT-2024 09:41:46
LNR.MQNLG.0315.S      0.213030      0.210000      0.345010 0.1 0.1 0.1   $13.705 775765 paramètres RST 24-OCT-2024 09:41:46
*INCLY Instr_Roll_Theo
LNR.MQNLG.0315.E -.02854603059 RF -.02864788976
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LNR       ;MQNLG.0510.S                    ;   21.90780; 1745.99360; 2149.64401;2436.14643;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
BEAM_LNR       ;MQNLG.0510.E                    ;   21.65780; 1746.21529; 2149.75958;2436.14644;436.15092;;; -.000030;269.4057;;;24-OCT-2024; ;;coordonnées théoriques dans le système CCS au 24-OCT-2024 09:40:47;
*FRAME RSTI_LNR.MQNLG.0510   1746.215286   2149.759583   2436.146441 0 0  269.405700 1
*FRAME RST_LNR.MQNLG.0510 0 0 0 .00190985932 -.0018119472 0 1
*FRAME RSTRI_LNR.MQNLG.0510     -0.000073     -0.000160     -0.000067 -.04889230956 0 -.01426025988 1 TX TY TZ RX RZ 
*FRAME RSTR_LNR.MQNLG.0510 0 0 0 0 0 0 1 RY
*CALA
BEAM_LNR.MQNLG.0510.S                                            0.000000      0.250000      0.000001   $21.908 775615 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:47
BEAM_LNR.MQNLG.0510.E                                            0.000000      0.000000      0.000000   $21.658 775612 coordonnées théoriques dans le système RST au 24-OCT-2024 09:41:47
*OBSXYZ
LNR.MQNLG.0510.E      0.212860      0.040000      0.344990 0.1 0.1 0.1   $21.698 775774 paramètres RST 24-OCT-2024 09:41:47
LNR.MQNLG.0510.S      0.212860      0.210000      0.344990 0.1 0.1 0.1   $21.868 775775 paramètres RST 24-OCT-2024 09:41:47
*INCLY Instr_Roll_Theo
LNR.MQNLG.0510.E .00809888576 RF .00827605704
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME

";
            Tools.Artefact.CreateArtefact(fileContent: datContent, artefactName: "L2_BGM2_Step00_dat_file.dat");
        }

        string changeStep = @"<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""137"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1267"" Ypos=""953"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""111"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1267"" Ypos=""953"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />";

        /// <summary>
        /// start a GEM module and rogrammatically load a dat file (because macro do not record keys in opening and saving windows :(
        /// 
        /// </summary>
        [TestMethod()]
        public void L2_BGM2_Step01_Start_with_dat_file_Test()
        {
            var datPath = Tools.Artefact.GetArtefactFullPath("L2_BGM2_Step00_dat_file.dat"); // here first so that the test fails if the file is not existing

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""234"" Ypos=""281"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""234"" Ypos=""281"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Guided modules (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Guided modules end"" />
	  
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""371"" Ypos=""300"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""371"" Ypos=""300"" Delta=""0"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Element alignment (beginning)"" />
	  <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Element alignment end"" />
	  {changeStep}
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""1000"" Ypos=""760"" Delta=""0"" />
	  <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""200"" Button=""Left"" Direction=""Up"" Xpos=""1000"" Ypos=""760"" Delta=""0"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var mms = tsunami.MeasurementModules;
                Assert.IsTrue(mms.Count > 0, $"No measurement modules");

                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;
                Assert.IsTrue(ggm != null, "Guided Group Module object is NULL");

                var sgm = ggm.SubGuidedModules;
                Assert.IsTrue(sgm.Count > 0, $"no sub Guided Group Module found in {ggm}");

                var gmp = sgm[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(ggm != null, $"sub Guided Group Module are missing suppossed to be 5 we have: {sgm.Count}");

                ggm._ElementManager.AddElementsFromFile(new FileInfo(datPath), TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes.CCS, true, TSU.Common.Elements.Point.Types.Reference, updateView: false);
                tsunami.BOC_Context.SelectedGeodeFilePaths.Add(datPath);
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");

                Assert.IsTrue(tsunami != null, "TSUNAMI oject is NULL");
                Assert.IsTrue(tsunami.Points.Count == 650, "Expecting 650 points in BGM_Step1_Start_with_dat_file_Test.tsu");
            });
        }

        /// <summary>
        /// choice operation
        /// choice magnet ZDSHR.0132, ZDSHR.0606 and ZDSHR.0717
        /// choice freestation, 
        /// t2
        /// RRRR0.5
        /// face1
        /// 1m
        /// ref: 
        /// NID.LNE15. 
        /// NID.LNE1.  
        /// NID.LNE16. 
        /// ZQNA.0101.E
        /// 
        /// </summary>
        [TestMethod()]
        public void L2_BGM2_Step02_Setup_to_polar_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM2_Step01_Start_with_dat_file_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""74"" trace=""Enter operation number+"" Button=""Left"" Direction=""Down"" Xpos=""550"" Ypos=""488"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""129"" trace=""Enter operation number+"" Button=""Left"" Direction=""Up"" Xpos=""550"" Ypos=""488"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Enter operation number (beginning)"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""6"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""7"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""29"" trace=""button2+OK"" Button=""Left"" Direction=""Down"" Xpos=""907"" Ypos=""582"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""90"" trace=""button2+OK"" Button=""Left"" Direction=""Up"" Xpos=""907"" Ypos=""582"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""MB: New operation: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Enter operation number end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""356"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{M}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""31"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{O}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""238"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1270"" Ypos=""934"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""97"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1270"" Ypos=""934"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""270"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""525"" Ypos=""336"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""138"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""525"" Ypos=""336"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{Z}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""89"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{D}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""74"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{S}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{H}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""70"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""27"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""1002"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""521"" Ypos=""397"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""521"" Ypos=""397"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""258"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""517"" Ypos=""451"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""517"" Ypos=""451"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""290"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""527"" Ypos=""542"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""527"" Ypos=""542"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""225"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1302"" Ypos=""952"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""89"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1302"" Ypos=""952"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""36"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""293"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Down"" Xpos=""1110"" Ypos=""852"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""114"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Up"" Xpos=""1110"" Ypos=""852"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""40"" Delay=""0"" Action=""BB: Polar Alignment (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Polar Alignment end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""285"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1031"" Ypos=""268"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1031"" Ypos=""268"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Free station (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""298"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""1076"" Ypos=""628"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""137"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""1076"" Ypos=""628"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""47"" Delay=""0"" Action=""MB: Station point name?: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""48"" Delay=""0"" Action=""BB: Free station end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""202"" trace=""Select Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""1462"" Ypos=""465"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""50"" Delay=""90"" trace=""Select Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""1462"" Ypos=""465"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""BB: Select Instrument (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""277"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1472"" Ypos=""215"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""209"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1472"" Ypos=""215"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""67"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""68"" Delay=""0"" Action=""BB: Options end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""273"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1564"" Ypos=""333"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""74"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1564"" Ypos=""333"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""71"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""72"" Delay=""0"" Action=""BB: List View end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""230"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""295"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""295"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""54"" Delay=""38"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""55"" Delay=""65"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""1036"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""129"" Ypos=""336"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""129"" Ypos=""336"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""266"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""874"" Ypos=""950"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""97"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""874"" Ypos=""950"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""60"" Delay=""0"" Action=""MB: Select: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""61"" Delay=""0"" Action=""BB: Select Instrument end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""266"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""1517"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""82"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""1517"" Ypos=""573"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""64"" Delay=""0"" Action=""BB: Change Reflector (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""377""  Button=""Left"" Direction=""Down"" Xpos=""1472"" Ypos=""215"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""209""  Button=""Left"" Direction=""Up"" Xpos=""1472"" Ypos=""215"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""67"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""68"" Delay=""0"" Action=""BB: Options end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""273"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1564"" Ypos=""333"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""74"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1564"" Ypos=""333"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""71"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""72"" Delay=""0"" Action=""BB: List View end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""938"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""154"" Ypos=""266"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""154"" Ypos=""266"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""75"" Delay=""60"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""76"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""1025"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""110"" Ypos=""328"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""130"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""110"" Ypos=""328"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""80"" Delay=""293"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""856"" Ypos=""929"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""81"" Delay=""89"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""856"" Ypos=""929"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""82"" Delay=""0"" Action=""MB: Choose a Reflector: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""83"" Delay=""200"" Action=""BB: Change Reflector end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""242"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Down"" Xpos=""1515"" Ypos=""634"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""85"" Delay=""82"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Up"" Xpos=""1515"" Ypos=""634"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""86"" Delay=""0"" Action=""BB: Switch between 1 face and double face (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""87"" Delay=""0"" Action=""BB: Switch between 1 face and double face end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""88"" Delay=""209"" trace=""Number of measurement+"" Button=""Left"" Direction=""Down"" Xpos=""1524"" Ypos=""738"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""89"" Delay=""106"" trace=""Number of measurement+"" Button=""Left"" Direction=""Up"" Xpos=""1524"" Ypos=""738"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""90"" Delay=""0"" Action=""BB: Number of measurement (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""91"" Delay=""208"" trace=""button4+1"" Button=""Left"" Direction=""Down"" Xpos=""1066"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""130"" trace=""button4+1"" Button=""Left"" Direction=""Up"" Xpos=""1066"" Ypos=""588"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""93"" Delay=""0"" Action=""MB: Number of measurement: 1"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""94"" Delay=""0"" Action=""BB: Number of measurement end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""214"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1360"" Ypos=""954"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""96"" Delay=""98"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1360"" Ypos=""954"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""97"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""98"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""3169"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""567"" Ypos=""354"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""567"" Ypos=""354"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""101"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{N}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""102"" Delay=""86"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""103"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{D}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""104"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""105"" Delay=""40"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{L}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""106"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{N}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""107"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{E}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""107"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData="" "" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""109"" Delay=""95"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""|"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""112"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData="" "" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""113"" Delay=""17"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{Z}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""114"" Delay=""34"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{Q}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{N}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""116"" Delay=""42"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData="" "" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""109"" Delay=""95"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""|"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData="" "" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{S}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{L}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{N}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{-}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""2827"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""518"" Ypos=""595"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""212"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""518"" Ypos=""595"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""214"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""543"" Ypos=""708"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""543"" Ypos=""708"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""293"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""545"" Ypos=""572"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""545"" Ypos=""572"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""217"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""554"" Ypos=""739"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""121"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""555"" Ypos=""739"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""24"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""550"" Ypos=""684"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""550"" Ypos=""684"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""147"" Delay=""241"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1341"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""148"" Delay=""114"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1341"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""149"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""150"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1001"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1371"" Ypos=""945"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""105"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1371"" Ypos=""945"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "2 manget should be selected");
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "2 manget should be selected");
                var mag1 = gmp.MagnetsToBeAligned[0];
                var mag2 = gmp.MagnetsToBeAligned[1];
                var mag3 = gmp.MagnetsToBeAligned[2];
                Assert.AreEqual("LNE01.ZDSHR.0132", mag1._Name, "magnet to align 1 must be LNE01.ZDSHR.0132");
                Assert.AreEqual("LNE06.ZDSHR.0606", mag2._Name, "magnet to align 1 must be LNE06.ZDSHR.0606");
                Assert.AreEqual("LNE07.ZDSHR.0717", mag3._Name, "magnet to align 1 must be LNE07.ZDSHR.0717");

                var gmPolar = ggm.SubGuidedModules.Find(x => x.ObservationType == TSU.Common.ObservationType.Polar);
                var stations = gmPolar.StationModules;
                Assert.IsTrue(stations.Count == 1, "1 station module should exist");
                var station = gmPolar.StationModules[0] as TSU.Polar.Station.Module;
                Assert.IsTrue(station != null, "2 manget should be selected");

                Assert.IsTrue(station.stationParameters._Instrument._Name.StartsWith("T2_"), "not a T2");

                var defaultM = station.StationTheodolite.Parameters2.DefaultMeasure;
                Assert.IsTrue(defaultM.Reflector._Name.StartsWith("RRR"), "not a RRR");

                var todos = gmPolar.ElementsTobeMeasured;
                Assert.IsTrue(todos.Count == 5, "not 5 measure to do");
                var meas1 = todos.Last() as Point;
                var meas5 = todos.First() as Point;
                Assert.IsTrue(meas1._Name == "193.NID.LNE15.", "not 193.NID.LNE15");
                Assert.IsTrue(meas5._Name == "193.ST.LNA-4.", "not 193.ST.LNa-4.");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM2_Step03_Take_REFS_AND_COMPUTE_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM2_Step02_Setup_to_polar_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    {changeStep}
    {changeStep}
    {changeStep}

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""237"" Delay=""256"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1149"" Ypos=""458"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""238"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1149"" Ypos=""458"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""239"" Delay=""258"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1149"" Ypos=""475"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""240"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1149"" Ypos=""475"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""241"" Delay=""265"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""DataGridViewTextBoxEditingControl"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""242"" Delay=""242"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""DataGridView"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""237"" Delay=""256"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1149"" Ypos=""458"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""238"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1149"" Ypos=""458"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""239"" Delay=""258"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1149"" Ypos=""378"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""240"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1149"" Ypos=""378"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""241"" Delay=""265"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""DataGridViewTextBoxEditingControl"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""242"" Delay=""242"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""DataGridView"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""250"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""562"" Ypos=""284"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""121"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""562"" Ypos=""284"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""282"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""726"" Ypos=""719"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""726"" Ypos=""719"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""267"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""949"" Ypos=""729"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""97"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""949"" Ypos=""729"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""704"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""235"" Ypos=""269"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""235"" Ypos=""269"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""18"" Delay=""738"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""97"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""34"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""21"" Delay=""90"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""25"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""26"" Delay=""77"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""20"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""229"" Ypos=""304"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""13"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""229"" Ypos=""304"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""32"" Delay=""82"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""33"" Delay=""81"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""34"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""35"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""73"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""41"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""76"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""264"" Ypos=""341"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""264"" Ypos=""341"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""44"" Delay=""42"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""45"" Delay=""90"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""46"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""47"" Delay=""65"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""48"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""49"" Delay=""89"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""50"" Delay=""97"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""51"" Delay=""85"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""782"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""809"" Ypos=""924"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""74"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""809"" Ypos=""924"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""54"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""704"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1019"" Ypos=""663"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""114"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1019"" Ypos=""663"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""57"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""742"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""561"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""561"" Ypos=""334"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""722"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""416"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""416"" Ypos=""262"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""62"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""63"" Delay=""70"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""64"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""65"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""66"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""67"" Delay=""42"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""68"" Delay=""42"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""69"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""70"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""71"" Delay=""26"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""447"" Ypos=""302"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""21"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""447"" Ypos=""302"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""73"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""74"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""75"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""76"" Delay=""52"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""44"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""78"" Delay=""30"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""79"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""80"" Delay=""79"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""81"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""82"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""49"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""272"" Ypos=""335"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""84"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""272"" Ypos=""335"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""85"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""86"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""87"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""88"" Delay=""74"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""89"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""93"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""92"" Delay=""94"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""93"" Delay=""778"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""914"" Ypos=""966"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""94"" Delay=""89"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""914"" Ypos=""966"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""95"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""96"" Delay=""709"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1057"" Ypos=""648"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""97"" Delay=""98"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1057"" Ypos=""648"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""98"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""99"" Delay=""721"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""576"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""100"" Delay=""16"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""576"" Ypos=""383"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""101"" Delay=""722"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""485"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""102"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""485"" Ypos=""261"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""103"" Delay=""55"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""104"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""105"" Delay=""34"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""106"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""107"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""109"" Delay=""64"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""110"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""111"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""112"" Delay=""32"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""113"" Delay=""29"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""477"" Ypos=""300"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""114"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""477"" Ypos=""300"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""64"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""116"" Delay=""43"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""117"" Delay=""71"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""118"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""119"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""120"" Delay=""67"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""121"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""122"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""123"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""124"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""478"" Ypos=""343"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""125"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""478"" Ypos=""343"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""126"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""127"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""128"" Delay=""30"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""129"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""130"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""131"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""132"" Delay=""68"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""133"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""134"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""135"" Delay=""749"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""831"" Ypos=""919"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""136"" Delay=""122"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""831"" Ypos=""919"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""137"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""189"" Delay=""729""  Button=""Left"" Direction=""Down"" Xpos=""1022"" Ypos=""629"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""190"" Delay=""89""  Button=""Left"" Direction=""Up"" Xpos=""1022"" Ypos=""629"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""141"" Delay=""707"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1034"" Ypos=""638"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""142"" Delay=""106"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1034"" Ypos=""638"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""143"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""144"" Delay=""723"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""562"" Ypos=""423"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""145"" Delay=""16"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""562"" Ypos=""423"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""146"" Delay=""751"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""479"" Ypos=""272"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""147"" Delay=""15"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""479"" Ypos=""272"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""148"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""149"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""150"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""151"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""152"" Delay=""34"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""153"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""154"" Delay=""15"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""155"" Delay=""12"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""156"" Delay=""15"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""157"" Delay=""90"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""158"" Delay=""41"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""467"" Ypos=""301"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""159"" Delay=""54"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""467"" Ypos=""301"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""160"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""161"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""162"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""163"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""164"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""165"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""166"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""169"" Delay=""47"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""170"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""171"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""172"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""463"" Ypos=""339"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""173"" Delay=""59"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""463"" Ypos=""339"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""174"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""175"" Delay=""14"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""176"" Delay=""61"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""177"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""178"" Delay=""46"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""179"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""184"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""185"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""186"" Delay=""762"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""903"" Ypos=""943"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""187"" Delay=""97"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""903"" Ypos=""943"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""188"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""189"" Delay=""729""  Button=""Left"" Direction=""Down"" Xpos=""1022"" Ypos=""629"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""190"" Delay=""89""  Button=""Left"" Direction=""Up"" Xpos=""1022"" Ypos=""629"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""189"" Delay=""729"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1072"" Ypos=""649"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""190"" Delay=""89"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1072"" Ypos=""649"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""191"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""192"" Delay=""790"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""561"" Ypos=""476"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""193"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""561"" Ypos=""476"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""194"" Delay=""726"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""Button"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""195"" Delay=""57"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""429"" Ypos=""279"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""196"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""429"" Ypos=""278"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""197"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""198"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""199"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""200"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""201"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""202"" Delay=""66"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""203"" Delay=""40"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""204"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""205"" Delay=""23"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""206"" Delay=""58"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""361"" Ypos=""304"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""207"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""361"" Ypos=""304"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""208"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""209"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""210"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""211"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""212"" Delay=""39"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""213"" Delay=""37"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""214"" Delay=""60"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""215"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""216"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""217"" Delay=""25"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""218"" Delay=""54"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""359"" Ypos=""336"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""219"" Delay=""54"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""359"" Ypos=""336"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""220"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""221"" Delay=""23"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""."" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""222"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""223"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""224"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""225"" Delay=""33"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""226"" Delay=""10"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""227"" Delay=""11"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""228"" Delay=""713"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""866"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""229"" Delay=""74"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""866"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""230"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""189"" Delay=""729""  Button=""Left"" Direction=""Down"" Xpos=""1022"" Ypos=""629"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""190"" Delay=""89""  Button=""Left"" Direction=""Up"" Xpos=""1022"" Ypos=""629"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""234"" Delay=""786"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1000"" Ypos=""658"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""235"" Delay=""145"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1000"" Ypos=""658"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""236"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />
{changeStep}

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""253"" Delay=""765"" trace=""Ctrl+"" Button=""Left"" Direction=""Down"" Xpos=""693"" Ypos=""289"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""254"" Delay=""114"" trace=""Ctrl+"" Button=""Left"" Direction=""Up"" Xpos=""693"" Ypos=""289"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""255"" Delay=""0"" Action=""BB: Control 'opening' (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""256"" Delay=""0"" Action=""BB: Control 'opening' end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""257"" Delay=""705"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""435"" Ypos=""185"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""258"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""435"" Ypos=""185"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""259"" Delay=""230"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""872"" Ypos=""920"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""260"" Delay=""97"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""872"" Ypos=""920"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""261"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""262"" Delay=""746"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""927"" Ypos=""597"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""263"" Delay=""90"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""927"" Ypos=""597"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""264"" Delay=""0"" Action=""MB: Closure OK: OK!"" />
{changeStep}

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""269"" Delay=""757"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Down"" Xpos=""823"" Ypos=""321"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""270"" Delay=""105"" trace=""Computing (LGC2)+"" Button=""Left"" Direction=""Up"" Xpos=""823"" Ypos=""321"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""271"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""1514"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""971"" Ypos=""794"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""98"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""971"" Ypos=""794"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""774"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""863"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""113"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""863"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""MB: Compensation: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""741"" trace=""button1+Yes"" Button=""Left"" Direction=""Down"" Xpos=""893"" Ypos=""571"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""138"" trace=""button1+Yes"" Button=""Left"" Direction=""Up"" Xpos=""893"" Ypos=""571"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""MB: Are you sure?: Yes"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""13"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />
{changeStep}
{changeStep}
{changeStep}
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "2 manget should be selected");
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "2 manget should be selected");
                var mag1 = gmp.MagnetsToBeAligned[0];
                var mag2 = gmp.MagnetsToBeAligned[1];
                var mag3 = gmp.MagnetsToBeAligned[2];
                Assert.AreEqual("LNE01.ZDSHR.0132", mag1._Name, "magnet to align 1 must be LNE01.ZDSHR.0132");
                Assert.AreEqual("LNE06.ZDSHR.0606", mag2._Name, "magnet to align 1 must be LNE06.ZDSHR.0606");
                Assert.AreEqual("LNE07.ZDSHR.0717", mag3._Name, "magnet to align 1 must be LNE07.ZDSHR.0717");

                var gmPolar = ggm.SubGuidedModules.Find(x => x.ObservationType == TSU.Common.ObservationType.Polar);
                var stations = gmPolar.StationModules;
                Assert.IsTrue(stations.Count == 1, "1 station module should exist");
                var station = gmPolar.StationModules[0] as TSU.Polar.Station.Module;
                Assert.IsTrue(station != null, "station is null");

                Assert.IsTrue(station.stationParameters.Setups.BestValues.SigmaZero < 5, "sigma0 problem for station");


                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM2_Step04_Acquire_Test()
        {
            int datagridOffset = 10;
            string startMeasure = @"
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""290"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""627"" Ypos=""340"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""98"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""627"" Ypos=""340"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Goto and measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Goto and measure end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""333"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""300"" Ypos=""271"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""300"" Ypos=""271"" Delta=""0"" />";
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM2_Step03_Take_REFS_AND_COMPUTE_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    {changeStep}
    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0606.E")}
    {Tools.ClickInDgvWithYOffset(729, 599, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence("209.266145", "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence("128.648249", "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence("1.862047", "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0606.T")}
    {Tools.ClickInDgvWithYOffset(729, 660, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence("206.844477", "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence("123.586799", "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence("2.236280", "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />


    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0606.S")}
    {Tools.ClickInDgvWithYOffset(729, 690, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence("193.453568", "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence("129.080412", "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence("1.836448", "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />


    {Tools.ClickInDgvWithYOffset(438, 540, datagridOffset)}

    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0717.E")}
    {Tools.ClickInDgvWithYOffset(729, 660, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence(221.396132, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(117.920037, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(2.912772, "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0717.T")}
    {Tools.ClickInDgvWithYOffset(729, 720, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence(224.466274, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(116.220787, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(3.210883, "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0717.S")}
    {Tools.ClickInDgvWithYOffset(729, 750, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence(211.917741, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(116.482582, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(3.159539, "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    {Tools.ClickInDgvWithYOffset(438, 599, datagridOffset)}


    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0132.E")}
    {Tools.ClickInDgvWithYOffset(729, 690, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence(120.831662, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(111.354760, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(4.567162, "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0132.T")}
    {Tools.ClickInDgvWithYOffset(729, 750, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence(125.677662, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(110.989492, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(4.717352, "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    {Tools.CommentWithinTheMacro("LNE06     ;ZDSHR.0132S")}
    {Tools.ClickInDgvWithYOffset(729, 780, datagridOffset)}
    {startMeasure}
    {Tools.GetKeySequence(118.901597, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""305"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(110.489468, "SplitContainer")}
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""12"" Delay=""281"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    {Tools.GetKeySequence(4.939223, "SplitContainer")}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""365"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""82"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""836"" Ypos=""932"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "2 manget should be selected");
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "2 manget should be selected");
                var mag1 = gmp.MagnetsToBeAligned[0];
                var mag2 = gmp.MagnetsToBeAligned[1];
                var mag3 = gmp.MagnetsToBeAligned[2];
                Assert.AreEqual("LNE01.ZDSHR.0132", mag1._Name, "magnet to align 1 must be LNE01.ZDSHR.0132");
                Assert.AreEqual("LNE06.ZDSHR.0606", mag2._Name, "magnet to align 1 must be LNE06.ZDSHR.0606");
                Assert.AreEqual("LNE07.ZDSHR.0717", mag3._Name, "magnet to align 1 must be LNE07.ZDSHR.0717");

                var gmPolar = ggm.SubGuidedModules.Find(x => x.ObservationType == TSU.Common.ObservationType.Polar);
                var stations = gmPolar.StationModules;
                Assert.IsTrue(stations.Count == 1, "1 station module should exist");
                var station = gmPolar.StationModules[0] as TSU.Polar.Station.Module;
                Assert.IsTrue(station != null, "station is null");

                Assert.IsTrue(station.stationParameters.Setups.BestValues.SigmaZero < 5, "sigma0 problem for station");
                var takens = station.StationTheodolite.MeasuresTaken.FindAll(x => x.GeodeRole == TSU.IO.SUSoft.Geode.Roles.A_as_Alignment);
                Assert.IsTrue(takens.Count == 9, $"looking for 9 measure not {takens.Count}");
                Assert.IsTrue(takens[0]._Point._Name == "LNE06.ZDSHR.0606.E", $"\"LNE06.ZDSHR.0606.E\"");
                Assert.IsTrue(takens[1]._Point._Name == "LNE06.ZDSHR.0606.T", $"\"LNE06.ZDSHR.0606.T\"");
                Assert.IsTrue(takens[2]._Point._Name == "LNE06.ZDSHR.0606.S", $"\"LNE06.ZDSHR.0606.S\"");
                Assert.IsTrue(takens[3]._Point._Name == "LNE07.ZDSHR.0717.E", $"\"LNE07.ZDSHR.0606.E\"");
                Assert.IsTrue(takens[4]._Point._Name == "LNE07.ZDSHR.0717.T", $"\"LNE07.ZDSHR.0717.T\"");
                Assert.IsTrue(takens[5]._Point._Name == "LNE07.ZDSHR.0717.S", $"\"LNE07.ZDSHR.0717.S\"");
                Assert.IsTrue(takens[6]._Point._Name == "LNE01.ZDSHR.0132.E", $"\"LNE01.ZDSHR.0132.E\"");
                Assert.IsTrue(takens[7]._Point._Name == "LNE01.ZDSHR.0132.T", $"\"LNE01.ZDSHR.0132.T\"");
                Assert.IsTrue(takens[8]._Point._Name == "LNE01.ZDSHR.0132.S", $"\"LNE01.ZDSHR.0132.S\"");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM2_Step05_BOC_compute_Test()
        {

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM2_Step04_Acquire_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""1402"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1328"" Ypos=""931"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""53"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1328"" Ypos=""931"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""1130"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1226"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""49"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1226"" Ypos=""432"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "2 manget should be selected");
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 3, "2 manget should be selected");
                var mag1 = gmp.MagnetsToBeAligned[0];
                var mag2 = gmp.MagnetsToBeAligned[1];
                var mag3 = gmp.MagnetsToBeAligned[2];
                Assert.AreEqual("LNE01.ZDSHR.0132", mag1._Name, "magnet to align 1 must be LNE01.ZDSHR.0132");
                Assert.AreEqual("LNE06.ZDSHR.0606", mag2._Name, "magnet to align 1 must be LNE06.ZDSHR.0606");
                Assert.AreEqual("LNE07.ZDSHR.0717", mag3._Name, "magnet to align 1 must be LNE07.ZDSHR.0717");

                var gmPolar = ggm.SubGuidedModules.Find(x => x.ObservationType == TSU.Common.ObservationType.Polar);
                var stations = gmPolar.StationModules;
                Assert.IsTrue(stations.Count == 1, "1 station module should exist");
                var station = gmPolar.StationModules[0] as TSU.Polar.Station.Module;
                Assert.IsTrue(station != null, "station is null");

                Assert.IsTrue(station.stationParameters.Setups.BestValues.SigmaZero < 5, "sigma0 problem for station");
                var takens = station.StationTheodolite.MeasuresTaken.FindAll(x => x.GeodeRole == TSU.IO.SUSoft.Geode.Roles.A_as_Alignment);
                Assert.IsTrue(takens.Count == 9, $"looking for 9 measure not {takens.Count}");
                Assert.IsTrue(takens[0]._Point._Name == "LNE06.ZDSHR.0606.E", $"\"LNE06.ZDSHR.0606.E\"");
                Assert.IsTrue(takens[1]._Point._Name == "LNE06.ZDSHR.0606.T", $"\"LNE06.ZDSHR.0606.T\"");
                Assert.IsTrue(takens[2]._Point._Name == "LNE06.ZDSHR.0606.S", $"\"LNE06.ZDSHR.0606.S\"");
                Assert.IsTrue(takens[3]._Point._Name == "LNE07.ZDSHR.0717.E", $"\"LNE07.ZDSHR.0606.E\"");
                Assert.IsTrue(takens[4]._Point._Name == "LNE07.ZDSHR.0717.T", $"\"LNE07.ZDSHR.0717.T\"");
                Assert.IsTrue(takens[5]._Point._Name == "LNE07.ZDSHR.0717.S", $"\"LNE07.ZDSHR.0717.S\"");
                Assert.IsTrue(takens[6]._Point._Name == "LNE01.ZDSHR.0132.E", $"\"LNE01.ZDSHR.0132.E\"");
                Assert.IsTrue(takens[7]._Point._Name == "LNE01.ZDSHR.0132.T", $"\"LNE01.ZDSHR.0132.T\"");
                Assert.IsTrue(takens[8]._Point._Name == "LNE01.ZDSHR.0132.S", $"\"LNE01.ZDSHR.0132.S\"");

                var boc = Tsunami2.Properties.BOC_Context;
                Assert.IsTrue(boc.ExistingResults.Count == 3, $"wrong boc count {boc.ExistingResults.Count}/3");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }
    }
}