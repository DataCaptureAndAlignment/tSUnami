﻿using Functional_Tests;
using MathNet.Numerics.LinearRegression;
using MathNet.Numerics.RootFinding;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using TSU.Common.Compute.Compensations.BeamOffsets;
using TSU.Common.Measures;
using TSU.Line.GuidedModules;
using TSU.Views;
using UnitTestGenerator;
using static TSU.Tools.Macro;

namespace Functional_Tests.BOC.GEM
{
    [TestClass]
    public class Manual
    {
        [TestMethod()]
        public void L2_BGM_Step00_dat_file_Test()
        {
            Tools.Artefact.GetArtefactFullPath(nameof(Functional.L0_START_STEP3_SplashScreen_Errors_Test) + ".tsut"); // used to fail if the splashscreen test had an error
            string datContent = @"LHC       ;MB.B10R8.T                      ;23704.43194; 4323.88178; 4646.44565;2332.11066;333.03385;99.999;  .011244;  .004544;226.5202; 1;A;24-NOV-2023; HCLBBL_000-IN001128;;Socket Circulant - Calculé avec le HCLBBL_000-IN001128 automatiquement avec autom_lhc_socket.fmx - old_slot_id = 248569;
LHC       ;MB.B10R8.S                      ;23704.43256; 4323.39704; 4646.65948;2332.11671;333.03982;14.300;  .011244;  .004544;226.5202; 1;O;24-NOV-2023; HCLBBL_000-IN001128;;Socket Circulant - Calculé avec le HCLBBL_000-IN001128 automatiquement avec autom_lhc_socket.fmx - old_slot_id = 248569;
LHC       ;MQML.10R8.E                     ;23708.68129; 4321.67872; 4642.77427;2332.14029;333.06123;  .745;  .019314;  .004578;226.6825; 1;A;24-NOV-2023; HCLQNCB001-CR000643;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - HCLQNCB001-CR000643 lhcsock.fmx par cpodevin- ref_angle recalculé 22-Mar-2006 - old_slot_id = 248574;
LHC       ;MQML.10R8.T                     ;23712.38304; 4320.65379; 4639.17839;2332.14849;333.06762;99.999;  .019314;  .004578;226.6825; 1;O;24-NOV-2023; HCLQNCB001-CR000643;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - HCLQNCB001-CR000643 lhcsock.fmx par cpodevin- ref_angle recalculé 22-Mar-2006 - old_slot_id = 248574;
LHC       ;MQML.10R8.S                     ;23712.38798; 4320.16956; 4639.38877;2332.15880;333.07784; 4.800;  .019314;  .004578;226.6825; 1;O;24-NOV-2023; HCLQNCB001-CR000643;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - HCLQNCB001-CR000643 lhcsock.fmx par cpodevin- ref_angle recalculé 22-Mar-2006 - old_slot_id = 248574;
LHC       ;MB.A11R8.E                      ;23717.03578; 4318.26815; 4635.14820;2332.17385;333.09051;  .301;  .011339;  .004612;226.8448; 1;A;24-NOV-2023;  ;;recalculation bumped coordinates 02-10-2015 - Socket Circulant -  - old_slot_id = 248578;
LHC       ;MB.A11R8.M                      ;23722.43772; 4316.05611; 4630.21999;2332.19685;333.11076;99.999;  .011339;  .004612;226.8448; 1;O;24-NOV-2023;  ;;recalculation bumped coordinates 02-10-2015 - Socket Circulant -  - old_slot_id = 248578;
LHC       ;MB.A11R8.T                      ;23727.83898; 4314.33003; 4625.07451;2332.21830;333.12954;99.999;  .011339;  .004612;226.8448; 1;O;24-NOV-2023;  ;;recalculation bumped coordinates 02-10-2015 - Socket Circulant -  - old_slot_id = 248578;
LHC       ;MB.A11R8.S                      ;23727.83969; 4313.84563; 4625.29106;2332.22441;333.13557;14.300;  .011339;  .004612;226.8448; 1;O;24-NOV-2023;  ;;recalculation bumped coordinates 02-10-2015 - Socket Circulant -  - old_slot_id = 248578;
LHC       ;MB.B11R8.E                      ;23732.69550; 4311.84799; 4620.86624;2332.24700;333.15568; 1.031;  .010488;  .004680;227.1694; 1;A;24-NOV-2023;  ;;Socket Circulant -  - old_slot_id = 357340;
LHC       ;MB.B11R8.M                      ;23738.09606; 4309.61127; 4615.95070;2332.27075;333.17669;99.999;  .010488;  .004680;227.1694; 1;O;24-NOV-2023;  ;;Socket Circulant -  - old_slot_id = 357340;
LHC       ;MB.B11R8.S                      ;23743.49458; 4307.37820; 4611.03574;2332.29597;333.19917;14.300;  .010488;  .004680;227.1694; 1;O;24-NOV-2023;  ;;Socket Circulant -  - old_slot_id = 357340;
LHC       ;MB.B11R8.T                      ;23743.49520; 4307.86083; 4610.81557;2332.29032;333.19360;99.999;  .010488;  .004680;227.1694; 1;O;24-NOV-2023;  ;;Socket Circulant -  - old_slot_id = 357340;
LHC       ;LECL.11R8.E                     ;23748.51574; 4305.29343; 4606.46840;2332.32372;333.22437;  .351;  .014570;  .004713;227.3317; 1;A;24-NOV-2023; HCLECL_000-CR000001;;recalculation bumped coordinates 02-10-2015 - Socket Circulant - HCLECL_000-CR000001_insert.csv HCLECL_000-CR000001 pwinkes - old_slot_id = 103783;
BEAM_LHC.B2    ;LECL.11R8.E                     ;23745.78120; 4306.69683; 4608.67229;2331.88548;332.78750;;;  .004713;227.3317;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;LECL.11R8.S                     ;23758.55590; 4301.37932; 4597.05707;2331.94569;332.84124;;;  .004713;227.3317;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;LECL.11R8.E                     ;23745.92564; 4306.52046; 4608.75306;2331.88820;332.79019;;;  .004713;227.3317;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;LECL.11R8.S                     ;23758.70034; 4301.20294; 4597.13784;2331.94841;332.84393;;;  .004713;227.3317;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
*FRAME RSTI_103783_LHC.LECL.11R8   4306.520460   4608.753060   2331.888050 0 0  227.331744 1
*FRAME RST_103783_LHC.LECL.11R8 0 0 0 -.30003889872 .84192964896 0 1
*FRAME RSTBI1_103783_LHC.LECL.11R8      0.000000      0.000000      0.000150 0 0 0 1
*FRAME RSTBI2_103783_LHC.LECL.11R8 0 0 0 0 0 0 1
*FRAME RSTB_103783_LHC.LECL.11R8 0 0 0 0 0 0 1
*FRAME RSTRI_103783_LHC.LECL.11R8     -0.001110     -0.001280     -0.001410 .00009966884 0 .00079735073 1 TX TY TZ RX RZ
*FRAME RSTR_103783_LHC.LECL.11R8 0 0 0 0 0 0 1 RY
*CALA
BEAM_LHC.B2.LECL.11R8.E                                         -0.194003      0.000008      0.000000   $23745.781 795735 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:25
BEAM_LHC.B2.LECL.11R8.S                                         -0.194003     12.774708      0.000000   $23758.556 795736 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:25
BEAM_LHC.LECL.11R8.E                                             0.000000      0.000000      0.000000   $23745.926 742255 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:25
BEAM_LHC.LECL.11R8.S                                             0.000007     12.774701      0.000003   $23758.700 742256 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:25
*OBSXYZ
LHC.LECL.11R8.E      0.170250      2.590100      0.421100 0.1 0.1 0.1   $23748.516 514677 paramètres RST 24-NOV-2023 10:30:25
LHC.LECL.11R8.M      0.171320      6.388450      0.421110 0.1 0.1 0.1   $23752.314 514678 paramètres RST 24-NOV-2023 10:30:25
LHC.LECL.11R8.S      0.169880     10.188400      0.421210 0.1 0.1 0.1   $23756.114 514680 paramètres RST 24-NOV-2023 10:30:25
LHC.LECL.11R8.T     -0.359800     10.185700      0.420410 0.1 0.1 0.1   $23756.111 514679 paramètres RST 24-NOV-2023 10:30:25
*INCLY Instr_Roll_Theo
LHC.LECL.11R8.E .92754482242 RF .09612958563
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LHC.B2    ; MCS.B11R8.E                     ;23745.32020; 4306.88872; 4609.09145;2331.88332;332.78557;;;  .004680;227.1694;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCS.B11R8.S                     ;23745.43019; 4306.84294; 4608.99144;2331.88383;332.78603;;;  .004680;227.1694;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MB.B11R8.E                      ;23730.80096; 4312.89924; 4622.30800;2331.81536;332.72499;;;  .004680;227.1694;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MB.B11R8.S                      ;23745.10095; 4306.97999; 4609.29080;2331.88228;332.78464;;;  .004680;227.1694;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MB.B11R8.E                      ;23730.94590; 4312.72266; 4622.38831;2331.81793;332.72753;;;  .004680;227.1694;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MB.B11R8.S                      ;23745.24589; 4306.80341; 4609.37111;2331.88485;332.78718;;;  .004680;227.1694;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCS.B11R8.S                     ;23745.57464; 4306.66656; 4609.07220;2331.88640;332.78857;;;  .004680;227.1694;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCS.B11R8.E                     ;23745.46465; 4306.71235; 4609.17221;2331.88588;332.78810;;;  .004680;227.1694;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
*FRAME RSTI_103782_LHC.MB.B11R8   4312.722660   4622.388310   2331.817930 0 0  227.007132 1
*FRAME RST_103782_LHC.MB.B11R8 0 0 0 -.29577354624 .84339387443 .16233804195 1
*FRAME RSTRI_103782_LHC.MB.B11R8     -0.001219     -0.001270     -0.001349 .00083695525 0 .00023595015 1 TX TY TZ RX RZ
*FRAME RSTR_103782_LHC.MB.B11R8 0 0 0 0 0 0 1 RY
*CALA
BEAM_LHC.B2.MCS.B11R8.E                                         -0.193442     14.519235      0.000004   $23745.320 677807 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:26
BEAM_LHC.B2.MCS.B11R8.S                                         -0.193166     14.629226     -0.000004   $23745.430 677808 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:26
BEAM_LHC.B2.MB.B11R8.E                                          -0.194002      0.000001     -0.000002   $23730.801 671591 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:26
BEAM_LHC.B2.MB.B11R8.S                                          -0.194007     14.299984     -0.000003   $23745.101 671592 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:26
BEAM_LHC.MB.B11R8.E                                              0.000000      0.000000      0.000000   $23730.946 684735 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:26
BEAM_LHC.MB.B11R8.S                                             -0.000005     14.299982      0.000000   $23745.246 684736 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:26
BEAM_LHC.MCS.B11R8.S                                             0.000840     14.628733      0.000000   $23745.575 690954 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:26
BEAM_LHC.MCS.B11R8.E                                             0.000555     14.518738     -0.000001   $23745.465 690953 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:26
*OBSXYZ
LHC.MB.B11R8.S      0.171360     12.548680      0.417090 0.1 0.1 0.1   $23743.495 201227 paramètres RST 24-NOV-2023 10:30:26
LHC.MB.B11R8.E      0.171730      1.749600      0.418650 0.1 0.1 0.1   $23732.696 199998 paramètres RST 24-NOV-2023 10:30:26
LHC.MB.B11R8.M      0.173070      7.150160      0.417110 0.1 0.1 0.1   $23738.096 203685 paramètres RST 24-NOV-2023 10:30:26
LHC.MB.B11R8.T     -0.359150     12.549300      0.418460 0.1 0.1 0.1   $23743.495 202456 paramètres RST 24-NOV-2023 10:30:26
*INCLY Instr_Roll_Theo
LHC.MB.B11R8.E .66769382008 RF -.16440132664
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LHC.B2    ; MB.A11R8.E                      ;23715.14046; 4319.31203; 4636.59511;2331.74319;332.66080;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MB.A11R8.S                      ;23729.44044; 4313.45924; 4623.54789;2331.80914;332.71946;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCS.A11R8.E                     ;23729.65971; 4313.36899; 4623.34808;2331.81016;332.72037;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCS.A11R8.S                     ;23729.76971; 4313.32371; 4623.24783;2331.81067;332.72082;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCD.11R8.E                      ;23714.77270; 4319.46169; 4636.93102;2331.74151;332.65931;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCD.11R8.S                      ;23714.83870; 4319.43483; 4636.87073;2331.74181;332.65958;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCDO.11R8.E                     ;23714.80121; 4319.45009; 4636.90498;2331.74154;332.65932;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCDO.11R8.S                     ;23714.80121; 4319.45009; 4636.90498;2331.74154;332.65932;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCO.11R8.E                      ;23714.77121; 4319.46230; 4636.93239;2331.74150;332.65930;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCO.11R8.S                      ;23714.83721; 4319.43544; 4636.87210;2331.74180;332.65957;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MB.A11R8.E                      ;23715.28640; 4319.13505; 4636.67451;2331.74576;332.66334;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MB.A11R8.S                      ;23729.58638; 4313.28225; 4623.62729;2331.81172;332.72201;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCS.A11R8.E                     ;23729.80515; 4313.19221; 4623.42793;2331.81273;332.72291;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCS.A11R8.S                     ;23729.91514; 4313.14693; 4623.32769;2331.81324;332.72337;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCD.11R8.E                      ;23714.91914; 4319.28451; 4637.00997;2331.74409;332.66186;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCD.11R8.S                      ;23714.98513; 4319.25765; 4636.94969;2331.74439;332.66213;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCO.11R8.S                      ;23714.98364; 4319.25826; 4636.95106;2331.74438;332.66212;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCDO.11R8.E                     ;23714.94764; 4319.27291; 4636.98394;2331.74412;332.66187;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCDO.11R8.S                     ;23714.94764; 4319.27291; 4636.98394;2331.74412;332.66187;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCO.11R8.E                      ;23714.91764; 4319.28512; 4637.01134;2331.74408;332.66185;;;  .004612;226.8448;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
*FRAME RSTI_103781_LHC.MB.A11R8   4319.135050   4636.674510   2331.745660 0 0  226.682456 1
*FRAME RST_103781_LHC.MB.A11R8 0 0 0 -.29144453179 .84492176189 .16233804195 1
*FRAME RSTBI1_103781_LHC.MB.A11R8      0.000000      0.000000      0.000100 0 0 0 1
*FRAME RSTBI2_103781_LHC.MB.A11R8 0 0 0 0 0 -.16233804195 1
*FRAME RSTB_103781_LHC.MB.A11R8 0 0 0 0 0 .16233804195 1
*FRAME RSTRI_103781_LHC.MB.A11R8     -0.001364     -0.001260     -0.001433 -.00052532298 0 .00042292952 1 TX TY TZ RX RZ
*FRAME RSTR_103781_LHC.MB.A11R8 0 0 0 0 0 0 1 RY
*CALA
BEAM_LHC.B2.MB.A11R8.E                                          -0.193992     -0.000003      0.000002   $23715.140 670727 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MB.A11R8.S                                          -0.193993     14.299977      0.000004   $23729.440 670728 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MCS.A11R8.E                                         -0.193429     14.519226      0.000005   $23729.660 676943 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MCS.A11R8.S                                         -0.193147     14.629228      0.000004   $23729.770 676944 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MCDO.11R8.E                                         -0.193128     -0.339241      0.000002   $23714.801 821067 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MCDO.11R8.S                                         -0.193128     -0.339241      0.000002   $23714.801 821068 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MCO.11R8.E                                          -0.193052     -0.369241      0.000003   $23714.771 675679 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MCO.11R8.S                                          -0.193219     -0.303240      0.000003   $23714.837 675680 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MCD.11R8.E                                          -0.193055     -0.367741      0.000003   $23714.773 674447 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.B2.MCD.11R8.S                                          -0.193223     -0.301741      0.000003   $23714.839 674448 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MB.A11R8.E                                              0.000000      0.000000      0.000000   $23715.286 683871 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MB.A11R8.S                                              0.000008     14.299985      0.000011   $23729.586 683872 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MCS.A11R8.E                                             0.000565     14.518737      0.000005   $23729.805 690089 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MCS.A11R8.S                                             0.000851     14.628730      0.000004   $23729.915 690090 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MCD.11R8.E                                              0.000940     -0.367254      0.000008   $23714.919 687593 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MCD.11R8.S                                              0.000771     -0.301254      0.000008   $23714.985 687594 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MCDO.11R8.E                                             0.000867     -0.338754      0.000008   $23714.948 819839 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MCDO.11R8.S                                             0.000867     -0.338754      0.000008   $23714.948 819840 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MCO.11R8.E                                              0.000943     -0.368754      0.000008   $23714.918 688825 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
BEAM_LHC.MCO.11R8.S                                              0.000775     -0.302754      0.000008   $23714.984 688826 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:27
*OBSXYZ
LHC.MB.A11R8.S      0.172510     12.553290      0.418510 0.1 0.1 0.1   $23727.840 201226 paramètres RST 24-NOV-2023 10:30:27
LHC.MB.A11R8.E      0.171820      1.749380      0.417790 0.1 0.1 0.1   $23717.036 199997 paramètres RST 24-NOV-2023 10:30:27
LHC.MB.A11R8.T     -0.358120     12.552580      0.419440 0.1 0.1 0.1   $23727.839 202455 paramètres RST 24-NOV-2023 10:30:27
LHC.MB.A11R8.M      0.172990      7.151320      0.415860 0.1 0.1 0.1   $23722.438 203684 paramètres RST 24-NOV-2023 10:30:27
*INCLY Instr_Roll_Theo
LHC.MB.A11R8.E .72186252327 RF -.11157589116
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LHC.B2    ; MCS.B10R8.E                     ;23706.25421; 4322.92837; 4644.71212;2331.70241;332.62457;;;  .004544;226.5202;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCS.B10R8.S                     ;23706.36420; 4322.88361; 4644.61164;2331.70291;332.62501;;;  .004544;226.5202;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MB.B10R8.E                      ;23691.73497; 4328.80378; 4657.98929;2331.63642;332.56602;;;  .004544;226.5202;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MB.B10R8.S                      ;23706.03496; 4323.01760; 4644.91238;2331.70141;332.62368;;;  .004544;226.5202;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MB.B10R8.E                      ;23691.88189; 4328.62639; 4658.06779;2331.63900;332.56857;;;  .004544;226.5202;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCS.B10R8.E                     ;23706.40064; 4322.75118; 4644.79107;2331.70498;332.62711;;;  .004544;226.5202;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCS.B10R8.S                     ;23706.51063; 4322.70642; 4644.69059;2331.70549;332.62756;;;  .004544;226.5202;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MB.B10R8.S                      ;23706.18188; 4322.84021; 4644.99089;2331.70398;332.62622;;;  .004544;226.5202;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
*FRAME RSTI_103779_LHC.MB.B10R8   4328.626390   4658.067790   2331.639000 0 0  226.357844 1
*FRAME RST_103779_LHC.MB.B10R8 0 0 0 -.28711551734 .84638598736 .16233804195 1
*FRAME RSTRI_103779_LHC.MB.B10R8     -0.001341     -0.001260     -0.001627 .00006232645 0 .00099722328 1 TX TY TZ RX RZ
*FRAME RSTR_103779_LHC.MB.B10R8 0 0 0 0 0 0 1 RY
*CALA
BEAM_LHC.B2.MCS.B10R8.E                                         -0.193447     14.519227      0.000006   $23706.254 677775 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.MCS.B10R8.S                                         -0.193172     14.629226      0.000003   $23706.364 677776 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.MB.B10R8.E                                          -0.194000     -0.000003     -0.000003   $23691.735 671559 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.MB.B10R8.S                                          -0.194014     14.299986      0.000010   $23706.035 671560 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MB.B10R8.E                                              0.000000      0.000000      0.000000   $23691.882 684703 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MCS.B10R8.E                                             0.000553     14.518738      0.000001   $23706.401 690921 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MCS.B10R8.S                                             0.000827     14.628737      0.000008   $23706.511 690922 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MB.B10R8.S                                             -0.000010     14.299980      0.000003   $23706.182 684704 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
*OBSXYZ
LHC.MB.B10R8.E      0.170920      1.749650      0.419210 0.1 0.1 0.1   $23693.632 199996 paramètres RST 24-NOV-2023 10:30:28
LHC.MB.B10R8.M      0.172250      7.148960      0.416570 0.1 0.1 0.1   $23699.031 203683 paramètres RST 24-NOV-2023 10:30:28
LHC.MB.B10R8.S      0.171530     12.550670      0.418440 0.1 0.1 0.1   $23704.433 201225 paramètres RST 24-NOV-2023 10:30:28
LHC.MB.B10R8.T     -0.358310     12.550050      0.419430 0.1 0.1 0.1   $23704.432 202454 paramètres RST 24-NOV-2023 10:30:28
*INCLY Instr_Roll_Theo
LHC.MB.B10R8.E .71580699599 RF -.11895176785
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
BEAM_LHC.B2    ; MQML.10R8.E                     ;23707.93321; 4322.24509; 4643.17846;2331.70998;332.63128;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MQML.10R8.S                     ;23712.73322; 4320.29168; 4638.79397;2331.73195;332.65079;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;BPM.10R8.E                      ;23707.18820; 4322.54827; 4643.85897;2331.70657;332.62825;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;BPM.10R8.S                      ;23707.18820; 4322.54827; 4643.85897;2331.70657;332.62825;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCBCB.10R8.E                    ;23712.92321; 4320.21436; 4638.62041;2331.73294;332.65169;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCBCB.10R8.S                    ;23713.82721; 4319.84647; 4637.79467;2331.73708;332.65536;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCBCV.10R8.S                    ;23713.82721; 4319.84646; 4637.79467;2331.73696;332.65524;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC.B2    ;MCBCV.10R8.E                    ;23712.92320; 4320.21435; 4638.62041;2331.73282;332.65157;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MQML.10R8.S                     ;23712.87963; 4320.11450; 4638.87293;2331.73453;332.65334;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;BPM.10R8.E                      ;23707.33463; 4322.37108; 4643.93792;2331.70914;332.63079;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;BPM.10R8.S                      ;23707.33463; 4322.37108; 4643.93792;2331.70914;332.63079;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MQML.10R8.E                     ;23708.07964; 4322.06790; 4643.25741;2331.71255;332.63382;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCBCB.10R8.E                    ;23713.06964; 4320.03718; 4638.69937;2331.73552;332.65424;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCBCB.10R8.S                    ;23713.97364; 4319.66929; 4637.87363;2331.73966;332.65791;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées théoriques dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCBCH.10R8.E                    ;23713.06964; 4320.03717; 4638.69937;2331.73540;332.65412;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
BEAM_LHC       ;MCBCH.10R8.S                    ;23713.97363; 4319.66928; 4637.87363;2331.73954;332.65779;;;  .004578;226.6825;;;24-NOV-2023; ;;coordonnées bumpées dans le système CCS au 24-NOV-2023 10:30:25;
*FRAME RSTI_103780_LHC.MQML.10R8   4322.067900   4643.257410   2331.712670 0 0  226.682456 1
*FRAME RST_103780_LHC.MQML.10R8 0 0 0 -.29144453179 .84492176189 0 1
*FRAME RSTBI1_103780_LHC.MQML.10R8      0.000000      0.000000     -0.000120 0 0 0 1
*FRAME RSTBI2_103780_LHC.MQML.10R8 0 0 0 0 0 0 1
*FRAME RSTB_103780_LHC.MQML.10R8 0 0 0 0 0 0 1
*FRAME RSTRI_103780_LHC.MQML.10R8     -0.001125     -0.001260     -0.001491 -.00026525879 0 -.00238732912 1 TX TY TZ RX RZ
*FRAME RSTR_103780_LHC.MQML.10R8 0 0 0 0 0 0 1 RY
*CALA
BEAM_LHC.B2.MQML.10R8.E                                         -0.194000     -0.000005      0.000005   $23707.933 680675 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.MQML.10R8.S                                         -0.193994      4.800000      0.000000   $23712.733 680676 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.BPM.10R8.E                                          -0.193996     -0.745004      0.000005   $23707.188 669395 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.BPM.10R8.S                                          -0.193996     -0.745004      0.000005   $23707.188 669396 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.MCBCB.10R8.E                                        -0.193995      4.990004      0.000000   $23712.923 814603 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.MCBCB.10R8.S                                        -0.193994      5.894004      0.000000   $23713.827 814604 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.MCBCV.10R8.E                                        -0.193995      4.990004      0.000000   $23712.923 673375 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.B2.MCBCV.10R8.S                                        -0.193994      5.894004      0.000000   $23713.827 673376 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MQML.10R8.S                                             0.000001      4.799992      0.000006   $23712.880 693822 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.BPM.10R8.E                                              0.000004     -0.744999      0.000001   $23707.335 682541 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.BPM.10R8.S                                              0.000004     -0.744999      0.000001   $23707.335 682542 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MQML.10R8.E                                             0.000000      0.000000      0.000000   $23708.080 693821 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MCBCB.10R8.E                                            0.000000      4.989996      0.000006   $23713.070 814297 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MCBCB.10R8.S                                            0.000001      5.893996      0.000005   $23713.974 814298 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MCBCH.10R8.E                                            0.000000      4.989996      0.000006   $23713.070 686435 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
BEAM_LHC.MCBCH.10R8.S                                            0.000001      5.893996      0.000005   $23713.974 686436 coordonnées théoriques dans le système RST au 24-NOV-2023 10:30:28
*OBSXYZ
LHC.MQML.10R8.E      0.164500      0.601650      0.422840 0.1 0.1 0.1   $23708.681 511735 paramètres RST 24-NOV-2023 10:30:28
LHC.MQML.10R8.S      0.165280      4.308340      0.424370 0.1 0.1 0.1   $23712.388 511736 paramètres RST 24-NOV-2023 10:30:28
LHC.MQML.10R8.T     -0.362750      4.303400      0.421090 0.1 0.1 0.1   $23712.383 511737 paramètres RST 24-NOV-2023 10:30:28
*INCLY Instr_Roll_Theo
LHC.MQML.10R8.E 1.229549603 RF .39544846738
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
";
            Tools.Artefact.CreateArtefact(fileContent: datContent, artefactName: "L2_BGM_Step00_dat_file.dat");

            string vertRabotContent = @"Vertical,,,,,,,,,,,,
Source = Database Accl(s): L4LT Category : S Cumul : -.085492 - 11.43788,PyRabot Version 1.4.4_beta2,20-Sep-2024~09:23:53,,,,,,,,,
Name,Rabot DCum,DCum,Smooth,Rough,Rough - Smooth,Displacement,Smooth-tol,Smooth+tol,Element,Point to move,Offset date,Offset comment
,,,,,,,,,,,,
TT41.MBG.411550.S,0,25739.393,-2.98,-2.98,0,0,-3.16,-2.8,LHC.MQ.20L1,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25739.393
,1.422273,25740.815,-2.924,-2.885,0.039,0,-3.104,-2.744,,,,
TT41.QTGF.411600.E,2.844547,25742.237,-2.869,-2.79,0.079,0,-3.049,-2.689,,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25742.237
,4.96106,25744.354,-2.862,,,,-3.042,-2.682,,,,
TT41.MBG.411550.E,7.077573,25746.471,-2.855,-2.94,-0.085,0,-3.035,-2.675,LHC.MB.C20L1,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25746.471
LHC.MB.C20L1.M,12.476603,25751.87,-2.845,-3.02,-0.175,0,-3.025,-2.665,,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25751.870
TT41.QTGF.411600.S,17.87647,25757.271,-2.842,-2.82,0.022,0,-3.022,-2.662,,,05.04.2021,LS2-Mesure ini Arc81 : cala GITL-Q8 - CUM = 25757.271
,20.304832,25759.7,-2.843,,,,-3.023,-2.663,,,,
TT41.BPG.411605.E,22.733195,25762.129,-2.844,-2.766,0.078,0,-3.024,-2.664,LHC.MB.B20L1,,13.04.2021,LS2 Depla_1 vertical Arc 81 - CUM = 25762.129
TT41.BPG.411605.S,28.133361,25767.529,-2.852,-2.951,-0.099,0,-3.032,-2.672,,,13.04.2021,LS2 Depla_1 vertical Arc 81 - CUM = 25767.529
LHC.MB.B20L1.S,33.535721,25772.932,-2.864,-2.969,-0.105,0,-3.044,-2.684,,,13.04.2021,LS2 Depla_1 vertical Arc 81 - CUM = 25772.932
,35.963821,25775.3605,-2.87,,,,-3.05,-2.69,,,,
LHC.MQ.19R1.E,1783.348223,863.248,-0.921,-0.807,0.114,0,-1.101,-0.741,LHC.MQ.19R1,,23.03.2021,LS2 Depla_1 vertical Arc 12 - CUM = 863.248
,1784.770615,864.6705,-0.904,-0.847,0.057,0,-1.084,-0.724,,,,
LHC.MQ.19R1.S,1786.193008,866.093,-0.888,-0.888,0,0,-1.068,-0.708,,,23.03.2021,LS2 Depla_1 vertical Arc 12 - CUM = 866.093
,1788.309524,868.21,-0.863,,,,-1.043,-0.683,,,,
LHC.MB.A20R1.E,1790.42604,870.327,-0.838,-0.86,-0.022,0,-1.018,-0.658,LHC.MB.A20R1,,26.02.2021,LS2-Mesure ini ArcS12 : cala GITL / Q8R1 - CUM = 870.327
LHC.MB.A20R1.M,1795.824254,875.726,-0.776,-0.87,-0.094,0,-0.956,-0.596,,,26.02.2021,LS2-Mesure ini ArcS12 : cala GITL / Q8R1 - CUM = 875.726
LHC.MB.A20R1.S,1801.224133,881.126,-0.717,-0.74,-0.023,0,-0.897,-0.537,,,26.02.2021,LS2-Mesure ini ArcS12 : cala GITL / Q8R1 - CUM = 881.126
,1803.653169,883.556,-0.692,,,,-0.872,-0.512,,,,
LHC.MB.A11R8.E,1806.082205,885.986,-0.668,-0.76,-0.092,0,-0.848,-0.488,LHC.MB.B20R1,,25.03.2021,LS2 Depla_1 vertical Arc 12 - CUM = 885.986
LHC.MB.A11R8.M,1811.482475,891.387,-0.619,-0.642,-0.023,0,-0.799,-0.439,,,25.03.2021,LS2 Depla_1 vertical Arc 12 - CUM = 891.387
LHC.MB.A11R8.T,1816.883213,896.788,-0.579,-0.478,0.101,0,-0.759,-0.399,,,25.03.2021,LS2 Depla_1 vertical Arc 12 - CUM = 896.788
,1819.311721,899.218,-0.566,,,,-0.746,-0.386,,,,
LHC.MB.B10R8.T,1821.740229,901.648,-0.553,-0.73,-0.177,0,-0.733,-0.373,LHC.MB.C20R1,,26.02.2021,LS2-Mesure ini ArcS12 : cala GITL / Q8R1 - CUM = 901.648
LHC.MB.B10R8.S,1827.139154,907.047,-0.536,-0.53,0.006,0,-0.716,-0.356,,,26.02.2021,LS2-Mesure ini ArcS12 : cala GITL / Q8R1 - CUM = 907.047
LHC.MQML.10R8.E,1832.538535,912.447,-0.535,-0.57,-0.035,0,-0.715,-0.355,,,26.02.2021,LS2-Mesure ini ArcS12 : cala GITL / Q8R1 - CUM = 912.447
,1834.664668,914.5735,-0.542,,,,-0.722,-0.362,,,,
LHC.MQML.10R8.S,1836.790801,916.7,-0.548,-0.55,-0.002,0,-0.728,-0.368,LHC.MQ.20R1,,26.02.2021,LS2-Mesure ini ArcS12 : cala GITL / Q8R1 - CUM = 916.700
,1838.213123,918.1225,-0.504,-0.505,-0.001,0,-0.684,-0.324,,,,
LHC.MQML.10R8.T,1839.635446,919.545,-0.46,-0.46,0,0,-0.64,-0.28,,,26.02.2021,LS2-Mesure ini ArcS12 : cala GITL / Q8R1 - CUM = 919.545
";

            string radialRabotContent = @"Radial,,,,,,,,,,,,
Source = Database Accl(s): L4LT Category : S Cumul : -.085492 - 11.43788,PyRabot Version 1.4.4_beta2,20-Sep-2024~09:23:53,,,,,,,,,
Name,Rabot DCum,DCum,Smooth,Rough,Rough - Smooth,Displacement,Smooth-tol,Smooth+tol,Element,Point to move,Offset date,Offset comment
,,,,,,,,,,,,
TT41.MBG.411550.E,0,25739.393,-2.467,-2.467,0,0,-2.647,-2.287,LHC.MQ.20L1,,03.05.2021,LS2 Depla_1 Radial Arc 81 - CUM = 25739.393
,1.422273,25740.815,-2.46,-2.47,-0.009,0,-2.64,-2.28,,,,
TT41.MBG.411550.S,2.844547,25742.237,-2.453,-2.472,-0.019,0,-2.633,-2.273,,,03.05.2021,LS2 Depla_1 Radial Arc 81 - CUM = 25742.237
,4.96106,25744.354,-2.453,,,,-2.633,-2.273,,,,
TT41.QTGF.411600.E,7.077573,25746.471,-2.453,-2.343,0.11,0,-2.633,-2.273,LHC.MB.C20L1,,05.05.2021,LS2 Depla_2 radial Arc 81 - CUM = 25746.471
,12.477021,25751.871,-2.453,-2.397,0.056,0,-2.633,-2.273,,,,
TT41.QTGF.411600.S,17.876469,25757.271,-2.453,-2.451,0.002,0,-2.633,-2.273,,,05.05.2021,LS2 Depla_2 radial Arc 81 - CUM = 25757.271
,20.304832,25759.7,-2.452,,,,-2.632,-2.272,,,,
TT41.BPG.411605.E,22.733194,25762.129,-2.452,-2.484,-0.032,0,-2.632,-2.272,LHC.MB.B20L1,,05.05.2021,LS2 Depla_2 radial Arc 81 - CUM = 25762.129
,28.134457,25767.5305,-2.451,-2.469,-0.018,0,-2.631,-2.271,,,,
TT41.BPG.411605.S,33.53572,25772.932,-2.45,-2.454,-0.004,0,-2.63,-2.27,,,05.05.2021,LS2 Depla_2 radial Arc 81 - CUM = 25772.932
,35.96382,25775.3605,-2.449,,,,-2.629,-2.269,,,,
LHC.MQ.19R1.E,1783.348202,863.248,1.228,1.195,-0.033,0,1.048,1.408,LHC.MQ.19R1,,14.04.2021,LS2 Depla_2 radial Arc 12 - CUM = 863.248
,1784.770594,864.6705,1.235,1.238,0.002,0,1.055,1.415,,,,
LHC.MQ.19R1.S,1786.192986,866.093,1.242,1.28,0.038,0,1.062,1.422,,,14.04.2021,LS2 Depla_2 radial Arc 12 - CUM = 866.093
,1788.309503,868.21,1.253,,,,1.073,1.433,,,,
LHC.MB.A11R8.E,1790.426019,870.327,1.263,1.238,-0.025,0,1.083,1.443,LHC.MB.A20R1,,14.04.2021,LS2 Depla_2 radial Arc 12 - CUM = 870.327  
,1795.825065,875.7265,1.287,1.273,-0.014,0,1.107,1.467,,,,
LHC.MB.A11R8.M,1801.224111,881.126,1.312,1.308,-0.004,0,1.132,1.492,,,14.04.2021,LS2 Depla_2 radial Arc 12 - CUM = 881.126
,1803.653148,883.556,1.321,,,,1.141,1.501,,,,
LHC.MB.A11R8.T,1806.082184,885.986,1.331,1.334,0.003,0,1.151,1.511,LHC.MB.B20R1,,31.03.2021,LS2 Depla_1 radial Arc 12 - CUM = 885.986
,1811.482688,891.387,1.349,1.394,0.045,0,1.169,1.529,,,,
LHC.MB.B10R8.T,1816.883191,896.788,1.367,1.455,0.088,0,1.187,1.547,,,31.03.2021,LS2 Depla_1 radial Arc 12 - CUM = 896.788
,1819.311699,899.218,1.373,,,,1.193,1.553,,,,
LHC.MB.B10R8.S,1821.740208,901.648,1.379,1.42,0.041,0,1.199,1.559,LHC.MB.C20R1,,26.02.2021,LS2-Mesure ini Arc12 - CUM = 901.648
,1827.139361,907.0475,1.387,1.385,-0.002,0,1.207,1.567,,,,
LHC.MQML.10R8.E,1832.538514,912.447,1.395,1.35,-0.045,0,1.215,1.575,,,26.02.2021,LS2-Mesure ini Arc12 - CUM = 912.447
,1834.664646,914.5735,1.395,,,,1.215,1.575,,,,
LHC.MQML.10R8.S,1836.790779,916.7,1.396,1.4,0.004,0,1.216,1.576,LHC.MQ.20R1,,26.02.2021,LS2-Mesure ini Arc12 - CUM = 916.700
,1838.213102,918.1225,1.373,1.375,0.002,0,1.193,1.553,,,,
LHC.MQML.10R8.T,1839.635424,919.545,1.35,1.35,0,0,1.17,1.53,,,26.02.2021,LS2-Mesure ini Arc12 - CUM = 919.545
";

            using (StreamWriter writer = new StreamWriter($@"C:\data\tsunami\Theoretical Files\vertical_rabot_file.csv"))
            {
                // Write the content of the string variable to the file
                writer.WriteLine(vertRabotContent);
            }
            using (StreamWriter writer = new StreamWriter($@"C:\data\tsunami\Theoretical Files\radial_rabot_file.csv"))
            {
                // Write the content of the string variable to the file
                writer.WriteLine(radialRabotContent);
            }

            Assert.IsTrue(File.Exists($@"C:\data\tsunami\Theoretical Files\vertical_rabot_file.csv"));
            Assert.IsTrue(File.Exists($@"C:\data\tsunami\Theoretical Files\radial_rabot_file.csv"));
        }

        /// <summary>
        /// start a GEM module and rogrammatically load a dat file (because macro do not record keys in opening and saving windows :(
        /// 
        /// </summary>
        [TestMethod()]
        public void L2_BGM_Step01_Start_with_dat_file_Test()
        {
            var datPath = Tools.Artefact.GetArtefactFullPath("L2_BGM_Step00_dat_file.dat"); // here first so that the test fails if the file is not existing

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
    <Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
      IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
      <KeyStrokesAndMouseClicks>
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""234"" Ypos=""281"" Delta=""0"" />
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""234"" Ypos=""281"" Delta=""0"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Guided modules (beginning)"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Guided modules end"" />
	  
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""200"" Button=""Left"" Direction=""Down"" Xpos=""371"" Ypos=""300"" Delta=""0"" />
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""371"" Ypos=""300"" Delta=""0"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Element alignment (beginning)"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Element alignment end"" />
	  
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""1000"" Button=""Left"" Direction=""Down"" Xpos=""1316"" Ypos=""960"" Delta=""0"" />
	      <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""100"" Button=""Left"" Direction=""Up"" Xpos=""1316"" Ypos=""960"" Delta=""0"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Next step (beginning)"" />
	      <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""356"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""14"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""518"" Ypos=""669"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""3"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{M}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""31"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{O}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""5"" Delay=""27"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />

      </KeyStrokesAndMouseClicks>
    </Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var mms = tsunami.MeasurementModules;
                Assert.IsTrue(mms.Count > 0, $"No measurement modules");

                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;
                Assert.IsTrue(ggm != null, "Guided Group Module object is NULL");

                var sgm = ggm.SubGuidedModules;
                Assert.IsTrue(sgm.Count > 0, $"no sub Guided Group Module found in {ggm}");

                var gmp = sgm[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(ggm != null, $"sub Guided Group Module are missing suppossed to be 5 we have: {sgm.Count}");

                ggm._ElementManager.AddElementsFromFile(new FileInfo(datPath), TSU.Common.Elements.Coordinates.CoordinateSystemsTsunamiTypes.CCS, true, TSU.Common.Elements.Point.Types.Reference, updateView: false);
                tsunami.BOC_Context.SelectedGeodeFilePaths.Add(datPath);

                Assert.IsTrue(tsunami != null, "TSUNAMI oject is NULL");
                Assert.IsTrue(tsunami.Points.Count == 14, "Expecting 14 points in BGM_Step1_Start_with_dat_file_Test.tsu");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step02_BOC_With_Rabot()
        {
            //var VerticalRabot = Tools.Artefact.GetArtefactFullPath("vertical_rabot_file.csv");
            //var RadialRabot = Tools.Artefact.GetArtefactFullPath("radial_rabot_file.csv");

            var testMethod = MethodBase.GetCurrentMethod();

            F_Test test = new F_Test(testMethod)
            {
                NewPlacementForMessage = true,
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step01_Start_with_dat_file_Test) + ".tsut"),

                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"">
  <WorkingParameters Height=""1038"" Width=""1920"" FunctionKey=""F4"" SpeedFactor=""1"" />
  <KeyStrokesAndMouseClicks>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""411"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""637"" Ypos=""333"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""13"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""637"" Ypos=""333"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)""/>

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{V}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{E}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{C}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{L}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{_}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{B}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{O}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{_}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{F}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{L}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{E}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{C}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{S}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{V}}"" Target=""TextBox"" />
<IInputActivity xsi:type=""KeyStroke____"" Pos.=""4"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button""/>

<IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""360"" trace=""button1+Expected Offset"" Button=""Left"" Direction=""Down"" Xpos=""858"" Ypos=""627"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""16"" trace=""button1+Expected Offset"" Button=""Left"" Direction=""Up"" Xpos=""858"" Ypos=""627"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""MB: Please choose type of Rabot offset displacement: Expected Offset""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Import offsets/displacements end""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""397"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Down"" Xpos=""762"" Ypos=""364"" Delta=""0""/>
<IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""73"" trace=""Import offsets/displacements+"" Button=""Left"" Direction=""Up"" Xpos=""762"" Ypos=""364"" Delta=""0""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Import offsets/displacements (beginning)""/>
<IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Import offsets/displacements end""/>

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{D}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{L}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{_}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{R}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{B}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{O}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{T}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""5"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{_}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{F}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{I}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{L}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{E}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""9"" Delay=""8"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{.}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""10"" Delay=""4"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{C}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""11"" Delay=""6"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{S}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""8"" Delay=""7"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{V}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""13"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""Button""/>
  </KeyStrokesAndMouseClicks>
</Macro>"

            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.AreEqual(gmp.RabotStrategy, TSU.Common.Compute.Rabot.Strategy.Expected_Offset, "Rabot Display Type is not defined");
                Assert.IsTrue(gmp.VerticalRabot.Count > 0, "Vertical Rabot Data has not been inserted");
                Assert.IsTrue(gmp.RadialRabot.Count > 0, "Radial Rabot Data has not been inserted");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step03_Setup_to_polar_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step02_BOC_With_Rabot) + ".tsut"),
                MacroXmlContent = @"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""L2_P_NF_Step1_Administrative_setup_with_T2_Test"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""335"" trace=""Open Operation Manager to select one+"" Button=""Left"" Direction=""Down"" Xpos=""581"" Ypos=""341"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""Open Operation Manager to select one+"" Button=""Left"" Direction=""Up"" Xpos=""581"" Ypos=""341"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Open Operation Manager to select one (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""326"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""175"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""10"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""175"" Ypos=""298"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""316"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""213"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""213"" Ypos=""330"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""316"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""365"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""365"" Ypos=""355"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""341"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""885"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""106"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""885"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""MB: Operation Selection: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Open Operation Manager to select one end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""1360"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1318"" Ypos=""934"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""83"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1318"" Ypos=""934"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""317"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""541"" Ypos=""443"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""122"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""541"" Ypos=""443"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""302"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""532"" Ypos=""473"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""73"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""532"" Ypos=""473"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""337"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1335"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""138"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1335"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""25"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""386"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""958"" Ypos=""586"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""85"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""958"" Ypos=""586"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""MB: MQML.10R8 Magnet serial number check: OK"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""333"" trace=""button1+I know"" Button=""Left"" Direction=""Down"" Xpos=""864"" Ypos=""571"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""102"" trace=""button1+I know"" Button=""Left"" Direction=""Up"" Xpos=""864"" Ypos=""571"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""MB: Wrong serial number: I know"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""386"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Down"" Xpos=""1164"" Ypos=""843"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""138"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Up"" Xpos=""1164"" Ypos=""843"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""BB: Polar Alignment (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Polar Alignment end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[0] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.OperationManager.SelectedOperation.value > 0 , "2 manget should be selected");
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step04_Setup_polar_station_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step03_Setup_to_polar_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""393"" trace=""orientationStation+"" Button=""Left"" Direction=""Down"" Xpos=""1613"" Ypos=""280"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""88"" trace=""orientationStation+"" Button=""Left"" Direction=""Up"" Xpos=""1613"" Ypos=""280"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Station Orientation only (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""210"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""194"" Ypos=""443"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""194"" Ypos=""443"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""270"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""844"" Ypos=""939"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""98"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""844"" Ypos=""939"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""MB: Point stationned: Select"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""226"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""885"" Ypos=""580"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""105"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""885"" Ypos=""580"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""MB: Instrument height: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Station Orientation only end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""294"" trace=""Select Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""1465"" Ypos=""446"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""89"" trace=""Select Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""1465"" Ypos=""445"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Select Instrument (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""286"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""270"" Ypos=""613"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""270"" Ypos=""613"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""98"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""270"" Ypos=""613"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""270"" Ypos=""613"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""254"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""205"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""205"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""253"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""331"" Ypos=""556"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""331"" Ypos=""556"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""266"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""846"" Ypos=""661"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""137"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""846"" Ypos=""661"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""MB: Select: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""27"" Delay=""0"" Action=""BB: Select Instrument end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""205"" trace=""Number of measurement+"" Button=""Left"" Direction=""Down"" Xpos=""1493"" Ypos=""693"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""97"" trace=""Number of measurement+"" Button=""Left"" Direction=""Up"" Xpos=""1493"" Ypos=""693"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Number of measurement (beginning)"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""290"" trace=""button4+1"" Button=""Left"" Direction=""Down"" Xpos=""1062"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""90"" trace=""button4+1"" Button=""Left"" Direction=""Up"" Xpos=""1062"" Ypos=""585"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""MB: Number of measurement: 1"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Number of measurement end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""262"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Down"" Xpos=""1604"" Ypos=""638"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""97"" trace=""Switch between 1 face and double face+"" Button=""Left"" Direction=""Up"" Xpos=""1604"" Ypos=""638"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""BB: Switch between 1 face and double face (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""38"" Delay=""0"" Action=""BB: Switch between 1 face and double face end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""39"" Delay=""286"" trace=""Change Reflector+"" Button=""Left"" Direction=""Down"" Xpos=""1491"" Ypos=""549"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""90"" trace=""Change Reflector+"" Button=""Left"" Direction=""Up"" Xpos=""1493"" Ypos=""549"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""41"" Delay=""0"" Action=""BB: Change Reflector (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""273"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""254"" Ypos=""287"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""82"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""254"" Ypos=""287"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""277"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""904"" Ypos=""920"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""74"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""904"" Ypos=""920"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""46"" Delay=""0"" Action=""MB: Choose a Reflector: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""47"" Delay=""0"" Action=""BB: Change Reflector end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""221"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1316"" Ypos=""940"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""98"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1316"" Ypos=""940"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""50"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""229"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""517"" Ypos=""785"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""517"" Ypos=""785"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""262"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1405"" Ypos=""949"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""82"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1405"" Ypos=""949"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""56"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""57"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""273"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1405"" Ypos=""949"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""82"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1405"" Ypos=""949"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""60"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""61"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""62"" Delay=""253"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""547"" Ypos=""279"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""63"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""547"" Ypos=""279"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""223"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""771"" Ypos=""722"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""51"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""771"" Ypos=""722"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""259"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""960"" Ypos=""724"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""51"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""960"" Ypos=""724"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""MB: Ready to measure? with the following settings?: OK"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""274"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""382"" Ypos=""182"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""97"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""382"" Ypos=""182"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""66"" Delay=""294"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""856"" Ypos=""922"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""67"" Delay=""97"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""856"" Ypos=""922"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""68"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""69"" Delay=""277"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""944"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""70"" Delay=""98"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""944"" Ypos=""587"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""71"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""72"" Delay=""246"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""1043"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""114"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""1043"" Ypos=""647"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""74"" Delay=""0"" Action=""MB: Point successfully measured: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""75"" Delay=""290"" trace=""Next step blocked+"" Button=""Left"" Direction=""Down"" Xpos=""1309"" Ypos=""935"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""76"" Delay=""73"" trace=""Next step blocked+"" Button=""Left"" Direction=""Up"" Xpos=""1309"" Ypos=""935"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""77"" Delay=""0"" Action=""BB: Next step blocked (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""78"" Delay=""266"" trace=""button2+Force next step"" Button=""Left"" Direction=""Down"" Xpos=""1151"" Ypos=""480"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""79"" Delay=""129"" trace=""button2+Force next step"" Button=""Left"" Direction=""Up"" Xpos=""1151"" Ypos=""480"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""80"" Delay=""0"" Action=""MB: Next step blocked: Force next step"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""81"" Delay=""0"" Action=""BB: Next step blocked end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""82"" Delay=""289"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""746"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""83"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""746"" Ypos=""254"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""84"" Delay=""0"" Action=""BB: Control 'opening' (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""85"" Delay=""0"" Action=""BB: Control 'opening' end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""86"" Delay=""289"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""405"" Ypos=""183"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""87"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""405"" Ypos=""183"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""88"" Delay=""233"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""891"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""89"" Delay=""105"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""891"" Ypos=""925"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""90"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""91"" Delay=""254"" trace=""button1+Continue"" Button=""Left"" Direction=""Down"" Xpos=""977"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""92"" Delay=""74"" trace=""button1+Continue"" Button=""Left"" Direction=""Up"" Xpos=""977"" Ypos=""602"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""93"" Delay=""0"" Action=""MB: Big differences in distances (station &lt;-&gt; point): Continue"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""94"" Delay=""217"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""976"" Ypos=""611"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""95"" Delay=""90"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""976"" Ypos=""611"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""96"" Delay=""0"" Action=""MB: Closure OK: OK!"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""97"" Delay=""253"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1315"" Ypos=""940"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""98"" Delay=""106"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1315"" Ypos=""940"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""99"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""100"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""101"" Delay=""122"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""721"" Ypos=""284"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""102"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""721"" Ypos=""284"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""103"" Delay=""0"" Action=""BB: Computing (LGC2) (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""104"" Delay=""1261"" trace=""button1+OK!"" Button=""Left"" Direction=""Down"" Xpos=""965"" Ypos=""711"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""105"" Delay=""90"" trace=""button1+OK!"" Button=""Left"" Direction=""Up"" Xpos=""965"" Ypos=""711"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""106"" Delay=""0"" Action=""MB: Computing results (LGC adjustment): OK!"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""107"" Delay=""1274"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""903"" Ypos=""738"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""108"" Delay=""73"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""903"" Ypos=""738"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""109"" Delay=""0"" Action=""MB: Compensation: Valid"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""110"" Delay=""0"" Action=""BB: Computing (LGC2) end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""111"" Delay=""1274"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1286"" Ypos=""941"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""112"" Delay=""90"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1286"" Ypos=""941"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""113"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""114"" Delay=""0"" Action=""BB: Next step end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };

            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;
                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                var station = gmp._ActiveStationModule._Station as TSU.Polar.Station;
                var param2 = station.Parameters2;
                Assert.IsTrue(param2._StationPoint._Name == "LHC.MQML.10R8.S");

                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                Assert.IsTrue(param2._IsSetup, "station not setup");


                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step05_Acquire_Test()
        {
            int datagridOffset = 30;
            string startMeasure = @"
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""790"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""627"" Ypos=""340"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""98"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""627"" Ypos=""340"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""5"" Delay=""0"" Action=""BB: Goto and measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""6"" Delay=""0"" Action=""BB: Goto and measure end"" />";

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step04_Setup_polar_station_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""777"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1285"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1285"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""200"" Action=""BB: Next step end"" />

    {Tools.CommentWithinTheMacro("LHC.MB.A11R8.E")}
    {Tools.ClickInDgvWithYOffset(693, 598, datagridOffset)}
    {startMeasure}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""11"" Delay=""769"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""549"" Ypos=""434"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""549"" Ypos=""434"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""389"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""851"" Ypos=""690"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""9"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""851"" Ypos=""690"" Delta=""0"" />


    {Tools.CommentWithinTheMacro("LHC.MB.A11R8.T")}
    {Tools.ClickInDgvWithYOffset(693, 690, datagridOffset)}
    {startMeasure}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""785"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""554"" Ypos=""188"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""6"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""554"" Ypos=""188"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""354"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""866"" Ypos=""910"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""9"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""866"" Ypos=""910"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""26"" Delay=""0"" Action=""MB: Enter values: Valid"" />


    {Tools.CommentWithinTheMacro("LHC.MB.A11R8.S")}
    {Tools.ClickInDgvWithYOffset(693, 720, datagridOffset)}
    {startMeasure}
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""754"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""562"" Ypos=""180"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""562"" Ypos=""180"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""366"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""860"" Ypos=""921"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""9"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""860"" Ypos=""921"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""37"" Delay=""0"" Action=""MB: Enter values: Valid"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");

                Assert.IsTrue(tsunami.MeasurementModules != null && tsunami.MeasurementModules.Count > 0, "Measurement module not found");

                TSU.Common.Guided.Group.Module ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                Assert.IsNotNull(ggm, "Measurement module is not a TSU.Common.Guided.Group.Module");

                Gathering.GetDataFrom(tsunami.MeasurementModules, "LHC.MB.A11R8", DateTime.MinValue, out var gatheredmeasures);


                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                var m1 = gmp.StationModules[0]._Station.MeasuresTaken[2];
                Assert.IsTrue(m1 != null, "mE missing");
                Assert.AreEqual("LHC.MB.A11R8.E", m1._Point._Name);


                var m2 = gmp.StationModules[0]._Station.MeasuresTaken[3];
                Assert.IsTrue(m2 != null, "mT missing");
                Assert.AreEqual("LHC.MB.A11R8.T", m2._Point._Name);


                var m3 = gmp.StationModules[0]._Station.MeasuresTaken[4];
                Assert.IsTrue(m3 != null, "mS missing");
                Assert.AreEqual("LHC.MB.A11R8.S", m3._Point._Name);

                Assert.IsTrue(gatheredmeasures != null && gatheredmeasures.Polar != null && gatheredmeasures.Polar.Count > 0, "No polar measure gathered");

                var measuresPolaire = gatheredmeasures.Polar[0].Item2;
                var station = gatheredmeasures.Polar[0];

                Assert.IsTrue(measuresPolaire != null && measuresPolaire.Count > 2, "Not enough polar measures gathered");

                TSU.Polar.Measure measure = measuresPolaire[2].Measure as TSU.Polar.Measure;

                Assert.IsNotNull(measure != null, "The 3rd measure is not a TSU.Polar.Measure");

                Assert.AreEqual(100.631, measure.Angles.Corrected.Horizontal.Value, 0.001);

                tsunami.ValueToDisplayType = TSU.Polar.GuidedModules.Steps.StakeOut.DisplayTypes.Offsets; // set it for next step rabot

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step06_BOC_compute_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step05_Acquire_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""277"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1285"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1285"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""265"" Button=""Left"" Direction=""Down"" Xpos=""1252"" Ypos=""418"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""122"" Button=""Left"" Direction=""Up"" Xpos=""1252"" Ypos=""418"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />
 
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""3204"" Button=""Left"" Direction=""Down"" Xpos=""1374"" Ypos=""967"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""3021"" Button=""Left"" Direction=""Up"" Xpos=""1374"" Ypos=""967"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;


                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                Assert.IsTrue(tsunami.BOC_Context.ExistingResults.Count == 1, "1 BOC results is expected and missing");
                var lastResults = tsunami.BOC_Context.ExistingResults[0];
                Assert.IsTrue(lastResults.Parameters.PointsToUseAsOBSXYZ.FindAll(x => x.Used == true).Count == 3, "3 points expected as OBSXYZ");
                Assert.IsTrue(lastResults.BeamPoints.Count == 20, "20 beam computed points are expected and missing");
                Assert.AreEqual(4319.2582, lastResults.BeamPoints[19]._Coordinates.Ccs.X.Value, 0.0001, "X value of estimated beam point is wrong");


                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 15, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 37, "wrong rows count in dgv");


                // header
                int column = 2;
                Assert.AreEqual("o(Rbv) [mm]", dgv.Columns[column].HeaderText, "wrong header ORbv [mm]"); column++;
                Assert.AreEqual("o(Lbv) [mm]", dgv.Columns[column].HeaderText, "wrong header OLbv [mm]"); column++;
                Assert.AreEqual("o(Hbv) [mm]", dgv.Columns[column].HeaderText, "wrong header OHbv [mm]"); column++;
                Assert.AreEqual("ExpO Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header EORbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;
                Assert.AreEqual("ExpO Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header Exp OHbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;

                Assert.AreEqual("Move Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Rbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;
                Assert.AreEqual("Move Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Hbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");

                // .E
                column = 2;
                var row = dgv.Rows[2];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .E"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .E"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .E"); column++;

                // smooth, rough, rought- smooth
                // LHC.MB.A11R8 rad : 1.263,1.238,-0.025
                // LHC.MB.A11R8 vert: -0.668,-0.76,-0.092

                Assert.AreEqual("1.26", row.Cells[column].Value, "wrong value in dgv for O(Rbv)expected for .E"); column++;
                Assert.AreEqual("-0.67", row.Cells[column].Value, "wrong value in dgv for O(Hbv)expected for .E"); column++;

                Assert.AreEqual("1.26", row.Cells[column].Value, "wrong value in dgv for Rbv)move for .E"); column++;
                Assert.AreEqual("-0.67", row.Cells[column].Value, "wrong value in dgv for Hbv)move for .E"); column++;


                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step07_BOC_Add_second_Magnet_Test()
        {
            int datagridOffset = 0;
            Func<int, string> ClickNameInDgvWithOffset = (Y) => { return $@"
<IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""285"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""441"" Ypos=""{Y + datagridOffset}"" Delta=""0"" />
<IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""441"" Ypos=""{Y + datagridOffset}"" Delta=""0"" />"; };

            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step06_BOC_compute_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""212"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1273"" Ypos=""934"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1273"" Ypos=""934"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />

    {ClickNameInDgvWithOffset(572)}

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""257"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""669"" Ypos=""{658 + datagridOffset}"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""669"" Ypos=""{658 + datagridOffset}"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""214"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""746"" Ypos=""346"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""98"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""746"" Ypos=""346"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Goto and measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Goto and measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""237"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""814"" Ypos=""417"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""814"" Ypos=""417"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""15"" Delay=""213"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""844"" Ypos=""685"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""89"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""844"" Ypos=""685"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""17"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""286"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""717"" Ypos=""{715 + datagridOffset}"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""19"" Delay=""89"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""717"" Ypos=""{715 + datagridOffset}"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""262"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""737"" Ypos=""332"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""81"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""737"" Ypos=""332"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""22"" Delay=""0"" Action=""BB: Goto and measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""23"" Delay=""0"" Action=""BB: Goto and measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""24"" Delay=""249"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""728"" Ypos=""189"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""728"" Ypos=""189"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""266"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""894"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""89"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""894"" Ypos=""928"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""28"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""29"" Delay=""290"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""725"" Ypos=""{747 + datagridOffset}"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""105"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""725"" Ypos=""{747 + datagridOffset}"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""294"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""689"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""81"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""689"" Ypos=""356"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Goto and measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Goto and measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""273"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""637"" Ypos=""192"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""74"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""637"" Ypos=""192"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""281"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""897"" Ypos=""921"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""38"" Delay=""106"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""897"" Ypos=""921"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""39"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""40"" Delay=""226"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""719"" Ypos=""{780 + datagridOffset}"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""41"" Delay=""109"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""719"" Ypos=""{780 + datagridOffset}"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""213"" trace=""Measure+"" Button=""Left"" Direction=""Down"" Xpos=""732"" Ypos=""349"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""98"" trace=""Measure+"" Button=""Left"" Direction=""Up"" Xpos=""733"" Ypos=""349"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""44"" Delay=""0"" Action=""BB: Goto and measure (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""45"" Delay=""0"" Action=""BB: Goto and measure end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""46"" Delay=""237"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""663"" Ypos=""200"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""47"" Delay=""106"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""663"" Ypos=""200"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""1121"" trace=""button1+Valid"" Button=""Left"" Direction=""Down"" Xpos=""870"" Ypos=""916"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""222"" trace=""button1+Valid"" Button=""Left"" Direction=""Up"" Xpos=""870"" Ypos=""916"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""50"" Delay=""0"" Action=""MB: Enter values: Valid"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""51"" Delay=""279"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""52"" Delay=""210"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""+"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""252"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1123"" Ypos=""441"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""97"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1123"" Ypos=""441"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""55"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""56"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""6022"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1250"" Ypos=""942"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""97"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1250"" Ypos=""942"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""59"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""60"" Delay=""0"" Action=""BB: Next step end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;


                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                Assert.IsTrue(tsunami.BOC_Context.ExistingResults.Count >= 2, "2 BOC results is expected and missing");
                var lastResults = tsunami.BOC_Context.ExistingResults.LastOrDefault(x => x.AssemblyId == "LHC.MB.B11R8");
                Assert.IsTrue(lastResults.BeamPoints.Count == 8, "8 beam computed points are expected and missing");
                Assert.IsTrue(lastResults.Parameters.PointsToUseAsOBSXYZ.FindAll(x => x.Used == true).Count == 4, "4 points expected as OBSXYZ");
                Assert.AreEqual(4319.2582, tsunami.BOC_Context.ExistingResults[0].BeamPoints[19]._Coordinates.Ccs.X.Value, 0.0001, "X value of estimated beam point is wrong");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step08_BOC_with_level_Setup_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step07_BOC_Add_second_Magnet_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""247"" trace=""Element alignment (Created on: ): Polar Alignment+"" Button=""Left"" Direction=""Down"" Xpos=""985"" Ypos=""131"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""89"" trace=""Element alignment (Created on: ): Polar Alignment+"" Button=""Left"" Direction=""Up"" Xpos=""985"" Ypos=""131"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Element alignment (Created on: 28/05/2024 9:25:29): Polar Alignment (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Element alignment (Created on: 28/05/2024 9:25:29): Polar Alignment end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""85"" trace=""Level station +"" Button=""Left"" Direction=""Down"" Xpos=""1154"" Ypos=""317"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""90"" trace=""Level station +"" Button=""Left"" Direction=""Up"" Xpos=""1154"" Ypos=""317"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Level station  (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Level station  end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""174"" trace=""Select Instrument+"" Button=""Left"" Direction=""Down"" Xpos=""673"" Ypos=""280"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""82"" trace=""Select Instrument+"" Button=""Left"" Direction=""Up"" Xpos=""673"" Ypos=""281"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Select Instrument (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""12"" Delay=""152"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1525"" Ypos=""483"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""73"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1525"" Ypos=""483"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""14"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Options end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""16"" Delay=""106"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1656"" Ypos=""611"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""82"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1656"" Ypos=""611"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""18"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: List View end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""153"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""363"" Ypos=""518"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""363"" Ypos=""518"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""22"" Delay=""52"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{N}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""23"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{A}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""24"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""25"" Delay=""1005"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""180"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""26"" Delay=""90"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""180"" Ypos=""590"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""27"" Delay=""234"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""879"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""28"" Delay=""73"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""879"" Ypos=""681"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""29"" Delay=""0"" Action=""MB: Choose a level: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""30"" Delay=""0"" Action=""BB: Select Instrument end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""177"" trace=""Select Default Main Staff+"" Button=""Left"" Direction=""Down"" Xpos=""640"" Ypos=""346"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""91"" trace=""Select Default Main Staff+"" Button=""Left"" Direction=""Up"" Xpos=""640"" Ypos=""346"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""33"" Delay=""0"" Action=""BB: Select Default Main Staff (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""34"" Delay=""169"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""197"" Ypos=""449"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""35"" Delay=""114"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""197"" Ypos=""449"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""36"" Delay=""11"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""37"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""11"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{BACKSPACE}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{G}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""40"" Delay=""12"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""41"" Delay=""49"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""2018"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""146"" Ypos=""510"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""113"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""146"" Ypos=""510"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""44"" Delay=""634"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""943"" Ypos=""755"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""45"" Delay=""89"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""943"" Ypos=""755"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""46"" Delay=""0"" Action=""MB: Choose a default main staff: Select"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""47"" Delay=""0"" Action=""BB: Select Default Main Staff end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""48"" Delay=""194"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1374"" Ypos=""948"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""49"" Delay=""90"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1374"" Ypos=""948"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""50"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""51"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""170"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""590"" Ypos=""540"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""81"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""590"" Ypos=""540"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""54"" Delay=""189"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1313"" Ypos=""945"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""55"" Delay=""66"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1313"" Ypos=""945"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""56"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""57"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""58"" Delay=""165"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1139"" Ypos=""640"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""59"" Delay=""65"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1139"" Ypos=""640"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""60"" Delay=""30"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""61"" Delay=""48"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                var stationModule = ggm.ActiveSubGuidedModule._ActiveStationModule as TSU.Level.Station.Module;
                var station = stationModule._Station as TSU.Level.Station;

                Assert.IsTrue(station != null, "sttion level is null");
                Assert.IsTrue(station._Parameters._Instrument._Model == "NA2", "must be an NA2");
                Assert.IsTrue(stationModule._defaultStaff._Model == "G16", "must be an G16");

                Assert.IsTrue(ggm.SubGuidedModules[2]._ActiveStationModule._Station.MeasuresTaken[8]._Point._Name == "LHC.LECL.11R8.E", "Should be a measurement for LHC.LECL.11R8.E");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }


        [TestMethod()]
        public void L2_BGM_Step09_BOC_with_level_Acquisition_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step08_BOC_with_level_Setup_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""73"" Delay=""1314"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1168"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""74"" Delay=""216"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1168"" Ypos=""393"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""75"" Delay=""227"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""76"" Delay=""66"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""77"" Delay=""09"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""78"" Delay=""09"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""79"" Delay=""78"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""80"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""81"" Delay=""48"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""82"" Delay=""53"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""83"" Delay=""89"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""88"" Delay=""70"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""89"" Delay=""122"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""90"" Delay=""85"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""91"" Delay=""57"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""92"" Delay=""26"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""93"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""94"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""95"" Delay=""59"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""96"" Delay=""28"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""97"" Delay=""19"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""98"" Delay=""14"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""99"" Delay=""63"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""100"" Delay=""58"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""101"" Delay=""77"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""102"" Delay=""24"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""103"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""104"" Delay=""13"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{9}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""105"" Delay=""65"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""106"" Delay=""36"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{4}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""107"" Delay=""22"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""108"" Delay=""45"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{6}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""109"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{8}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""110"" Delay=""62"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""111"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""112"" Delay=""18"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{5}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""113"" Delay=""16"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{2}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""114"" Delay=""21"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""116"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{3}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""117"" Delay=""20"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""118"" Delay=""35"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""119"" Delay=""14"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{7}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""115"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""116"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""116"" Delay=""41"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{0}}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""120"" Delay=""335"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""121"" Delay=""116"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1613"" Ypos=""213"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""122"" Delay=""73"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1613"" Ypos=""213"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""123"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""124"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""135"" Delay=""2059"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""936"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""136"" Delay=""79"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""936"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""135"" Delay=""2059"" trace=""button1+OK"" Button=""Left"" Direction=""Down"" Xpos=""936"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""136"" Delay=""79"" trace=""button1+OK"" Button=""Left"" Direction=""Up"" Xpos=""936"" Ypos=""565"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""137"" Delay=""0"" Action=""MB: Outward station level exported: OK"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""138"" Delay=""0"" Action=""BB: Next step end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                //test.LetMeInteractWithTsunamiIfCTempStopTxtExist();
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                Assert.IsTrue(tsunami.BOC_Context.ExistingResults.Count == 5, $"2 BOC results is expected and missing. found {tsunami.BOC_Context.ExistingResults.Count}");
                var lastResults = tsunami.BOC_Context.ExistingResults[4];
                Assert.IsTrue(lastResults.BeamPoints.Count == 8, "8 beam computed points are expected and missing");
                Assert.IsTrue(lastResults.Parameters.PointsToUseAsOBSXYZ.FindAll(x => x.Used == true).Count == 4, "4 points expected as OBSXYZ");
                Assert.AreEqual(4319.2582, tsunami.BOC_Context.ExistingResults[0].BeamPoints[19]._Coordinates.Ccs.X.Value, 0.001, "X value of estimated beam point is wrong");
                Assert.AreEqual(0, tsunami.BOC_Context.ExistingResults[0].Sigma0, 0.1, "sigma0 expected around 0");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step10_BOC_compute_Polar_Plus_Level_Test ()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step09_BOC_with_level_Acquisition_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
     <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""246"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""697"" Ypos=""546"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""697"" Ypos=""546"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""3"" Delay=""273"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""700"" Ypos=""437"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""4"" Delay=""8"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""700"" Ypos=""437"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""286"" trace=""Manager modules+"" Button=""Left"" Direction=""Down"" Xpos=""258"" Ypos=""442"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""10"" trace=""Manager modules+"" Button=""Left"" Direction=""Up"" Xpos=""258"" Ypos=""442"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Manager modules (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Manager modules end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""261"" trace=""Beam Offset Computation+"" Button=""Left"" Direction=""Down"" Xpos=""420"" Ypos=""936"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""6"" trace=""Beam Offset Computation+"" Button=""Left"" Direction=""Up"" Xpos=""420"" Ypos=""936"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Beam Offset Computation (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Beam Offset Computation end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""265"" trace=""Manager+"" Button=""Left"" Direction=""Down"" Xpos=""623"" Ypos=""131"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""9"" trace=""Manager+"" Button=""Left"" Direction=""Up"" Xpos=""623"" Ypos=""131"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: Manager: 0 item(s) selected (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: Manager: 0 item(s) selected end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""29"" trace=""Select Assembly and Gather measurements+"" Button=""Left"" Direction=""Down"" Xpos=""769"" Ypos=""240"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""8"" trace=""Select Assembly and Gather measurements+"" Button=""Left"" Direction=""Up"" Xpos=""769"" Ypos=""240"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""19"" Delay=""0"" Action=""BB: Select Assembly and Gather measurements (beginning)"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""289"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""132"" Ypos=""384"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""9"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""132"" Ypos=""384"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""280"" trace=""button1+Select"" Button=""Left"" Direction=""Down"" Xpos=""837"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""23"" Delay=""9"" trace=""button1+Select"" Button=""Left"" Direction=""Up"" Xpos=""837"" Ypos=""927"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""24"" Delay=""0"" Action=""MB: Magnets: Select"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""2039"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""878"" Ypos=""566"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""20"" Delay=""23"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""878"" Ypos=""566"" Delta=""0"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");
                var results = tsunami.BOC_Context.ExistingResults;
                Assert.IsTrue(results.Count == 6, $"6 BOC results is expected and we have {results.Count}");
                var lastResults = results[5];
                Assert.IsTrue(lastResults.AssemblyId == "LHC.MB.A11R8", "last boc should concern LHC.MB.A11R8");
                Assert.IsTrue(lastResults.BeamPoints.Count == 20, "20 beam computed points are expected and missing");
                var obss = lastResults.Parameters.PointsToUseAsOBSXYZ.FindAll(x => x.Used == true);
                Assert.IsTrue(obss.Count == 3, $"3 points expected as OBSXYZ, we have :{obss.Count}");
                Assert.AreEqual(4319.2582, tsunami.BOC_Context.ExistingResults[0].BeamPoints[19]._Coordinates.Ccs.X.Value, 0.0001, "X value of estimated beam point is wrong");
                Assert.AreEqual(0, tsunami.BOC_Context.ExistingResults[0].Sigma0, 0.1, "sigma0 expected around 0");
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step11_BOC_With_Roll()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step06_BOC_compute_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""578"" trace=""Element alignment (Created on: ): Polar Alignment+"" Button=""Left"" Direction=""Down"" Xpos=""1045"" Ypos=""125"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""56"" trace=""Element alignment (Created on: ): Polar Alignment+"" Button=""Left"" Direction=""Up"" Xpos=""1046"" Ypos=""125"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Element alignment (Created on: 3/06/2024 17:41:16): Polar Alignment (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Element alignment (Created on: 3/06/2024 17:41:16): Polar Alignment end"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""5"" Delay=""514"" trace=""Roll survey+"" Button=""Left"" Direction=""Down"" Xpos=""1148"" Ypos=""257"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""6"" Delay=""32"" trace=""Roll survey+"" Button=""Left"" Direction=""Up"" Xpos=""1148"" Ypos=""257"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""7"" Delay=""0"" Action=""BB: Roll survey (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""8"" Delay=""0"" Action=""BB: Roll survey end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""9"" Delay=""569"" trace=""Options+"" Button=""Left"" Direction=""Down"" Xpos=""1664"" Ypos=""309"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""10"" Delay=""31"" trace=""Options+"" Button=""Left"" Direction=""Up"" Xpos=""1664"" Ypos=""309"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""11"" Delay=""0"" Action=""BB: Options (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""12"" Delay=""0"" Action=""BB: Options end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""13"" Delay=""589"" trace=""List View+"" Button=""Left"" Direction=""Down"" Xpos=""1731"" Ypos=""419"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""14"" Delay=""43"" trace=""List View+"" Button=""Left"" Direction=""Up"" Xpos=""1731"" Ypos=""419"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""15"" Delay=""0"" Action=""BB: List View (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""16"" Delay=""0"" Action=""BB: List View end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""17"" Delay=""57"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""708"" Ypos=""365"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""18"" Delay=""38"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""708"" Ypos=""365"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""19"" Delay=""51"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{W}}"" Target=""TextBox"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""20"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{Y}}"" Target=""TextBox"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""21"" Delay=""1539"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""485"" Ypos=""425"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""22"" Delay=""68"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""485"" Ypos=""425"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""30"" Delay=""559"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""493"" Ypos=""417"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""31"" Delay=""12"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""493"" Ypos=""417"" Delta=""0"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""32"" Delay=""579"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1297"" Ypos=""937"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""33"" Delay=""52"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1297"" Ypos=""937"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""34"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""35"" Delay=""0"" Action=""BB: Next step:  (press 'n') end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""36"" Delay=""220"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1079"" Ypos=""440"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""37"" Delay=""43"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1079"" Ypos=""440"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""38"" Delay=""50"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""39"" Delay=""29"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""42"" Delay=""274"" trace=""none"" Button=""Left"" Direction=""Down"" Xpos=""1145"" Ypos=""444"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""43"" Delay=""62"" trace=""none"" Button=""Left"" Direction=""Up"" Xpos=""1145"" Ypos=""444"" Delta=""0"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""44"" Delay=""74"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{SUBTRACT}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""45"" Delay=""90"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""46"" Delay=""69"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{1}}"" Target=""SplitContainer"" />
    <IInputActivity xsi:type=""KeyStroke____"" Pos.=""51"" Delay=""65"" CTRL=""false"" SHIFT=""false"" ALT=""false"" KeyData=""{{ENTER}}"" Target=""SplitContainer"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""52"" Delay=""1305"" trace=""Element alignment (Created on: ): Roll survey+"" Button=""Left"" Direction=""Down"" Xpos=""1113"" Ypos=""128"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""53"" Delay=""35"" trace=""Element alignment (Created on: ): Roll survey+"" Button=""Left"" Direction=""Up"" Xpos=""1113"" Ypos=""128"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""54"" Delay=""0"" Action=""BB: Element alignment (Created on: 3/06/2024 17:41:16): Roll survey (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""55"" Delay=""0"" Action=""BB: Element alignment (Created on: 3/06/2024 17:41:16): Roll survey end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""56"" Delay=""200"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Down"" Xpos=""1220"" Ypos=""481"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""57"" Delay=""59"" trace=""Polar Alignment+"" Button=""Left"" Direction=""Up"" Xpos=""1220"" Ypos=""481"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""58"" Delay=""0"" Action=""BB: Polar Alignment (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""59"" Delay=""0"" Action=""BB: Polar Alignment end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""60"" Delay=""296"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1250"" Ypos=""931"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""61"" Delay=""51"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1250"" Ypos=""931"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""62"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""63"" Delay=""0"" Action=""BB: Next step end"" />

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""64"" Delay=""1288"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Down"" Xpos=""1184"" Ypos=""426"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""65"" Delay=""58"" trace=""Compute Beam Offsets+"" Button=""Left"" Direction=""Up"" Xpos=""1184"" Ypos=""426"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""66"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""67"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />

  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                Assert.IsTrue(tsunami.BOC_Context.ExistingResults.Count == 2, "1 BOC results is expected");
                var lastResults = tsunami.BOC_Context.ExistingResults[1];
                Assert.IsTrue(lastResults.BeamPoints.Count == 20, "8 beam computed points are expected and missing");
                Assert.IsTrue(lastResults.Parameters.PointsToUseAsOBSXYZ.FindAll(x => x.Used == true).Count == 3, "3 points expected as OBSXYZ");
                Assert.IsTrue(lastResults.Parameters.Inclys.Count == 1, "1 points expected as INCLY");
                Assert.AreEqual(4319.2584, lastResults.BeamPoints[19]._Coordinates.Ccs.X.Value, 0.0002, "X value of estimated beam point is wrong");
                Assert.IsTrue(Directory.Exists(test.FolderPath), $" missing folder {test.FolderPath}");
                string lgcInputFile = lastResults.LgcProjectPath + ".inp";
                Assert.IsTrue(File.Exists(lgcInputFile));
                Assert.IsTrue(CheckStringInFile(lgcInputFile, "LHC.MB.A11R8.E   0.7002817   RF   -0.1115759"));
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        static bool CheckStringInFile(string filePath, string stringToSearch)
        {
            try
            {
                foreach (string line in File.ReadLines(filePath))
                {
                    if (line.Contains(stringToSearch))
                    {
                        return true;
                    }
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("The file was not found.");
            }
            return false;
        }

        [TestMethod()]
        public void L2_BGM_Step20_Change_radi_to_relative_Offset()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step05_Acquire_Test) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;

                ggm.RabotStrategy =  TSU.Common.Compute.Rabot.Strategy.Relative_Displacement;
                var admin = ggm.SubGuidedModules.First(x => x.guideModuleType == TSU.ENUM.GuidedModuleType.AlignmentManager);
                admin.RabotStrategy = TSU.Common.Compute.Rabot.Strategy.Relative_Displacement;
                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

        [TestMethod()]
        public void L2_BGM_Step21_BOC_compute_Test()
        {
            var testMethod = MethodBase.GetCurrentMethod();
            F_Test test = new F_Test(testMethod)
            {
                TsuPath = Tools.Artefact.GetArtefactFullPath(nameof(L2_BGM_Step20_Change_radi_to_relative_Offset) + ".tsut"),
                MacroXmlContent = $@"<?xml version=""1.0"" encoding=""utf-8""?>
<Macro xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xml=""http://www.w3.org/XML/1998/namespace"" Name=""{testMethod.Name}"" >
  IntegrationTestsResults<WorkingParameters Height=""1035"" Width=""1920"" FunctionKey=""F1"" SpeedFactor=""1"" />  
  <KeyStrokesAndMouseClicks>

    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""277"" trace=""Next step+"" Button=""Left"" Direction=""Down"" Xpos=""1285"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""97"" trace=""Next step+"" Button=""Left"" Direction=""Up"" Xpos=""1285"" Ypos=""944"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />


    <IInputActivity xsi:type=""MouseActivity"" Pos.=""7"" Delay=""265"" Button=""Left"" Direction=""Down"" Xpos=""1252"" Ypos=""418"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""8"" Delay=""122"" Button=""Left"" Direction=""Up"" Xpos=""1252"" Ypos=""418"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""9"" Delay=""0"" Action=""BB: Compute Beam Offsets (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""10"" Delay=""0"" Action=""BB: Compute Beam Offsets end"" />
 
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""1"" Delay=""304"" Button=""Left"" Direction=""Down"" Xpos=""1374"" Ypos=""967"" Delta=""0"" />
    <IInputActivity xsi:type=""MouseActivity"" Pos.=""2"" Delay=""321"" Button=""Left"" Direction=""Up"" Xpos=""1374"" Ypos=""967"" Delta=""0"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""3"" Delay=""0"" Action=""BB: Next step (beginning)"" />
    <IInputActivity xsi:type=""Trace________"" Pos.=""4"" Delay=""0"" Action=""BB: Next step end"" />
  </KeyStrokesAndMouseClicks>
</Macro>"
            };
            test.Execute(testAction: (tsunami) =>
            {
                Assert.IsTrue(tsunami != null, "TSUNAMI object is NULL");
                var ggm = tsunami.MeasurementModules[0] as TSU.Common.Guided.Group.Module;


                var gmp = ggm.SubGuidedModules[4] as TSU.Common.Guided.Module;
                Assert.IsTrue(gmp.MagnetsToBeAligned.Count == 2, "2 manget should be selected");
                Assert.AreEqual("LHC.MB.A11R8", gmp.MagnetsToBeAligned[0]._Name, "magnet to align 1 must be LHC.MB.A11R8");
                Assert.AreEqual("LHC.MB.B11R8", gmp.MagnetsToBeAligned[1]._Name, "magnet to align 1 must be LHC.MB.B11R8");

                Assert.IsTrue(tsunami.BOC_Context.ExistingResults.Count == 1, "1 BOC results is expected and missing");
                var lastResults = tsunami.BOC_Context.ExistingResults[0];
                Assert.IsTrue(lastResults.Parameters.PointsToUseAsOBSXYZ.FindAll(x => x.Used == true).Count == 3, "3 points expected as OBSXYZ");
                Assert.IsTrue(lastResults.BeamPoints.Count == 20, "20 beam computed points are expected and missing");
                Assert.AreEqual(4319.2582, lastResults.BeamPoints[19]._Coordinates.Ccs.X.Value, 0.0001, "X value of estimated beam point is wrong");

                // datagridView
                var dgv = TsuView.FindVisibleDataGridView(tsunami.View);
                Assert.IsTrue(dgv != null, "no datagrid view visible found");

                Assert.IsTrue(dgv.Columns.Count == 15, "wrong columns count");
                Assert.IsTrue(dgv.Rows.Count == 37, "wrong rows count in dgv");

                // header
                int column = 2;
                Assert.AreEqual("o(Rbv) [mm]", dgv.Columns[column].HeaderText, "wrong header ORbv [mm]"); column++;
                Assert.AreEqual("o(Lbv) [mm]", dgv.Columns[column].HeaderText, "wrong header OLbv [mm]"); column++;
                Assert.AreEqual("o(Hbv) [mm]", dgv.Columns[column].HeaderText, "wrong header OHbv [mm]"); column++;
                Assert.AreEqual("ExpO Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header EORbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;
                Assert.AreEqual("ExpO Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header EOHbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;

                Assert.AreEqual("Move Rbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Rbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");
                column++;
                Assert.AreEqual("Move Hbv [mm]", dgv.Columns[column].HeaderText, "wrong header MOVE Hbv [mm]");
                Assert.IsTrue(dgv.Columns[column].Visible == true, $"Column {column} should be visible");

                // .E
                column = 2;
                var row = dgv.Rows[2];
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Rbv) for .E"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Lbv) for .E"); column++;
                Assert.AreEqual("0.00", row.Cells[column].Value, "wrong value in dgv for o(Hbv) for .E"); column++;

                // smooth, rough, rought- smooth
                // LHC.MB.A11R8 rad : 1.263,1.238,-0.025
                // LHC.MB.A11R8 vert: -0.668,-0.76,-0.092

                Assert.AreEqual("0.03", row.Cells[column].Value, "wrong value in dgv for O(Rbv)expected for .E"); column++;
                Assert.AreEqual("0.09", row.Cells[column].Value, "wrong value in dgv for O(Hbv)expected for .E"); column++;

                Assert.AreEqual("0.03", row.Cells[column].Value, "wrong value in dgv for Rbv)move for .E"); column++;
                Assert.AreEqual("0.09", row.Cells[column].Value, "wrong value in dgv for Hbv)move for .E"); column++;

                test.SaveTsunamiProjectAsArtefact(tsunami, $"{testMethod.Name}.tsut");
            });
        }

    }
}
