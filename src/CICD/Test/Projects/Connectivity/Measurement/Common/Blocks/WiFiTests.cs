﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Globalization;
using ManagedNativeWifi;
using System.IO;
using System.Xml;

namespace TSU.Connectivity_Tests
{
    [TestClass()]
    public class WiFiTests
    {
        private const string interfaceName = "WiFi";
        private const string interfaceDevice = "Wi-Fi";
        private string atSSID;
        private string tsSSID;
        private string gitlabTag;
        private string SSIDandIPpath;

        [TestInitialize]
        public void Initialize()
        {

            var tsunamiPath = Environment.CurrentDirectory.Split(new string[] { "src" }, StringSplitOptions.None);
            this.SSIDandIPpath = Path.Combine(tsunamiPath[0], InstrumentFileTests.SSID_FILE_PATH);
            string instrumentsForTestsPath = Path.Combine(tsunamiPath[0], InstrumentFileTests.SN_FILE_PATH);

            XmlDocument doc = new XmlDocument();
            doc.Load(SSIDandIPpath);
            this.gitlabTag = File.ReadAllText(instrumentsForTestsPath);
            this.atSSID = WiFi.GetInstrumentSSID(doc, gitlabTag.Split(' ')[0]);
            this.tsSSID = WiFi.GetInstrumentSSID(doc, gitlabTag.Split(' ')[1]);
           
        }

        [TestMethod()]
        [TestCategory("WiFiAdapter")]
        [DataRow("Wi-Fi")]
        [DataRow("WI-FI")]
        [DataRow("wi-fi")]
        [DataRow("Wi")]
        [DataRow("Fi")]
        [DataRow("-")]
        [DataRow("wi")]
        public void IsAdapterEnabledTrueTests(string name)
        {
            // interfaceDevice for Windows10 is "Wi-Fi"
            // If tests failed, means that interfaceDevice is not anymore "Wi-Fi"

            // original case, case sensitive and different variation
            Assert.IsTrue(WiFi.IsAdapterEnabled(name, true), "wifi adapter not available");

        }

        [TestMethod()]
        [TestCategory("WiFiAdapter")]
        [DataRow("wifi")]
        [DataRow("WiFi")]
        [DataRow("wi_fi")]
        [DataRow("  ")]
        [DataRow(" ")]

        public void IsAdapterEnabledFalseTests(string name)
        {
            // interfaceDevice for Windows10 is "Wi-Fi"
            // If tests failed, means that interfaceDevice is not anymore "Wi-Fi"

            // empty or white space and different variation
            Assert.IsFalse(WiFi.IsAdapterEnabled(name, true));
        }


        [TestMethod()]
        [TestCategory("WiFiAdapter")]
        public void DisableAdapterTest()
        {
            // Disconnect WiFi adapter and test IsAdapterEnabled is false
            string disableCommand = $"netsh interface set interface \"{interfaceName}\" disable";
            bool resultExpected = WiFi.RunProcess($" /C {disableCommand}");
            bool resultActual = WiFi.IsAdapterEnabled(interfaceDevice, true);
            Assert.AreNotEqual(resultExpected, resultActual);
            Assert.IsFalse(resultActual);
        }

        [TestMethod()]
        [TestCategory("WiFiAdapter")]
        public void EnableAdapterManualTest()
        {
            // Connect WiFi adapter and test IsAdapterEnabled is true (before disable)
            string disableCommand = $"netsh interface set interface \"{interfaceName}\" disable";
            WiFi.RunProcess($" /C {disableCommand}");

            string enableCommand = $"netsh interface set interface \"{interfaceName}\" enable";
            bool resultExpected = WiFi.RunProcess($" /C {enableCommand}");
            bool resultActual = WiFi.IsAdapterEnabled(interfaceDevice, true);

            Assert.AreEqual(resultExpected, resultActual);
            Assert.IsTrue(resultActual);
        }

        [TestMethod()]
        [TestCategory("WiFiAdapter")]
        public void EnableAdpaterTest()
        {
            // Enable WiFi which is already connected (before disable)
            string disableCommand = $"netsh interface set interface \"{interfaceName}\" disable";
            WiFi.RunProcess($" /C {disableCommand}");
            Assert.AreEqual(WiFi.EnableAdapterAsAdmin(interfaceName, true), WiFi.IsAdapterEnabled(interfaceDevice, true));

        }


        [TestMethod()]
        [TestCategory("WiFiAdapter")]
        public void EnableConenctedAdpaterTest()
        {
            // Enable WiFi which is already connected
            WiFi.EnableAdapterAsAdmin(interfaceName, true);
            Assert.IsFalse(WiFi.EnableAdapterAsAdmin(interfaceName, true));

        }

        [TestMethod()]
        [TestCategory("WiFiAdapter")]
        public void DisableEnableAdapterManualTest()
        {

            // Disconnect and connect WiFi adapter and test IsAdapterEnabled is true
            string enableCommand = $"netsh interface set interface \"{interfaceName}\" enable";
            string disableCommand = $"netsh interface set interface \"{interfaceName}\" disable";
            bool resultExpected = WiFi.RunProcess($"/C {disableCommand} && {enableCommand}");
            bool resultActual = WiFi.IsAdapterEnabled(interfaceDevice, true);

            Assert.AreEqual(resultExpected, resultActual);
            Assert.IsTrue(resultActual);

        }

        [TestMethod()]
        [TestCategory("WiFiAdapter")]
        public void DisableEnableAdpaterTest()
        {

            //Assert.IsTrue(WiFi.DisableAndRenableAdapterAsAdmin(interfaceName, true)); // commented beacuse need administrion approval
            Assert.IsTrue(WiFi.IsAdapterEnabled(interfaceDevice, true));

        }


        [TestMethod()]
        [TestCategory("WiFiConnection")]
        public void ConnectATSSIDTest()
        {
            Console.WriteLine("eee" + this.atSSID);
            bool resultExpected = WiFi.Connect(this.atSSID, true);
            WiFi.WaitForSsidAvailibity(this.atSSID);
            Assert.IsTrue(resultExpected);
            

        }
        [TestMethod()]
        [TestCategory("WiFiConnection")]
        public void ConnectTSSSIDTest()
        {
            bool resultExpected = WiFi.Connect(this.tsSSID, true);
            WiFi.WaitForSsidAvailibity(this.tsSSID);
            Assert.IsTrue(resultExpected);

        }

        [TestMethod()]
        [TestCategory("WiFiConnection")]
        public void DisconnectNetworkTest()
        {           

            AvailableNetworkPack availableNet = NativeWifi.EnumerateAvailableNetworks()
                                            .FirstOrDefault(x => x.Ssid.ToString().Contains(this.atSSID));

            if (availableNet != null && !string.IsNullOrEmpty(availableNet.ProfileName))
            {
                NativeWifi.DisconnectNetwork(interfaceId: availableNet.Interface.Id);
            }

            Assert.IsFalse(WiFi.IsConnected(this.atSSID, true));
        }

        [TestMethod()]
        [TestCategory("WiFiConnection")]

        public void AvailableATNetworkTest()
        { 
            AvailableNetworkPack availableNet = NativeWifi.EnumerateAvailableNetworks()
                                            .FirstOrDefault(x => x.Ssid.ToString().Contains(this.atSSID));
            Assert.IsNotNull(availableNet);  
        }

        [TestMethod()]
        [TestCategory("WiFiConnection")]
        public void AvailableTSNetworkTest()
        {
            AvailableNetworkPack availableNet = NativeWifi.EnumerateAvailableNetworks()
                                            .FirstOrDefault(x => x.Ssid.ToString().Contains(this.tsSSID));
            Assert.IsNotNull(availableNet);
        }

        [TestMethod()]
        [TestCategory("WiFiConnection")]
        public void ConnectATNetworkTest()
        {
            AvailableNetworkPack availableNet = NativeWifi.EnumerateAvailableNetworks()
                                            .FirstOrDefault(x => x.Ssid.ToString().Contains(this.atSSID));
         
            bool resultsExpected = NativeWifi.ConnectNetwork(interfaceId: availableNet.Interface.Id,
                                            profileName: availableNet.ProfileName,
                                            bssType: availableNet.BssType);

            Assert.IsTrue(resultsExpected);           

        }

        [TestMethod()]
        [TestCategory("WiFiConnection")]
        public void ConnectTSNetworkTest()
        {

            AvailableNetworkPack availableNet = NativeWifi.EnumerateAvailableNetworks()
                                            .FirstOrDefault(x => x.Ssid.ToString().Contains(this.tsSSID));


            bool resultsExpected = NativeWifi.ConnectNetwork(interfaceId: availableNet.Interface.Id,
                                            profileName: availableNet.ProfileName,
                                            bssType: availableNet.BssType);

            Assert.IsTrue(resultsExpected);

        }


        [TestMethod()]
        [TestCategory("WiFiInterface")]
        [DataRow("WiFi")]
        [DataRow("wiFi")]
        public void GetInterfaceNameEqualTests(string name)
        {
            // original case and case sensitive
            Assert.AreEqual(name, WiFi.GetFirstWirelessInterfaceUpName(), true, CultureInfo.CurrentCulture);
        }

        [TestMethod()]
        [TestCategory("WiFiInterface")]
        [DataRow("")]
        [DataRow("  ")]
        [DataRow("wi")]
        [DataRow("wi_Fi")]

        public void GetInterfaceNameNotEqualTests(string name)
        {
            // empty or white space, and different wrong variation
            Assert.AreNotEqual(name, WiFi.GetFirstWirelessInterfaceUpName(), true, CultureInfo.CurrentCulture);

        }


        [TestMethod()]
        [TestCategory("DHCP")]
        public void EnableDHCPTest()
        {
            Assert.AreEqual(WiFi.EnableAutoDHCP(interfaceName, true), WiFi.IsDhcpEnabled("WiFi", true));

        }

        [TestMethod()]
        [TestCategory("DHCP")]
        public void EnableDHCPManualTest()
        {
            string enableCommand = $"netsh interface ip set address \"{interfaceName}\" dhcp";

            Process p = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = "CMD.exe",
                    Arguments = $" /C {enableCommand}",
                    UseShellExecute = true,
                    Verb = "runas",
                    RedirectStandardOutput = false,
                    CreateNoWindow = true
                }
            };
            p.Start();
            p.WaitForExit();

            Assert.IsTrue(WiFi.IsDhcpEnabled(interfaceName, true));
        }

        [TestMethod()]
        [TestCategory("Profile")]
        public void IsProfileExistTest()
        {
            var Instr_Active = WiFi.GetListOfActiveInstrument(this.SSIDandIPpath);
            foreach (var instr in Instr_Active)
            {
                Assert.AreEqual(WiFi.SetProfile(instr.Ssid.ToString(), true), WiFi.IsProfileExist(instr.Ssid.ToString(), true));
            }

        }

        [TestMethod()]
        [TestCategory("Profile")]
        public void CreateProfileWithEmptyNameTest()
        {
            string pathXml = WiFi.CreateXMLForProfile("");
            Assert.IsFalse(WiFi.CreateProfileBasedOnXML(pathXml, "", true));
        }

        [TestMethod()]
        [TestCategory("Profile")]
        public void CreateProfileWithTheSameNameTest()
        {
            // Create profile not associated with any wifi 
            string profileName = "temp";
            string pathXml = WiFi.CreateXMLForProfile(profileName);
            WiFi.CreateProfileBasedOnXML(pathXml, profileName, true);

            string path2Xml = WiFi.CreateXMLForProfile(profileName);
            Assert.IsTrue(WiFi.CreateProfileBasedOnXML(path2Xml, profileName, true));

            WiFi.RunProcess($" /C netsh wlan delete profile \"temp\"");

        }


        [TestMethod()]
        [TestCategory("Profile")]
        public void DeleteProfileTests()
        {
            string path2Xml = WiFi.CreateXMLForProfile("test");
            Assert.IsTrue(WiFi.CreateProfileBasedOnXML(path2Xml, "test", true));

            WiFi.RunProcess($" /C netsh wlan delete profile \"test\"");
            Assert.IsFalse(WiFi.IsProfileExist("test", true));

        }

        [TestMethod()]
        [TestCategory("Profile")]
        public void CreateProfileWithSpecialNameTest()
        {
            string specialChar = "+!;_@";
            string pathXml = WiFi.CreateXMLForProfile(specialChar);
            Assert.IsTrue(WiFi.CreateProfileBasedOnXML(pathXml, specialChar, true));

            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo
                                                                 ("netsh", "wlan delete profile \"{specialChar}\"");
            System.Diagnostics.Process p = new System.Diagnostics.Process
            {
                StartInfo = psi,
            };
            p.Start();
            p.WaitForExit();

            Assert.IsFalse(WiFi.IsProfileExist(specialChar, true));

        }


        [TestMethod()]
        [TestCategory("SSID")]
        public void CheckGitlabTag()
        {
            
        }

        [TestMethod()]
        [TestCategory("SSID")]
        public void CheckSSIDAtExistInPreferencesTests()
        {
            Console.WriteLine("Tooo: " + this.atSSID);
            Assert.IsFalse(string.IsNullOrEmpty(this.atSSID));
        }

        [TestMethod()]
        [TestCategory("SSID")]

        public void CheckSSIDTsExistInPreferencesTests()
        {
            Assert.IsFalse(string.IsNullOrEmpty(this.tsSSID));
        }


        [TestMethod()]
        [TestCategory("SSID")]
        public void GetNetWithSSIDForATTests()
        {

            
            // original case
            Assert.AreEqual(this.atSSID, WiFi.GetFirstNetContaining(this.atSSID).Ssid.ToString());

            // case sensitive 
            string smallCase = this.atSSID.ToLower();
            string lowerCase = this.atSSID.ToUpper();
            string oneLowerCase = this.atSSID.Replace("P", "p");
            Assert.AreEqual(this.atSSID, WiFi.GetFirstNetContaining(smallCase).Ssid.ToString());
            Assert.AreEqual(this.atSSID, WiFi.GetFirstNetContaining(lowerCase).Ssid.ToString());
            Assert.AreEqual(this.atSSID, WiFi.GetFirstNetContaining(oneLowerCase).Ssid.ToString());

            // Wrong ssid
            string wrongCase = this.atSSID.Replace("P", "R");
            string wrongNumberCase = this.atSSID.Split('_')[2];
            Assert.IsNull(WiFi.GetFirstNetContaining(wrongCase));
            Assert.IsNull(WiFi.GetFirstNetContaining(wrongNumberCase));
           
        }
        [TestMethod()]
        [TestCategory("SSID")]
      
        public void GetNetWithSSIDForTSTests()
        {
            // original case
            Assert.AreEqual(this.tsSSID, WiFi.GetFirstNetContaining(this.tsSSID).Ssid.ToString());

            // case sensitive 
            string smallCase = this.tsSSID.ToLower();
            string lowerCase = this.tsSSID.ToUpper();
            string oneLowerCase = this.tsSSID.Replace("P", "p");
            Assert.AreEqual(this.tsSSID, WiFi.GetFirstNetContaining(smallCase).Ssid.ToString());
            Assert.AreEqual(this.tsSSID, WiFi.GetFirstNetContaining(lowerCase).Ssid.ToString());
            Assert.AreEqual(this.tsSSID, WiFi.GetFirstNetContaining(oneLowerCase).Ssid.ToString());

            // Wrong ssid
            string wrongCase = this.tsSSID.Replace("P", "R");
            string wrongNumberCase = this.tsSSID.Split('_')[2];
            Assert.IsNull(WiFi.GetFirstNetContaining(wrongCase));
            Assert.IsNull(WiFi.GetFirstNetContaining(wrongNumberCase));

        }

        [TestMethod()]
        [TestCategory("SSID")]
        public void GetNetWithEmptySSIDTests()
        {
            // empty or white space string
            Assert.IsNull(WiFi.GetFirstNetContaining(""));
        }

        [TestMethod()]
        public void IsTurnWifiOnTest()
        {
            Assert.IsTrue(WiFi.TurnOn(interfaceDevice, true));

        }

        [TestMethod()]
        public void IsDNSAutoEnabledTest()
        {
            Assert.AreEqual(WiFi.EnableAutoDNS(interfaceName, true), WiFi.IsDNSAutoEnabled(interfaceName, true));

        }

        [TestMethod()]
        public void CheckDHCPModeTest()
        {
            Assert.AreEqual(WiFi.CheckDHCPMode(interfaceName, true), WiFi.IsDhcpEnabled(interfaceName, true));

        }
    }
}