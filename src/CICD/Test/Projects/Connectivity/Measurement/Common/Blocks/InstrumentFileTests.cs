﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Xml;
using System.IO;
using UnitTestGenerator;
using System.Net;

namespace TSU.Connectivity_Tests
{
    [TestClass()]
    public class InstrumentFileTests
    {
        /// <summary>
        /// Relative filepath of the file that contains the serail number of the instrumentto be used during the connectivity test.
        /// This file is written by a CI script based on the comment inside the Git Tab that trigger the tests0.
        /// It is just the SNs separeted by a space: 392052 884369
        /// </summary>
        public static string SN_FILE_PATH = "src\\CICD\\Test\\Projects\\Connectivity\\bin\\Debug" +
            "\\Preferences\\Instruments and Calibrations\\instrumentsForTests.txt";

        /// <summary>
        /// 
        /// </summary>
        public static string SSID_FILE_PATH = "src\\CICD\\Test\\Projects\\Connectivity\\bin\\Debug" +
            "\\Preferences\\Instruments and Calibrations\\SSID_and_IP.xml";

        string SSIDandIPpath;
        string instrumentsForTestsPath;
        string serialNumberAT;
        string serialNumberTS;
        XmlDocument xmlFile;
        string gitlabTag;

        [TestInitialize]
        public void Initalize()
        {
            var tsunamiPath = Environment.CurrentDirectory.Split(new string[] { "src" }, StringSplitOptions.None);
            this.SSIDandIPpath = Path.Combine(tsunamiPath[0], SSID_FILE_PATH);
            this.instrumentsForTestsPath = Path.Combine(tsunamiPath[0], SN_FILE_PATH);

            this.gitlabTag = File.ReadAllText(instrumentsForTestsPath);
            this.serialNumberAT = this.gitlabTag.Split(' ')[0];
            this.serialNumberTS = this.gitlabTag.Split(' ')[1];

            this.xmlFile = new XmlDocument();
            this.xmlFile.Load(this.SSIDandIPpath);

        }

        [TestMethod()]
        [TestCategory("XML")]
        public void CheckXMLFileExtist()
        {
            Assert.IsTrue(File.Exists(this.SSIDandIPpath), " ssid and ip file is not DirectoryNotFoundException");
        }

        [TestMethod()]
        [TestCategory("TXT")]
        public void CheckTXTFileExists()
        {
            Assert.IsTrue(File.Exists(this.instrumentsForTestsPath), "cannot find instrumentForTest file");
        }


        [TestMethod()]
        [TestCategory("XML")]
        public void CheckIPForATExistInXML()
        {
            var IP = Ip.GetIPFromXML(this.xmlFile, this.serialNumberAT);
            Assert.IsFalse(string.IsNullOrEmpty(IP));
        }

        [TestMethod()]
        [TestCategory("XML")]
        public void CheckIPForTSExistInXML()
        {
            var IP = Ip.GetIPFromXML(this.xmlFile, this.serialNumberTS);
            Assert.IsFalse(string.IsNullOrEmpty(IP));
        }

        [TestMethod()]
        [TestCategory("XML")]
        public void CheckSSIDForATExistInXML()
        {
            var SSID = WiFi.GetInstrumentSSID(this.xmlFile, this.serialNumberAT);
            Assert.IsFalse(string.IsNullOrEmpty(SSID));
        }

        [TestMethod()]
        [TestCategory("XML")]
        public void CheckSSIDForTSExistInXML()
        {
            var SSID = WiFi.GetInstrumentSSID(this.xmlFile, this.serialNumberTS);
            Assert.IsFalse(string.IsNullOrEmpty(SSID));

        }

        [TestMethod()]
        [TestCategory("XML")]
        public void CheckSNForATExistInXML()
        {
            bool exists = false;
            foreach (XmlNode node in this.xmlFile.DocumentElement.ChildNodes)
            {
                if (node.Attributes["SN"]?.InnerText == this.serialNumberAT)
                {
                    exists = true;
                }
            }
            Assert.IsTrue(exists);
        }

        [TestMethod]
        [TestCategory("XML")]
        public void CheckSNForTSExistInXML()
        {
            bool exists = false;
            foreach (XmlNode node in this.xmlFile.DocumentElement.ChildNodes)
            {
                if (node.Attributes["SN"]?.InnerText == this.serialNumberTS)
                {
                    exists = true;
                }
                
            }
            Assert.IsTrue(exists);
        }

        [TestMethod]
        [TestCategory("XML")]
        public void CheckTypeForAtExistInXML()
        {
            var results = "";
            foreach(XmlNode node in this.xmlFile.DocumentElement.ChildNodes)
            {
                if (node.Attributes["SN"]?.InnerText == this.serialNumberAT)
                {
                    results = node.Attributes["Type"]?.InnerText;               
                }
            }
            Assert.IsFalse(string.IsNullOrEmpty(results));              
        }

        [TestMethod]
        [TestCategory("XML")]
        public void CheckTypeTSExistInXML()
        {
            var results = "";
            foreach (XmlNode node in this.xmlFile.DocumentElement.ChildNodes)
            {
                if (node.Attributes["SN"]?.InnerText == this.serialNumberAT)
                {
                    results = node.Attributes["Type"]?.InnerText;
                }                    
            }
            Assert.IsFalse(string.IsNullOrEmpty(results));
        }

        [TestMethod]
        [TestCategory("TXT")]
        public void CheckTXTIsNotEmpty()
        {
            Assert.IsFalse(string.IsNullOrEmpty(this.gitlabTag));             
        }

        [TestMethod]
        [TestCategory("TXT")]
        public void CheckNumberOfInstrumentInGitlabtag()
        {
            string[] instruments = this.gitlabTag.Split(' ');
            Assert.AreEqual(instruments.Length, 2);
        }

        [TestMethod]
        [TestCategory("TXT")]
        public void CheckSNforATIsValid()
        {
            var firstTag = this.gitlabTag.Split(' ')[0];
            Assert.IsFalse(string.IsNullOrEmpty(firstTag));
            Assert.IsTrue(firstTag.All(char.IsDigit));
        }

        [TestMethod]
        [TestCategory("TXT")]
        public void CheckSNforTSIsValid()
        {
            var secondTag = this.gitlabTag.Split(' ')[1];
            Assert.IsFalse(string.IsNullOrEmpty(secondTag));
            Assert.IsTrue(secondTag.All(char.IsDigit));
        }

        [TestMethod]
        [TestCategory("XML")]
        public void CheckIPFormat()
        {
            IPAddress ip;
            var ipadress = Ip.GetIPFromXML(this.xmlFile, this.serialNumberAT);
            bool validateIP = IPAddress.TryParse(ipadress, out ip);
            Assert.IsTrue(validateIP);
        }

        [TestMethod]
        [TestCategory("XML")]
        public void CheckSNforATCompatibleWithSSID()
        {
            var ssid = WiFi.GetInstrumentSSID(this.xmlFile, this.serialNumberAT);
            Assert.IsTrue(ssid.Contains(this.serialNumberAT));    
        }

        [TestMethod]
        [TestCategory("XML")]
        public void CheckSNforTSCompatibleWithSSID()
        {
            var ssid = WiFi.GetInstrumentSSID(this.xmlFile, this.serialNumberTS);
            Assert.IsTrue(ssid.Contains(this.serialNumberTS));      
        }

        [TestMethod]
        [TestCategory("XML")]

        public void CheckAttributesExistInXML()
        {
            XmlNodeList nodes = this.xmlFile.DocumentElement.ChildNodes;
 
            Assert.AreEqual(nodes[0].Attributes[0].Name, "Type");
            Assert.AreEqual(nodes[0].Attributes[1].Name, "SN");
            Assert.AreEqual(nodes[0].Attributes[2].Name, "SSID");
            Assert.AreEqual(nodes[0].Attributes[3].Name, "IP");
        }
    }
}