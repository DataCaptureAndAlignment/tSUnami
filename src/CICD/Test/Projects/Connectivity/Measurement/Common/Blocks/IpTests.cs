﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Xml;
using System.IO;

namespace TSU.Connectivity_Tests
{
    [TestClass()]
    public class IpTests
    {

        string ipAdressAT;
        string ipAdressTS;
        string serialNumberAT;
        string serialNumberTS;
        string atSSID;
        string tsSSID;
        string SSIDandIPpath;
        string instrumentsForTestsPath;

        [TestInitialize]
        public void Initalize()
        {
            var tsunamiPath = Environment.CurrentDirectory.Split(new string[] { "src" }, StringSplitOptions.None);
            this.SSIDandIPpath = Path.Combine(tsunamiPath[0], InstrumentFileTests.SSID_FILE_PATH);
            this.instrumentsForTestsPath = Path.Combine(tsunamiPath[0], InstrumentFileTests.SN_FILE_PATH);

            XmlDocument doc = new XmlDocument();
            doc.Load(this.SSIDandIPpath);
            string gitlabTag = File.ReadAllText(this.instrumentsForTestsPath);
            this.serialNumberAT = gitlabTag.Split(' ')[0];
            this.serialNumberTS = gitlabTag.Split(' ')[1];
            this.atSSID = WiFi.GetInstrumentSSID(doc, this.serialNumberAT);
            this.tsSSID = WiFi.GetInstrumentSSID(doc, this.serialNumberTS);
            this.ipAdressAT = Ip.GetIPFromXML(doc, this.serialNumberAT);
            this.ipAdressTS = Ip.GetIPFromXML(doc, this.serialNumberTS);

            WiFi.Connect(this.atSSID, true);
            WiFi.Connect(this.tsSSID, true);

        }

        [TestMethod()]
        public void CheckATIPExistInXML()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.SSIDandIPpath);
            var results = Ip.GetIPFromXML(doc, this.serialNumberAT);
            Assert.IsFalse(string.IsNullOrEmpty(results));         
        }

        [TestMethod()]
        public void CheckTSIPExistInXML()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(this.SSIDandIPpath);
            var results = Ip.GetIPFromXML(doc, this.serialNumberTS);
            Assert.IsFalse(string.IsNullOrEmpty(results));       
        }

        [TestMethod()]
        public void CheckGitlabTag()
        {
            string gitlabTag = File.ReadAllText(this.instrumentsForTestsPath);
            string[] instruments = gitlabTag.Split(' ');
            Assert.AreEqual(instruments.Length, 2);

        }


        [TestMethod()]
        public void IpATPingTest()
        {
     
            bool pingActual = Ip.Ping(this.ipAdressAT);
            bool pingExpected = Ip.PingFromCMD(this.ipAdressAT, useShell: false);

            Assert.AreEqual(pingExpected, pingActual);
            
        }

        [TestMethod()]
        public void IpTSPingTest()
        {

            bool pingActual = Ip.Ping(this.ipAdressTS);
            bool pingExpected = Ip.PingFromCMD(this.ipAdressTS);

            Assert.AreEqual(pingExpected, pingActual);

        }

        [TestMethod()]
        public void IpForATxResponseTest()
        {
            // Failed tests means that IP of instrument was not available
            bool pingResults = Ip.PingFromCMD(this.ipAdressAT);

            Assert.IsTrue(pingResults);
 

        }

        [TestMethod()]

        public void IpForTSxResponseTest()
        {
            // Failed tests means that IP of instrument was not available
            bool pingResults = Ip.PingFromCMD(this.ipAdressTS);

            Assert.IsTrue(pingResults);

        }

        [TestMethod()]

        public void IpDuplicatedTests()
        {

            bool duplicatResults = Ip.IsDuplicated(this.ipAdressAT);
            Assert.IsFalse(duplicatResults);

        }

       
        [TestMethod()]
        public void GetMyIpTests()
        {
            var host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            var exc = host.AddressList
                .Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork
                && x.ToString().Contains("192.168.") || x.ToString().Contains("128.141."));

            Assert.ThrowsException<Exception>(() => Ip.GetMyIp());

        }


        

    }
}