﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments.Device.AT40x;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D = TSU.Common.Instruments.Device;
using System.IO;
using System.Xml;
using TSU.Connectivity_Tests;
using System.Text.RegularExpressions;
using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;
using E = TSU.Common;
using System.Diagnostics.Eventing.Reader;

namespace TSU.Connectivity_Tests.Instruments.Device.AT40x.Tests
{
    [TestClass()]
    public class ConnectionTests
    {


        private string name = "test";
        
        public static D.AT40x.Module moduleAt;
        public static string ipAdressAT;
        public static string idAT;

        public static void FindIPAndConnect()
        {
            string serialNumberAT;
            string atSSID;

            var tsunamiPath = Environment.CurrentDirectory.Split(new string[] { "src" }, StringSplitOptions.None);
            string SSIDandIPpath = Path.Combine(tsunamiPath[0], InstrumentFileTests.SSID_FILE_PATH);
            string instrumentsForTestsPath = Path.Combine(tsunamiPath[0], InstrumentFileTests.SN_FILE_PATH);

            string gitlabTag = File.ReadAllText(instrumentsForTestsPath);
            XmlDocument doc = new XmlDocument();
            doc.Load(SSIDandIPpath);

            var parts = Regex.Split(gitlabTag, @"\s+|\n");
            serialNumberAT = parts[0];

            atSSID = WiFi.GetInstrumentSSID(doc, serialNumberAT);
            if (atSSID == "")
                throw new Exception($"SSID for {serialNumberAT} not found in the file '{SSIDandIPpath}'");

            idAT = WiFi.GetInstrumentID(doc, serialNumberAT);

            ipAdressAT = Ip.GetIPFromXML(doc, serialNumberAT);
            if (ipAdressAT == "")
                throw new Exception($"IP for {serialNumberAT} not found in the file '{instrumentsForTestsPath}'");

            // Connect wifi, before connecting instrument
            WiFi.Connect(atSSID, true);

            // Create At40x object
            Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();

            Tsunami2.InitializePropertiesForUnitTesting();

            D.AT40x.Instrument obj = (D.AT40x.Instrument)Preferences.Preferences.Instance.Instruments.Find(x => x.Id == idAT);
            moduleAt = new D.AT40x.Module(null, obj);
        }

        [TestInitialize]
        public void Initialize()
        {
            try
            {
                FindIPAndConnect();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        private void ModuleAt_MeasurementAvailable(object sender, M.MeasurementEventArgs e)
        {
            throw new NotImplementedException();
        }

        [TestMethod]
        [TestCategory("ConnectionAtX")]
        public void ConnectedAlreadyTest()
        {

            Assert.ThrowsException<InvalidOperationException>(() => moduleAt.ConnectAdapter(new Ip(ipAdressAT)));

        }

        [TestMethod()]
        [TestCategory("ConnectionAtX")]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        public void ConnectAndMeasureTest()
        {
            try
            {
                //Connect to instrument 
                moduleAt.ConnectAdapter(new Ip(ipAdressAT));
                bool isConnected = moduleAt.IsConnectedToAT(ipAdressAT);
                Assert.IsTrue(isConnected);
                Assert.AreEqual(isConnected, moduleAt.InstrumentIsOn);


                //Change reflector
                moduleAt.ToBeMeasuredData = new Polar.Measure()
                {
                    Face = I.FaceType.DoubleFace,
                    DirectMeasurementWanted = true,
                    Distance = new M.MeasureOfDistance() { Reflector = I.Reflector.Reflector.GetbyName("RFI_001") }
                };
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

        [TestMethod()]
        [TestCategory("ConnectionAtX")]
        public void ResetConnectionTest()
        {
            moduleAt.ResetConnection();
            Assert.IsFalse(moduleAt.InstrumentIsOn);
            moduleAt.Disconnect();
        }


        [TestMethod()]
        [TestCategory("ConnectionAtX")]
        public void DisconnectTest()
        {
            moduleAt.Disconnect();
            Assert.IsFalse(moduleAt.InstrumentIsOn);
        }


        [TestMethod()]
        [TestCategory("ConnectionAtX")]
        [DataRow("192.168.0.200")]
        [DataRow("192.168.0.101")]
        [DataRow("191.168.0.100")]
        [DataRow("191.169.0.100")]
        public void ConnectToInvalidIPAdapterTest(string invalidIP)
        {
            Assert.ThrowsException<InvalidOperationException>(() => moduleAt.ConnectAdapter(new Ip(invalidIP))); 
        }

        [TestMethod()]
        [TestCategory("ConnectionAtX")]
        public void ConnectAutomaticallyTest()
        {
            Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
            D.AT40x.Instrument obj = (D.AT40x.Instrument)Preferences.Preferences.Instance.Instruments.Find(x => x.Id == idAT);
            D.AT40x.Module m = new D.AT40x.Module(null, obj);
            TSU.WiFi.TryToConnectAutomaticallyWithWifi(m.ConnectToAT, SN: obj._SerialNumber ) ;
            Assert.IsTrue(m.InstrumentIsOn);
            m.Disconnect();
        }
    }
}