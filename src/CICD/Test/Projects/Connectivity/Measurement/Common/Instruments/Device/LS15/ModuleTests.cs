﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments.Device.LS15;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Common.Measures;
using System.Text.RegularExpressions;

namespace TSU.Common.Instruments.Device.LS15.Tests
{
    [TestClass()]
    public class ModuleTests
    {
        public const double PI = Math.PI;

        [TestMethod()]
        public void ConnectivityTest()
        {
            // intialize waht we need
            string serialNumber = "703434";
            var instrument = new LS15.Instrument() { _SerialNumber = serialNumber }; // this is a bit sad to force the SN, but for DNA03 we have only one, so fine.
            LS15.Module module = new LS15.Module(null, instrument);

            Result result;

            // check port availibility
            var availableComPorts = module.GetAllPorts();
            Assert.IsTrue(availableComPorts.Any(), "No COM port available on the machine that is running the test");

            // check connection
            result = module.Connect();
            if (!result.Success)
                result = module.TryConnectOthersPorts();
            Assert.IsTrue(result.Success, $"Connection to LS15 not failed: '{result.ErrorMessage}'");
            Assert.IsTrue(module.InstrumentIsOn, $"LS15 not considered as connected");

            // check serial number
            result = module.GetSerialNumber();
            string LSConnectedSN = result.AccessToken;
            Regex regexObj = new Regex(@"[^\d]");
            LSConnectedSN = regexObj.Replace(LSConnectedSN, "");
            Assert.IsTrue(result.Success, $"Couldnt get the serial number of the LS15 '{result.ErrorMessage}'");
            Assert.AreEqual(serialNumber, LSConnectedSN, $"Connection to LS15 failed: '{result.ErrorMessage}'");

            // check battery level
            var batteryLevel = module.GetIntBatteryLevel();
            Assert.IsTrue(batteryLevel > 10, "Battery level is below 10% further tests cancelled");

            // check initialisation
            result = module.InitializeSensor();
            Assert.IsTrue(result.Success, $"Couldnt initialise the LS15 '{result.ErrorMessage}'");

            // check measurement
            result = module.Measure();
            Assert.IsTrue(result.Success, $"LS15 measurement failed: '{result.ErrorMessage}'");
            (double levelReading, double dist) = Module.ConvertMeasurementResults(result);
            Assert.IsTrue(levelReading != -9999, "Height measurement return error value (-9999)");
            Assert.IsTrue(levelReading > 0, "Height measured in smaller than 0.0m");
            Assert.IsTrue(levelReading < 2, "Height measure in bigger than 2.0m");
            Assert.IsTrue(dist > 1, "Distance measure betwen the level and the rod is smaller than 1.0m (not expected)");
            Assert.IsTrue(dist < 10, "Distance measure betwen the level and the rod is longer than 10.0m (not expected)");

            // check compass
            MeasureOfLevel measure = new MeasureOfLevel();
            result = module.GetCompassValue();
            Assert.IsTrue(result.Success, $"LS15 Compass measurement failed: '{result.ErrorMessage}'");
            double compassValue;
            if (double.TryParse(result.AccessToken, out compassValue))
            {
                Assert.IsTrue(compassValue >= 0, "Compass value in smaller than 0 grad");
                Assert.IsTrue(compassValue <= 400, "Compass value in bigger than 400 grad");
            }

            // check disconnection
            result = module.Disconnect();
            Assert.IsTrue(result.Success, $"LS15 disconnection failed: '{result.ErrorMessage}'");
            Assert.IsTrue(module.InstrumentIsOn == false, $"LS15 not considered as disconnected");
        }
    }
}