﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments.Device.DNA03;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TSU.Polar.GuidedModules.Steps.StakeOut.Tools;

namespace TSU.Common.Instruments.Device.DNA03.Tests
{
    [TestClass()]
    public class ModuleTests
    {
        [TestMethod()]
        public void ConnectivityTest()
        {
            // intialize waht we need
            string serialNumber = "334338";
            var dna03 = new DNA03.Instrument() { _SerialNumber = serialNumber }; // this is a bit sad to force the SN, but for DNA03 we have only one, so fine.
            DNA03.Module dna00Module = new DNA03.Module(null, dna03);

            Result result;

            // check port availibility
            var availableComPorts = dna00Module.GetAllPorts();
            Assert.IsTrue(availableComPorts.Any(), "No COM port available on the machine that is running the test");

            // check connection
            result = dna00Module.Connect();
            Assert.IsTrue(result.Success, $"Connection to DNA03 not failed: '{result.ErrorMessage}'");
            Assert.IsTrue(dna00Module.InstrumentIsOn, $"DNA03 not considered as connected");

            // check serail number
            result = dna00Module.GetSerialNumber();
            Assert.IsTrue(result.Success, $"Couldnt get the serial number of the DNA '{result.ErrorMessage}'");
            Assert.AreEqual(serialNumber, result.AccessToken, $"Connection to DNA03 not failed: '{result.ErrorMessage}'");

            // check battery level
            var batteryLevel = dna00Module.GetIntBatteryLevel();
            Assert.IsTrue(batteryLevel > 10, "Battery level is below 10% further tests cancelled");

            // check initialisation
            result = dna00Module.InitializeSensor();
            Assert.IsTrue(result.Success, $"Couldnt initialise the  DNA '{result.ErrorMessage}'");

            // check measurement
            result = dna00Module.Measure();
            Assert.IsTrue(result.Success, $"DNA03 measurement failed: '{result.ErrorMessage}'");
            (double levelReading, double dist) = DNA03.Module.ConvertMeasurementResults(result);
            Assert.IsTrue(levelReading != -9999, "Height measurement return error value (-9999)");
            Assert.IsTrue(levelReading > 0, "Height measured in smaller than 0.0m");
            Assert.IsTrue(levelReading < 2, "Height measure in bigger than 2.0m");
            Assert.IsTrue(dist > 1, "Distance measure betwen the level and the rod is smaller than 1.0m (not expected)");
            Assert.IsTrue(dist < 10, "Distance measure betwen the level and the rod is longer than 10.0m (not expected)");

            // check disconnection
            result = dna00Module.Disconnect();
            Assert.IsTrue(result.Success, $"DNA03 disconnection failed: '{result.ErrorMessage}'");
            Assert.IsTrue(dna00Module.InstrumentIsOn == false, $"DNA03 not considered as disconnected");

        }
    }
}