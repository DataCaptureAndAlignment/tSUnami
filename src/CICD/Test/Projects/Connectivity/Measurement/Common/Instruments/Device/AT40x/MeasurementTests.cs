﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments.Device.AT40x;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D = TSU.Common.Instruments.Device;
using System.Xml;
using System.IO;
using TSU.Common.Elements;
using System.Diagnostics;
using System.Threading;
using System.Reflection.Metadata;
using TSU.Connectivity_Tests;
using AT = TSU.Common.Instruments.Device.AT40x;
using static TSU.Common.Instruments.Device.AT40x.Target;
using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;
using E = TSU.Common;
using TSU.Common.Compute.Compensations.BeamOffsets;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.InteropServices.WindowsRuntime;

namespace TSU.Connectivity_Tests.Instruments.Device.AT40x.Tests
{
    [TestClass()]
    public class MeasurementsTests
    {
        static D.AT40x.Module moduleAT
        {
            get
            {
                return ConnectionTests.moduleAt;
            }
        }


        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            try
            {
                Action a = () => {
                    ConnectionTests.FindIPAndConnect();

                    // Initialize AT40X
                    moduleAT.ConnectAdapter(new Ip(ConnectionTests.ipAdressAT));
                    moduleAT.SubscribeAt40xEvents();
                    moduleAT.GetInitializationStatus();
                    if (!moduleAT.IsInitialized)
                        moduleAT.InitializeByLeica_Sync();
                    moduleAT._measurement.mode = AT.Module.aT40xMeasurementMode.Fast;
                };
                // Create a new STA thread
                Thread staThread = new Thread(new ThreadStart(a));
                staThread.SetApartmentState(ApartmentState.STA); // Set the apartment state to STA
                staThread.Start();
                staThread.Join();
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [TestCategory("AtX")]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        public void InitalizingATTest()
        {
            bool init = moduleAT.GetInitializationStatus();
            Assert.IsTrue(moduleAT.IsInitialized);
            Assert.AreEqual(init, moduleAT.IsInitialized, "AT not initialized");
        }



        [TestMethod]
        [TestCategory("AtX")]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        public void MeasureOnePointATTest()
        {
            E.Elements.Point P1 = new E.Elements.Point()
            {
                _Name = "test_1",
                _Coordinates = new E.Elements.CoordinatesInAllSystems() { Ccs = new E.Elements.Coordinates("", -1218.12964, 10395.427050, 422.778475) }
            };

            double v1 = 79.336822;
            double v2 = 99.388499;
            double v3 = 10.746851;
            double v4 = 0.070000;

            Polar.Measure measure = new Polar.Measure()
            {
                _OriginalPoint = P1,
                _Point = P1,
                Angles = new M.MeasureOfAngles()
                {
                    Corrected = new E.Elements.Angles()
                    {
                        Horizontal = new DoubleValue(v1),
                        Vertical = new DoubleValue(v2)
                    }
                },
                Distance = new M.MeasureOfDistance()
                {
                    Corrected = new DoubleValue(v3),
                    Reflector = new I.Reflector.Reflector() { _Name = "RFI0.5_1", _Model = "RFI0.5" }
                },
                Extension = new DoubleValue(v4),
                _Status = new M.States.Good(),
                NumberOfMeasureToAverage = 1,
                Face = I.FaceType.Face1
            };

            TSU.Result results = null;
            bool received = false;
            //void connectivityTestMeasureWithAt40x()
            //{
                moduleAT._ToBeMeasureTheodoliteData = measure;
                moduleAT.SetTargetAsSelected();
                moduleAT.MeasurementAvailable += HandleMeasureReceivedEvent;

                eventSlim.Reset();
                results = moduleAT.Measure(); // this returns only thaht the measure started not that it fnished or get values
                received = eventSlim.Wait(TimeSpan.FromSeconds(10)); // tha async method should reply in 5sec
            //}
            //// Create a new STA thread
            //Thread staThread = new Thread(new ThreadStart((Action)connectivityTestMeasureWithAt40x));
            //staThread.SetApartmentState(ApartmentState.STA); // Set the apartment state to STA
            //staThread.Start();

            //staThread.Join();
            Assert.IsTrue(results.Success == true, $"Measure denied {results.AccessToken}");

            Assert.IsTrue(received);
            Assert.IsTrue(moduleAT.JustMeasuredData != null);
            var m = moduleAT.JustMeasuredData as Polar.Measure;
            //Assert.AreEqual(298.376, m.Angles.corrected.Horizontal.Value, 0.01); 

            TSU.Connectivity_Tests.InstrumentsTools.RecordOnDiskTheOrientation("AT40x_StartingPosition.txt", m);
            TSU.Connectivity_Tests.InstrumentsTools.ReadOnDiskTheOrientation("AT40x_StartingPosition.txt", out var m2);

            Assert.AreEqual(83.836, m.Angles.corrected.Vertical.Value, 0.01);
            Assert.AreEqual(2.424, m.Distance.Corrected.Value, 0.01);
        }


        private ManualResetEventSlim eventSlim = new ManualResetEventSlim(false);

        private void HandleMeasureReceivedEvent(object sender, M.MeasurementEventArgs e)
        {
            // Your event handling logic
            eventSlim.Set(); // Signal that the event has occurred
        }

        private void HandleEvent(object sender, EventArgs e)
        {
            // Your event handling logic
            eventSlim.Set(); // Signal that the event has occurred
        }

        [TestMethod]
        [TestCategory("AtX")]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        public void TwoPointsGoToATTest()
        {
            // supposingly on the P1 of 926-R005 test zone and moving to P2
            double oH = 298.376 - 246.481;
            double oV = 83.836 - 88.156;
            double oD = 2.424 - 2.021;

            E.Elements.Point P2 = new E.Elements.Point()
            {
                _Name = "test_1",
                _Coordinates = new E.Elements.CoordinatesInAllSystems() { Ccs = new E.Elements.Coordinates("", -1418.12964, 10395.427050, 422.778475) }
            };

            double v1_1 = 79.336822;
            double v2_1 = 99.388499;
            double v3_1 = 10.746851;
            double v4_1 = 0.070000;

            Polar.Measure measure1 = new Polar.Measure()
            {
                _OriginalPoint = P2,
                _Point = P2,
                Angles = new M.MeasureOfAngles()
                {
                    Corrected = new E.Elements.Angles()
                    {
                        Horizontal = new DoubleValue(v1_1),
                        Vertical = new DoubleValue(v2_1)
                    }
                },
                Distance = new M.MeasureOfDistance()
                {
                    Corrected = new DoubleValue(v3_1),
                    Reflector = new I.Reflector.Reflector() { _Name = "RFI0.5", _Model = "RFI0.5" }
                },
                Extension = new DoubleValue(v4_1),
                _Status = new M.States.Good(),
                NumberOfMeasureToAverage = 1,
                Face = I.FaceType.Face1
            };

            moduleAT._ToBeMeasureTheodoliteData = measure1;

            //move from P1 to P2
            moduleAT._motor.MovebyAngle(ENUM.movingDirection.Left, oH);
            moduleAT._motor.MovebyAngle(ENUM.movingDirection.Down, Math.Abs(oV));

            //measure P2
            moduleAT.MeasurementAvailable += HandleMeasureReceivedEvent;
            eventSlim.Reset();

            moduleAT.SetTargetAsSelected();
            var results = moduleAT.Measure(); // this returns only thaht the measure started not that it fnished or get values
            bool received = eventSlim.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(received);
            Assert.IsTrue(moduleAT.JustMeasuredData != null);
            var m = moduleAT.JustMeasuredData as Polar.Measure;
            //Assert.AreEqual(246.481, m.Angles.corrected.Horizontal.Value, 0.01);  
            Assert.AreEqual(88.156, m.Angles.corrected.Vertical.Value, 0.01);       
            Assert.AreEqual(2.021, m.Distance.Corrected.Value, 0.01); 


            E.Elements.Point P1 = new E.Elements.Point()
            {
                _Name = "test_2",
                _Coordinates = new E.Elements.CoordinatesInAllSystems() { Ccs = new E.Elements.Coordinates("", -1218.12964, 10395.427050, 422.778475) }
            };

            double v1_2 = 79.336822;
            double v2_2 = 99.388499;
            double v3_2 = 10.746851;
            double v4_2 = 0.070000;

            Polar.Measure measure2 = new Polar.Measure()
            {
                _OriginalPoint = P1,
                _Point = P1,
                Angles = new M.MeasureOfAngles()
                {
                    Corrected = new E.Elements.Angles()
                    {
                        Horizontal = new DoubleValue(v1_2),
                        Vertical = new DoubleValue(v2_2)
                    }
                },
                Distance = new M.MeasureOfDistance()
                {
                    Corrected = new DoubleValue(v3_2),
                    Reflector = new I.Reflector.Reflector() { _Name = "RFI0.5_1", _Model = "RFI0.5" }
                },
                Extension = new DoubleValue(v4_2),
                NumberOfMeasureToAverage = 1,
                Face = I.FaceType.Face1,
                _Status = new M.States.Good(),
            };

            //move bcak to P1 
            Thread.Sleep(1000);
            moduleAT._motor.MovebyAngle(ENUM.movingDirection.Right, oH);
            moduleAT._motor.MovebyAngle(ENUM.movingDirection.Up, Math.Abs(oV));

            //measure P2
            moduleAT.MeasurementAvailable += HandleMeasureReceivedEvent;
            eventSlim.Reset();

            moduleAT.SetTargetAsSelected();
            results = moduleAT.Measure(); // this returns only thaht the measure started not that it fnished or get values
            received = eventSlim.Wait(TimeSpan.FromSeconds(10));

            Assert.IsTrue(received);
            Assert.IsTrue(moduleAT.JustMeasuredData != null);
            m = moduleAT.JustMeasuredData as Polar.Measure;
            //Assert.AreEqual(246.481, m.Angles.corrected.Horizontal.Value, 0.01);  
            Assert.AreEqual(83.836, m.Angles.corrected.Vertical.Value, 0.01);
            Assert.AreEqual(2.424, m.Distance.Corrected.Value, 0.01);
        }


        [TestMethod]
        [TestCategory("AtX")]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        public void BatterylevelTest()
        {
            moduleAT._status.GetBatteryLevel();
            Assert.IsTrue(moduleAT._status.batteryLevelSensor > 100, "AT is not powered up from the wall"); // meaning that we connected to wall power supply
        }
    }














}