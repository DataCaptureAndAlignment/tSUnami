﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments.Device.TC2002;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSU.Common.Instruments.Device.TC2002.Tests
{
    [TestClass()]
    public class ModuleTests
    {
        [TestMethod()]
        public void ConnectTest()
        {
            // intialize waht we need
            string serialNumber = "1";
            var instrument = new TC2002.Instrument() { _SerialNumber = serialNumber }; // this is a bit sad to force the SN, but for DNA03 we have only one, so fine.
            //var finalModule = new Polar.Module(); // we need it because some DSAflags are used in the InstrumentModule.Measure()
            TC2002.Module module = new TC2002.Module(null, instrument);

            Result result;

            // check port availibility
            var availableComPorts = module.GetAllPorts();
            Assert.IsTrue(availableComPorts.Any(), "No COM port available on the machine that is running the test");

            // check connection
            Assert.IsTrue(module.InstrumentIsOn == false, $"TC2002 not considered as disconnected before trying to connect");
            result = module.Connect();
            Assert.IsTrue(result.Success, $"Connection to TC2002 not failed: '{result.ErrorMessage}'");
            Assert.IsTrue(module.InstrumentIsOn, $"DNA03 not considered as connected");

            // check initialisation
            result = module.InitializeSensor();
            Assert.IsTrue(result.Success, $"Couldnt initialise the  DNA '{result.ErrorMessage}'");
            //// seems we have no way to get some of those value to test them.

            // check measurement
            var measure = new Polar.Measure()
            {
                Face = FaceType.Face1,
                Reflector = new Reflector.Reflector() {_Name = "testc" },
                NumberOfMeasureToAverage = 1
            };
            module.ToBeMeasuredData = measure;

            module.PauseBetweenAnglesAndDistance = false; // avoid a message between angle and distance measurement
            result = module.Measure();
            double doubl = 1;
            Assert.IsTrue(result.Success, $"TC2002 measurement failed: '{result.ErrorMessage}'");
            var measured = module.JustMeasuredData as Polar.Measure;
            Assert.IsTrue(measured.Angles.Corrected.Horizontal.Value > 0, "AH from AT40x is smaller than 0 gon (not expected)");
            Assert.IsTrue(measured.Angles.Corrected.Horizontal.Value < 400, "AH from AT40x is bigger than 400 gon (not expected)");
            Assert.IsTrue(measured.Angles.Corrected.Vertical.Value > 0, "AV from AT40x is smaller than 0 gon (not expected)");
            Assert.IsTrue(measured.Angles.Corrected.Vertical.Value < 200, "AH from AT40x is bigger than 200 gon (not expected)");
            Assert.IsTrue(measured.Distance.Corrected.Value > 0, "Distance from AT40x is smaller than 0 m (not expected)");
            Assert.IsTrue(measured.Distance.Corrected.Value < 10, "AH from AT40x is bigger than 10m (not expected)");


            // check disconnection
            result = module.Disconnect();
            Assert.IsTrue(result.Success, $"TC2002 disconnection failed: '{result.ErrorMessage}'");
            Assert.IsTrue(module.InstrumentIsOn == false, $"TC2002 not considered as disconnected");
        }
    }
}