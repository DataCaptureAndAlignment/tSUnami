﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments.Device.AT40x;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using D = TSU.Common.Instruments.Device;
using System.Xml;
using System.IO;
using TSU.Common.Elements;
using System.Diagnostics;
using System.Threading;
using System.Reflection.Metadata;
using TSU.Connectivity_Tests;
using AT = TSU.Common.Instruments.Device.AT40x;
using static TSU.Common.Instruments.Device.AT40x.Target;
using M = TSU.Common.Measures;
using I = TSU.Common.Instruments;
using E = TSU.Common;
using TSU.Common.Compute.Compensations.BeamOffsets;
using System.Diagnostics.Eventing.Reader;
using System.Runtime.InteropServices.WindowsRuntime;

namespace TSU.Connectivity_Tests
{
    public static class InstrumentsTools
    {
        
        public static bool ReadOnDiskTheOrientation(string filePath, out Polar.Measure measure)
        {
            measure = new Polar.Measure();

            using (StreamReader reader = new StreamReader(filePath))
            {
                string line = reader.ReadLine();
                if (double.TryParse(line, out double d))
                    measure.Angles.Corrected.Horizontal.Value = d;
                else
                    return false;

                line = reader.ReadLine();
                if (double.TryParse(line, out d))
                    measure.Angles.Corrected.Vertical.Value = d;
                else
                    return false;

                line = reader.ReadLine();
                if (double.TryParse(line, out d))
                    measure.Distance.Corrected.Value = d;
                else
                    return false;
            }
            return true;
        }

        public static bool RecordOnDiskTheOrientation(string filePath, Polar.Measure m)
        {
            try
            {
                // Simulate some test logic
                File.WriteAllText(filePath, string.Empty); // Clears the file content

                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    writer.WriteLine(m.Angles.Corrected.Horizontal.Value);
                    writer.WriteLine(m.Angles.Corrected.Vertical.Value);
                    writer.WriteLine(m.Distance.Corrected.Value);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}