﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Instruments.Device.TS60;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using D = TSU.Common.Instruments.Device;
using TSU.Connectivity_Tests;
using static System.Net.Mime.MediaTypeNames;
using System.Text.RegularExpressions;
using static TSU.Polar.GuidedModules.Steps.StakeOut.Tools;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using TSU.Common.Elements;
using UnitTestGenerator;
using MathNet.Numerics;

namespace TSU.Common.Instruments.Device.TS60.Tests
{
    [TestClass()]
    public class ConnectionTests
    {
        private string name = "test";
        string serialNumberTS;
        string tsSSID;
        string idTS;
        string ipAdressTS;
        D.TS60.Module moduleTS;


        [TestInitialize]
        public void Initialize()
        {
            var tsunamiPath = Environment.CurrentDirectory.Split(new string[] { "src" }, StringSplitOptions.None);
            string SSIDandIPpath = Path.Combine(tsunamiPath[0], InstrumentFileTests.SSID_FILE_PATH);
            string instrumentsForTestsPath = Path.Combine(tsunamiPath[0],InstrumentFileTests.SN_FILE_PATH);

            if (!File.Exists(instrumentsForTestsPath))
                Assert.Fail($"{instrumentsForTestsPath} not found");

            string gitlabTag = File.ReadAllText(instrumentsForTestsPath);
            XmlDocument doc = new XmlDocument();
            doc.Load(SSIDandIPpath);

            var parts = Regex.Split(gitlabTag, @"\s+|\n");
            this.serialNumberTS = parts[1];
            this.tsSSID = WiFi.GetInstrumentSSID(doc, this.serialNumberTS);
            this.idTS = WiFi.GetInstrumentID(doc, this.serialNumberTS);
            this.ipAdressTS = Ip.GetIPFromXML(doc, this.serialNumberTS);

            // Connect wifi, before connecting instrument
            WiFi.Connect(this.tsSSID, true);

            // Create At40x object
            Preferences.Preferences.Instance.InitializeWithAllXmlParametersFiles();
            D.TS60.Instrument obj = (D.TS60.Instrument)Preferences.Preferences.Instance.Instruments.Find(x => x.Id == this.idTS);
            this.moduleTS = new D.TS60.Module(null, obj);

        }

        [TestMethod()]
        [TestCategory("ConnectionTS")]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [DeploymentItem(@"Preferences\", @"Preferences\")]
        public void ConnectTCPTest()
        {
            try
            {
                var module = this.moduleTS;
                string serialNumber = this.serialNumberTS;
                var ts60 = module.Instrument as TS60.Instrument;

                Result result = null;

                //Connect to instrument 
                module.Connect();
                Assert.IsTrue(this.moduleTS.tcpConnected);

                //Disconnnect to instrument 
                module.Disconnect();
                Assert.IsFalse(this.moduleTS.tcpConnected);

                //ReConnect to instrument 
                module.Connect();
                Assert.IsTrue(this.moduleTS.tcpConnected);

                //module.MoveBy(0,80);

                // check serail number
                result = module.GetSerialNumber();
                Assert.IsTrue(result.Success, $"Couldnt get the serial number of the LS15 '{result.ErrorMessage}'");
                Assert.IsTrue(result.AccessToken.Contains(serialNumber), $"Connection to LS15 not failed: '{result.ErrorMessage}'");

                // check battery level
                result = module.GetBatteryLevel();
                string[] parts = result.AccessToken.Split(' ');
                if (double.TryParse(parts[3], out double batteryLevel))
                    Assert.IsTrue(batteryLevel > 10, "Battery level is below 10% further tests cancelled");
                else
                    Assert.Fail("Battery level couldn't be retrieved");

                // check initialisation
                result = module.InitializeSensor();
                Assert.IsTrue(result.Success, $"Couldnt do the TS60 sensor initialisation'{result.ErrorMessage}'");

                // check measurement
                ts60.AtrWanted = true;

                // FACE1
                module.GoFace(1);
                var mInfo = "TS60 face1 measurement#1 with ATR";
                result = module.GetDistanceAndAngle(out DoubleValue distance1, out Angles angles1, purgeDistance: true);
                Assert.IsTrue(result.Success, $"{mInfo} failed: '{result.ErrorMessage}'");
                Assert.IsFalse(distance1.Value < 1, $"{mInfo}: Distance measure between the ts60 and the target is smaller than 1.0m (not expected)");
                Assert.IsFalse(distance1.Value > 10, $"{mInfo}: Distance measure between the ts60 and the target is larger than 10.0m (not expected)");
                Assert.IsFalse(angles1.Horizontal.Value < 0, $"{mInfo}: AH must be greater than 0 gon");
                Assert.IsFalse(angles1.Horizontal.Value > 400, $"{mInfo}: AH must be smaller than 400 gon");
                Assert.IsFalse(angles1.Vertical.Value < 20, $"{mInfo}: AH must be greater than 20 gon");
                Assert.IsFalse(angles1.Vertical.Value > 180, $"{mInfo}: AH must be smaller than 180 gon");

                // Face2
                module.GoFace(2);
                mInfo = "TS60 face2 measurement#2 with ATR";
                result = module.GetDistanceAndAngle(out DoubleValue distance2, out Angles angles2, purgeDistance: true);
                Assert.IsTrue(result.Success, $"{mInfo} failed: '{result.ErrorMessage}'");
                Assert.IsFalse(distance1.Value < 1, $"{mInfo}: Distance measure between the ts60 and the target is smaller than 1.0m (not expected)");
                Assert.IsFalse(distance1.Value > 10, $"{mInfo}: Distance measure between the ts60 and the target is larger than 10.0m (not expected)");
                Assert.IsFalse(angles1.Horizontal.Value < 0, $"{mInfo}: AH must be greater than 0 gon");
                Assert.IsFalse(angles1.Horizontal.Value > 400, $"{mInfo}: AH must be smaller than 400 gon");
                Assert.IsFalse(angles1.Vertical.Value < 20, $"{mInfo}: AH must be greater than 20 gon");
                Assert.IsFalse(angles1.Vertical.Value > 180, $"{mInfo}: AH must be smaller than 180 gon");

                var differenceInDistanceInMm = (distance2.Value - distance1.Value) * 1000;
                Assert.AreEqual(distance1.Value, distance2.Value, 0.001, $"{mInfo}: Distance difference between F1 and F2 must be within 1mm and is {differenceInDistanceInMm}");

                // simulated wrong measurement way to high
                module.GoFace(1);
                ts60.AtrWanted = false; // because the angles need a distance if atr is on.
                double dV = 80; //gon
                module.MoveBy(-dV, -dV);

                mInfo = "TS60 face1 measurement#3 without ATR";
                result = module.GetDistanceAndAngle(out DoubleValue distance3, out Angles angles3, purgeDistance: true);
                Assert.IsFalse(result.Success, $"{mInfo} failed: '{result.ErrorMessage}'");

                // move back and remeasure
                module.MoveTo(angles1.Horizontal.Value, angles1.Vertical.Value);
                ts60.AtrWanted = false;
                mInfo = "TS60 face1 measurement#4 with ATR";
                result = module.GetDistanceAndAngle(out DoubleValue distance4, out Angles angles4, purgeDistance: true);
                Assert.IsTrue(result.Success, $"{mInfo} failed: '{result.ErrorMessage}'");

                differenceInDistanceInMm = (distance4.Value - distance1.Value) * 1000;
                Assert.AreEqual(distance1.Value, distance4.Value, 0.001, $"{mInfo}: Distance difference between F1 and F2 must be within 1mm and is {differenceInDistanceInMm}");

                //Disconnnect to instrument 
                module.Disconnect();
                Assert.IsFalse(this.moduleTS.tcpConnected);
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        [TestMethod]
        [TestCategory("ConnectionTS")]
        public void ConnectedAlreadyTest()
        {

            Assert.ThrowsException<InvalidOperationException>(() => this.moduleTS.Connect());

        }

      

        [TestMethod()]
        [TestCategory("ConnectionTS")]
        public void DisconnectTest()
        {
            this.moduleTS.Disconnect();
            Assert.IsFalse(this.moduleTS.InstrumentIsOn);
        }

        [TestMethod()]
        [TestCategory("ConnectionTS")]
        [DataRow("192.168.0.200")]
        [DataRow("192.168.0.101")]
        [DataRow("191.168.0.100")]
        [DataRow("191.169.0.100")]
        public void ConnectToInvalidIPAdapterTest(string invalidIP)
        {
            //Assert.ThrowsException<InvalidOperationException>(() => this.moduleTS.ConnectAdapter(new Ip(invalidIP)));
        }


    }
}