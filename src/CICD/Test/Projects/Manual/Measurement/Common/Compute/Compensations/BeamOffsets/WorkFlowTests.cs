﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TSU.Common.Compute.Compensations.BeamOffsets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSU.Common.Elements;
using TSU.Tools;
using System.Windows.Forms;
using TSU.Tools;

namespace TSU.Common.Compute.Compensations.BeamOffsets.Tests
{

    [TestClass()]
    public class WorkFlowTests
    {

        public void AddEstimatedParameters(Frame.Parameters ep, int v1, int v2, int v3)
        {
            ep = new Frame.Parameters();
            ep.RX = new Frame.Parameters.Parameter() { Value = v1 };
            ep.RY = new Frame.Parameters.Parameter() { Value = v2 };
            ep.RZ = new Frame.Parameters.Parameter() { Value = v3 };
        }

        public void AddInclyResiduals(List<Frame.FrameMeasurement.Incly> Inclys, string name, double v1)
        {
            var existing = Inclys.Find(x => x.Point == name);
            if (existing == null)
            {
                existing = new Frame.FrameMeasurement.Incly();
                Inclys.Add(existing);
            }

            existing.Point = name;
            existing.Roll.Residual = v1;
        }

        public void AddObsResiduals(List<Frame.FrameMeasurement.ObsXYZ> obs, string name, double v1, double v2, double v3)
        {
            var existing = obs.Find(x => x.Point == name);
            if (existing == null)
            {
                existing = new Frame.FrameMeasurement.ObsXYZ();
                obs.Add(existing);
            }

            existing.Point = name;
            existing.X.Residual = v1;
            existing.Y.Residual = v2;
            existing.Z.Residual = v3;
        }

        public void AddObsSigma(List<Frame.FrameMeasurement.ObsXYZ> obs, string name, double v1, double v2, double v3)
        {

            var existing = obs.Find(x => x.Point == name);
            if (existing == null)
            {
                existing = new Frame.FrameMeasurement.ObsXYZ();
                existing.X = new Frame.FrameMeasurement.Observation();
                existing.Y = new Frame.FrameMeasurement.Observation();
                existing.Z = new Frame.FrameMeasurement.Observation();

                obs.Add(existing);
            }

            existing.Point = name;
            existing.X.Sigma = v1;
            existing.Y.Sigma = v2;
            existing.Z.Sigma = v3;
        }

        private Tilt.Measure CreateRollMeasure(Point p, double v)
        {
            return new Tilt.Measure()
            {
                _OriginalPoint = p,
                _Point = p,
                _ReadingBeamDirection = new DoubleValue(v),
                _ReadingOppositeBeam = new DoubleValue(-v),
                _Status = new Measures.States.Good(),

            };
        }

        private Point GetCcsPoint(string v1, double v2, double v3, double v4)
        {
            return new Elements.Point()
            {
                _Name = v1,
                _Coordinates = new Elements.CoordinatesInAllSystems() { Ccs = new Elements.Coordinates("", v2, v3, v4) }
            };

        }

        private Polar.Measure CreatePolarMeasure(Point P, double v1, double v2, double v3, double v4)
        {
            return new Polar.Measure()
            {
                _OriginalPoint = P,
                _Point = P,
                Angles = new Measures.MeasureOfAngles()
                {
                    Corrected = new Elements.Angles()
                    {
                        Horizontal = new DoubleValue(v1),
                        Vertical = new DoubleValue(v2)
                    }
                },
                Distance = new Measures.MeasureOfDistance()
                {
                    Corrected = new DoubleValue(v3),
                    Reflector = new Instruments.Reflector.Reflector() { _Name = "RRR1.5.1" }
                },
                Extension = new DoubleValue(v4),
                _Status = new Measures.States.Good(),
            };
        }

        private string getLgcInputForAStationMeasuringAMagnet()
        {
            return @"*TITR
Fichier d'input créé le 21-MAR-2023
Opération n 28159, 3D
*RS2K
*PUNC   OUT1
*FAUT     .01     .10
*PREC 6
*INSTR
*POLAR AT403.395161 RRR1.5.1 0 0 0 0
RRR1.5.1 3 3 .02 6 0 0 0 0 0 0
*INCL W1015 30 0 0 0 0 
*CALA
LHC.NID.6L5-02.                                           -1236.035780    10387.263670    421.714840   $13124.946    816819   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.6L5-01.                                           -1232.143620    10386.806220    422.282030   $13128.359    816820   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.5L5-05.                                           -1228.076280    10388.522930    423.143080   $13132.774    816821   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.5L5-03.                                           -1219.457340    10395.059090    422.911140   $13143.250    816822   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.5L5-02.                                           -1211.533740    10397.629260    421.385950   $13151.552    816812   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.5L5-01.                                           -1209.602220    10396.516630    423.395550   $13152.904    816813   coordonnées au 23-FEB-2023 00:00:00
LHC.NID.4L5-30.                                           -1203.185430    10398.887610    422.179090   $13159.739    816814   coordonnées au 23-FEB-2023 00:00:00
*POIN
LHC.STL.20230213BDKW01.                                   -1220.196700    10392.463530    422.945020   $-999.900    826501   coordonnées au 23-FEB-2023 00:00:00
LHC.MQML.5L5.E                                            -1230.463560    10389.646990    422.994024   $13131.001    516195   coordonnées réelles au 23-FEB-2023 00:00:00
LHC.MQML.5L5.S                                            -1227.047300    10391.080730    422.947241   $13134.706    516197   coordonnées réelles au 23-FEB-2023 00:00:00
LHC.B2.TCL.5L5.E                                          -1218.129640    10395.427050    422.778475   $13144.917    640301   coordonnées réelles au 23-FEB-2023 00:00:00
LHC.B2.TCL.5L5.S                                          -1217.539460    10395.674480    422.770584   $13145.557    640302   coordonnées réelles au 23-FEB-2023 00:00:00
% LHC.MQML.5L5.T                                            -1227.251700    10391.565710    422.952300   $13134.707    516196   coordonnées théoriques au 23-FEB-2023 00:00:00
*TSTN   LHC.STL.20230213BDKW01.   AT403.395161    IHFIX   IH  0.000000       %station n329490
*V0   
*ANGL   
LHC.NID.6L5-02.                                         76.094579   $1504199 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-01.                                         68.132229   $1504200 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-05.                                         66.758588   $1504201 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-03.                                        213.948228   $1504202 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-02.                                        262.038593   $1504203 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-01.                                        273.024865   $1504204 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.4L5-30.                                        273.291549   $1504205 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         76.094577   $1504206 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=2 cc, sV=2 cc, sD=0.00 mm,Off.: dH=0cc, dV=5.2cc, dD=-0.01mm' - Was Control according to Tsunami
LHC.B2.TCL.5L5.S                                       240.289988   $1504207 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.B2.TCL.5L5.E                                       235.052329   $1504208 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         76.094776   $1504209 - 13-FEB-2023 - TSU: 'Quick measure!, Control F1 / 1 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm, - Off.: dH=2cc, dV=6.8cc, dD=0mm' - Was Control according to Tsunami
LHC.MQML.5L5.S                                          83.602564   $1504210 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.MQML.5L5.E                                          79.236822   $1504211 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         76.094672   $1504212 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm,Off.: dH=0.9cc, dV=0.8cc, dD=0mm' - Was Control according to Tsunami
*ZEND   
LHC.NID.6L5-02.                                        104.689045 TH  0.000000   $1504213 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-01.                                        103.190317 TH  0.000000   $1504214 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-05.                                         98.568672 TH  0.000000   $1504215 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-03.                                        100.799551 TH  0.000000   $1504216 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-02.                                        109.762797 TH  0.000000   $1504217 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-01.                                         97.472580 TH  0.000000   $1504218 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.4L5-30.                                        102.679813 TH  0.000000   $1504219 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                        104.689560 TH  0.000000   $1504220 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=2 cc, sV=2 cc, sD=0.00 mm,Off.: dH=0cc, dV=5.2cc, dD=-0.01mm' - Was Control according to Tsunami
LHC.B2.TCL.5L5.S                                       101.594899 TH  0.070000   $1504221 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.B2.TCL.5L5.E                                       101.700755 TH  0.070000   $1504222 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                        104.689727 TH  0.000000   $1504223 - 13-FEB-2023 - TSU: 'Quick measure!, Control F1 / 1 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm, - Off.: dH=2cc, dV=6.8cc, dD=0mm' - Was Control according to Tsunami
LHC.MQML.5L5.S                                          99.342179 TH  0.070000   $1504224 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.MQML.5L5.E                                          99.288499 TH  0.070000   $1504225 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                        104.689129 TH  0.000000   $1504226 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm,Off.: dH=0.9cc, dV=0.8cc, dD=0mm' - Was Control according to Tsunami
*DIST   
LHC.NID.6L5-02.                                         16.716185 TH  0.000000   $1504227 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-01.                                         13.235201 TH  0.000000   $1504228 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-05.                                          8.812266 TH  0.000000   $1504229 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.5L5-03.                                          2.699071 TH  0.000000   $1504230 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-02.                                         10.206392 TH  0.000000   $1504231 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.5L5-01.                                         11.352288 TH  0.000000   $1504232 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.4L5-30.                                         18.199988 TH  0.000000   $1504233 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         16.716176 TH  0.000000   $1504234 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=2 cc, sV=2 cc, sD=0.00 mm,Off.: dH=0cc, dV=5.2cc, dD=-0.01mm' - Was Control according to Tsunami
LHC.B2.TCL.5L5.S                                         4.169290 TH  0.070000   $1504235 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=0 cc, sV=0 cc, sD=0.00 mm'
LHC.B2.TCL.5L5.E                                         3.614616 TH  0.070000   $1504236 - 13-FEB-2023 - TSU: 'Quick measure!, Good F1 / 1 Standard: => sH=1 cc, sV=0 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         16.716186 TH  0.000000   $1504237 - 13-FEB-2023 - TSU: 'Quick measure!, Control F1 / 1 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm, - Off.: dH=2cc, dV=6.8cc, dD=0mm' - Was Control according to Tsunami
LHC.MQML.5L5.S                                           6.989141 TH  0.070000   $1504238 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=1 cc, sV=1 cc, sD=0.00 mm'
LHC.MQML.5L5.E                                          10.646851 TH  0.070000   $1504239 - 13-FEB-2023 - TSU: 'Good 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm'
LHC.NID.6L5-02.                                         16.716183 TH  0.000000   $1504240 - 13-FEB-2023 - TSU: 'Control 2F / 2 Standard: => sH=0 cc, sV=1 cc, sD=0.00 mm,Off.: dH=0.9cc, dV=0.8cc, dD=0mm' - Was Control according to Tsunami
*END
";
        }

        private string getContentFromFileWithAllFrames()
        {
            return @"
bla
bla
*FRAME RSTI_103035_LHC.MQML.5L5  -1231.090090  10389.569100   2416.394520 0 0   74.733794 1
*FRAME RST_103035_LHC.MQML.5L5 0 0 0 .7911910531 -.41577637333 0 1
*FRAME RSTRI_103035_LHC.MQML.5L5     -0.001198      0.001174     -0.000592 .00074272462 0 -.00045093994 1 TX TY TZ RX RZ 
*FRAME RSTR_103035_LHC.MQML.5L5 0 0 0 0 0.001336902 0 1 RY
*CALA
BEAM_LHC.MQML.5L5.S                                             -0.000005      4.800002      0.000003   $13135.200 693826 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.E                                             -0.000003     -0.745000      0.000001   $13129.655 683395 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.BPMR.5L5.S                                             -0.000003     -0.745000      0.000001   $13129.655 683396 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MQML.5L5.E                                              0.000000      0.000000      0.000000   $13130.400 693825 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 804768 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCV.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 804769 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.E                                            -0.000004      4.990007      0.000003   $13135.390 686523 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
BEAM_LHC.MCBCA.5L5.S                                            -0.000005      5.894007      0.000003   $13136.294 686524 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% LHC.MQML.5L5.T     -0.360470      4.306890      0.420950 0.1 0.1 0.1   $13134.707 516196 paramètres RST 23-FEB-2023 00:00:00
*OBSXYZ
LHC.MQML.5L5.E      0.168800      0.601580      0.421630 0.1 0.1 0.1   $13131.001 516195 paramètres RST 23-FEB-2023 00:00:00
LHC.MQML.5L5.S      0.167070      4.306810      0.420700 0.1 0.1 0.1   $13134.706 516197 paramètres RST 23-FEB-2023 00:00:00
*INCLY W1015
LHC.MQML.5L5.E  -0.533487369 RF -0.030169411
*ENDFRAME
*ENDFRAME
*ENDFRAME
*ENDFRAME
% *FRAME RSTI_103036_LHC.B2.TCL.5L5  -1218.223830  10395.171970   2416.221390 0 0   74.733794 1
% *FRAME RST_103036_LHC.B2.TCL.5L5 0 0 0 .7911910531 -.4197870779 0 1
% *FRAME RSTRI_103036_LHC.B2.TCL.5L5     -0.001234      0.001459     -0.000687 -.0008276057 0 -.0067481696 1 TX TY TZ RX RZ 
% *FRAME RSTR_103036_LHC.B2.TCL.5L5 0 0 0 0 -.00019098593 0 1
% *CALA
% BEAM_LHC.B2.TCL.5L5.E                                            0.000000      0.000000      0.000000   $13144.737 743347 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% BEAM_LHC.B2.TCL.5L5.S                                            0.000003      0.999999     -0.000002   $13145.737 743348 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% BEAM_LHC.B2M.TCL.5L5.E                                           0.096996     -0.000006      0.000009   $13144.585 225933 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% BEAM_LHC.B2M.TCL.5L5.S                                           0.096990      0.999997      0.000007   $13145.585 225934 coordonnées théoriques dans le système RST au 23-FEB-2023 00:00:00
% *OBSXYZ
% LHC.B2.TCL.5L5.E     -0.200020      0.179390      0.370010 0.1 0.1 0.1   $13144.917 640301 paramètres RST 23-FEB-2023 00:00:00
% LHC.B2.TCL.5L5.S     -0.200020      0.819440      0.370020 0.1 0.1 0.1   $13145.557 640302 paramètres RST 23-FEB-2023 00:00:00
% *ENDFRAME
% *ENDFRAME
% *ENDFRAME
% *ENDFRAME
*END
";
        }

        [DeploymentItem(@"Preferences\", @"Preferences\")]
        [DeploymentItem(@"libs\SuSoft\", @"libs\SuSoft\")]
        [DeploymentItem(@"libs\Leica\", @"libs\Leica\")]
        [TestMethod()]
        public void ComputeTest_Show_Results_For_customization()
        {
            //TODO-toRestore: TEST_BUG
            //TSU.Common.Compute.Compensations.BeamOffsets.WorkFlow.RecomputeWithCustomization () ----> UseToUpdateBOParameters () is null refernce
            //Tsunami tsunami = new Common.Tsunami(null);
            //Tsunami2.Properties = tsunami;
            //Splash.Module splashScreenModule = new Splash.Module(tsunami);
            //tsunami.View.SplashScreen = splashScreenModule.View; // this is because the slash view is used to invoke the UI thread from the Module.InvokeOnApplicationDispatcher

            //TSU.Preferences.Preferences.Instance.tsunami = tsunami;

            //Application.Run(splashScreenModule.View);
            //Tsunami t = new Tsunami();

            FinalModule module = new Polar.Module(Tsunami2.Properties, "pm");

            string id = "LHC.MQML.5L5";

            string inputWithoutFrame = getLgcInputForAStationMeasuringAMagnet();

            List<(Polar.Station, List<MeasureWithStatus>)> polarItems = new List<(Polar.Station, List<MeasureWithStatus>)>();
            List<MeasureWithStatus> polarMeasures = new List<MeasureWithStatus>();
            Elements.Point E = GetCcsPoint("LHC.MQML.5L5.E", -1218.12964, 10395.427050, 422.778475);
            Elements.Point S = GetCcsPoint("LHC.MQML.5L5.S", -1217.53946, 10395.674480, 422.770584);
            //Elements.Point T = GetCcsPoint("LHC.MQML.5L5.T", -1227.25170, 10391.565710, 422.952300);

            polarMeasures.Add(new MeasureWithStatus(true, CreatePolarMeasure(E, 79.336822, 99.388499, 10.746851, 0.070000)));
            polarMeasures.Add(new MeasureWithStatus(true, CreatePolarMeasure(S, 83.602564, 99.342179, 6.989141, 0.070000)));

            polarItems.Add((null, polarMeasures));

            List<MeasureWithStatus> rollMeasures = new List<MeasureWithStatus>();

            rollMeasures.Add(new MeasureWithStatus(true, CreateRollMeasure(E, -0.533487369)));

            List<Frame.FrameMeasurement.ObsXYZ> pointsToUseAsOBSXYZ = new List<Frame.FrameMeasurement.ObsXYZ>();
            AddObsSigma(pointsToUseAsOBSXYZ, E._Name, 0.0001, 0.01, 0.0001);
            AddObsSigma(pointsToUseAsOBSXYZ, S._Name, 0.0001, 0.01, 0.0001);

            List<Frame.FrameMeasurement.Incly> inclys = new List<Frame.FrameMeasurement.Incly>();
            AddInclyResiduals(inclys, E._Name, 0.0007);

            BeamOffsets.Parameters param = new BeamOffsets.Parameters { IdSuAssembly = id, PointsToUseAsOBSXYZ = pointsToUseAsOBSXYZ, Inclys = inclys };

            Frame root = Frame.FromLGCInputFormat_GetRootFrameInputFormatString(getContentFromFileWithAllFrames());
            Frame AssemblyFrame = Frame.GetAssemblyLastFrameByName(root, id);

            Results results2 = new Results();
            results2.Frame = AssemblyFrame.DeeperFrame;
            results2.Sigma0 = 1.01;
            AddObsSigma(results2.Frame.Measurements.ObsXyzs, E._Name, 0.0001, 0.01, 0.0001);
            AddObsSigma(results2.Frame.Measurements.ObsXyzs, S._Name, 0.0001, 0.01, 0.0001);
            AddObsResiduals(results2.Frame.Measurements.ObsXyzs, E._Name, 0.0001, 0.0002, 0.0003);
            AddObsResiduals(results2.Frame.Measurements.ObsXyzs, S._Name, 0.0001, 0.0002, 0.0003);
            AddObsResiduals(results2.Frame.Measurements.ObsXyzs, "LHC.MQML.5L5.T", 0.0001, 0.0002, 0.0003);
            AddInclyResiduals(results2.Frame.Measurements.Inclinaisons, E._Name, 0.0004);
            AddEstimatedParameters(results2.Frame.EstimatedParameters, 5, 6, 7);

            Macro validateWindow = new Macro()
            {
                KeyStrokesAndMouseClicks = new List<Macro.IInputActivity>()
                {
                    new Macro.KeyStroke(){ Shift = true, KeyData = System.Windows.Forms.Keys.Up.ToString(), DelayInMillis = 100 },
                    new Macro.KeyStroke(){ KeyData = System.Windows.Forms.Keys.Tab.ToString(), DelayInMillis = 100 },
                    new Macro.KeyStroke(){ KeyData = System.Windows.Forms.Keys.Space.ToString(), DelayInMillis = 100 },
                }
            };
            Frame newResults = null;

            GatheredMeasures gatheredMeasures = new GatheredMeasures() { Polar = polarItems, Roll = rollMeasures };
            Action action = () =>
            {
                Results r = WorkFlow.Compute(module,
                new List<FinalModule>() { module },
                id, ObservationType.All, forceSetup: true);
            };

            action();

            //Macro.Play(validateWindow, action, PlayIfNotActive:true  );

            //     solution is too force wait for the specific time
            //    Macro.WaitFor(validateWindow);

            //while (true)
            //{

            //}

            Assert.AreEqual("0.01", "0");
        }
    }
}