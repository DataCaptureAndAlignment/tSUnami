# Replace these variables with your details
$gitlabUrl = "https://gitlab.cern.com"  # Your GitLab instance URL
$projectId = "DataCaptureAndAlignment/tSUnami"     # The ID of your project
$sourceBranch = "dev"              # The branch you want to merge from
$targetBranch = "master"           # The branch you want to merge into
$mergeRequestTitle = "Merge dev into master"

# Use the CI_JOB_TOKEN provided by GitLab CI/CD
$ciJobToken = $env:CI_JOB_TOKEN

# Check if CI_JOB_TOKEN is available
if (-not $ciJobToken) {
    Write-Error "CI_JOB_TOKEN is not available. Ensure this script is running within a GitLab CI pipeline."
    exit 1
}

# API endpoint to create a merge request
$url = "$gitlabUrl/api/v4/projects/$projectId/merge_requests"

# Headers including the CI_JOB_TOKEN for authentication
$headers = @{
    "PRIVATE-TOKEN" = $ciJobToken
}

# Data for the merge request
$data = @{
    "source_branch" = $sourceBranch
    "target_branch" = $targetBranch
    "title"         = $mergeRequestTitle
}

# Convert data to JSON format
$jsonData = $data | ConvertTo-Json

# Make the POST request to create the merge request
$response = Invoke-RestMethod -Uri $url -Method Post -Headers $headers -Body $jsonData -ContentType "application/json"

# Check the response
if ($response -ne $null -and $response | Get-Member -Name id) {
    Write-Output "Merge request created successfully!"
    Write-Output "Merge request details: $($response | ConvertTo-Json -Depth 3)"
} else {
    Write-Output "Failed to create merge request"
}