function CopyLogs()
{
	param([string]$pathToCopyLogs)

		
	$ErrorOccured = $false
	try{
		$Date = (Get-Date).ToString("yyyyMMdd")
		$PathToLog = "C:\DATA\Tsunami\Measures\$Date\Logs"
		$Copied = 0
		$SizeCopied = 0
		if (!(Test-Path $pathToCopyLogs)){
			New-Item $pathToCopyLogs -ItemType Directory | Out-Null
		}
        Get-ChildItem $PathToLog -Recurse -Filter *log.html | Foreach-Object {
		    Move-Item $_.FullName -Destination $pathToCopyLogs -ErrorAction Stop
			$Copied = $Copied + 1
			$SizeCopied = $SizeCopied + $_.Length
        }
		$FormattedSize = Format-ByteSize $SizeCopied
		Write-Host $GREEN"$Copied logs ($FormattedSize) were copied to $pathToCopyLogs."
	}catch{
		ExceptionMessage $_.Exception
		$ErrorOccured = $true
	}
	return $ErrorOccured	
}

$Shlwapi = Add-Type -MemberDefinition '
    [DllImport("Shlwapi.dll", CharSet=CharSet.Auto)]public static extern int StrFormatByteSize(long fileSize, System.Text.StringBuilder pwszBuff, int cchBuff);
' -Name "ShlwapiFunctions" -namespace ShlwapiFunctions -PassThru

Function Format-ByteSize([Long]$Size) {
    $Bytes = New-Object Text.StringBuilder 20
    $Return = $Shlwapi::StrFormatByteSize($Size, $Bytes, $Bytes.Capacity)
    If ($Return) {$Bytes.ToString()}
}