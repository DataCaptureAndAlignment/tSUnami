
# Path to files 
$defaultXMLReports = "TestResults\TestResults.xml"

# Copy xml reports from default directory to new directory
# $pathToCopyXMLReports - path where XML should be copy (to avoid overwriting)
# $pathToXMLReports - default path for XML report in GitLab Runner 
# Return:
# $ErrorOccured - true/false - if error occured or not
function CopyXMLReports()
{
	param([string]$pathToCopyXMLReports,
	[int]$testNumber)

	$copy = "Copying XML report"
	Write-Host $WHITE$copy

	try{	
		#NormalMessage "Default GitLab Runner directory for XML report is: $pathToXMLReports"			
		if (!(Test-Path $pathToCopyXMLReports)){ 
			New-Item $pathToCopyXMLReports -ItemType Directory | Out-Null 
		}			
		$xmlFile = "$pathToCopyXMLReports\$projectName"+$testNumber+".xml"					
		Copy-Item $defaultXMLReports -Destination $xmlFile -ErrorAction Stop
		if ($testNumber -eq 0)
		{
			Write-Host $YELLOW"XML report copied into: $pathToCopyXMLReports"
			Write-Host ""
		}
	}catch{
		ExceptionMessage $_.Exception
		Write-Host ""
		return $false
	}
	return $xmlFile
}

# Read XML tests report to find number of failures
# GitLab Runner is not checking if there are failures in tests. 
# GitLab Runner fails only when it is an error

# Check number of failures in all tests
# $XMLReportPath - path to XML reporst created during the running the tests
# Return:
# failureNumber - number of failures counted in the XML reports
# $ErrorOccured - true/false - if error occured or not
function CheckFailuresInTests()
{	
	param([string]$XMLReportPath)

	$fail = "Checking test failures in XML report"
	$failureNumber = 0
	StartMessage $fail
	try{
		$failureNumber = Select-Xml -Path $XMLReportPath -XPath 'testsuites/ testsuite' | ForEach-Object { $_.Node.failures }-ErrorAction Stop
		if ($failureNumber -gt 0)
		{
			ErrorMessage "$failureNumber errors found in XML reports"
		}
		Write-Host ""
	}catch{	
		ExceptionMessage $_.Exception
		Write-Host ""
		exit 1
	}
	return $failureNumber
}