# Script to run unit test and upload test results reports in .XML to GitLab
param (
	[string]$projectName = "none",
	[boolean]$stepByStep = $false,
	[boolean]$instrumentNeeded = $false,
	[string]$instrumentSNFromTag = "not provided",
	[string]$snFromKey = "not provided"
)

& query session

write-host $projectName
write-host $snFromKey

# Global variables for command and options
$global:cicdPath = "src\CICD"
$global:scriptsPath = "$cicdPath\Scripts"
$global:projectsPath = "$cicdPath\Test\Projects"
$global:projectPath = "$projectsPath\$projectName"
$global:pathToCopyXMLReports = "$cicdPath\Test_Results\$projectName"
        
$global:projectFile = "$projectPath\$projectName.csproj"
$global:projectExecutionPath = "$projectPath\bin\Debug\"
$global:projectDLL = "$projectExecutionPath\$projectName"+"_Tests.dll"
$global:projectArtefactsPath = "$projectPath\bin\Debug\$projectName"+"_Tests\artefacts"


.  ".\$scriptsPath\colored_message.ps1"
.  ".\$scriptsPath\xml_export.ps1"
.  ".\$scriptsPath\log_export.ps1"

$currentActionName = "Running $projectName tests"
StartMessage $currentActionName

$hasFailed = $false
try
{
	$dummy = & ".\$scriptsPath\restore_packages.ps1" $projectFile; if ($LastExitCode -ne 0) { $hasFailed = $true }
	$dummy = & ".\$scriptsPath\install_susoft_dependencies.ps1" $projectFile "SUSoft dependencies installation"; if ($LastExitCode -ne 0) { $hasFailed = $true }
    $dummy = & ".\$scriptsPath\build.ps1" $projectFile; if ($LastExitCode -ne 0) { $hasFailed = $true }
	& ".\$scriptsPath\do_test.ps1" $projectDLL $stepByStep $instrumentNeeded $instrumentSNFromTag $snFromKey $projectArtefactsPath $projectPath; if ($LastExitCode -ne 0) { $hasFailed = $true }
	if (!$stepByStep)
	{
		$xmlFile = CopyXMLReports $pathToCopyXMLReports; if ($LastExitCode -ne 0) { $hasFailed = $true }
		$global:testFailureCount = CheckFailuresInTests $xmlFile; if ($LastExitCode -ne 0) { $hasFailed = $true }
	}
	
	CopyLogs $pathToCopyXMLReports

	if ($hasFailed)
	{ 
		Write-Host $RED"One of the sub-task of the job failed"
		exit 1 
	}

	Write-Host $Blue"Finally:"
	if ($global:testFailureCount -eq 0){
		Write-Host $GREEN"All good!"
		Exit 0
	}
	else
	{
		if (!$stepByStep)
		{
			Write-Host $GREEN""($global:testTotalCount-$global:testFailureCount-$global:testNotRunnedCount)"/"$global:testTotalCount "test(s) passed" 
			Write-Host $YELLOW""($global:testNotRunnedCount)"/"$global:testTotalCount "test(s) not runned" 
			Write-Host $RED""($global:testFailureCount)"/"$global:testTotalCount "test(s) failed" 
		}
		else 
		{
			Write-Host $RED""($global:testFailureCount) "test(s) failed" 
		}
		Exit 1
	}
}
catch
{	
	ExceptionMessage $_.Exception
	Exit 1
}