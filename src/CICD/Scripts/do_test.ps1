param (
[string]$projectDll = "none",
[boolean]$stepByStep = $false,
[boolean]$instrumentNeeded = $false,
[string]$instrumentSNFromTag = "not provided",
[string]$snFromKey = "not provided",
[string]$projectArtefactsPath = "not provided",
[string]$projectPath = "not provided"
)


# put something in this to run only this test and its parents
#$UniqueMethodToRun = "L2_FSD_Step03_Setup_polar_station_Test"
$ForceToRunAllParentTests = $true

Add-Type -AssemblyName System.Windows.Forms

# Define the file path to delete the preference file
$filePath = "C:\data\tsunami\Preferences\GuiPreferences_for_v2.xml"

# Check if the file exists
if (Test-Path -Path $filePath)
{
    try {
        # Remove the file
        Remove-Item -Path $filePath -Force
        Write-Output "File deleted successfully: $filePath"
    } catch {
        Write-Error "Failed to delete file: $filePath. Error: $_"
    }
}
else
{
    Write-Output "File not found: $filePath"
}

# Global variables for command and options
$vstest = "C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\CommonExtensions\Microsoft\TestWindow\vstest.console.exe"
$options = "/logger:""junit;verbosity=minimal"" /PlatForm:x86"

# Execute build process
Write-Host $WHITE"Running $projectDll From Current Path: $($PWD.Path)"

if ($instrumentNeeded) {
	$snProvidedInTag = $instrumentSNFromTag -ne "not provided"
	$snProvidedByKey = $snFromKey -ne "not provided"

	if ($snProvidedInTag) {
		Write-Host $GREEN$instrumentSNFromTag" instruments will be used".
	} else{
		Write-Host $YELLOW"No instrument S/N provided in the GIT TAG message, if the tests need instruments, the 'instrumentsForTests.txt' from the repo will be used".
	}

	if ($snProvidedInTag) {
		# Save tag with info about instrument S/N for connectivity test
		$save = "Saving instruments tags for run tests"
		StartMessage $save
		Write-Output $WHITE""
		Write-Host $BLUE$instrumentSNFromTag
	
		$instrumentsForTests = "src\CICD\Test\Projects\Connectivity\bin\Debug\Preferences\Instruments and Calibrations\instrumentsForTests.txt"
		$instrumentSNFromTag | Out-File $instrumentsForTests
		if (Test-Path $instrumentsForTests) {
			EndMessage $save
		} else {
			ErrorMessage $save
		}
	}
}

if ($stepByStep -ne $true) {

	Write-Host $WHITE"Run all tests together"
	& $vstest $projectDll $options

}
else
{
	# get tests list, order it and run them one by one
	try {	
		$listReturned = & $vstest $projectDll $options /ListTests:$testDLL
		$global:testNumber = 0
		$allTests = $listReturned[3..($listReturned.Length - 1)]


		Write-Host $BLUE"Before filtering : "$WHITE$allTests

		# Filter tests (when you want to test a specific one)
		# $allTests = $allTests | Where-Object { $_ -match '\s{3,}(L0_START_Step[123])|(L1_Op28360_Step[12]).+' }
		# $allTests = $allTests | Where-Object { $_ -match '\s{3,}L0_START_Step1.+' }

		# $allTests = $allTests | Where-Object { $_ -match '\s{3,}(L0_START_Step[123])|(L2_BGM_Step0[01234]).+' }    # L2_BGM_Step03_Setup_to_polar_Test
		

		Write-Host $BLUE"After filtering : "$WHITE$allTests

		$global:testTotalCount = $allTests.Count

		if ($global:testTotalCount -lt 1) {
			Write-Host $Red"No tests found!"
			exit 1
		}
		$sortedTests = $alltests | Sort-Object 

		Write-Host $BLUE"After sorting : "$WHITE$sortedTests
		$global:testFailureCount = 0
		

		# Variable to track whether we should start running tests
		$RunOnlyOneTest = ![string]::IsNullOrWhiteSpace($UniqueMethodToRun)
		if($RunOnlyOneTest)
		{
			& ".\$scriptsPath\SearchForPrerequiredTest.ps1" -folderPath $projectPath -methodName $UniqueMethodToRun -ForceToRunAllParentTests $ForceToRunAllParentTests -artefactsPath $projectArtefactsPath
		}
		else
		{
			foreach ($test in $sortedTests)
			{
				if ($test -match '\s{3,}(?<TestName>.+)')
				{
					# MinimizeAllWindows # do not work when you dont look at the tests
					& ".\$scriptsPath\run_one_test_by_name" -testName $Matches.TestName
				}
			}
		}
	} catch {
		ExceptionMessage $_.Exception	
		exit 1
	}
}

if($LastExitCode -eq 0 ) {
	Write-Host $GREEN"Test execution succeed" 
	Write-Host ""
	exit 0
} else {
	Write-Host $RED"Test execution failed" 
	Write-Host ""
	exit 1
}