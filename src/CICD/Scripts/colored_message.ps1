# Variables for text color
$esc="$([char]27)" 
$RED="$esc[31m" 
$GREEN="$esc[92m"
$BLUE="$esc[96m"
$BACKBLUE="$esc[44m"
$WHITE="$esc[0m"
$YELLOW="$esc[33m"
$PURPLE="$esc[35m"
$CYAN="$esc[36m"

# Function to define what to show at the begging of the process
function StartMessage()
{
	param([string]$message)	
	Write-Host $BLUE"$message" 
}

# Function to define what to show when error occurs during process
function ErrorMessage()
{
	param([string]$message)
	Write-Host $RED"$message failed" 
}

# Function to define what to show at the end of successful process
function EndMessage()
{
	param([string]$message)
	Write-Host $GREEN"$message succeed" 		
}

# Function to define what to show at normal process
function NormalMessage()
{
	param([string]$message)
	Write-Host $WHITE"$message"	
}

# Function to show exception
function ExceptionMessage()
{
	param([string]$message)
	Write-Host $RED"Exception output: "
	Write-Host $EXC"$message" 
	
	exit 1
}