param (
		[string]$projectFile = "Path\To\Your\Project.csproj"
	)

.  ".\src\CICD\Scripts\colored_message.ps1"

# Global variables for command and options
$command = "C:\Program Files\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\MSBuild.exe"
$options = "-consoleLoggerParameters:ErrorsOnly"
$platform = "x86"

# Execute build process
Write-Host $WHITE"Building $projectFile From Current Path: $($PWD.Path)"
$results = & $command $projectFile /p:Configuration=Debug /p:Platform=$platform $options
Write-Host $WHITE$results
if($LastExitCode -eq 0 ){
	Write-Host $GREEN"Build succeed" 
	Write-Host ""
	exit 0
}else{
	Write-Host $RED"Build failed" 
	Write-Host ""
	exit 1
}