# Global variables for command and options
$global:cicdPath = "src\CICD"
$global:scriptsPath = "$cicdPath\Scripts"
$global:hasFailed = $false
$tsunamiProjectPath = "src/Tsunami/Tsunami.csproj"

.  ".\$scriptsPath\colored_message.ps1"

function tryAndCatch
{
	param(
		[string]$scriptPath,
		[string]$scriptParameter,
		[string]$errorMessage)

	try
	{ 
		$dummy = & $scriptPath $scriptParameter 
	}
	catch
	{  
		$global:hasFailed = $true
	} 
	finally
	{
		if ($LASTEXITCODE -ne 0)
		{
			ErrorMessage $errorMessage
			$global:hasFailed = $true
		}
	}; 
}

$hasFailed = $false
try
{
	$dummy = &  ".\$scriptsPath\copy_local_version_of_packages.ps1"; if ($LastExitCode -ne 0) { $hasFailed = $true }
    # tryAndCatch ".\$scriptsPath\install_susoft_dependencies.ps1" $projectFile "SUSoft dependencies installation" # not necessary as we will not run tsunami
	$dummy = &  ".\$scriptsPath\restore_packages.ps1" $tsunamiProjectPath "Packages restauration"; if ($LastExitCode -ne 0) { $hasFailed = $true }
	$dummy = &  ".\$scriptsPath\build.ps1" $tsunamiProjectPath "Build"; if ($LastExitCode -ne 0) { $hasFailed = $true }

	if ($hasFailed)
	{ 
		ErrorMessage "A part of the job failed"
		exit 1 
	}
}
catch
{	
	ExceptionMessage $_.Exception
	Exit 1
}