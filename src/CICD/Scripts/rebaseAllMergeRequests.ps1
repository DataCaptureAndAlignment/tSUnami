# Define variables
$GitLabUrl = "https://gitlab.cern.ch/"  # Replace with your GitLab instance URL
$ProjectId = "14585"              # Replace with your project ID
$PrivateToken = "glpat-aCJsd3EseyqKsMseUDq-" # Replace with your personal access token

# API endpoint to get all open merge requests
$MergeRequestsUrl = "$GitLabUrl/api/v4/projects/$ProjectId/merge_requests?state=opened"

# Get all open merge requests
$Headers = @{
    "PRIVATE-TOKEN" = $PrivateToken
}
$MergeRequests = Invoke-RestMethod -Uri $MergeRequestsUrl -Headers $Headers -Method Get

# Loop through merge requests and rebase those that require it
foreach ($mr in $MergeRequests) {
    $MrId = $mr.iid
    $MergeStatus = $mr.merge_status

    if ($MergeStatus -eq "can_be_merged") {
        Write-Host "Rebasing merge request #$MrId..."
        
        # API endpoint to trigger rebase
        $RebaseUrl = "$GitLabUrl/api/v4/projects/$ProjectId/merge_requests/$MrId/rebase"
        
        # Trigger the rebase
        $RebaseResponse = Invoke-RestMethod -Uri $RebaseUrl -Headers $Headers -Method Put
        
        Write-Host "Rebase triggered for merge request #$MrId."
    } else {
        Write-Host "Merge request #$MrId does not require a rebase or cannot be merged."
    }
}
