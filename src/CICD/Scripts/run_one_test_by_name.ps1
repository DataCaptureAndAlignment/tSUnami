param (
	[Parameter(Mandatory = $true)]
	[string]$testNameInput
)

$global:testNumber++
$criteria = "/Tests:" + $testNameInput
$testName = "$BLUE Test $global:testNumber/$testTotalCount : $($testNameInput)"
$fullOutputAsList = & $vstest $projectDll $options $criteria 
# remove 5 first useless lines
$newStringList = $fullOutputAsList[3..($fullOutputAsList.Count - 1)]
# trasnforme into a single string
$multiLineFullOutput = $newStringList -join "`r`n"
# Remove the long trace Split the string based on the ignore text and take the first part
$reducedString = $multiLineFullOutput -replace '(?s)Stack Trace:.*$', ''
# check if the text contains "Failed"
$bContainsFailed = $reducedString -imatch "Failed"
$bContainsMissingArtefact = $reducedString -imatch "missing artefact"
$bContainsPassed = $reducedString -imatch "Passed"
if ($reducedString -imatch ".*\[(([0-9]+ [dhms])( [0-9]+ [hms]){0,2})\].*") {
    $testTime = " lasted " + $Matches[1]
} else {
    $testTime = ""
}
if ($bContainsMissingArtefact) {
	Write-Host "$testName ${BLUE}NOT RUNNED${WHITE}"
	ErrorMessage $reducedString
	$global:testNotRunnedCount++
} elseif ($bContainsFailed) {
	Write-Host "$testName ${RED}FAILED${WHITE}$testTime"
	ErrorMessage $reducedString
	CopyXMLReports $pathToCopyXMLReports $testNumber
	$global:testFailureCount++
} elseif ($bContainsPassed)	{
	Write-Host "$testName ${GREEN}OK${WHITE}$testTime" 
} else {
	Write-Host "$testName ${RED}CRASHED${WHITE}$testTime"
	ErrorMessage $reducedString
	CopyXMLReports $pathToCopyXMLReports $testNumber
	$global:testFailureCount++
}

