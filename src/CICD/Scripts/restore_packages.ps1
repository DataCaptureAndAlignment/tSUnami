param (
		[string]$projectFile = "none"
	)

.  ".\src\CICD\Scripts\colored_message.ps1"

# Global variables for command and options
$command = "C:\Program Files\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\MSBuild.exe"
$options = "-consoleLoggerParameters:ErrorsOnly"

Write-Host $BLUE"Restoring $projectFile From Current Path: $($PWD.Path)"
$results = & $command /t:restore $projectFile $options
Write-Host $WHITE$results
if($LastExitCode -eq 0 ){
	Write-Host $GREEN"Restoration succeed" 
	Write-Host ""
	exit 0
}else{
	Write-Host $RED"Restoration failed" 
	Write-Host ""
	exit 1
}