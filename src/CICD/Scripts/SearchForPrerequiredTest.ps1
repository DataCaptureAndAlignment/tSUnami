param (
    [Parameter(Mandatory = $true)]
    [string]$folderPath,

    [Parameter(Mandatory = $true)]
    [string]$methodName,

    [Parameter(Mandatory = $true)]
    [Boolean]$ForceToRunAllParentTests,

    [Parameter(Mandatory = $true)]
    [string]$artefactsPath
)

# Function: Check if the file exists
function CheckIfTestAlreadyRunned {
    param (
        [Parameter(Mandatory = $true)]
        [string]$NameOfValue
    )

    # Define the base path for the artifacts
    $artifactBasePath = "C:\Pascal\tsunami\src\CICD\Test\Projects\Functional\bin\Debug\Functional_Tests\Artefactsold"
    $artifactBasePath = $artefactsPath

    # Construct the full file path
    $filePath = Join-Path -Path $artifactBasePath -ChildPath ($NameOfValue + ".tsut")

    # Check if the file exists
    if (Test-Path $filePath) {
        Write-Output "File exists: $filePath"
        return $true
    } else {
        Write-Output "File does not exist: $filePath"
        return $false
    }
}

# Function: Search for method and return the nameof value
function SearchParentTestNameInAllCsFiles {
    param (
        [Parameter(Mandatory = $true)]
        [string]$FolderPath,

        [Parameter(Mandatory = $true)]
        [string]$MethodName
    )

    # Ensure the folder exists
    if (-Not (Test-Path $FolderPath)) {
        Write-Error "The folder path '$FolderPath' does not exist."
        return
    }

    # Get all .cs files in the folder
    $csFiles = Get-ChildItem -Path $FolderPath -Recurse -Filter *.cs

    if ($csFiles.Count -eq 0) {
        Write-Output "No .cs files found in the folder."
        return
    }

    foreach ($file in $csFiles) {
        # Read the file content as a single string
        $content = Get-Content $file.FullName -Raw
    
        # Regex to match the method definition
        $methodRegex = [regex]::new("public void\s+$MethodName\s*\(\s*\)")
        $methodMatch = $methodRegex.Match($content)
    
        if ($methodMatch.Success) {
            # Get the position where the method definition ends
            $methodEndPosition = $methodMatch.Index + $methodMatch.Length
    
            # Extract content after the method definition
            $remainingContent = $content.Substring($methodEndPosition)
    
            # Find the next [TestMethod()] position
            $testMethodRegex = [regex]::new("\[TestMethod\(\)\]")
            $testMethodMatch = $testMethodRegex.Match($remainingContent)
    
            # Limit the search to content before [TestMethod()]
            if ($testMethodMatch.Success) {
                $contentToSearch = $remainingContent.Substring(0, $testMethodMatch.Index)
            } else {
                $contentToSearch = $remainingContent  # No [TestMethod()] found, search all remaining content
            }
    
            # Regex to find the first nameof() usage
            $nameofRegex = [regex]::new("nameof\(([^\)]*\.)*(?<Value>[^\)]+)\)")
            $nameofMatch = $nameofRegex.Match($contentToSearch)
    
            if ($nameofMatch.Success) {
                # Return the captured value inside nameof()
                return $nameofMatch.Groups['Value'].Value
            }
        }
    }

    # Return null if nothing is found
    return $null
}


function Check-TestRecursively {
    param (
        [Parameter(Mandatory = $true)]
        [string]$FolderPath,

        [Parameter(Mandatory = $true)]
        [string]$MethodName
    )

    # Append another tab to it
    $global:tab += "`t"

    # Call the Search-MethodAndNameOf function to find the parent test
    $foundString = SearchParentTestNameInAllCsFiles -FolderPath $FolderPath -MethodName $MethodName

    if ($foundString)
    {
        Write-Output $PURPLE$global:tab"Found parent-test: $foundString"

        # Call the Check-TsutFileExists function to check if the parent test has already run
        if (-not $ForceToRunAllParentTests -and (CheckIfTestAlreadyRunned -NameOfValue $foundString) -eq $true)
        {
            Write-Output $GREEN$global:tab"Parent already runned and produced"
        } 
        else
        {
            Write-Output $BLUE$global:tab"Parent-test should run"
            # Recursively call Check-TestRecursively with the parent test's name
            Check-TestRecursively -FolderPath $FolderPath -MethodName $foundString
        }
    }
    else
    {
        Write-Output $PURPLE$global:tab"No parent-test found for: $MethodName"
    }

    Write-Output $PURPLE$global:tab"Running: $MethodName"
    & ".\$scriptsPath\run_one_test_by_name" -testName $MethodName
    
    $global:tab = $global:tab -replace "`t$", ""
}



# Example usage
#$folderPath = "C:\Pascal\tsunami\src\CICD\Test\Projects\Functional"
#$methodName = "L3_ECARTO_RS2K_Step2_ExpectedOffset"
$global:tab = ""
Check-TestRecursively -FolderPath $folderPath -MethodName $methodName