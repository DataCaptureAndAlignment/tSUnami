.  ".\src\CICD\Scripts\colored_message.ps1"

$dependenciesTSU = @('src\Tsunami\Preferences\TSUNAMIDependencies.xml')

# Path to DFS installer
$DFSinstallDependencies = @('\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\Install\SUSOFT')
$DFSinstallTestDependencies = @('\\cern.ch\dfs\Support\SurveyingEng\Computing\Software\test')


# Checking if standalone version of application is available in src\Tsunami
# $appPath - path to installed application (ApplicationPath in TsunamiDependencies.xml)
# Return:
# True/False - if application exists in src\Tsunami
function CheckStandalone()
{
	param([string] $appPath)

	$app = $appPath -replace "{application}", ""
	return Test-Path -Path "src\Tsunami\$app" 		
}

# Get full path to installer directory on DFS
# $installerPath - shortened path to installer path (InstallerPath in TsunamiDependencies.xml)
# It can be {SUSoft} or {SUTest}
# $softwareName - name of the software (Name in TsunamiDependencies.xml)
# $version - version number of software (V TsunamiDependencies.xml)
# Return:
# $dfsPath - full path to software installer on DFS 
function GetFullInstallerPath()
{
	param([string]$installerPath, 
			[string]$softwareName,
			[string]$version)

	if ($installerPath -like "*{susoft}*")
	{
		$exeName = $installerPath -replace "{susoft}", ""
		$dfsPath = "$DFSinstallDependencies\$softwareName\$version$exeName"							
	}
	elseif ($installerPath -like "*{sutest}*")
	{
		$exeName = $installerPath -replace "{sutest}", ""
		$dfsPath = "$DFSinstallTestDependencies\$softwareName\$version$exeName"							
	}
	else
	{
		$exeName = $installerPath -replace "susoft", ""
		$dfsPath  = "$DFSinstallDependencies$exeName"				
	}
	
	return $dfsPath
}

# Get application path to copy standalone version
# $appPath - shortened path to installed application (ApplicationPath in TsunamiDependencies.xml)
# It looks like: ApplicationPath="{application}\libs\SuSoft\2.7.beta_5\LGC.exe"
# Return:
# $app - path to application directory (without {application} tag and name of .exe)
# It looks like: "\libs\SuSoft\2.7.beta_5\"
function GetAppPathToCopy()
{	
	param([string]$appPath)
		
	$temp1 = $appPath.split("\")
	$temp2 = $temp1[1..($temp1.Count-2)]
	$app = $temp2 -join '\'
	return $app
}

# Copy standalone version from DFS to TSunami\libs
# $installerPath - shortened path to installer path (InstallerPath in TsunamiDependencies.xml)
# $appPath - shortened path to installed application (ApplicationPath in TsunamiDependencies.xml)
# $softwareName - name of the software (Name in TsunamiDependencies.xml)
# $version - version number of software (V TsunamiDependencies.xml)
function CopyStandalone()
{
	param([string]$installerPath,
		[string]$appPath,
		[string]$softwareName,
		[string]$version)

	$exePath = GetFullInstallerPath $installerPath $softwareName $version
	$app = GetAppPathToCopy $appPath
	Copy-Item -Path $exePath -Destination "src\Tsunami\$app" -Recurse -Force
	NormalMessage "$softwareName$version succesfully installed"
}

# Install SuSoft application for Tsunami, which are not installed on VM
# $installerPath - shortened path to installer path (InstallerPath in TsunamiDependencies.xml)
# $softwareName - name of the software (Name in TsunamiDependencies.xml)
# $version - version number of software (V TsunamiDependencies.xml)
function InstallTSUnamiDependencies()
{
	param([string]$installerPath,
		[string]$softwareName, 
		[string]$version)

	NormalMessage "Installing $softwareName $version ...."
	$dfsPath = GetFullInstallerPath $installerPath $softwareName $version	

	try{
		$exe = Get-ChildItem -Path $dfsPath -ErrorAction Stop
		NormalMessage "Installator found: $exe"	
		Start-Process -Wait -FilePath $exe -ArgumentList '/S','/v','/qn' -NoNewWindow -passthru
		EndMessage "Installation $softwareName $version"
	}catch{
		ExceptionMessage $_.Exception	
		Exit 1				
	}
}
# Checking SuSOFT dependencies needed for Tsunami (depends on TSUNAMIDependencies.xml)
Write-Host $PURPLE"Checking SuSOFT dependencies needed for Tsunami (depends on TSUNAMIDependencies.xml)"

[xml]$xmlElm = Get-Content -Path $dependenciesTSU -ErrorAction Stop
$name = $xmlElm.Application.Dependencies.Application.Name
$v = $xmlElm.Application.Dependencies.Application.V
$installerPath = $xmlElm.Application.Dependencies.Application.InstallerPath
$appPath = $xmlElm.Application.Dependencies.Application.ApplicationPath

for ($i = 0; $i -le $name.length-1; $i++)
{
    $software = $name[$i]+" " + $v[$i]
    $result = Get-Package | Where-Object {$_.Name -eq $software} | Select-Object Name, Version
    if (!$result)
    {
        if ($appPath[$i] -like "*{program files}*"){
            NormalMessage "TSUnami need $software which was not found. GitLab Runner try to install it..."
			try 
			{
				InstallTSUnamiDependencies $installerPath[$i] $name[$i] $v[$i]	
			}
			catch {
				ErrorMessage "Cannot install $software."
				ExceptionMessage $_.Exception	
				Exit 1		
			}
            
        }	
        elseif ($appPath[$i] -like "*{application}*")
        {				
            if (!(CheckStandalone $appPath[$i])){
                NormalMessage "TSUnami need $software standalone version which was not found. GitLab runner try to copy it .... "
                CopyStandalone $installerPath[$i] $appPath[$i] $name[$i] $v[$i]
            }else{
                NormalMessage "$software standalone version found"					
            }
        }
    }
    else
    {
        NormalMessage "$software installed"
    }
}
Exit 0	

