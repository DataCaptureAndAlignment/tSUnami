Write-Host "Restoring configuration files..."
Copy-Item -Path "src\Tsunami\Preferences\Instruments and Calibrations\etalo.dat" -Destination "C:\DATA\Tsunami\preferences\Instruments and Calibrations" -Recurse -Force
Copy-Item -Path "src\Tsunami\Preferences\Instruments and Calibrations\instrum.dat" -Destination "C:\DATA\Tsunami\preferences\Instruments and Calibrations" -Recurse -Force
Copy-Item -Path "src\Tsunami\Preferences\chantier.dat" -Destination "C:\DATA\Tsunami\preferences" -Recurse -Force
Write-Host "Configuration files retored"
