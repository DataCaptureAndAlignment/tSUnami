.  ".\src\CICD\Scripts\colored_message.ps1"

$dependenciesVS = @('C:\Users\apcdev\source\repos\tSUnami\src\packages\')

# Checking and installing Visual Studio dependencies 
Write-Host $PURPLE"Checking and copying Visual Studio dependencies"
try
{
    if (Test-Path -Path $dependenciesVS)
    {
        $allDepend = Get-ChildItem -Path $dependenciesVS
        foreach ($file in $allDepend)
        {
            $checkFolder = "src" + $file.Name
            if (Test-Path $checkFolder){
                Write-Host $RED"$file not found"
                exit 1
            }
            else{
                Copy-Item -Path $file.FullName -Destination "src\packages\" -Recurse -Force
                NormalMessage "$file succesfully installed"
            }	
        }
        Write-Host $GREEN"Copying succesfully"	
        Write-Host ""
        exit 0	
    }
    else
    {
        Write-Host $RED"The folder does not exist at: $dependenciesVS"
        Write-Host ""
        Exit 1
    }
}
catch 
{
    ExceptionMessage $_.Exception
	Exit 1
}