﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TSUResxSorter
{
    /// <summary>
    /// this test has just as purpose to do the sorting in one click from the solution manager by "Run Test";
    /// </summary>
    [TestClass()]
    public class SorterTests
    {
        [TestMethod()]
        public void UseThisTestToCreateANewResourceOrToSortThem()
        {
            string optionalName = "";
            string english = "good measure are needed";
            string french = "bonne measure sont nécessaires";

            

            Assert.IsTrue(true);
        }
    }

    static class Program
    {
        static void Main()
        {
            // first we sort.
            new Sorter().DoSort();


            // ENGLISH OR  NOTHING
            Console.WriteLine("To create a new entry in the ressoruce files, entre the english version:");
            string english = Console.ReadLine();
            if (english=="")
                return;

            // FRENCH
            Console.WriteLine("French version:");
            string french = Console.ReadLine();
            if (french == "")
                french = english;

            // UNIQUE NAME
            Console.WriteLine("Resource unique name: (optional)");
            string optionalName = Console.ReadLine();


            var sorter = new TSUResxSorter.Sorter();
            try
            {
                string name = sorter.AddANewResourceAtTheEndOfTheFile(english, french, optionalName);

                sorter.DoSort();
            }
            catch (Exception ex )
            {
                Console.WriteLine(ex.Message);
            }

            // Keep the console open
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        /// Use Linq to sort the elements.  The comment, schema, resheader, assembly, metadata, data appear in that order, 
        /// with resheader, assembly, metadata and data elements sorted by name attribute.

    }

    public class Sorter
    {
        public string AddANewResourceAtTheEndOfTheFile(string english, string french, string name = "")
        {
            var uniqueName = name == "" ? "T_" + RemoveDoubleSpaces(english): name;

            uniqueName = uniqueName.Trim().ToUpper();

            string pattern = "[ ,!?.;-]";
            uniqueName = Regex.Replace(uniqueName, pattern, "_");

            var uniqueNamWithQuotes = $"\"{uniqueName}\"";
            string uniqueValue = "t8y9h6h5e";

            string oneResourceContent = $@" <data name={uniqueNamWithQuotes} xml:space=""preserve"">
    <value>{uniqueValue}</value>
 </data>";

            string designerContent = $@"
        /// <summary>
        ///   Looks up a localized string similar to Add Team.
        /// </summary>
        public static string {uniqueName}
        @< get
            @<
                return ResourceManager.GetString({uniqueNamWithQuotes}, resourceCulture);
           @>
        @>
        ";
            designerContent = designerContent.Replace("@<", "{").Replace("@>", "}");

            string englishVersionContent = oneResourceContent.Replace(uniqueValue, english);
            string frenchVersionContent = oneResourceContent.Replace(uniqueValue, french);

            bool alreadyExist = StringExistsInFile(EnglishResourcesFile, uniqueName);
            if (!alreadyExist)
            {
                InsertStringBefore(EnglishResourcesFile, "</root>", englishVersionContent, skipLines: false);
                InsertStringBefore(FrenchResourcesFile, "</root>", frenchVersionContent, skipLines: false);
                InsertStringBefore(ResourcesDesignerFile, "/// <summary>", designerContent, skipLines: true);
               Console.WriteLine("Unique resource name: ' $\"{R." + uniqueName + "}\"'");
            }
            else
                throw new Exception("name not unique");
            return uniqueName;
        }

        static class ResxSorter
        {
            public static XDocument SortDataByName(XDocument resx)
            {
                return new XDocument(
                  new XElement(resx.Root.Name,
                    from comment in resx.Root.Nodes() where comment.NodeType == XmlNodeType.Comment select comment,
                    from schema in resx.Root.Elements() where schema.Name.LocalName == "schema" select schema,
                    from resheader in resx.Root.Elements("resheader") orderby (string)resheader.Attribute("name") select resheader,
                    from assembly in resx.Root.Elements("assembly") orderby (string)assembly.Attribute("name") select assembly,
                    from metadata in resx.Root.Elements("metadata") orderby (string)metadata.Attribute("name") select metadata,
                    from data in resx.Root.Elements("data") orderby (string)data.Attribute("name") select data
                  )
                );
            }
        }

        static void InsertStringBefore(string filePath, string searchString, string stringToInsert, bool skipLines)
        {
            // Read all lines from the file
            string[] lines = File.ReadAllLines(filePath);

            // Find the index of the line containing the specified string after skipping lines

            Random rnd = new Random(DateTime.Now.Millisecond);


            int numberOfLineToSkip = skipLines ? rnd.Next(6000, lines.Length) : 0;
            int currentIndex = -1;
            for (int i = numberOfLineToSkip; i < lines.Length; i++)
            {
                if (lines[i].Contains(searchString))
                {
                    currentIndex = i;
                    break;
                }
            }

            if (currentIndex == -1)
            {
                Console.WriteLine("Specified string not found in the file.");
                return;
            }

            // Insert the new string into the array of lines just before the line containing the specified string
            Array.Resize(ref lines, lines.Length + 1);
            Array.Copy(lines, currentIndex, lines, currentIndex + 1, lines.Length - currentIndex - 1);
            lines[currentIndex] = stringToInsert;

            // Write the modified lines back to the file
            File.WriteAllLines(filePath, lines);

            Console.WriteLine($"String inserted successfully in '{filePath}'.");
        }

        static bool StringExistsInFile(string filePath, string searchString)
        {
            // Check if the file exists
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"File '{filePath}' not found.");
            }

            // Read the file contents
            string fileContents = File.ReadAllText(filePath);

            // Check if the string exists in the file contents
            return fileContents.Contains("\""+searchString+ "\"");
        }

        static string RemoveDoubleSpaces(string input)
        {
            // Define the regular expression pattern
            string pattern = @"\s+";

            // Replace multiple spaces with a single space
            string result = Regex.Replace(input, pattern, " ");

            return result;
        }

        static string relativePath = @"\src\Tsunami\Properties\";
        static string FRENCH_FILE_NAME = @"resources.fr-FR.resx";
        static string ENGLISH_FILE_NAME = @"resources.resx";
        static string DESIGNER_FILE_NAME = @"resources.Designer.cs";

        static string AbsoluteRepoPath
        {
            get
            {
                string startingPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                return FoundDirectory(startingPath, relativePath);
            }
        }

        static string EnglishResourcesFile
        {
            get
            {
                return AbsoluteRepoPath + ENGLISH_FILE_NAME;
            }
        }

        static string ResourcesDesignerFile
        {
            get
            {
                return AbsoluteRepoPath + DESIGNER_FILE_NAME;
            }
        }

        static string FrenchResourcesFile
        {
            get
            {
                return AbsoluteRepoPath + FRENCH_FILE_NAME;
            }
        }

        public void DoSort()
        {
            try
            {
                Sort(EnglishResourcesFile);
                Sort(FrenchResourcesFile);
            }
            catch (Exception ex)
            {
                Console.Write(string.Format("Could not sort and save the resource files: {0}", ex.Message));

            }
        }

        private static string FoundDirectory(string startingPath, string resxRelative)
        {
            DirectoryInfo parentPath;
            string resx = startingPath + resxRelative;
            while (!Directory.Exists(resx))
            {
                parentPath = System.IO.Directory.GetParent(startingPath);
                if (parentPath != null)
                    startingPath = parentPath.FullName;
                else
                {
                    Console.Write(string.Format("Could not find the dir '{0}'. \r\n", resx));
                    return null;
                }
                resx = startingPath + resxRelative;
            }
            return resx;
        }

        private static void Sort(string resx)
        {
            XmlReader inputStream = XmlReader.Create(resx);

            // Create a linq XML document from the source.
            XDocument doc = XDocument.Load(inputStream);
            // Create a sorted version of the XML
            XDocument sortedDoc = ResxSorter.SortDataByName(doc);
            inputStream.Close();
            inputStream.Dispose();
            // Save it to the target
            try
            {
                sortedDoc.Save(resx);
                Console.Write(string.Format("...file '{0}' successfully sorted! \r\n", resx));
            }
            catch (Exception ex)
            {
                Console.Write(string.Format("Could not save the file '{0}', please make sure it is not used by another process: {1} \r\n", resx, ex.Message));
            }
        }
    }

}
