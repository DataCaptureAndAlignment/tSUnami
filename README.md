﻿Tsunami stands for the Survey Unified Notepad for Alignment and Measurement interventions.
It merged SFB with Carnet 4000 and pfb

Documentation:
--------------

	is available on DFS: in Test/Psainvit/Tsunami_Docs


Installation TIPS:
------------------

	To install nuget manager:
		https://visualstudiogallery.msdn.microsoft.com/4ec1526c-4a8c-4a84-b702-b21a8f5293ca


	To intall OpenTk:
		go to tool/nuget/manager and type in the powershell: Install-Package OpenTK

	To intall unit test generator
		go to tool/extension and update/online/unit test generator
		may be you have to update to vs2013 update 5 to make it working.


	To be able to use surveylib.dll, NAGc.dll must installed for example by the installer of SFB, or manually

	Attention pour utilisation de LTControl en 64 bit i needed to have ATL90.dll (microsoft dll) in the tsunami directory


Versionning:
------------
   For Tsunami we are using the following numbering:
   - #.##.## (Major.Minor.Revision)
      - Major, for complete change of the software, not forseen fot the moment...
      - Minor, for every break of backward compatibility or for every big new improvement or set of improvements
      - Revision, for every final release.

   For the beta versions, called 'Tsunami.beta' or 'Tiramisu' we are using the following numbering:
      - #.##.##_beta(##)

Architecture:
-------------

I think we are stuck to 32bit as there is no 64bit version of GEOCOM.dll etc...